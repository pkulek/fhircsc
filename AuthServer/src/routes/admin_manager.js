
const config = require('../../config.json');
const moment = require('moment'); // for date time
const STATUS_CODES = require('http').STATUS_CODES
const sql = require("mssql")
const https = require('https')
const timediff = {starttime:moment(),endtime:moment(),elapsed:0}
const jwtauth = require("../auth/jwtauth")()
let CJSON = require("circular-json");
let fs = require('fs')
let utils = require('../utils') ;

const isEmpty =  utils.isEmpty; 
const authurl = config.authurl
const Client = require('node-rest-client').Client;
let URL = require('url');

//----------------------------------------------------------------------------------
exports.editConfig = (req,res) =>{
    let params = req.params
    let filename = params.filename
    let mode = params.mode
    if(isEmpty(filename)) {
        filename = __dirname+'\\..\\..\\config.json';
    }
    if(isEmpty(mode)){
        mode="view,tree"
    }
    console.log(__dirname)
    fs.readFile(filename,'utf8',(err,data)=>{
        if(typeof data == 'string'){
            data = JSON.parse(data)
        }
        data = JSON.stringify(data,null,4)
        res.render('json_viewer',{"mode":"edit",data:data,page_title:"Config Editor"} ) 
    })
}
//----------------------------------------------------------------------------------
exports.saveConfig = (req,res) =>{
    let params = req.params
    let filename = params.filename
    let data = params.data
    if(isEmpty(data)){
        data = req.body
    }
    if(isEmpty(filename)) {
        filename = __dirname+'\\..\\..\\config.json';
    }
    if(! isEmpty(data) && ! isEmpty(filename)){
        fs.writeFile( filename,JSON.stringify(data,null,4),(err)=>{
            res.send(data)
        })
    }
}
//------------------------------------------------------------------------------
exports.loadROLES = (req,res) =>{
    let roles = config.roles
    let html = ""
    roles.forEach((elem)=>{
        html += `<div><input class="roles-input" type="checkbox" name="roles" value="${elem}"> ${elem}</input></div>`
    })
    res.send(html)
}
//------------------------------------------------------------------------------
exports.loadSOURCE = (req,res) =>{
    let source = config.source
    let html = ""
    
    Object.keys(source).forEach( (elem,key)=> {
        console.log(elem,key)
        html += `<div><input class="source-input" type="checkbox" name="source" value="${elem}"> ${elem}</input></div>`
    })
    res.send(html)
}
//----------------------------------------------------------- 
let configqeparams = (targetqe) =>{
    let qeconfig = utils.clone(_qeconfig)
    return qeconfig
}
//---------------------------------------------------------------------------
let runQry =  (qry,callback) => {    
    new sql.Request().query(qry, (err, result) => {
        if(err) {
           console.log("\n\nFhirManager RunQuery err:%s  \n\nqry=\n%s\n\n",err,qry)
        }
        if(callback){
            if(err) {
                callback(err,[] )
            } else {
                callback(err,result.recordsets[0])
            }
        }
    })    
}
//----------------------------------------------------------------------------------
exports.getConfig = (req,res) =>{
   
    let params = req.params
    let key = params.key
    let result = config
    if(! isEmpty(key)){
        result = config[key]
    }
  
    console.log("getconfig params = %j\nresult=%s",params,result)
    res.send(result)
}
//----------------------------------------------------------------------------------



