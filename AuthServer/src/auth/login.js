const DEBUG = false;
const LOGGER = false;

const config = require('../../config.json')
const utils = require('../utils') ;
const isEmpty =  utils.isEmpty;
const sql = require(config.db);
const saltChars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const crypto = require('crypto');
const moment = require('moment'); // for date time
const passwordHash = require('./password-hash');
const email = require('../mail');

//
let LOGINRESET = config.loginconfig.loginreset;
let tokenExpire = config.loginconfig.tokenExpire;
let pwdExpire = config.loginconfig.pwdExpire?config.loginconfig.pwdExpire:129600
let tokenExpireFormat = config.loginconfig.tokenExpireFormat;
//

const DICT_TABLE = "dict" 
const FCS_SADMIN = ['FCS_ALL'] ;
const FCS_ADMIN = ['FCS_ALL'] ;
const FCS_OPERATIONS = ['FCS_USERSVIEW','FCS_USEREDIT','FCS_USERDELETE','FCS_USERREGISTER'] ;
const FCS_USER = ['FCS_USER'] ;
const _roles = ['FCS_ALL','FCS_ADD','FCS_EDIT','FCS_VIEW','FCS_AUDIT','FCS_REPORTSVIEW','FCS_REPORTS', 
              'FCS_USEREDIT','FCS_USERDELETE','FCS_USERREGISTER','FCS_USERADMINVIEW','FCS_ADMINUSERCREATE' ];         
const _groups = [ {'SUPERADMIN':FCS_SADMIN} ,
                {'FCS_ADMIN':FCS_ADMIN} ,
                {'FCS_USER':FCS_USER},
                {'FCS_OPERATIONS':FCS_OPERATIONS}] ;     

module.exports = function () {  

   //-------------------------------------------------------------------------
    let generatePassword = (len)=> {
        len = len || 12
        let p ='!@#$+-*&_'
        let randomstring=''
        let s=Math.floor(Math.random() * (p.length-1));
        for(let i=0; i<len; ++i){
            randomstring += s==i?
                            saltChars.charAt(Math.floor(Math.random() * saltChars.length)):
                            saltChars.charAt(Math.floor(Math.random() * saltChars.length));
        }
        return randomstring;
    }
    //-----------------------------------------------------------------------------------
    let genTokenID = (len) => {
        len = len || 20 ;
        if (crypto && crypto.randomBytes) {
            return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
        } else {
            for (var i = 0, salt = ''; i < len; i++) {
                salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
            }
            return salt;
        }
    }
    //---------------------------------------------------------------------------------
     module.htmllogin = (req,res)=>{
        res.render('login', {page_title: 'Login'});    
    }
    //---------------------------------------------------------------------------------
    module.htmladmin = (req,res)=>{
        res.render('adminusers', {page_title: 'Login'});    
    }
    //---------------------------------------------------------------------------------
    module.update = (req,res)=>{
        let params = getLoginParams(req)
        console.log("UPDATE params = %j",params) 
        saveUser(params,(err,result)=>{
            res.send(result)
        } )         
    }
    //--------------------------------------------------------------------------------
    module.userlist = (req,res)=>{
        getuserlist((err,data)=>{
            res.send(data)
        })
    }
    //---------------------------------------------------------------------------------
    let getuserlist = (callback) =>{
        let list = {cargo:[]}
        let qry = ` select date,id,cargo from dict where type = 'USER' and parent ='CREDENTIALS' `
        runQry(qry,(err,result)=>{
            result.forEach((elem,i)=>{
                let cargo = JSON.parse(elem.cargo)
                cargo.date = elem.date
                list.cargo.push(cargo)
            })
            if (callback) {
                callback(err,list);
            } 
        })
    }
     //--------------------------------------------------------------------------------
     module.userjwt = (req,res)=>{
        let params = getLoginParams(req)
        let userid = params.userid
        getUser(userid,(err,data)=>{
           
            res.send(data)
        })
    }
    //--------------------------------------------------------------------------
    let runQry =  module.runQry = (qry,callback) => {    
        try {
            
            console.log("\nrunQry=%s\n",qry)
            new sql.Request().query(qry, (err, result) => {
                if(err) {
                     console.log("login  Connect error%s\n\n Run query=%s",err,qry)
                }
             
                callback(err,result.recordsets[0])
              
            })    
        } catch(err){
            console.log("run query error=%s",err)
            callback(err,{})
        }
    }
    //------------------------------------------------------------------------------		                
    let saveUser= module.saveUser = (data,callback) => {
        let qry = ` -- insert or update 
         `
        let userid = data.userid
        var now = moment().format(tokenExpireFormat);  
        tokenid = genTokenID()
        getUser(userid,(err,result)=> {
            if( ! isEmpty(result)) {
                    // go through data and update  result
                    Object.keys(data).forEach((key) =>{
                        result[key] = data[key];
                    });
                    console.log("saveUser=%j",result)
                    qry += ` UPDATE dict SET date = GETDATE() ,cargo = '${JSON.stringify(result)}' where type = 'USER' and parent = 'CREDENTIALS' and id='${userid}' `
            } else {
                qry += ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'USER', '${userid}', 'CREDENTIALS','', GETDATE(),'${JSON.stringify(data)}' ) `
            }
         
            runQry(qry,(err,result)=>{
                    if (callback) {
                        callback(err,result);
                    } 
               
            })
        })
    }
    //--------------------------------
    module.tokenidUserid = (req,res)=>{
        let params = getLoginParams(req)
        let tokenid = params.tokenid
        gettokenidUser(tokenid,(err,data)=>{
            res.send(data)
        })
    }

     //------------------------------------------------------------------------------		                
     let gettokenidUser = module.gettokenidUser = (tokenid,callback) => {
        let qry = 
    `
        select parent from dict where id = '${tokenid}' and type = 'TOKN' 
    `
        let _result = 'Not Found'
        runQry(qry,(err,result)=>{
            if( ! isEmpty(result)) {
                _result = result[0].parent
            }
            if (callback) {
                callback(err,_result);
            } 
        })
    }
    //------------------------------------------------------------------------------		                
    let getUser = module.getUser = (userid,callback) => {
        let qry = 
    `
        select cargo from dict where id = '${userid}' and type = 'USER' and parent = 'CREDENTIALS'
    `
        runQry(qry,(err,result)=>{
            let cargo = {}
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            } else {
                err = "USER NOT FOUND"
            }
            if (callback) {
                callback(err,cargo);
            } 
        })
    }
    //-----------------------------------------------------------------------------------------
    module.passwordChange = (req,res) => { //GET returns tokenid
        let params = getLoginParams(req)
      //  let params = req.params
        let userid = params.userid
        let password = params.password
        let source = params.source
        _login(userid,password,source,(err,token)=>{
            res.send(token)
        })
    }
    //-----------------------------------------------------------------------------------
    let sendMail = (email_config,callback)=>{
        email.send(email_config, 
            (err, message) => { 
                callback(err,message)
            }
        )
    }
    //-----------------------------------------------------------------------------
    module.logout = (req,res) => { // logou
        let params = getLoginParams(req)
        let tokenid = params.tokenid;
        if ( ! isEmpty( tokenid )){
            removeToken(tokenid,(err,token)=>{
           
            })
        }
    };
    //--------------------------------------------------------------------------------
    module.verifyASA = (req,res)=>{
        let params = getLoginParams(req)
        let tokenid = params.tokenid;
        let code = params.code;
        let userid = params.userid;
        getToken(tokenid,(err,token) =>{  
            token.count= token/count +1
            //saveToken(userid,token,(err,result)=>{
                //res.send(user)
            //})  
        
           if(err) {  
                res.send(err)
           } else {
                res.send(  (token.passcode == code && token.userid==userid)?"CODE_VERIFIED":"CODE_FAILED" )
           }
        })
    }
    //-----------------------------------------------------------------------------------------
    module.login = (req,res) => { //GET returns tokenid
        let io = req.app.get("socket.io")
        let params = getLoginParams(req)
        let userid = params.userid
        let password = params.password
        let source = params.source        
        let registerCode = params.registerCode
        if(io){
            io.emit("LOGIN_MSG","registerCode")    
        }
        if(! isEmpty(registerCode)){
            //calling("registerCode")
            getAuthToken(registerCode,(err,result)=>{
                if(result.error) {
                    res.send(err)
                } else {
                    getUser(result.userid,(err,user)=>{
                        verifyCredentials(user,source,(user) =>{
                            // create token id
                            let now = moment().format(tokenExpireFormat);  
                            user.tokenid = genTokenID() 
                            user.lastAccess=now;           
                            user.status = 'VERIFIED'
                            user.expire = moment().add(tokenExpire, 'seconds').format(tokenExpireFormat);
                            saveToken(user.userid,user,(err,result)=>{
                                res.send(user.tokenid)
                            })                           
                        })
                    })
                }
            })
        } else {
            console.log("PARAMS",userid,password,source)
            _login(userid,password,source,(err,token)=>{
                
                res.send(""+token+"")
            })
        }
    }
    //-----------------------------------------------------------------------------------------
    let verifyCredentials = (user,source,callback) =>{
        let now = moment().format(tokenExpireFormat);  
        let validsource = user.source.split(',').includes(source)
        let valid = [1,'1','Yes'].includes(user.valid )
        let active = [1,'1','Yes'].includes(user.active)
        let pwdReset = [1,'1','Yes'].includes(user.pwdReset)
        user.lastAccess = now
        user.expire = moment().add( tokenExpire, 'seconds').format(tokenExpireFormat);
        // check if password expired
        user.status = "USER OK";           
        user.msg = "USER OK";                  
        //console.log(valid,active,pwdReset)  
        if ( ! active ){
            user.status = "ACCOUNT INVALID";
            user.msg = "ACCOUNT INVALID";
            user.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
            //disableUser(user)
        } else if (! valid){
           user.status = "INVALID TOKEN";
            user.msg = "INVALID TOKEN";
        } else if ( pwdReset) {
            user.status = "RESET PASSWORD";
            user.msg = "RESET PASSWORD";
        } else if (now > user.pwdExpire ){
            console.log("expired now=%s  ex=%s" ,now,user.pwdExpire)
            user.status = "EXPIRED PASSWORD";
            user.msg = "EXPIRED PASSWORD";
        } else if (now > user.expire ){
            user.msg = "EXPIRED TOKEN";
            user.status = "EXPIRED TOKEN";
        } else {
            user.status = "USER OK"                
        }              
        callback(user)
    }
    //------------------------------------------------------------------------------------------
    let _login = module._login = (userid,password,source,callback) => { //GET returns tokenid
        let now = moment().format(tokenExpireFormat);  
        getUser(userid,(err,user)=>{
            console.log("login user",user)
            if(err)   console.log("Error User Login : %s ",err );
            if ( ! isEmpty(user) ) {
                passwordHash(password).verifyAgainst(user.hash, (error, verified)=> {   
                        if(error) {
                            user.msg = 'PASSWORD_ERROR';
                            user.msgDetail = 'Error in verification, Something went wrong!';
                        }
                        user.count +=1
                        if(verified){
                           verifyCredentials(user,source,(user) =>{
                                if(user.status == "USER OK"){
                                    user.tokenid = genTokenID() ;
                                    //console.log("user tokenid = %s",user.tokenid)
                                    user.lastAccess=now;           
                                    user.expire = moment().add(tokenExpire, 'seconds').format(tokenExpireFormat);
                                    user.count=0
                                    let use2Factor = [1,'1','Yes'].includes(user.use2Factor )
                                    if (use2Factor && ! user.asasent )  {
                                        let asacode = genPasscode(6)
                                        user.passcode = asacode
                                        console.log("ASA code "+asacode)
                                        let msg = ` Use this code for FCS ASA verification ${asacode}`
                                        let email_config = {
                                            text:    msg ,
                                            from:    "FCS@nyehealth.org", 
                                            to:      `${user.email}`,
                                            cc:      "dpal@nyehealth.org",
                                            subject: "FCS pass code"
                                        }
                                        sendMail(email_config,(err,msg)=>{
                                            user.msg = 'ASA code sent'  
                                            user.status = "ASA CODE SENT"
                                            user.asasent = 1 
                                            saveToken(user.userid,user,(err,result)=>{
                                                console.log(err,result)
                                               // savePWDHistory(user,(err,result)=>{
                                                    callback(err,user.tokenid)
                                               // })
                                            })       
                                        })
                                   
                                    } else { //asa code already 
                                        user.msg = 'Verified' 
                                        user.status = 'VERIFIED'
                                        user.asasent = 0
                                        //console.log("SAVING TOKEN %j",user)
                                        saveToken(user.userid,user,(err,result)=>{
                                          //  console.log(err,result)
                                            //savePWDHistory(user,(err,result)=>{
                                                callback(err,user.tokenid)
                                            //})
                                        })    

                                        //console.log("Login verified=%s \nerror=%s\n\ntoken=%j\nsource=%s",verified,error,user,source)
                                    }
                                
                                } else if (user.status == 'EXPIRED PASSWORD') {    
                                    user.msg = "Password Expired - Please Change Password"
                                    callback(err,user)
                                }  else { 
                                    console.log("NOT OK ",user.status,user.count)
                                    if(user.count > 4 ) {
                                        user.expire = moment().add(- tokenExpire, 'seconds').format(tokenExpireFormat);
                                        user.valid = 0;
                                        user.msg = "USER LOCKED OUT"
                                        user.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
                                        //disableUser(user)
                                    } else {
                                        let validsource = user.source?user.source.split(',').includes(source):""
                                        let valid = [1,'1','Yes'].includes(user.valid )
                                        let active = [1,'1','Yes'].includes(user.active)
                                        let pwdReset = [1,'1','Yes'].includes(user.pwdReset)
                                        user.msg = ! validsource ? 'Source Not verified ' :""
                                        user.msg += ! verified ? ' userid or password not verified':""
                                        user.msg += valid?"":" not a validated user"
                                    }
                                    user.expire = moment().add( tokenExpire, 'seconds').format(tokenExpireFormat);
                                   // saveToken(user.userid,user,(err,result)=>{
                                   //     console.log(err,result)
                                       // savePWDHistory(user,(err,result)=>{
                                            callback(err,user)
                                       // })
                                   // })   
                                }
                            })
                        } else {
                            user.status = "PASSWORD NOT VERIFIED";
                            user.msg = "Password not verified ";
                            callback(err,"n/a")
                        }
                })
            } else {
                user.status = "USER NOT FOUND";
                user.msg = "User not found ";
                callback(err,user)
            }
        });               
    };
      //------------------------------------------------------------------------------		                
      let saveToken = (userid,token,callback) => {
        let type = 'TOKN';
        let date_time = moment().format('YYYYMMDD');
        removeExpiredTOKN((err)=>{ 
            let  qry = ` insert into ${DICT_TABLE}(type,id,parent,date,date_time,cargo) VALUES( '${type}', '${token.tokenid}', '${userid}', GETDATE(),'${date_time}','${JSON.stringify(token)}' ) `
            runQry(qry,(err,result)=>{
                if(callback) {
                    callback(err,result)
                }
            });
        })
    }
    //--------------------------------------------------------------------------------
    let getToken = module.getToken = (tokenid,callback) => {  
        removeExpiredTOKN((err)=>{ 
            if (isEmpty(tokenid) ) {
                callback("no token ID",'')
            } else{
                let type ='TOKN'
                let qry = `select cargo from ${DICT_TABLE}  where id = '${tokenid}' and type= '${type}'  ` ;        
                let cargo = {}
                runQry(qry,(err,result)=>{
                    if( ! isEmpty(result)) {
                        cargo = JSON.parse(result[0].cargo);
                    }
                    if (callback) { 
                        callback(err,cargo);
                    } 
                })
            }
        })
    }
    //------------------------------------------------------------------------------		                
    module.genPwd = (length) => {
        return generatePassword(length) ;
    }
    //------------------------------------------------------------------------------		                
    let getRoles = (userid,callback) => { 
        getUser(userid,(err,token)=>{
            if  ( ! isEmpty( err ) ) {
                console.error(err)
                callback(err,_roles)
            } else {
                if(typeof token == 'string') {
                    token= JSON.parse(token);
                }
                callback(err,token.roles)
            }
        })
    };
    //------------------------------------------------------------------------------		                
    let setRoles =  (userid,roles,callback) => { 
        //add roles to token
        try {
            getUser(userid,(err,token)=>{
                if  ( ! isEmpty( err ) ) {
                    console.error(err)
                    callback(err,{})
                } else {
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    //set new roles and save
                    token.roles = roles;
                    saveUser(token,function(err,data){           
                        if(callback){
                            callback(err,data)
                        }
                    })            
                }
            })
        } catch(e) {
            callback(e,"")
        }
    };
    //-----------------------------------------------------------------------------
    let removeExpiredTOKN = (callback) =>{
        let type = 'TOKN';
        let qry = `select * from ${DICT_TABLE} where type = '${type}' ` ;
        let removelist = []
        //--------------------------
        let remove = (list,index) =>{
            if(index < list.length ) {
                let tokenid = list[index]
                let qry = ` delete from dict where type = 'TOKN' and id = '${tokenid}'  `
                console.log("remove qry=%s",qry)
                runQry(qry,(err,result)=>{
                    remove(list,index+1)
                })
            } else {
                return 0
            }
        }
        runQry(qry,(err,result)=>{
            let now = moment().format(tokenExpireFormat);
            if ((!isEmpty(result)) && (result.length > 0)) {
                result.forEach((_row)=>{
                    try {
                        let _userid = _row.parent
                        let _tokenid = _row.id
                        let token = _row.cargo
                        if( token) {
                            if(typeof token == 'string') {
                                token= JSON.parse(token);
                            }
                            let expire = token.expire;
                            let tokenid  = token.tokenid;
                            let userid = token.userid;
                           console.log("tokenid=%s \n      now=%s      expire=%s",_tokenid,now,expire)
                           console.log("now > expire =%s\nisEmpty(expire)=%s\nisEmpty(expire)  || now > expire =%s",now > expire ,isEmpty(expire),isEmpty(expire)  || now > expire )
                            if ( isEmpty(expire)  || now > expire )   {         
                                removelist.push(tokenid)                        
                            }    
                        }
                    } catch (err)  {
                        console.error(err)
                    }                            
                });   
                if(! isEmpty(removelist)) {
                    remove(removelist,0)
                }
            }
            if (callback) {
                callback(err)
            }
        })
    }
    //------------------------------------------------------------------------------
    let Token = module.Token = function(req,res) {
        let params = getLoginParams(req)
        let tokenid = params.tokenid
        let userid = params.userid
        let qry = `select top 1 cargo from ${DICT_TABLE}  where id = '${tokenid}' and type= 'TOKN' order by date desc ` ;    
        let cargo = {}    
        runQry(qry,(err,result)=>{
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            }
            res.send(cargo)
        })  
    }
    //------------------------------------------------------------------------------
    let token = module.token = (tokenid,callback)=>{
        let qry = `select top 1 cargo from ${DICT_TABLE}  where id = '${tokenid}' and type= 'TOKN' order by date desc ` ;    
        runQry(qry,(err,result)=>{
            let cargo = {}    
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            }
            callback(err,cargo)
        })  
    }
    //------------------------------------------------------------------------------		                
    let expireToken = module.expireToken = function(tokenid,callback)  {
        removeExpired(function(err){
            if ( ! isEmpty(tokenid)){ 
                getTOKN(tokenid,function(err,result){
                    if(DEBUG){
                        console.log("get token=%s",result);
                    }
                    callback( err,result) ;
                });
            } else {
                let token = newToken();
                token.msg = 'Invalid token';
                callback("Error",token);
            };
        });
    };
    //----------------------------------------------------------------------------
    let loginFlow = function(status,tokenid)  {
        getTOKN(tokenid, function(err,token){
            if (DEBUG) {
            // console.log("jtoken = %j err= %s",token,err)
            }
            if (isEmpty(token)) {
                token = newToken();
            } else {
                if (typeof token == 'string' ) {
                    token = JSON.parse(token)                                    
                }
            }
            let flowMsg = token['msg'] ;
            let userid = token.userid;
            let roles = token["roles"]
            getFLOW(status, "LOGIN_FLOW",function(err,data){ // returns a JS function
                if(LOGGER){
                    logger.LOG(userid,"AUDIT",tokenid,status,"",roles,JSON.stringify(token));
                }
                try {
                    let proc = eval(flowStatus) ;  //eg ATTEMPT2 = text
                    if (config.DEBUG) {
                        console.log("Login Flow Called, flow status = %s , token = %j ",flowStatus ,token);
                    }
                    if ( isEmpty(data)) {
                        eval(proc);
                    }else{
                        eval(data);
                    }
                } catch(ex) {
                    console.error(ex);
                }
            }); 
        });
        return false;
    };
    //-----------------------------------------------------------------------------
    module.verifyAccess = (req,res) => { 
        let params = getLoginParams(req)
        let role = params.role.split(',')
        var tokenid = params.tokenid
        getToken(tokenid,(err,token)=> {
            var verified = false
            if(token.roles){
                role.forEach(element => {
                    console.log("ELEMENT",element)
                    if( token.roles.includes(element) ){
                        verified = true
                    }
                });
            }
            console.log("role %j verified = %s",role,verified)
            res.send(verified)
        })
    }
    //----------------------------------------------------------------------------
    module.verifyToken = function (req,res){ 
        let params = getLoginParams(req)
        var tokenid = params.tokenid;
        // lookup user
        if(tokenid){
            getToken(tokenid,(err,token)=>{
                if (! isEmpty(err)) {
                    res.send( false);
                } else {
                    res.send( token.tokenid == tokenid ); 
                }    
            });
        } else {
            res.send(false)
        }
    }
    //-------------------------------------------------------------------------------
    module.htmlregister   = (req,res)=>{
        res.render('registeruser', { page_title: 'register'});
    }
     //-------------------------------------------------------------------------------
     module.htmllogin   = (req,res)=>{
        res.render('login', { page_title: 'login'});
    }
    //-----------------------------------------------------------------------------
    let isUser=(userid,callback)=>{
        let type = "USER";
        let parent = "CREDENTIALS";
        let qry = `select count(*) as qty from dict where type= '${type}' and parent = '${parent}' and id = '${userid}' `;
        let result = false;
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result[0]);               
        });    
    }
    //------------------------------------------------------------------------------
    module.approve = (req,res) => {
        let params = getLoginParams(req)
        let userid = params.userid
        getUser(userid,(err,user)=>{
            console.log("user=%j",user)
            if(err)   console.log("Error Approve : %s ",err );
            password='Remember1'
            passwordHash(password).hash((error, hash,salt,key) =>{
                user.salt = salt;
                user.hash = hash;
                user.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                user.valid =1
                user.active=1
                user.pwdReset=1
                saveUser(user,(err,data) => {    
                    let msg = 
                    `Congratulations your account has been approved
                    Temporary password is ${password} for userid ${userid}. 
                    Please use the link ${config.fhirhost} to login in.
                    You will need to change your password at the first login 
                    `
                    let email_config = {
                        text:    msg ,
                        from:    "FCS@nyehealth.org", 
                        to:      `${token.email}`,
                        cc:      "dpal@nyehealth.org",
                        subject: "FHIR Central Services"
                    }
                    user.msg = msg
                    sendMail(email_config,(err,result)=>{
                        saveEMAIL(user,(err,result)=>{
                            email.send(email_config, (err, message) => { 
                                console.log("email err="+err);
                                console.log("email message="+JSON.stringify(message));
                            } );
                            user.status = msg
                            res.send(user);
                        });      
                    }) 
                })
            })
        })
    }
      //------------------------------------------------------------------------------
      module.disable = (req,res) => {
        let userid = req.params.userid
        getUser(userid,(err,user)=>{
            console.log("user=%j",user)
            if(err)   console.log("Error Approve : %s ",err );
            user.valid =0
            user.active=0
            saveUser(user,(err,data) => {    
                res.send(user);
            });      
        })
    }
    //--------------------------------------------------
    let savePWDHistory = (token,callback) =>{ // cargo = token
       // console.log('Save PWD HISTORY= %j',token);
        let userid = token.userid
        let msg = token.msg
        let  qry = ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'PWD', '${userid}', 'PWDHISTORY','', GETDATE(),'${JSON.stringify(token)}' ) `
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result);               
        });              
    };
    //--------------------------------------------------
    let saveEMAIL = (token,callback) =>{ // cargo = token
        console.log('SaveEMAIL= %j',token);
        let userid = token.userid
        let qry = ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'EMAIL', '${userid}', 'FCSREGISTER','', GETDATE(),'${JSON.stringify(token)}' ) `

        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result);               
        });              
    };
    //----------------------------------------------------------------------------
    let saveAuthCode = (token,callback)=>{
        let registerCode = token.registerCode
        let userid = token.userid
        let type = 'REGISTER'
        let ext = "CODE"
        let qry = ` insert into dict(type,id,parent,ext,date,cargo) VALUES( '${type}', '${registerCode}', '${userid}','${ext}', GETDATE(),'${JSON.stringify(token)}' ) `
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result);               
        });      
    }
    //-----------------------------------------------------------------------------
    let getAuthCode=(userid,callback)=>{
        let type = 'REGISTER'
        let ext = "CODE"
        let qry = `select top 1 * from dict where type= '${type}' and parent = '${userid}' and ext ='${ext}' `
        console.log(qry)
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting getAuthCode result : %s ",err );
            let registerCode = "Error: No Registry Code Found"
            let cargo = {}
            if(typeof result[0] == 'string') {
                cargo = JSON.parse(result[0]);
            } else {
                cargo = result[0]
            }
            if(! isEmpty(cargo)) {
                registerCode = JSON.parse(cargo.cargo).registerCode
            } 
            callback( err,registerCode);               
        });  
    }
    //-----------------------------------------------------------------------------
    let getAuthToken=(registerCode,callback)=>{
        let type = 'REGISTER'
        let ext = "CODE"
        let qry = `select top 1 * from dict where type= '${type}' and id = '${registerCode}' and ext ='${ext}' `
        console.log(qry)
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting getAuth Token : %s ",err );
            let registerCode = {"error":"Error: No Registry Code Found"}
            let cargo = {}
            if(typeof result[0] == 'string') {
                cargo = JSON.parse(result[0]);
            } else {
                cargo = result[0]
            }
            if(! isEmpty(cargo)) {
                registerCode = JSON.parse(cargo.cargo)
            } 
            callback( err,registerCode);               
        });  
    }
    //----------------------------------------------------------------------------
    module.registerCode = (req,res) =>{
        let params = getLoginParams(req)
        let userid = params.userid;
        getAuthCode(userid,(err,result)=>{
            res.send(result)
        })
    }
    
    //----------------------------------------------------------------------------
    module.registerToken = (req,res) =>{
        let params = getLoginParams(req)
        let now = moment().format(tokenExpireFormat);
        let userid = params.userid;
        let registerCode = params.registerCode;
        getAuthToken(registerCode,(err,user)=>{
            user.tokenid = genTokenID() ;
            user.lastAccess=now;           
            user.expire = moment().add(tokenExpire, 'seconds').format(tokenExpireFormat);
            user.count=0
            user.use2Factor = 0
            saveToken(user.userid,user,(err,result)=>{
                res.send(user)
            })     
        })
    }
    //------------------------------------------------------------------------------
    module.register = (req,res) => {
        let params = getLoginParams(req)
        console.log("register params = %j\n\data=%j",params,req.body)
        let token = newToken();
        let password = generatePassword(16);
        token.userid = params.userid;
        token.fname = params.fname;
        token.lname = params.lname;
        token.phone = params.phone;
        token.email = params.email;
        token.group = params.group;
        token.source=params.source
        isUser(params.userid,(err,result)=>{
            if(result && result.qty > 0) { // user exist
                token.msg = 'user id found'
                token.status = `"User already exists please select another eg:${params.userid+'1'}`
                res.send(token)
            } else {
                //password='Remember1' // remove after testing
                password = config.useDefaultPassword ?"Remember1":password
                passwordHash(password).hash(function(error, hash,salt,key) {
                    token.salt = salt;
                    token.hash = hash;
                    token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                    token.valid = 0; // activate
                    token.active = 0; // activate
                    token.pwdReset =1 ; // need to reset password
                    token.expire = moment().add(tokenExpire, 'seconds').format(tokenExpireFormat);
                    token.registerCode = genTokenID();
                    token.roles = FCS_OPERATIONS ;           
                    saveUser(token,function(err,data){    
                        savePWDHistory(token,(err,result)=>{
                            let msg = 
` You have applied to be registered as a new user on the FHIR Central Services.  
 FCS team is reviewing your request and will update you on approval.`
                            let email_config = {
                                text:    msg ,
                                from:    "FCS@nyehealth.org", 
                                to:      `${token.email}`,
                                cc:      "dpal@nyehealth.org",
                                subject: "FHIR Central Services"
                            }
                            token.msg = msg
                            saveAuthCode(token,(err)=>{
                                sendMail(email_config,(err,message)=>{
                                    console.log("email err="+err);
                                    console.log("email message="+JSON.stringify(message));
                                  //  saveEMAIL(token,(err,result)=>{
                                        token.status = msg
                                        res.send(token);
                                  //  });      
                                })
                            })
                        });    
                    });
                }); 
            }
        })
    }
    //------------------------------------------------------------------------------		
    module.resetpwd = function(req,res) {  // change password in the Admin tool for admin users
        let params = getLoginParams(req)
        let userid = params.userid
        let tokenid = params.tokenid 
        if ( ! isEmpty(userid)) {
            getUser(userid, function(err,data)  {
                if  ( ! isEmpty( err ) ) {
                    console.log("get user error",err)
                    res.send(err);
                } else {
                    let token = data
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    let newpassword = generatePassword()
                    passwordHash(newpassword).hash( function(error, hash,salt,key) {
                        if (error) {
                            console.error("resetpwd err= %j",error)
                            res.send(error);
                        } else {
                            // to do check to see if password already exists in history
                            token.salt = salt;
                            token.hash = hash;
                            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            token.pwdReset = 0;                            
                            saveUser(token,function(err,data) {
                                //console.log("\n\nreset pwd save user token = %j\n\n",token)
                                //savePWDHistory(token, (err,result)=> {
                                    let msg = 
                                    ` Your new password has been reset, use temporary ${newpassword} it will expire in ${token.pwdExpire}.  
                                     click on this link ${config.fhirhost} to login and it will prompt you to change your password`
                                    let email_config = {
                                        text:    msg ,
                                        from:    "FCS@nyehealth.org", 
                                        to:      `${email}`,
                                        cc:      "dpal@nyehealth.org",
                                        subject: "FHIR Central Services"
                                    }
                                    token.msg = msg
                                    sendMail(email_config,(err,message)=>{
                                        console.log("email err="+err);
                                        console.log("email message="+JSON.stringify(message));
                                        saveEMAIL(token,(err,result)=>{
                                            res.send(token);
                                        });      
                                    })  
                               // });                         
                            });
                        }    
                    }); 
                }       
            });
        }
    }
    //-----------------------------------------------------------------------------------
    let genPasscode = function(len) {
        len = len || 8 ;
        let sChars = '0123456789';
        let sCharsCount = sChars.length;      
        let out = ''
        for (var i = 0, salt = ''; i < len; i++) {
            out += sChars.charAt(Math.floor(Math.random() * sCharsCount));
        }
        return out;
    }
    //-----------------------------------------------------------------------------------
    module.forgotPassword = (req,res) =>{
        let params = getLoginParams(req)
        let email = params.email;
        let userid = params.userid;
        let tokenid = params.tokenid;
        if ( ! isEmpty(userid)) {
            getUser(userid, (err,data) => {
                if  ( ! isEmpty( err ) ) {
                    console.error(err)
                    res.send(err);
                } else {
                    let token = data
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    let newpassword = generatePassword()
       
                    console.log("forgot password token = %j",token)
                    passwordHash(newpassword).hash( (error, hash,salt,key) =>{
                        if (error) {
                            console.error(error)
                            res.send(error);
                        } else {
                            // to do check to see if password already exists in history
                            token.salt = salt;
                            token.hash = hash;
                            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            token.pwdReset = "0";      
                            token.status = "PASSWORD CHANGE"
                            token.msg = `Password Changed for User ${userid}`
                            let msg = 
                            ` Your new password has been generated ${newpassword} it will expire in ${token.pwdExpire}.  
                             click on this link ${config.fhirhost} to login and it will prompt you to change your password`
                            let email_config = {
                                text:    msg ,
                                from:    "FCS@nyehealth.org", 
                                to:      `${email}`,
                                cc:      "dpal@nyehealth.org",
                                subject: "FHIR Central Services"
                            }
                            sendMail(email_config,(err,message)=>{
                                console.log("email err="+err);
                                console.log("email message="+JSON.stringify(message));
                                saveEMAIL(token,(err,result)=>{
                                    saveUser(token,(err,data) =>{
                                        savePWDHistory(token, (err,result)=> {
                                            console.log("callback err=%,result=%s",err,result)
                                        });                         
                                    });       
                                });   
                                res.send(token);   
                            })
                        }
                    })
                }
            })
        }
    }
    //------------------------------------------------------------------------------		
    module.changepwd = function(req,res){  // change password on the 
        let params = getLoginParams(req)
        let userid = params.userid
        let password = params.password
        let newpassword = params.newpassword
        console.log("change pwd params = %j",params)
        getUser(userid,(err,user)=>{
           // console.log(user,err)
            if(err)  {
                console.log("Error Change password : %s ",err );
                user.status = err
                user.msg = err
                res.send(user);
            } else {
                // check if old password = saved password            
                passwordHash(password).verifyAgainst(user.hash, (error, verified)=> {   
                    if(error) {
                        user.msg = 'PASSWORD_ERROR';
                        user.msgDetail = 'Error in verification, Something went wrong!';
                        res.send(user);
                    } 
                    if(verified) {
                        passwordHash(newpassword).hash((error, hash,salt,key) =>{
                            user.salt = salt;
                            user.hash = hash;
                            user.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            user.pwdReset = 0
                            saveUser(user,(err,data) => {    
                                user.status = "PASSWORD CHANGE"
                                user.msg = `Password Changed for User ${userid}`
                                savePWDHistory(user,(err,result)=>{
                                    let link = config.fhirhost
                                    console.log("\n\nconfig=%j\n\n",config)
                                    let msg = 
                                    ` Your new password has been generated "${newpassword}" it will expire in ${user.pwdExpire}.  
                                     click on this link ${link} to login and it will prompt you to change your password`
                                    let email_config = {
                                        text:    msg ,
                                        from:    "FCS@nyehealth.org", 
                                        to:      `${email}`,
                                        cc:      "dpal@nyehealth.org",
                                        subject: "FHIR Central Services"
                                    }
                                    sendMail(email_config,(err,message)=>{
                                        console.log("email err="+err);
                                        console.log("email message="+JSON.stringify(message));
                                        saveEMAIL(user,(err,result)=>{
                                                 
                                        });   
                                        res.send(user);   
                                    })
                                })
                            });
                        })
                    } else {
                        user.status = "INVALID PASSWORD"
                        user.msg = "Invalid password please contact your administrator"
                        res.send(user);
                    }
                })
            }
        })       
    }
    //------------------------------------------------------------------		                
    let captchaImg = function() {                   
        let num = parseInt(Math.random()*9000+1000);                                   
        let p = new captchapng(70,25,num); // width,height,numeric captcha
        p.color(115, 95, 197, 100);  // First color: background (red, green, blue, alpha)
        p.color(30, 104, 21, 255); // Second color: paint (red, green, blue, alpha)
        var img = p.getBase64();
        return {img:img,num:num};
    };
    //------------------------------------------------------------------------------		     
    module.captchapng = function(req, res){         
        var capt = captchaImg();                         
        var validcode = new Buffer(capt.img).toString('base64');
        var imgbase64 = new Buffer(capt.img,'base64');
        res.send(imgbase64);
    };
    //------------------------------------------------------------------------------		     
    module.captcha = function(req, res){         
        var capt = captchaImg();                         
        var validcode = new Buffer(capt.img).toString('base64');       
        //res.render('login', { page_title: 'Captcha',image:capt.img, num:capt.num});
        res.send({ num:capt.num, image:capt.img});
    };
    //-----------------------------------------------------------------------------
    let removeToken = (tokenid,callback) => {
        let type = 'TOKN';
        let qry = `select * from ${DICT_TABLE} where type = '${type}' and id ='${tokenid}' ` ;
        runQry(qry,(err,result)=>{
            if (callback){
                callback(err,result)
            }
        })
    };
    //-----------------------------------------------------------------------------
    var isRole = module.isRole = function(tokenid,role,callback ) {
        getTOKN(tokenid,function(err,res){
            if( isEmpty(err) && !isEmpty(res) ){
                token = JSON.parse(res) ;
                let result = false;
                let uroles= token.roles;
                let alist = role;
                if (typeof uroles == 'string'){
                    alist = uroles.split(',');
                } else {
                    alist = uroles;
                }
                if (typeof role == 'string'){
                    arole = role.split(',');
                }
                alist.forEach((part)=>{
                    if( part == 'FCS_ALL' ){
                        result = true 
                    } else {
                        arole.forEach((_role)=>{
                            if(  part == _role ) {
                                result = true;
                            }
                        })
                    }    
                })
                if(callback) {
                    callback(result) 
                } else{
                    return result
                }
            } else{
                if(callback) {
                    callback(false) 
                } else{
                    return false
                }            
            }
        });
    };        
    //----------------------------------------------------------------------------
    let authorised = module.authorised = (tokenid,roles,callback)=>{
        getTOKN(tokenid,function(err,token){
            if ( isEmpty(err)){
                //console.log("login.authorised jtoken = %j err= %s",token,err)
                if (typeof token == 'string' && ! isEmpty(token)) {
                    token = JSON.parse(token)
                }
                let userid = token.userid;
                if ( (! isEmpty(callback))  && (! isEmpty(token))  ) {
                    token.password='***********';
                    if (typeof roles == 'string' ) {
                        roles = roles.split(',');
                    }
                    //console.log('login._authorized roles='+roles);
                    if (typeof roles == 'array' ) {
                        var testroles = token.roles ;
                        if (typeof testroles == 'string' ) {
                            testroles = testroles.split(',');
                        }                    
                        //console.log('_authorized testroles='+testroles);
                        let uroles = new sets.Set(roles) ;
                        let test = ( uroles.contains(["PP_ALL","PP_ADMIN"]) || uroles.contains(roles) );
                        callback(test,testroles,token) ;
                    } else {
                        callback(false);
                    }
                } else {
                    if(LOGGER){
                        logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,'security token not found');
                    }            
                    callback(false);
                }
            } else {
                if(LOGGER){
                    logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,err);
                }            
                callback(false);
            }
        });
        return false;
    } 
    //------------------------------------------------------------------------------		
    var resetDisabledUser = function(token,callback){
        console.log("\n\n\nresetting user token = \n\n%j\n\n",token)
        saveUser(token,function(err,data){           
            if(callback){
                callback(err,data)
            }
        })
    }
    //-----------------------------------------------------------------------------
    let _disableUser = function(token,reset=true) { 
        let now = moment().format(tokenExpireFormat);
        let tokenid = token.tokenid;
        token.lastAccess=now;
        token.tokenid = '' ;
        //token.msg = "USER DISABLED"                
        token.valid = 0;        
        removeToken(tokenid,(err)=>{
            saveUser(token,(err,data)=>{                                
                // reset user after 10 mins
                //console.log("disabling user token=%j",token)
                if(reset) {
                    token.tokenid = '' ;
                    token.msg = "USER DISABLED"                
                    token.valid = 0;    
                    //console.log("set timwout for user token=%j",token,LOGINRESET/1000)
                    setTimeout( resetDisabledUser ,LOGINRESET,token);                    
                }
            });   
        })
    }
    //-----------------------------------------------------------------------------
    module.disableUser = function(req,res) { 
        let now = moment().format(tokenExpireFormat);
        let tokenid = req.body.tokenid;
        let userid = req.body.userid;
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if( ! isEmpty(tokenid) ) {
            getToken(tokenid,function(err,data) {
                if ( ! isEmpty(err)){
                    res.send({"msg":err});
                } else {
                    let token = data;                    
                    _disableUser(token)
                    res.send("User Disabled")
                }
            })
        }    
    }
    
    //-----------------------------------------------------------------------------
    module.userprofile =  (req,res) => { // logou
        var tokenid = req.query.tokenid;
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if(isEmpty(tokenid)){
            res.send("Error: no id");
            return;
        }
        getToken(tokenid,(err,token)=>{
            getUser(token.userid,function(err,data){
                    if  ( ! isEmpty( err ) ) {
                        res.send("Error");
                    } else {
                        let token = JSON.parse(data);            
                        res.send(token);
                    }       
            });
        })
    };
    //------------------------------------------------------------------------------		                
    let saveJWT= module.saveJWT = (userid,data,callback) => { //data = JWT Token
        let qry = ` -- insert or update 
         `
        let type = 'AUTH'
        let parent = "AUTHORIZATION"
        let ext = "JWT"
        var now = moment().format(tokenExpireFormat);  
        tokenid = genTokenID()
        getJWT(userid,(err,result)=> {
            console.log(result)
            if( ! isEmpty(result)) {
                // go through data and update  result
                Object.keys(data).forEach((key) =>{
                    result[key] = data[key];
                });
             //   console.log("saveJWT =%j",result)
                qry += ` UPDATE dict SET date = GETDATE() ,cargo = '${JSON.stringify(result)}' where type = '${type}' and parent = '${parent}' and id='${userid}' and ext='${ext}' `
            } else {
                qry += ` insert into dict(type,id,parent,ext,date,cargo) VALUES( '${type}', '${userid}', '${parent}','${ext}', GETDATE(),'${JSON.stringify(data)}' ) `
            }
            console.log(qry)
            runQry(qry,(err,result)=>{
                if (callback) {
                    callback(err,result);
                } 
            })
        })
    }
      //------------------------------------------------------------------------------		                
      let getJWT = module.getJWT = (userid,callback) => {
        let type = 'AUTH'
        let parent = "AUTHORIZATION"
        let ext = "JWT"
        let qry = 
    `
        select cargo from dict where id = '${userid}' and type = '${type}' and parent = '${parent}' and ext='${ext}' '
    `
        runQry(qry,(err,result)=>{
            let cargo = {}
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result);
            } else {
                err = "USER NOT FOUND"
            }
            if (callback) {
                callback(err,cargo);
            } 
        })
    }
    //----------------------------------------------------------------------------------------
    let openidJWT = module.openidJWT= ()=>{
    return {   
            "jit": "DF94DC89-28BA-4E5A-B71E-CA1B9C78E64E",
            "iss":"https://jwt-idp.example.com",
            "sub": "HEALTHELINK",
            "aud": "iqg-router",
            "exp": 1640727829,
            "iat": 1614727829,
            "auth_time":1614727829,
            "nonce":"",
            "acr":"",
            "amr":"",
            "azp":"",
            "prn":"mailto:mike@example.com",
            "subjectID": "HEALTHELINK",
            "subjectOrganization": "Healthelink RHIO",
            "SubjectRole": "{\"code\"\";\"256950004\",\"codeSystem\":\"2.16.840.1.113883.3.18.6.1.16\"}",
            "PurposeOfUse": "{\"TREATMENT\":\"\",\"codeSystem\":\"\"}",
            "resourceID": "\fcs",
            "name": "HEALTHELINK",
            "targetSystem": "GRRHIO",
            "nbf":1300815780
        }
    };
    //------------------------------------------------------------------------------		                
    let newToken = module.newToken = (userid) => {
        let _token = { 
                tokenid: genTokenID(),
                registerCode:"", //this is the Single sign on code for which apps to acces
                use2Factor:1,
                passcode:0,
                userid: userid,
                pwdExpire:moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ,
                pwdReset:1,
                attempt:5,
                fname:'',
                lname:'',
                email:'',
                phone:'',
                sms:"",
                hash:'',
                salt:'',
                link: '/FCS',
                callurl: '/',
                count: 0,
                lastAccess: '',
                msg: '',
                theme: 'redmond',
                langid:'en-US',
                valid: 0,
                active: 0,
                baseurl:'',
                msg:"",
                msgDetail:"",
                apps:["FCS","SPRL"],
                group:"FCS_USER",
                roles:_roles,
                avatar:"",
                expire:moment().add(tokenExpire, 'seconds').format(tokenExpireFormat)
        };
        return _token;
    };
    //-------------------------------------------------------------------------------------------------
    let getLoginParams = (req,callback)=>    {
        let headers = req.headers
        let _params = {}
        let query = req.query
        let params = req.params
        let data = req.body
        console.log("BODY=%j",data)
       // console.log("\nparams = %j\nquery=%j\nbody=%j\nheaders=%j",params,query,data,headers)
        if(! isEmpty(headers)){
            let auth = headers['authorization']
            let authtoken = headers['X-Token']
           //console.log("\n\nparams.auth = %s\n\ntoken=%s\n\n",auth, auth.split(" ")[1])
            if(auth && isEmpty(_params.token)) {
                _params.token = auth.split(" ")[1]
            }
            if(authtoken && isEmpty(_params.tokenid)) {
                _params.tokenid = authtoken.split(" ")[1]
            }
        }

        if(query){
            Object.keys(query).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = query[element] 
                }
            })
        }
        if(params){
            Object.keys(params).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = params[element] 
                }
            })
        }
        if(data) {
            Object.keys(data).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = data[element] 
                }
            }) 
        }
     //  console.log("_params = %j",_params)
        return callback ? callback(_params) : _params
    }
    //------------------------------------------------------------------------------
    return module

}
