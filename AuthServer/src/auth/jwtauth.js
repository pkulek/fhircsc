
   
 
   
module.exports = function () {
    const isEmpty = require("../utils").isEmpty
    const config = require('../../config.json')
    const jwt = require('jsonwebtoken');
    const fs = require('fs')
    const sql = require(config.db);
    //--------------------------------------------------------------------------
    let runQry =  module.runQry = (qry,callback) => {    
        try {
            //console.log("\nrunQry=%\n",qry)
            new sql.Request().query(qry, (err, result) => {
                if(err) {
                    console.log("login  Connect error%s\n\n Run query=%s",err,qry)
                }
                if(callback){
                    if(err) {
                        callback(err,{} )
                    } else {
                        callback(err,result.recordsets[0])
                    }
                }
            })    
        } catch(err){
            console.log("run query error=%s",err)
            callback(err,{})
        }
    }
    //------------------------------------------------------------------------------		                
    let jwtPayload = module.jwtPayload = (sub,callback) => {  '' 
    let qry = 
    `
    select NEWID() as jit,scope,aud,sub,iss,subjectID,subjectOrganization,SubjectRole,PurposeOfUse,resourceID,name,targetSystem from qeauth where sub = '${sub}'
    `
        runQry(qry,(err,result)=>{
            if(err) {
                console.log("login jwt payload data=%j \n err=%s",result,err)
            }
            if (callback) {
                callback(err,result[0]);
            } 
        })
    }
    //-----------------------------------------------------------------------------------------
    let payload = module.payload = (qe,callback) =>{
        // get from auth table
       // login.jwtPayload(qe,(err,result)=>{
        jwtPayload(qe,(err,result)=>{
                if(callback){
                callback(err,result)
            }
        })
    }
    //-----------------------------------------------------------------------------------------
    let gentoken = module.gentoken = (params,callback) =>{   
        let reqqe = params.sub 
        let target = params.target
        payload(reqqe,(err,result)=>{
            if (result) {
                result.targetSystem = target
                result.targetSystem =  result.targetSystem.toUpperCase() 
                //console.log("gentoken result = %j",result)
                let privateKey = fs.readFileSync(config.privateKey);
                jwt.sign(result, privateKey,config.jwtOption, (err, token) => {
                    if(callback){
                        callback(err,token)
                    }
                });
            } else {
                callback(err,{})
            }
        })
    }
     //----------------------------------------------------------------------------------------
     let openidJWT = (callback)=>{
       let payload = {
            "jit": "DF94DC89-28BA-4E5A-B71E-CA1B9C78E64E",
            "iss":"https://jwt-idp.example.com",
            "sub": "HEALTHELINK",
            "aud": "iqg-router",
            "exp": 1640727829,
            "iat": 1614727829,
            "auth_time":1614727829,
            "nonce":"",
            "acr":"",
            "amr":"",
            "azp":"",
            "prn":"mailto:mike@example.com",
            "subjectID": "HEALTHELINK",
            "subjectOrganization": "Healthelink RHIO",
            "SubjectRole": "{\"code\"\";\"256950004\",\"codeSystem\":\"2.16.840.1.113883.3.18.6.1.16\"}",
            "PurposeOfUse": "{\"TREATMENT\":\"\",\"codeSystem\":\"\"}",
            "resourceID": "\fcs",
            "name": "HEALTHELINK",
            "targetSystem": "GRRHIO",
            "nbf":1300815780
        }
        let privateKey = fs.readFileSync(config.privateKey);
        jwt.sign(payload, privateKey,config.jwtOption, (err, token) => {
            if(callback){
                callback(err,token)
            }
        });
    }
    //---------------------------------------------------------------------------------------
    module.authorize = (req,res)=>{ // openid authentication request params
        // https://openid.net/specs/openid-connect-core-1_0.html specification
        let params = getLoginParams(req)
        //required
        let scope = params.scope  // types are profile,email,address,phone
        let response_type = params.response_type
        let client_id = params.client_id
        let redirect_on = params.redirect_on
        //recomended
        let state = params.state
        // optional
        let response_mode = params.response_mode
        let nonce = params.nonce
        let display = params.display
        let prompt = params.prompt
        let max_age = params.max_age
        let ui_locales = params.ui_locales
        let id_token_hint = params.id_token_hint
        let login_hint = params.login_hint
        let act_values = params.act_values
        //


     /* example send
        GET /authorize?
        response_type=code
        &scope=openid%20profile%20email
        &client_id=s6BhdRkqt3
        &state=af0ifjsldkj
        &redirect_uri=https%3A%2F%2Fclient.example.org%2Fcb HTTP/1.1
      Host: server.example.com
*/
    }
    //-----------------------------------------------------------------------------------------
    module.gettoken = (req,res) =>{
        let params = getParams(req)
       // console.log("jwt gettoken params = %j",params)
        gentoken(params,(error,token)=>{
            //console.log("\njwtauth gettoken params=%j\ntoken=%j\n",params,token)
            res.send(token)
        })
    }
    //---------------------------------------------------------------------------------------
    module.saveToken = (userid,token,callback) => {    
        login.saveToken(userid,token,(err,result) =>{  // {tokenid:token}
            //console.log(err||result)
        })
    }
    //-----------------------------------------------------------------------------------------
    let decode = module.decode = (token,option, callback) =>{
        //let decoded = jwt.decode(token,{complete: true}) 
        if( isEmpty(option)) {
            option = {complete: true}
        }       
        let decoded = jwt.decode(token,option) 
        if(callback) {
            callback("",decoded)
        } else {
            return decoded
        }
    }
    //---------------------------------------------------------------------------------------
    let getQEcert = (token)=>{
        let payload = decode(token).payload
        let cert = config[payload.sub+"_jwtcert" ]
        let secret = fs.readFileSync(cert); 
        return secret
    }
    //-----------------------------------------------------------------------------------------
    let verify = module.verify = (token, secret,options, callback) =>{
        if(isEmpty(secret)){
            secret = getQEcert(token)
            //secret = fs.readFileSync(config.publicKey); 
        }
        options = options || config.jwtVerifyOption
        options = {}
        
        //options = options || {issuer: 'SHINNYapi.org'}
        jwt.verify(token,secret,options,(err,decoded) => {
            if (err) {
                console.log(err)
            }
            if(callback) {
                callback(err,decoded)
            } else{
                return decoded
            }
        })
    }
    //-----------------------------------------------------------------------------------------
    module.xverify = (req,res) =>{
        let params = getParams(req)
        let token = params.token
        let secret = params.secret
        let options = params.options
      //  console.log("\n_verify params = %j\n",params)
        verify(token,secret,options,(error,decoded)=>{
            res.send({error,decoded})
        })
    }
    //-----------------------------------------------------------------------------------------
    module.getdecoded = (req,res) =>{
        let params = getParams(req)
        let token = params.token
        let option = params.option
        //console.log(token)
        decode(token,option,(err,decoded)=>{
            res.send(decoded)
        })
    }
    
    //-----------------------------------------------------------------------------------------
    let getParams = (req)=> {
        let userid = req.query.userid;
        let pwd = req.query.pwd;
        let secret = req.query.secret
        let token = req.query.token;
        let tokenid = req.query.tokenid;
        let qe = req.query.qe;
        let sub = req.query.sub;    
        let target = req.query.target;
        let payload = req.query.payload;
        let option = req.query.option;
        let options = req.query.options;
        let body = req.body
        let oauth = req.get("Authorization")
        let MAC = req.get("Authorization: MAC")
        if(isEmpty(token)){
            token = req.params.token
        }
        if(isEmpty(secret)){
            secret = req.params.secret
        }
        if(! isEmpty(oauth)){
            if(oauth.indexOf(" ") > -1) {
                token = oauth.split(" ")[1]     
            }
            if(oauth.indexOf(":") > -1) {
                token = oauth.split(":")[1]     
            }
        }    
        if (isEmpty( userid )){       
            userid=req.params.userid;
        }
        if (isEmpty( pwd )){       
            pwd=req.params.pwd;
        }
        if (isEmpty( secret )){       
            secret=req.params.secret;
        }
        if (isEmpty( token )){       
            token=req.params.token;
        }
        if (isEmpty( tokenid )){       
            tokenid=req.params.tokenid;
        }
        if (isEmpty( payload )){       
            payload=req.params.payload;
        }
        if (isEmpty( qe )){       
            qe=req.params.qe;
        }
        if (isEmpty( sub )){       
            sub=req.params.sub;
        }
        if (isEmpty( sub )){       
            sub=body.sub;
        }
        if (isEmpty( target )){       
            target=req.params.target;
        }
        if (isEmpty( target )){       
            target=body.target;
        }
        if (isEmpty( option )){       
            option=req.params.option;
        }
        if (isEmpty( options )){       
            options=req.params.options;
        }
        if (isEmpty( options )){       
            options={}
        }     
     
        if (isEmpty( userid )){       
            userid="";
        }
        if (isEmpty( pwd )){       
            pwd="";
        }
        if (isEmpty( secret )){       
            secret="";
        }
        if (isEmpty( token )){       
            token="";
        }
        if (isEmpty( tokenid )){       
            tokenid="";
        }
        if (isEmpty( payload )){       
            payload = `{ error:"bad payload"}`
        }
        if (isEmpty( sub )){       
            sub = `{ error:"bad Subject"}`
        }
        if (isEmpty( qe )){       
            qe = `{ error:"bad QE"}`
        }
        return {qe,sub,target,option,payload,userid,pwd,secret,token,tokenid,oauth};
    }
    //-----------------------------------------------------------------------
    return module
};