/*
 * GET users listing.
*/
var CJSON = require('circular-json');
var moment = require('moment'); // for date time
var dict = require('../dic/dict')();
var login = require('./login');
var logger = require('./logging');
var passwordHash = require('../auth/password-hash');
var email = require('../mail');
var moment = require('moment'); // for date time

let utils = require('../utils') ;
let config = utils.getConfig(true).config;

let tokenExpire = config.loginconfig.tokenExpire;
let pwdExpire = config.loginconfig.pwdExpire;
let tokenExpireFormat = config.loginconfig.tokenExpireFormat;
const isEmpty =  require('../utils').isEmpty;

if ( isEmpty(tokenExpire) ){
    tokenExpire = 14;
}
if ( isEmpty(pwdExpire) ){
    pwdExpire = 90*60*24;
}
if ( isEmpty(tokenExpireFormat) ){
    tokenExpireFormat  = 'YYYYMMDDHHmm';
}
//----------------------------------------------------------------------------------
function getParams(req){
    var type = req.query.type;
    var userid = req.query.userid;
    var id = req.query.id;
    var parent = req.query.parent;
    var ext = req.query.ext;  
    var notes = req.query.notes;  
    var cargo = req.query.cargo;  
    var theme = req.query.theme;  
    var lang = req.headers["accept-language"] ;
    //console.log("logging HEADERS:="+JSON.stringify(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( userid )){
        userid="system" ;
    }    
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){
        parent=lang ;
    }    
    if ( isEmpty( ext )){
        ext="" ;
    }     
    if ( isEmpty( cargo )){
        cargo="" ;
    }       
    if ( isEmpty( theme )){
        theme="" ;
    }       
    if ( isEmpty( notes )){
        notes="" ;
    }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang,theme:theme};// emulate multiple return values
}
//----------------------------------------------------------------------------------
exports.disable = (req,res) => { // 
    let data = req.body;
    let userid = data.userid;    
    var tokenid = data.tokenid;
    //console.log("adminuser.disable.qquery="+JSON.stringify(req.query));
    dict.getUSER(userid,function(err,cargo){
        var token = JSON.parse(cargo);
        token.active = "0"; 
        token.valid = 0; 
        token.pwdReset = "1"; // need to reset password
        login.saveUser(token,function(err,data){    
            res.send("OK "+JSON.stringify(data));
        });        
    });
};
//----------------------------------------------------------------------------------
exports.update = function(req,res){ // 
    let data = req.body;
    if (typeof req.body == 'string') {
        data = JSON.parse(req.body);
    } 
    let userid = data.userid;
    dict.getUSER(userid,function(err,result) {
            let token = result
            if(typeof token == 'string'){
                token = JSON.parse(result)
            }
            token.userid = data.userid;
            token.fname = data.fname;
            token.lname = data.lname;
            token.email = data.email;
            token.phone = data.phone;
            token.group = data.group;
            token.roles = data.roles;            
            token.active = data.active; 
            token.pwdReset = data.pwdReset; // need to reset password
            token.pwdExpire = data.pwdExpire ;
            token['use2Factor'] = data.use2Factor;
            token.tokenid=""
            token.msg = data.msg;
            //console.log("\n\n\n\nget token data =%j\n\n\ntoken=%j\n\n\n",data,token)
            dict.saveUSER(token,function(err,resdata){ 

            });        
    });
    res.send("OK")
};
//----------------------------------------------------------------------------------
exports.pwdreset = function(req,res){ //    
    let data = req.body;
    if (typeof data == 'string') {
        data = JSON.parse(data);
    }
    let userid = data.userid;
    let tokenid = data.tokenid;
    let password = login.genPwd(8);
    //console.log("adminuser.pwdreset data="+JSON.stringify(data));
    dict.getUSER(userid,(err,cargo) => {
       //console.log("adminuser.pwdreset old token="+cargo);
        let token = JSON.parse(cargo);
        passwordHash(password).hash( (error, hash,salt,key) => {
            token.salt = salt;
            token.hash = hash;
            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
            token.valid = 1; // activate
            token.active = 1; // activate
            token.pwdReset = 1 ; // need to reset password
            token.password = password;
            dict.saveUSER(token,(err,data) => {    
                dict.savePWDHistory(token,(err,result) => {
                    email_pwdReset(token);
                    res.send("OK "+JSON.stringify(data));
                });    
            });                                
        });
    });
};
//---------------------------------------------------------------------------------
let newuser = exports.newuser = (token,callback)=>{
    login.newuser(token,(err,data)=>{
        if(callback){
            callback(err,data) ;
        }

    })
    /*
    if (isEmpty(token.password)) {
        token.password = login.genPwd(8);
    }
    passwordHash(token.password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        login.saveUser(token,function(err,data){});
    });
    */
}

//------------------------------------------------------------------------------
exports.register = function(req,res){
    let data = req.body;
    let token = login.newToken();
    let password = login.generatePassword(8);

    console.log("register  = %j",data)
    if (typeof data == 'string') {
        data = JSON.parse(data);
    }
    console.log("Registrer user = "+JSON.stringify(data));
    token.userid = data.userid;
    token.fname = data.fname;
    token.lname = data.fname;
    token.phone = data.phone;
    token.email = data.email;
    token.group = data.group;
    passwordHash(password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "1"; // activate
        token.pwdReset ="1" ; // need to reset password
        //console.log("login.register="+token);
        dict.saveUSER(token,function(err,data){         
            console.log("adminuser.saveuser=%j",data);    
            dict.savePWDHistory(token);    
            // send email
            let msg = 'you have been registered as a new user on the Project Manager Dashboard \n'+
                      '\nTemporary password is ' + password+' for userid '+token.userid +
                      '\nPlease connect to the link 192.168.160.131:45001 and login in.\n'+
                      'You will need to change your password at the first login' ;
             let email_config = {
                text:    msg ,
                from:    "pm_admin@nyecemail.com", 
                to:      `${token.email}`,
                cc:      "dpal@nyehealth.org",
                subject: "PM Dashboard Registration"
            }
       	    //console.log("EMAIL to "+token.email+"\n MSG="+msg);
            dict.set("EMAIL","PM_DASHBOARDREG","PM_DASBOARD","en-US",email_config,function(err,data){

            });                          
            email.send(email_config, 
                function(err, message)  { 
                    console.log("err="+err);
                    console.log("message="+JSON.stringify(message));
                }
            );
           
        });
    }); 
    res.send("OK"+token.tokenid);
}

//----------------------------------------------------------------------------------
function _save(id,parent,cargo,notes,callback){ // parent= type LOG INFO DEBUG ERROR etc
    var ext = "";
    var type="USER" ; 
    if ( isEmpty( parent )){ 
        parent='USERLOGIN' ; 
    } ;
    if ( isEmpty( id )){ 
        id ='admin' ; 
    } ;
    if ( isEmpty( notes)){ 
        notes ='' ; 
    } ;
    if ( isEmpty( cargo)){ 
        cargo ='' ; 
    } ;
    dict.set(type,id,parent,ext,cargo,notes,function(data){
         
    }) ;
}
//---------------------------------------------------
exports.user = function(req, res){
    var userid = req.params.userid;
    dict.ISUSER(userid,function(data,err){
        res.send(data);
    });
};
//---------------------------------------------------
exports.list = function(req, res){
    dict.getUSERS(function(err,data){
        //console.log("ADMIN users List " + JSON.stringify(data));
        if (typeof data == 'object') {
            data.forEach(function(obj,i){
                if (typeof data[i].cargo == 'string'){
                    user = JSON.parse(data[i].cargo) ;
                    data[i].lastAccess = user.lastAccess ;                
                    data[i].roles = user.roles ;
                    data[i].expires = user.expires ;
                    data[i].use2Factor = user.use2Factor;
                    data[i].group = user.group ;
                    data[i].lname = user.lname ;
                    data[i].fname = user.fname ;        
                    data[i].email = user.email ;        
                    data[i].phone = user.phone ;        
                    data[i].active = user.active ;        
                    data[i].userid = user.userid ;        
                    data[i].lastAccess = user.lastAccess ;                        
                    data[i].pwdReset = user.pwdReset ;
                    data[i].pwdExpire = user.pwdExpire ;
                    data[i].msg = user.msg ;
                }
            });    
        }
        res.send(data);
    });
};
//---------------------------------------------------
let usersids = exports.usersids = function(callback){
    dict.getUSERS(function(err,data){
        //console.log("ADMIN users List " + JSON.stringify(data));
        let aList = [];
        if (typeof data == 'object') {
            data.forEach(function(obj,i){
                if (typeof data[i].cargo == 'string'){
                    user = JSON.parse(data[i].cargo) ;
                    aList.push(user.userid) ;
                }
            });    
        }
        callback(aList)
    });
};
//---------------------------------------------------
exports.useridlist = function(req, res){
    usersids((list)=>{
        res.send(list)
    })
};
//---------------------------------------------
exports.htmllist = function(req, res){
    let tokenid = req.params.tokenid ;
    let type = req.params.type;
    if (isEmpty(tokenid)){
        tokenid = req.query.tokenid ;
    }
    if (isEmpty(type)){
        type = req.query.type ;
    }
    if (isEmpty( type )){       
         type="JSON";
    } 
    login.isRole(tokenid,'PM_ADMIN',function(result){
        if ( result) {
             adminuser_columns(function(data){
                 res.render('adminusers',{page_title:"PP admin user config setup",colsetup:data});      
             });   
        } else {
             res.send("Access Denied");                                   
        }
    }); 
 };
//---------------------------------------------
exports.sendmessage = function(req, res){
    let io = req.app.get("socket.io")
    let data = req.body;
    let users = data.users ; // should be an array of user ids
    let parent = data.parent ;
    let msg = data.msg ;
    if(isEmpty(parent) ){
        parent = "PM_MESSAGE";
    }
    if(isEmpty(msg)){
        msg = "no message";
    }
    if(! isEmpty(users)){
        if(typeof users == 'string'){
            users = users.split(",")
        }
        users.forEach((userid) => {
            let pipemsg = `${parent}${userid}`
           // console.log("send msg to %s, %s via pipe %s",userid,msg,pipemsg)
            io.emit(pipemsg,msg)          
        });           
        dict.savemessage(users,parent,msg,(err,data)=>{           
        })             
        res.send("OK")

    } else{
        res.send("No Users")
    }
 };
//---------------------------------------------
exports.sendnote = function(req, res){
    let io = req.app.get("socket.io")
    let data = req.body;
    let parent = data.parent ;
    let userid = data.userid ;
    let msg = data.msg ;
    if(isEmpty(parent) ){
        parent = "PM_NOTIFICATION";
    }
    if(isEmpty(msg)){
        msg = "no message";
    }
    let pipemsg = `${parent}`
    //console.log("send msg %s via pipe %s",msg,pipemsg)
    io.emit(pipemsg,msg)          
    dict.savemessage(userid,parent,msg,(err,data)=>{           
    })             
    res.send('OK')
};
 //---------------------------------------------
 exports.getmessage = function(req, res){
    let parent = req.query.parent;
    let tokenid= req.query.tokenid;
    let userid = ""
    let io = req.app.get("socket.io")
    if(isEmpty(parent) ){
        parent = "PM_MESSAGE";
    }    
    dict.getToken(tokenid,function(err,data){
        if( isEmpty(err) &&  ! isEmpty(data) ){
            var token = JSON.parse(data)
            userid = token.userid
        } 
        dict.getmessage(userid,parent,(err,data)=>{
            res.send(data)
        })             
    });
 };
 //---------------------------------------------------
adminuser_columns = function(callback) {
    // dict.get("JSON","USERADMINCOLUMNS","JQGRID","",(err,data)=> {
    //     if (  isEmpty(data)) {
    //let themelist = require('../utils').UIThemeList('',4);
    //console.log(themelist)
             var colsetup = { 
                 url:'/adminusers/list', 
                 editurl:'/adminuser/update',
                 altRows: false, 
                 deepempty: true,
                 autowidth: true,
                 ignoreCase: true,
                 datatype: "json",
                 mtype: "GET",       
                 height:'auto',
                 width:'auto',
                 hidegrid: false,
                 multiselect:false,
                 colModel:[
                     {label:'id',name:'userid', editable:true , width:115, editoptions:{size:26,readonly: "readonly"},formoptions:{label:'User ID'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                     {label:'active',name:'active', search:false,editable:true , width:45,formoptions:{label:'Active'}, formatoptions: { disabled: false}, edittype: "checkbox", editoptions: {value: "1:0"} },
                     {label:'pwd Reset',name:'pwdReset', search:false,editable:true , width:65,formoptions:{label:'Force Pwd Reset'}, formatoptions: { disabled: false}, edittype: "checkbox", editoptions: {value: "1:0"} },
                     {label:'pwd expires',name:'pwdExpire',search:false, editable:true ,width:85,editoptions: {size:20, dataInit: function(el) { $(el).datepicker({ controlType: 'select', dateFormat: "yyyymmdd" } ) } }, formoptions:{label:'Password Expiry'} },
                     {label:'Use Two Factor',name:'use2Factor', search:false,editable:true , width:65,formoptions:{label:'Use Two Factor Auth'}, formatoptions: { disabled: false}, edittype: "checkbox", editoptions: {value: "1:0"} },
                     {label:'fname',name:'fname', editable:true , width:115, editoptions:{size:26},formoptions:{label:'First Name'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                     {label:'lname',name:'lname', editable:true , width:115, editoptions:{size:26},formoptions:{label:'Last Name'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                     {label:'email',name:'email', editable:true ,width:125,editoptions:{size:26},formoptions:{label:'Email'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     {label:'phone',name:'phone', editable:true ,width:123,editoptions:{size:26},formoptions:{label:'Phone'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     {label:'group',name:'group',search:false, editable:true ,width:85,editoptions:{size:26},formoptions:{label:'group'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     {label:'roles',name:'roles',search:false, editable:true,hidden: true,editrules: { required: true,edithidden:true} ,width:185,edittype:"textarea",editoptions:{size:285,rows:"1",cols:"20"},formoptions:{label:'roles'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     {label:'last Access',name:'lastAccess', search:false,editable:false ,width:85,editoptions:{size:26},formoptions:{label:'Last Access'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     {label:'last MSG',name:'msg', search:false,editable:true ,width:85,editoptions:{size:26},formoptions:{label:'Last Message'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                     //{label:'notes',name:'notes',index:'notes', editable:true, hidden: true,,width:450, edittype:"textarea", editoptions:{size:400,rows:"12",cols:"100"},sortable:false,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                     {label:'', width:1,search:false}    
                 ],
                 pagerpos:"right",
                 loadonce:true,
                 toppager:true,
                 rowNum: 25,
                 rowTotal:5000,
                 viewrecords: true,
                 gridview: true,
                 autoencode: false,            
                 caption: ""          
             }
             dict.set("JSON","USERADMINCOLUMNS","JQGRID","",colsetup);
             callback( colsetup);
    //     } else {
    //         callback(JSON.parse(data)); 
    //     }       
    // });      
 };
//---------------------------------------------------
userpicklist_columns = function(callback) {
    //dict.get("JSON","USERPICKLISTCOLUMNS","JQGRID","",(err,data)=> {
    //    if (  isEmpty(data)) {
    //let themelist = require('../utils').UIThemeList('',4);
    //console.log(themelist)
            var colsetup = { 
                url:'/adminusers/list', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                hidegrid: false,
                multiselect:false, // show check boxes
                colModel:[
                    {label:'id',   name:'userid', editable:false , width:85, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'fname',name:'fname' , editable:false , width:85, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'lname',name:'lname' , editable:false , width:85, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'email',name:'email' , editable:false , width:85, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'phone',name:'phone' , editable:false , width:85, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} }
                ],
                pagerpos:"right",
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: false,            
                caption: ""          
            }
       //     dict.set("JSON","USERPICKLISTCOLUMNS","JQGRID","",colsetup);
            callback( colsetup);
     //   } else {
   //         callback(JSON.parse(data)); 
    //    }       
    //});      
 };
  //---------------------------------------------
exports.userpicklist = function(req, res){
    let tokenid = req.params.tokenid ;
    if (isEmpty(tokenid)){
        tokenid = req.query.tokenid ;
    }
    //login.authorised(tokenid,['PP_USERSVIEW'],function(data,roles){
   //     if ( data) {
            userpicklist_columns(function(data){
                 res.render('userpicklist',{colsetup:data,page_title:"Pick list"});      
             });   
    //    } else {
    //         res.send("Access Denied");                                   
    //    }
    //}); 
 };
//-----------------------------------------------------------  
let email_pwdReset = function(token) {
  let oMail = { 
        text:   
 `
 ${token.fname} ${token.lname}

 Your password has been reset for the PM Dashboard

             user id:  ${token.userid}
  temporary password:  ${token.password}
                url : http://192.168.160.131:45100


Please Change your password at next login.`
,
        from:    "pm_admin@nyecemail.com", 
        to:      token.email,
        cc:      "",
        subject: "PM Dashboard Password Reset"
    };
    email.sendEmail(token,'PMDASHBOARD_PWDRESET',oMail);
    return 0;
};
