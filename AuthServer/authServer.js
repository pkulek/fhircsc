

const config = require('./config.json')
//const sql = require(config.db);
const sql = require("mssql");
const utils = require('./src/utils')
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');
const app = express();
const fs = require('fs')
const cors = require('cors')
const SOCKETIO = false
const HTTPS = config.useHTTPS;
//-------------------------------------------------------
app.set('port', process.env.PORT || config.port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }))
app.use(express.json());
//app.use(bodyParser.urlencoded({ extended: true})); //if thid is mossing no body data in post
//app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));


app.all('/*', (req, res, next)=> {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});
/*
var server = http.createServer(app,(req,res)=>{})
server.listen(app.get('port'), function(){
    console.log("FHIR Auth server on port %s",app.get('port'))
    let db = config[config.db+"config"+"_"+config.devEnv]
    sql.connect(db, err=>{
        console.log("sql server %s connected %s",JSON.stringify(db),err?err:"OK")
    })        
});
*/

let connectsql = (callback)=>{
  
    let db = config[config.db+"config"+"_"+config.devEnv]
    sql.connect(db, err=>{
        callback(db,err)
    })   
}

let startHTTP=(port) =>{
    const http = require('http');
    port = port || config.httpPort
    httpServer = http.createServer(app,(req,res)=>{})
    httpServer.listen(port,()=>{
        console.log("\n\nHTTP - "+ config.app+" Version "+config.version+" listening on port: "+port);
        let db = config[config.db+"config"+"_"+config.devEnv]
        connectsql( (db,err)=>{
            console.log("sql server %s connected %s",JSON.stringify(db),err?err:"OK")
        })    
       // startHTTPS()
    });
}
let startHTTPS = (port) =>{
    const fs = require('fs')
    const https = require('https');
    port = port || config.httpsPort
    var https_options = {
        key: fs.readFileSync(config.privateKey),
        cert: fs.readFileSync(config.publicKey), 
        ca:fs.readFileSync(config.qeca),
    };
        server=    https.createServer(https_options,app).listen(port,()=>{
        console.log(`\n\nHTTPS - ${config.app} Version ${config.version} listening on port: ${port} ` );
        require("public-ip").v4().then(ip => {
            console.log("\nPublic ip address", ip);
        });   
    });
}
// if(HTTPS) {
   // startHTTPS()
//} else {
    startHTTP()
//}


if(SOCKETIO){
   
    let io = require('socket.io')(http);
  
    app.set('socket.io', io);      
    console.log("Using Socket IO")
    let listener = io.listen(httpServer);  
    listener.on('connection',(socket) => {  
        socket.emit('messages', 'Hello from server');
        console.log('IO Client connected...',CJSON.stringify(socket.id));   
        socket.on('LOGIN_MSG',  (data)=> {
            console.log("LOGIN_MSG data=",data);
           
        });                
        socket.on('CONFIG_MSG', (data)=>{  
            console.log(data)
        });   
    })

 
/*
    io.on('connection', (client) => {  
        console.log('IO Client connected...',CJSON.stringify(client.id));    
        client.on('join', function(data) {
            console.log(data);
            //client.emit('messages', 'Hello from server');
        });
    }); 
*/
}
//----------------------------------------------------------
let login = require('./src/auth/login')(config);
let jwtauth = require('./src/auth/jwtauth')(config);
let routes = require('./src/routes');
app.get('/', routes.index); 

app.get('/jwt/token',jwtauth.gettoken)
app.get('/jwt/token/:sub',jwtauth.gettoken) 
app.get('/jwt/token/:sub/:target',jwtauth.gettoken)
app.post('/jwt/token',jwtauth.gettoken)

app.get('/jwt/decode',jwtauth.getdecoded)
app.get('/jwt/decode/:token',jwtauth.getdecoded)

app.get('/jwt/verify',jwtauth.xverify)


app.get('/authorize',jwtauth.authorize);

app.post('/authorize',jwtauth.authorize);

//-----------------------------------------------------------

app.get('/register/code/:userid',login.registerCode)
app.get('/register/token/:registerCode',login.registerToken)

app.get('/token/verify/:tokenid',login.verifyToken);
app.get('/token/token/:tokenid',login.Token);

app.post('/admin/register/user',login.register); 
app.get('/admin/register/html',login.htmlregister);
app.get('/admin/html',login.htmladmin);
app.get('/admin/userlist',login.userlist);
app.get('/admin/userjwt/:userid',login.userjwt);
app.get('/admin/userjwt',login.userjwt);
app.post('/admin/update',login.update);

app.get('/admin/approve/:userid',login.approve);
app.get('/admin/disable/:userid',login.disable);
app.get('/admin/passwordchange/:userid',login.passwordChange);

app.get('/slogin/:registerCode',login.login);
app.post('/slogin/:registerCode',login.login);

app.get('/login',login.login);
app.get('/login/:userid',login.login);
app.get('/login/:userid/:password',login.login);
app.get('/login/:userid/:password/:source',login.login);



app.post('/login',login.login);
app.get('/login/:userid/:password/:source',login.login);


app.get('/login/html',  login.htmllogin)
app.get('/login/verifyASA/"tokenid/:userid/:code',  login.verifyASA)
app.post('/login/verifyASA',  login.verifyASA)

app.get('/logout',login.logout);
app.get('/logout/:tokenid',login.logout);
app.get('/access/verify',login.verifyAccess)
app.get('/access/verify/:tokenid',login.verifyAccess)
app.get('/access/verify/:tokenid/:role',login.verifyAccess)

app.post('/userpassword/reset/:userid',login.resetpwd);
app.get('/userpassword/reset/:userid',login.resetpwd);
app.post('/userpassword/forgot',login.forgotPassword);
app.post('/userpassword/change',login.changepwd);

app.get('/userid/:tokenid',login.tokenidUserid);


 //get config stuff
 let fhir = require('./src/routes/admin_manager');
 app.get('/config/edit',fhir.editConfig)
 app.post('/config/save',fhir.saveConfig)
 app.get('/config/roles',fhir.loadROLES)
 app.get('/config/source',fhir.loadSOURCE)
 app.get('/fhir/config/',fhir.getConfig)
 app.get('/fhir/config/:key',fhir.getConfig)
//---------------------------------------------------------------------------
// EXIT cleanup if needed
process.stdin.resume();//so the program will not close instantly
function exitHandler(options, err) {
    if (options.cleanup) {
        console.log('clean up');
    }
    if (err) console.log("App Error %s",err.stack);
    if (options.exit) process.exit();
}

//do something when app is closing
process.on('beforeExit', exitHandler.bind(null, {exit:true}));
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
 

