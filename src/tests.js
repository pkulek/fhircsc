
require('console-stamp')(console, 'yyyymmddHHmmss.l');
const DEBUG = false ;
const SAVETEST = true;
const sql = require("mssql")
const CJSON = require("circular-json");
const fs = require('fs')
const utils = require('./utils') ;
const config = require('../config.json');
const clone =  utils.clone;
const moment = require('moment'); // for date time
const fhir = require('./routes/fhir_manager.js')
const sprl =  require("./routes/sprl_manager")
const Client = require('node-rest-client').Client;
const isEmpty = utils.isEmpty
//---------------------------------------------------------------------------
let runQry =  (qry,callback) => {    
    new sql.Request().query(qry, (err, result) => {
        if(err) {
           console.log(err)
        }
        if(callback){
            if(err) {
                callback(err,[] )
            } else {
                callback(err,result.recordsets[0])
            }
        }
    })    
}
//-------------------------------------------------------------------------------
exports.getTestResults = (req,res)=> {
    let params = fhir.getFHIRparams(req)
    let qry = 'select * from tests order by timestamp desc'
    runQry(qry,(err,result)=>{
        res.send(result)
    })
}  
//-------------------------------------------------------------------------------
exports.TestResults = (req,res)=> {
    let params = fhir.getFHIRparams(req)
    res.render("tests",{params});      
}  
//-------------------------------------------------------------------------------
let runtest = (params,list,index,callbck)=>{
    if(index < list.length){
        let data = list[index]
        let request = data.request
        let mpiid = data.mpi
        let category = params.category
        let targetqe = params.targetqe
        let reqqe = params.reqqe
        let code =params.code
        let date=params.date
        let ourl = fhir.getFHIRurl(params,request )   
        console.log("ourl= %j \n  url= %s\nhost = %s  %s",ourl,req.url,req.host,req.path)
        if(! isEmpty(ourl)) {
            sprl.token(ourl.reqqe,(token)=>{        
                let cl = new Client(clientoptions)    
                let args = {
                    headers:{ "Authorization":"Bearer "+token,
                              "Content-Type":"application/fhir+json; charset=UTF-8",
                            } // request headers            
                };
                console.log(ourl.url)
                cl.get(ourl.url, args,(data, response) => {      
                    let result = {}
                    let status = response.statusCode
                    if(status == 200){
                        result = parseresult(data)    
                    } else {
                        result = {status}
                    }
                    console.log("Patient Search code = %s msg = %s",response.statusCode, STATUS_CODES[response.statusCode] )
                    if(SAVETEST) {
                        let out = {"sprlurl":req.headers.host+req.url,"url":ourl,"result":result,"status":status +" - "+ STATUS_CODES[response.statusCode],"error":""}
                        inserttest('patient',params,ourl,out)
                    }
                    res.send(parseresult(data))
                }).on('error',(error)=>{
                    if(SAVETEST) {
                        let out = {"sprlurl":req.headers.host+req.url,"url":ourl,"result":JSON.parse(CJSON.stringify(error).toString("utf-8").replace(/'/g,'')) ,"status":"Error","error":error.code}
                        inserttest('patient',params,ourl,out)
                    }
                    res.send(CJSON.stringify(error))
                    console.log("error=%s",CJSON.stringify(error.code))
                })
                //*/       
            }) 
        }
    } else {
        callback("finished")
    }
}
//-----------------------------------------------------------------------------
exports.runtests = (req,res)=>{
    let params = fhir.getFHIRParams(req)
}
//-------------------------------------------------------------------------------
exports.getTestrunResults = (req,res)=> {
    let params = fhir.getFHIRParams(req)
    let qry = 'select * from testrun '
    runQry(qry,(err,result)=>{
        res.send(result)
    })
}  
//-------------------------------------------------------------------------------
exports.TestrunResults = (req,res)=> {
    let params = fhir.getFHIRparams(req)
    res.render("testrun",{params});      
}  



