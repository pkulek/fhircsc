
/*
let REDIS_PORT = 6379 ;
let PROD_HOST = '192.168.180.141' ;
let STAGE_HOST = '192.168.160.141' ; 
let REDIS_OPTIONS =  {no_ready_check: true};
let  REDIS_HOST = eval(devEnv+'_HOST');
*/
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let redis = require("redis");
let utils = require('../utils') ;
let moment = require('moment'); // for date time
let connected = false;
let isEmpty =  utils.isEmpty; 
let _config = ""; 
let _module= {};
module.exports = function (config) {
  if (isEmpty(_module)) {
        let module = {};
        //----------------------------------------------------------------------------------
        if (! isEmpty(config) && isEmpty(_config)) {
            _config = config.dict.redis
        }   
        if (! isEmpty(config)) {
            if(isEmpty(_config)){
                _config = config
            }
        } else {
            if(isEmpty(_config)){
               _config =  utils.getConfig().config;
            }        
        }
        if(isEmpty(_config) || isEmpty(_config.redis)) {
            _config = { 
                    "dba":"redis",
                    "redis":{"host":"192.168.160.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}}
                    }                                 
        };
        console.log("Redis Config\n %j",_config.redis)

        //       "redis":{"host":"192.168.180.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
        module.connected = ()=>{
            return connected
        }
        let redisconf = _config.redis;
        try {
            module.DB0 = redis.createClient(redisconf.port, redisconf.host, redisconf.options);           
            module.DB0.auth(redisconf.auth, function (err) {
                if (err) {
                    connected = false ;
                    console.log("REDIS NOT Connected ");
                    console.error( err);
                } else {
                    console.log("REDIS Connected DBO");
                    connected = true ;
                }
            });
            module.DB1 = redis.createClient(redisconf.port, redisconf.host, redisconf.options);           
            module.DB1.auth(redisconf.auth, function (err) {
                if (err) {
                    connected = false ;
                    console.log("REDIS NOT Connected DB1");
                    console.error( err);
                } else {
                    console.log("REDIS Connected DB1");
                    connected = true ;
                }
            });
            /*            
            module.DB2 = redis.createClient(redisconf.port, redisconf.host, redisconf.options);           
            module.DB2.auth(redisconf.auth, function (err) {
                if (err) {
                    connected = false ;
                    console.error( err);
                } else {
                    connected = true ;
                }
            });
            module.DB3 = redis.createClient(redisconf.port, redisconf.host, redisconf.options);           
            module.DB3.auth(redisconf.auth, function (err) {
                if (err) {
                    connected = false ;
                    console.error( err);
                } else {
                    connected = true ;
                }
            });
            */
            module.DB0.select(0,function (){})
            module.DB1.select(1,function (){})
            //module.DB2.select(2,function (){})
            //module.DB3.select(3,function (){})
        } catch(Ex){
            console.error(Ex);
        }
        _module = module;
    }
    return _module
}

