
let fs = require('fs')
let select = require('xpath.js') ;
let dom = require('xmldom').DOMParser ;
let utils = require('./utils') ;
const isEmpty =  utils.isEmpty;


//---------------------------------------------------
exports.CCDtoJSON = function(xml,callback){
    let counts = {}
    if ( ! isEmpty(xml) ) {
        let xmldoc = new dom().parseFromString(xml,"application") ;   
        let sections = select(xmldoc, "//*[local-name()='structuredBody']/*[local-name()='component']/*[local-name()='section']");
        if (sections.length > 0) {
            sections.forEach(function(element, index, theArray) {
                    let section = new dom().parseFromString('<root>'+element.toString()+'</root>') ;                                
                    // get substances
                    let entries = select(section,"//*[local-name()='code']")
                    if(entries.length > 0){
                        entries.forEach(function(elem,i){
                            let codes = elem.toString().split(' ')
                            codes.shift()
                            codes.pop()
                            codes.toString().replace(/'/g,'').split(',')
                            let obj = {}
                            codes.forEach(function(elem,i){
                                obj[ elem.split('=')[0] ] = elem.replace(/"/g,'').split('=')[1]
                            })
                            if ( isEmpty(counts[obj.code] ) ) {
                                obj.count = 1
                                counts[obj.code] = obj
                            } else {
                                //obj.count += 1
                                counts[obj.code].count += 1
                            }                           
                        })
                    }
                    //let typeCode = select(entry, "//*[local-name()='typeCode']/text()").toString();   
                    fs.writeFile("/tmp/CCDTOJSON_entry"+index,'<root>'+entries.toString()+'</root>' , function(err) {
                        if(err) { console.log(err); }
                    }); 
            });
        }
        console.log(JSON.stringify(counts,null,4))
        fs.writeFile("/tmp/CCDTOJSON_",sections.toString() , function(err) {
            if(err) { console.log(err); }
        }); 
    }
}