
let utils = require('./utils')
let dict = require('./dic/dict');
let email = require('./mail');
let CJSON = require('circular-json');
let mysql = require('mysql')
let fs = require('fs');
let sshExec = require('ssh-exec');
let config = utils.getConfig().config;
let os = require('os');
let nrc = require('node-run-cmd');
let ISWIN = os.platform() == "win32" // shows win32 even for 64 bit
let isEmpty = utils.isEmpty ;
let sshConfig1 = {
        user: 'archive01\\qecerts',
        host: '192.168.170.25',
        password: '5J[a)e:}Es'
    }; 

let sshConfig = {
        user: 'pkulek',
        host: '192.168.160.131',
        password: 'EndorKroner#1'
    };

let cmd1 = 'cd /opt/ppadmin ; find ./ -maxdepth 3 -type f -iname "*.md" -printf "%h,%f,%CY-%Cm-%Cd %CT,%s,%u,%M\n"'

let cmd2 = ` find /opt/data -maxdepth 3 -type f -iname "*.pem" -printf '{ "path":"%h","file":"%f","date":"%CY-%Cm-%Cd" },'`
//let cmd2 = ` find /opt/ppadmin -maxdepth 3 -type f -iname "*.*" -printf "{ path:'%h',file:'%f',date:'%CY-%Cm-%Cd' },"`
let cmd3 = `cd /opt/ppadmin/data ; cat sPRLStageConfig.xml `

let cert = 'ca-cert.pem'
let cmd4 = `cd /opt/data ; openssl x509 -in ${cert} -noout -text `
const USEDICT = false;
//----------------------------------------------------------------------------------
exports.testxml = (req,res)=>{
    let cmd = `xcopy "\\\\192.168.170.25\\d$\\QECERTS" "\\\\192.168.180.55\\QE_Certs\\download" /c /s /e /y /z /f` ;
    let cmd1 = `cscript listfiles.vbs //Nologo` ;
    let nrc = require('node-run-cmd');
    nrc.run(cmd1, { 
        onData: (data) => {
           res.write(data);            
        } ,
        onDone:(data)=>{
            res.end()
        },
        onError: (data)=>{
           //res.write(data);
        }
    });
}
//----------------------------------------------------------------------------------
let extractIncludes = (template) =>{
    let out = []
    let a =  utils.ejsTags(template);
    a.forEach((o)=>{
        if (o.token.indexOf("include",0) !== -1 ){
            out.push(o)
        }
    })
    return out;
} 
//----------------------------------------------------------------------------------
let replaceIncludes = (template,callback) =>{
    let count = 0;
    let out = template;
    let olist = extractIncludes(template) ;
    let alen = olist.length;
    olist.forEach((o)=>{
        // get from dict
        dict.get_ejsTemplate(o.name.replace(".ejs",""),"",(err,templ)=>{
            count ++;
            if (! isEmpty(templ)) {
                out = out.replace(o.token,templ)
            }
            if( count == alen) {
                if (callback){
                    callback( isEmpty(out)?template:out );
                }
            }
        });
    })
} 
//----------------------------------------------------------------------------------
exports.configTHTML = (req,res) => {
    let qe = req.query.qe;
    if (USEDICT) {
        dict.get_ejsTemplate('sprlConfig1',req.app.get('views'),(err,template)=>{
            if(! isEmpty(template) ){  // array of templates
                let ejs = require('ejs');                  
                replaceIncludes(template,(out)=>{
                    res.send(ejs.render(out,{filename:"sprlConfig1.ejs",page_title:"SPRL admin ",qe:qe}));
                });
            } else {
                res.render('sprlConfig1',{page_title:"SPRL admin ",qe:qe});                       
            }     
        })
    } else {
        res.render('sprlConfig1',{page_title:"SPRL admin",qe:qe });                        
    }
}
//----------------------------------------------------------------------------------
exports.admin = (req,res) => {
    let qe = req.query.qe;
    if (USEDICT) {
        dict.get_ejsTemplate('sprlConfig',req.app.get('views'),(err,template)=>{
            if(! isEmpty(template) ){  // array of templates
                let ejs = require('ejs');                  
                replaceIncludes(template,(out)=>{
                    res.send(ejs.render(out,{filename:"sprlConfig.ejs",page_title:"SPRL admin ",qe:qe}));
                });
            } else {
                res.render('sprlConfig',{page_title:"SPRL admin ",qe:qe});                       
            }     
        })
    } else {
        res.render('sprlConfig',{page_title:"SPRL admin",qe:qe });                        
    }
}
//----------------------------------------------------------------------------------
exports.certsHtml = (req,res) => {
    let file = req.query.file;
    let path = req.query.path;    
    let qe = req.query.qe;
    if (isEmpty(qe)){
        qe = "";
    };
    if (USEDICT) {
        dict.get_ejsTemplate('sprlCerts',req.app.get('views'),(err,template)=>{
            if(! isEmpty(template) ){  // array of templates
                let ejs = require('ejs');            
                replaceIncludes(template,(out)=>{
                    res.send(ejs.render(out,{filename:"sprlCerts1.ejs",page_title:"QE certificates",qe:qe}));
                });
            } else {
                res.render('sprlCerts',{page_title:"QE certificates",qe:qe});                       
            }     
        })
    } else {   
        res.render('sprlCerts',{page_title:"QE certificates ",qe:qe});
    }                        
}
//---------------------------------------------------------------------------------
exports.readcert = (req,res)=>{
    let file = req.query.file;
    let path = req.query.path;      
    let type = req.query.type;
    if (isEmpty(type) ){
        type = 1 // 1 = cert, 2 = expiry dates, 3 = whole cert
    }          
    readCert(path,file,type,(err,data)=>{
        if (type == 2) {
            if (! err) {
                processCertDates(data,(out)=>{                                    
                    res.send(out);
                })
            } else {
                res.send(err);
            }
        } else {
             res.write(data) ;
             res.end();
        }
    });
}
//----------------------------------------------------------------------------------
exports.uithemes = (req,res)=>{   
    let path = req.query.path;      
    let type = req.query.type;
    if (isEmpty(path)) {
        path = __dirname+"/../public/themes/" ;
    }
    if (isEmpty(type)){
        type = 1;
    }
    res.send(utils.UIThemeList(path,type))
    //console.log(utils.UIThemeList(__dirname+"/../public/themes/",type))
}
//----------------------------------------------------------------------------------
let certfiles = (callback)=>{
    if(ISWIN ){
        certfilesWin((err,out)=>{
            callback(err,out);
        })
    } else {
        certfilesSSH((err,out)=>{
            callback(err,out)
        })
    }
}
//----------------------------------------------------------------------------------
let certfilesSSH = (callback)=>{
    let cmd = ` find /opt/data -maxdepth 3 -type f -iname "*.pem" -printf '{ "path":"%h","file":"%f","date":"%CY-%Cm-%Cd" },'`
    sshExec(cmd,sshConfig, (err, stdout, stderr) => {
        if(err) {
            //console.log(err, stdout,stderr)
            callback(err,[]);
        } else {        
            let count = 0;
            let out = '['+stdout.slice(0,-1)+']'
            let filelist = JSON.parse(out) ;
            let aOut = []
            if(filelist && filelist.length > 0 ){
                let slen = filelist.length;
                filelist.forEach(function(obj,i){
                    count ++;
                    //console.log("obj=%s",obj)
                    let a = obj.path.split("/");
                    obj.qe = a[3] ;            
                    obj.env = a[4] ;
                    aOut.push(obj);
                    if(slen == count){
                        callback(err,aOut);
                    }
                });                
            }
        }  
    })
}
//----------------------------------------------------------------------------------
let certfilesWin = (callback)=>{
    //let cmd = `xcopy "\\\\192.168.170.25\\d$\\QECERTS" "\\\\192.168.170.25\\q$\\Download" /c /s /e /y /z /f` ;
    let cmd = `cscript listfiles.vbs //Nologo` ;
    nrc.run(cmd, { 
        onData: (data) => {
            console.log("cerfiles data= %s",data)
           callback("",data);            
        } ,
        onError: (data)=>{
            console.log("error %s",data)
           callback("Error","[]");            
        }
    });
}
//----------------------------------------------------------------------------------
let alist = (rhio,aOut,callback) => {
    const PROCESSCERTS = false;
    if (isEmpty(aOut)){
        aOut = [];
    }
    if (isEmpty(rhio)) {
        rhio = "";
    }
    let count = 0;
    certfiles((err,filelist)=>{
        //console.log(eval(filelist));
        callback(eval(filelist))
    })
}  
//----------------------------------------------------------------------------------
exports.filelist = (req,res)=>{
    let qe = req.query.qe;
    let aOut = [];
    console.log("filelist called")
    alist(qe,aOut,(data)=>{
        console.log(" data = %s",data)
        data.forEach((obj,i)=>{
            if(obj.qe == qe || isEmpty(qe) ) {
                aOut.push(obj);
            }    
        })
        res.send(aOut);
    });
} 
//---------------------------------------------------------------------------------
/*
res.attachment('pdfname.pdf');
pdfstream.pipe(res);
Or if you have the pdf on disk, you can send it to the client simply with:

res.download('/path/to/file.pdf');
Or specify a custom filename that's presented to the browser:

res.download('/path/to/file.pdf', 'pdfname.pdf');


 //read the image using fs and send the image content back in the response
        fs.readFile('/path/to/a/file/directory/' + query.file, function (err, content) {
            if (err) {
                res.writeHead(400, {'Content-type':'text/html'})
                console.log(err);
                res.end("No such file");    
            } else {
                //specify Content will be an attachment
                res.setHeader('Content-disposition', 'attachment; filename='+query.file);
                res.end(content);
            }
        });
*/
//----------------------------------------------------------------------------------
exports.download = (req,res)=>{ // send as post
    let qe = req.query.qe;
    let header = req.query.header;
    if (isEmpty(qe)) {
        qe = 'AllQEs'
    }
    if (isEmpty(header)) {
        header = true;
    }
    xmlcsv(qe,header,(err,out)=>{
        let path = '/tmp'
        let file = qe+"_config.csv"
        let filename = path+'/'+file 

        fs = require('fs');

        //var file = __dirname + '/upload-folder/dramaticpenguin.MOV';

        //var filename = path.basename(file);
        //var mimetype = mime.lookup(file);

  res.setHeader('Content-disposition', 'attachment; filename=' + filename);
  res.setHeader('Content-type', 'application/csv');

  var filestream = fs.createReadStream(filename);
  filestream.pipe(res);


//        fs.writeFile(filename, out, function (err) {
            if (err) return console.log(err);



            /*
            res.setHeader("content-type", "application/csv");
            res.setHeader('Content-disposition', `attachment; filename=${qe}_config.csv`);
            res.sendFile(file,{root: path})
*/
/*
            res.set({
                'Content-Type': 'application/csv;charset=UTF-8',
                'Content-Disposition': "inline; filename='" + filename + "'"
            });
            res.send(new Buffer(filename, 'utf8'));
*/
  //      });



        //res.writeHead(200, {"Content-type":"application/text"})
        //res.writeHead(200, {"Content-type":"application/text","Content-disposition":"attachment":"filename="+filename}+"" )
        //res.setHeader("content-type", "application/csv");
        //res.setHeader('Content-disposition', `attachment; filename=${qe}_config.csv`);

        //res.setHeader('Content-disposition', 'attachment; filename='+filename+'');
        //res.set('Content-Type', 'text/csv');
        //res.status(200).send(out);

        //res.sendFile('nlny.png',{root: '/tmp/'})
        //res.end(out);
    })
}
//----------------------------------------------------------------------------------
exports.downloadcert = (req,res)=>{ // send as post
  let filename = req.query.filename;
  if ( isEmpty(filename) ){
      filename = req.params.filename;
      
  }
  console.log("download query = %j",req.query)
  console.log("download params = %j",req.params)
  res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
  res.setHeader('Content-type', 'Application/text');
  try {
      let filestream = fs.createReadStream(filename);
      filestream.pipe(res);
  } catch(e){
      console.error(e)
      res.send(e)
  }

}
//------------------------------------------------------------------------------------ 
let xmlcsv = (qe,header,callback)=>{ 
    let aOut = [];
    let csv = "";
    let count = 0;
    configfiles((err,data)=>{
        if(data.length > 0) {
            data.forEach((o,i)=>{
                let path = o.path;
                let file = o.file ;
                getxml(path,file,aOut,(out)=>{
                    count ++;
                    if (count ==  data.length ){
                        if ( header ){
                            csv = `"Environment","QE Name","QE OID","AA OID",XCPD Endpoint","XCA Query Endpoint","XCA Retrieve Endpoint","Repsotory Unique ID",NYeC PIX Enpoint"\n`
                        }
                        aOut.forEach((obj,i)=>{
                             if ( obj["hcid-name"] == qe ||  qe == 'ALL') {
                                 csv  += `"${obj.env}","${obj["hcid-name"]}","${obj["hcid"]}","${obj["aaoid"]}","${obj["xcpd-endpoint"]}","${obj["xca-query-endpoint"]}", "${obj["xca-retrieve-endpoint"]}","${obj["repoUniqueID"]}","${obj["pix_nyec_endpoint"]}"\n `;   
                             }   
                        })                        
                        callback("",csv);
                    }
                });
            }); 
        } else {
            callback("Error","");
        }
    });        
} 
//----------------------------------------------------------------------------------
exports.qexml = (req,res)=>{
    // get files
    let sent = false;
    let aOut = [];
    let count = 0;
    configfiles((err,data)=>{
        //console.log("data=%j",data)
        if(data.length > 0) {
            data.forEach((o,i)=>{
                let path = o.path;
                let file = o.file ;
                getxml(path,file,aOut,(out)=>{
                    count ++;
                    if (count ==  data.length ){
                        res.send(aOut);
                    }
                });
            }); 
        } else {
            res.send(aOut);
        }
    });    
}
//----------------------------------------------------------------------------------
let configfiles = (callback)=>{
    if(ISWIN ){ 
        let cmd = `cscript xmlfiles.vbs //Nologo` ;
        let nrc = require('node-run-cmd');
        nrc.run(cmd, { 
            onData: (data) => {
                //console.log("%j",data);
                callback("",eval(data));             
            } ,
            onDone:(data)=>{
                //console.log("OnDone=%s",data);
            },
            onError: (data)=>{
            }
        });            
    } else {    
        let cmd = ` find /opt/ppadmin/data -maxdepth 3 -type f -iname "*.xml" -printf '{ "path":"%h","file":"%f","date":"%CY-%Cm-%Cd" },' `
        sshExec(cmd,sshConfig, (err, stdout, stderr) => {
            callback(err,eval('['+stdout.slice(0,-1)+']'));
        });
    }
}
//---------------------------------------------------------------------------------
let xmlExec = (path,file,callback) =>{
    if(ISWIN ){ 
        let cmd = `cscript readfile.vbs ${path+file} //NoLogo `
        let nrc = require('node-run-cmd');
        nrc.run(cmd, { 
            onData: (data) => {
                //console.log("ondata=%s",data)
                callback("",data);            
            } ,
            onDone:(data)=>{
                //console.log("ondata=%s",data)
            },
            onError: (data)=>{
                console.log("onError=%s",data)
            }
        });            
    } else {    
        let cmd = `cd ${path} ; cat ${file} `
        sshExec(cmd,sshConfig, (err, stdout, stderr) => {
            callback(stdout)
        });
    }
}
//----------------------------------------------------------------------------------
let getxml =  (path,file,aOut,callback)=>{
    // bring back xml as string
    xmlExec(path,file,(err,xmlout)=>{
        //console.log("XMLOUT=%s\n",xmlout)
        let select = require('xpath.js') ;
        let dom = require('xmldom').DOMParser ;
        let parser = require('xml2json'); 
        let doc = new dom().parseFromString(xmlout) ;
        if (doc) {   
            let nodes = select(doc, "//QEList");
            let obj = JSON.parse( parser.toJson(nodes.toString()) );
            obj.QEList.QE.forEach((o,i,a)=>{
                o.env = 'N/A';
                if (file.toUpperCase().indexOf('PROD') !== -1 ) {
                    o.env = 'PROD';
                } else if (file.toUpperCase().indexOf('STAGE') !== -1 ) {
                    o.env = 'STAGE';
                } 
                o.ValidFrom = '';
                o.ValidTill = '';
                aOut.push(o);
            });
        }
        callback(aOut)        
    })
} 
//----------------------------------------------------------------------------------
let processCertDates = (data,callback)=> {
    let moment = require('moment'); // for date time
    let out = {};
    let aData = data.split(/(?:\r\n|\r|\n)/g);
    aData.forEach((value,index,array)=>{
        let v = value.trim();
        if (v.indexOf("Not Before:") !== -1){
            let o = v.replace("Not Before:", "");
            out.validFrom = moment(o, "MMM DD HH:mm:ss YYYY").format("YYYY-MM-DD HH:mm:ss") 
        }
        if (v.indexOf("Not After :") !== -1){
            let o = v.replace("Not After :", ""); 
            out.validTill = moment(o, "MMM DD HH:mm:ss YYYY").format("YYYY-MM-DD HH:mm:ss") 
        }
    });
    if (callback) {
        callback(out)
    } else {
        return out;
    }
}
//----------------------------------------------------------------------------------
let readCert = (path,file,type,callback)=>{
    if (isEmpty(type) ) {
        type = 1;
    }    
    if( ISWIN ){ 
        let cmd = `openssl x509 -in ${path}/${file} -noout -text `
        nrc.run(cmd, { 
                onData: (data) => {
                    callback("",data);            
                } ,
                onError: (data)=>{
                    callback("Error","");            
            }
        });        
    } else {
        let cmd = `openssl x509 -in ${path}/${file} -noout -text `
        if (type == 3 ){
            cmd = `openssl x509 -in ${path}/${file} -text `        
        }
        sshExec(cmd,sshConfig, (err, stdout, stderr) => {
            if(err) {
                console.log("\nreadCert err= %s\nstdout=%s\n",err, stdout,stderr)
            }
            callback(err,stdout)         
        })
    }
} 
//----------------------------------------------------------------------------------
exports.cert = (req,res)=>{
    let file = req.query.file;
    let path = req.query.path;
    let type = req.query.type;
    if (isEmpty(type) ){
        type =1;
    }    
    readCert(path,file,type,(err,data)=>{
        if(err) {
            res.send(err);
        } else {
            res.write(data);
            res.end();
        }
    });
}
//----------------------------------------------------------------------------------

