
!function(){
    let _config = {};
    String.prototype.replaceAll = function(str1, str2, ignore) {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    } 
    //---------------------------------------------------------------------------------
    String.prototype.capitalize = function(str){
        str = String(str);
        return str[0].toUpperCase() + str.substr(1, str.length);
    };
    //----------------------------------------------------------------------------------
     String.prototype.truncate = function(max) {
        return this.length > max ? this.substr(0, max-1) + '...' : this;
    } 
    //----------------------------------------------------------------------------------
    exports.truncate = function(str, max) {
        return str.truncate(max);
      }
    //----------------------------------------------------------------------------------
    exports.clone = function(o) { 
      return (JSON.parse(JSON.stringify(o)))
    }
    //---------------------------------------------------------------------------------
    exports.getuuid = (type) =>{
        return require('uuid/v4')()
    }
     //-------------------------------------------------------------
     exports.buildWhere = (type,id,parent,ext)=>{
        if (typeof type == 'object'){
            let o = type;
            id = o.id;
            parent = o.parent;
            ext = o.ext ;
            type = o.type;
        }
        let where = "";
        if ( ! isEmpty(type) ) {
            if( typeof type == 'string' ){
                where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
            }
        }
        if ( ! isEmpty(id)  ) {
            if( typeof id == 'string' ){
                where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
            }
        }
        if (! isEmpty(parent)) {
            if( typeof parent == 'string' ){
                where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
            }
        }
        if (! isEmpty(ext)) {
            if( typeof ext == 'string' ){
                where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
            }
        } 
        return where;   
    }
   
    //----------------------------------------------------------------------------------
    let isEmpty = exports.isEmpty = function(str) { // check if string is null or empty usually missing parameter
        if ( str == undefined ) {
            return true;
        } else if ( str == null ) {
            return true;
        } else if (typeof str == 'string' ){
                return typeof str == 'string' && !str.trim() || str == 'undefined' || str == 'null' || str === '[]'|| str === '{}';
        } else if (typeof str == 'array') {
            return str.length == 0;
        } else if ( typeof str == 'object' ) {
            return Object.keys(str).length == 0;
        } else {
            return str == null || str == undefined;    
        } 
    }
    //------------------------------------------------------
    exports.testConnection = (url,callback) =>{
        const tcpp = require('tcp-ping')
        surl = require('url');
        tcpp.probe(surl.parse(url).hostname,  surl.parse(url).port, (err, available) =>{
           callback(err,available)
        });
    }
    //-----------------------------------------------------------------------------------------
    let fillcolList = function(fields,callback) { // for autofil of jqgrid from mysql
        let colModel = [];       
        fields.forEach((elem,i)=>{
            let colobj = {} ;
            colobj["label"] = elem.name;
            colobj["name"] = elem.name;
            colobj["width"] = Math.min(elem.length,250);
            colobj["editable"] = true;
            colobj["search"] = true;        
            colModel.push(colobj)      
        })
        if(callback){
            callback(colModel)
        }
    }
  
    //---------------------------------------------------------------------------------
    let UIThemeList = exports.UIThemeList = (srcpath,type) =>{
        let fs = require('fs') ;
        let path = require('path')
        if (isEmpty(srcpath)) {
            srcpath = __dirname+"/../public/themes/" ;
        }
        let flist = fs.readdirSync(srcpath).filter(file => fs.lstatSync(path.join(srcpath, file)).isDirectory())
        if (type === 1 ) {
            return (flist)
        } else if (type === 4 ) {
            let slist = ""
            flist.forEach((o,i)=>{
                slist += `${o}:${o};`
            })    
            let newStr = slist.substring(0, slist.length-1);   
            return (newStr)
        } else {     
            let sellist =   `<select name ="jqtheme_select" id="jqtheme_select" class="jqtheme_selector">`
            flist.forEach((o,i)=>{
                sellist += `<option value="${o}">${o}</option>\n`
            })
            sellist +=  '</select>'
            return(sellist)
        }
    }
    //-----------------------------------------------------------------------------------
    exports.uithemes = (req,res)=>{   
        let path = req.query.path;      
        let type = req.query.type;
        if (isEmpty(path)) {
            path = __dirname+"/../public/themes/" ;
        }
        if (isEmpty(type)){
            type = 1;
        }
        res.send(UIThemeList(path,type))
        //console.log(utils.UIThemeList(__dirname+"/../public/themes/",type))
    }
    //-----------------------------------------------------------------------------------
    exports.ejsTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<%-"
        }
        if (isEmpty(sub2)){
            sub2 = "%>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if (token.indexOf("include") !== -1){            
                        let name = tag.replace(sub1+" include","").trim() ;
                        a.push({token,name});            
                    }
                }        
            });
        }
        return a;
    };
    //match(/<a [^>]+>([^<]+)<\/a>/)[1];
    //-----------------------------------------------------------------------------------
    let Tags = exports.Tags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<{"
        }
        if (isEmpty(sub2)){
            sub2 = "}>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    let name = token.replace(sub1,"").trim() ;
                    name = name.replace(sub2,"").trim() ;
                    a.push({token,name});            
                }        
            });
        }
        return a;
    };
    //<{WEB_010,login,en-US}>
    //----------------------------------------------------------------------------------
    let replaceLang = exports.replaceLang = (template,callback) =>{    
        let dict = require('./dic/dict');
        let out = []
        let a =  Tags(template,"<{","}>");
        let count = 0;
        let len = a.length;
        a.forEach((o)=>{
            console.log("Lang obj = %j",o);
            let aList = o.token.split(",");
            let aName = o.name.split(",");
            //console.log(aList)
            console.log(aName)
            dict.get("LANG,",aName[0],"LOOKUP",aName[2],(err,data)=>{
                count += 1;
                if (isEmpty(data)) {
                    dict.save("LANG",aName[0],"LOOKUP",aName[2],aName[1]) ;
                    data = aName[1]
                }
                console.log("Err=%s,data=%s toekn=%s",err,data,o.token)
                template = template.replace(o.token,data)
                console.log(count);
                if (count == len) {
                    console.log("callback %s",template)
                    callback(template)        
                }
            }) ;
            // relace tag with lang        
        })
    }     
    //-----------------------------------------------------------------------------------
    exports.scriptTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<script "
        }
        if (isEmpty(sub2)){
            sub2 = "</script>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if( token.includes('src=')  &&  token.includes(".js") ){     
                        let x =  tag.split('"');
                        x.forEach((src,i)=>{
                            if (src.includes(".js")){
                                a.push({name:src.trim(),token:token})
                            }
                        });       
                    }
                }        
            });
        }
        return a;
    };
    //-----------------------------------------------------------------------------
    exports.stylesheetTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<link"
        }
        if (isEmpty(sub2)){
            sub2 = ">"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if( token.includes('rel="stylesheet"')  &&  token.includes("href=") ){     
                        let x =  tag.split('"');
                        x.forEach((src,i)=>{
                            if (src.includes(".css")){
                                a.push({name:src.trim(),token:token})
                            }
                        });       
                    }
                }        
            });
        }
        return a;
    };
    //----------------------------------------------------------------------------
    exports.token = token = function(str,sub1,sub2){
        let result = "";
        let x = str.indexOf(sub1,0);
        let y = str.indexOf(sub2,x);
        if (x !== -1 && y !== -1) {
            //result = str.substr(x,y-x)+str.substr(y,sub2.length)
            //result = str.substr(x+sub1.length ,y-x)+str.substr(y,sub2.length)
            result = str.substr(x+sub1.length,y-x-sub2.length+2) 
        }
        return result;
    }
    //-----------------------------------------------------------------------------------
    exports.gettoken = (str,sub1,sub2) => {
        let a = [];
        let x = str.indexOf(sub1,0);
        let y = str.indexOf(sub2,0);
        while (x !== -1 && y !== -1) {
            let tag = str.substr(x,y-x);
            let etag = str.substr(y,sub2.length)
            let value = str.substr(x+sub1.length,y-x-sub1.length);
            let o = {x,y,value,token:tag+etag};
            a.push(o)
            x = str.indexOf(sub1,x+1);
            y = str.indexOf(sub2,y+1);            
        }
        return a;
    }
    //-----------------------------------------------------------------------------------
    exports.require = (mod,dict,callback) =>{
        let id = mod || "UTILS" ;
        get("js",mod,"FILE","",(err,data) =>{
            try{
                let _eval = require('eval');
                let utils = _eval(data,true) ;
                if (callback) {
                    callback(err,utils);
                }
            } catch(e){
                callback(e,"");
            }
        });
      
    }
    //----------------------------------------------------------------------------------- 
    module.exports.config = () => {
        return(_config);
    }
    //-----------------------------------------------------------------------------------
    let genToken = exports.genToken = function(len) {    
        let crypto = require('crypto');
        len = len || 20;
        if (crypto.randomBytes) {
            return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
        } else {
            for (var i = 0, salt = ''; i < len; i++) {
            salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
            }
            return salt;
        }
    }
    //----------------------------------------------------------------------------------
    // expects an array of objects value type
    //data=[{"lastupdate":"2017 10 30 13:18:08","id":"1001001184","type":"ISSUE","qe":"NYeC","parent":"PP_PORTAL","issueid":"ISSUE","cargo.priority":"SEV1","ext":"ENV_PROD","cargo.status":"OPENED","cargo.subject":"","notes":"\n","userid":"saadmin","_id_":"1001001184"},{"lastupdate":"2017 10 30 //13:17:55","id":"1001001182","type":"ISSUE","qe":"NYeC","parent":"PP_PORTAL","issueid":"ISSUE","cargo.priority":"SEV1","ext":"ENV_PROD","cargo.status":"OPENED","cargo.subject":"","notes":"","userid":"saadmin","_id_":"1001001182"}]
    //
    let JsonToCsv = exports.JsonToCsv = function(data,opts,callback){
        let err="";
        let temp = "";
        if (typeof opts == 'function' && isEmpty(callback)){
            callback = opts;
            opts = {};            
        }            
        if( isEmpty(opts)) {
            opts = {showHeader:true,filename:'/tmp/temp.csv'};            
        }        
        // get header
        if(typeof data == 'string'){
           //data = JSON.parse(data);
           data = eval(data);
        }
        console.log("data type = =%s",typeof data)
        console.log("data=%j",data)
        if( Array.isArray(data) ) {
            try {
                if (opts.showHeader ){                
                    var count = 0
                    Object.keys(data[0]).forEach(function(element,key,a) {
                        temp += (count ==0) ? `"${element}"`: `,"${element}"`
                        count++
                    });
                }
                Object.keys(data).forEach(function(element, key,a) {
                    var count = 0
                    var val = data[element];
                    Object.keys(val).forEach(function(element, key) {
                        temp += (count ==0) ? `\n"${val[element]}"`: `,"${val[element]}"`
                        count++
                    });
                });        
                if (! isEmpty(opts.filename) ){
                    let fs = require('fs')
                    fs.writeFileSync(opts.filename,temp);
                }
            } catch(ex) {
                err = ex;
            }       
        } else {
            err= "no data to process"
        }
        if(err){
            console.error(err);
        }
        if(callback) {
            callback(err,temp)    
        } else {
            return {"error":err,"list":temp}
        }
    }

}()
