
!function(){
    const config = require('../config.json');
    let CJSON = require("circular-json");
    let utils = require('./utils') ;
    const sql = require("mssql")
    const fhir = require('./routes/fhir_manager')
    const isEmpty =  utils.isEmpty;
    let qeconfig = config.qeconfig
    const authurl = config.authurl
    const Client = require('node-rest-client').Client;
    //---------------------------------------------------------------------------
    let runQry =  (qry,callback) => {    
        new sql.Request().query(qry, (err, result) => {
            if(err) {
                console.log("audit RunQuery err:%s  \nqry=%s",err)
            }
            if(callback){
                if(err) {
                    callback(err,[] )
                } else {
                    callback(err,result.recordsets[0])
                }
            }
        })    
    }
    //----------------------------------------------------------------------------
    let verifyAccess = (tokenid,role,callback) =>{
        let cl = new Client()    
        console.log(tokenid,role)
        let url = `${authurl}/access/verify/${tokenid}/${role}`
        console.log("verify access url=%s",url)
        cl.get(url, (data, response) => {    
            console.log(data)
            callback( data)
        })
    } 
    //---------------------------------------------------------------------------
    exports.auditView= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type  
        let id = params.id  
        let tokenid = params.tokenid
        console.log(tokenid)
       // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
            valid = true
            if(valid) {
                let qry = `select id,seq,type,reqid,source,target,mpi,oid,transactionID,homecommunityID,userid,requestPurpose,role,resource,url,status,timestamp,responsetime 
                from audit order by timestamp desc`
                runQry(qry,(err,result)=>{
                    res.send(result)
                })
            }else{
                res.send("Permission is required to access this Application")
            }
       // })
    }
    //---------------------------------------------------------------------------
    exports.auditViewResponse= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type  
        let id = params.id  
        let tokenid = params.tokenid
    // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
        console.log("auditViewResponse",params)
            let qry = ` DECLARE @sql VARCHAR(max)
            SET @sql= CAST('select response from audit where ID =${id}' as VARCHAR(MAX)   )
            exec (@sql)`
            runQry(qry,(err,result)=>{
                let resp = result[0].response
                console.log( resp.result)
                if( resp.includes('<SOAP') ){
                    let xml2js = require('xml2js'); 
                    // convert XML to JSON
                    xml2js.parseString(result[0].response, (err, result) => {
                        if(err) {
                            console.log(err)
                        }
                        let json = JSON.stringify(result, null, 4);
                        res.send(json);
                    });
                }else{
                    res.send(result[0].response)
                }
            })
    // })
    }
    //---------------------------------------------------------------------------
    exports.auditViewRequest= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type  
        let id = params.id  
        let tokenid = params.tokenid
        console.log("auditViewRequest",params)
    // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
            let qry = ` DECLARE @sql VARCHAR(max)
            SET @sql= CAST('select request from audit where ID =${id}' as VARCHAR(MAX)   )
            exec (@sql)`
            runQry(qry,(err,result)=>{
                res.send(result[0].request)
            })
    // })
    }
    //---------------------------------------------------------------------------
    exports.auditViewResult= (req,res) =>{
        let params = getauditParams(req)
        let id = params.id  
        let tokenid = params.tokenid
        console.log("auditViewRequest",params)
    // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
        let qry = ` DECLARE @sql VARCHAR(max)
        SET @sql= CAST('select result from audit where ID =${id}' as VARCHAR(MAX)   )
        exec (@sql)`
        runQry(qry,(err,result)=>{
            if(err) {
                console.log(err)
                res.send(err);
            } else {
                let resp = result[0].result
                //console.log("RESP=%s",resp)
                if( resp.includes('<SOAP-ENV') ){
                    /*
                    var parser = require('xml2json');
                    var json = parser.toJson(resp);
                    res.send(json)
                    //*/
                    //*
                    let xml2js = require('xml2js'); 
                    xml2js.parseString(resp, (err, result) => {
                        if(err) {
                            console.log(err)
                            res.send(err);
                        } else {
                            let json = JSON.stringify(result, null, 4);
                            res.send(json);
                        }
                    });
                    //*/
                } else {
                    res.send(result[0].result)
                }
            }
        })
    //})
    }
    //---------------------------------------------------------------------------
    exports.auditHtml= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type  
        let id = params.id  
        let tokenid = params.tokenid
    
       // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
            valid = true
            if(valid) {
                res.render("audit1",{page_title:"FHIR Audit Log "})
            }else{
                res.send("Permission is required to access this Application")
            }
       // })
    }
    //---------------------------------------------------------------------------
    exports.auditqerequest= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type  
        let id = params.id  
        let tokenid = params.tokenid
        console.log(tokenid)
       // verifyAccess(tokenid,"FCS_AUDIT",(valid)=>{
            valid = true
            if(valid) {
                if( ! isEmpty(type)) {
                    let qry = `select * from qerequestView order by timestamp desc`
                    if(params.type == 'request') {
                        qry = ` DECLARE @sql VARCHAR(max)
                                SET @sql= CAST('select ${type} from qeRequest where id =${id}' as VARCHAR(MAX)   )
                                exec (@sql)`
                    }
                    runQry(qry,(err,result)=>{
                        //console.log(result)
                        res.setHeader("Content-Type","text/json; charset=UTF-8")
                        if(result && result[0] && result[0].request ){
                            res.send(result[0].request)
                        } else if(result && result[0]  && result[0].result ){
                            res.send(result[0].result)
                        } else {
                            res.send(result)
                        }
                    })
                } else {
                    res.render("audit",{page_title:"FHIR Audit Log "})
                }
            }else{
                res.send("Permission is required to access this Application")
            }
       // })
    }
    //---------------------------------------------------------------------------
    exports.auditqeresponse= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type
        let id = params.id
        let tokenid = params.tokenid
        console.log("token id %s\n%j",tokenid,params)
      //  verifyAccess(tokenid,"FCS_ALL,FCS_AUDIT",(valid)=>{
          valid = true
            if(valid){
                let where = ""
                where = isEmpty(where)?"where qerequestID='"+id+"'":"and qerequestID='"+id+"'"
                let qry = `select id,qeRequestID,transactionID,status,resourceCount,responseTime,timestamp
                        from qeResponse ${where} `
                if(type == 'response') {
                    qry = ` DECLARE @sql VARCHAR(max)
                    SET @sql= CAST('select response from qeResponse where qerequestID =${id}' as VARCHAR(MAX)   )
                    exec (@sql)`
                }
                if(type == 'result') {
                    qry = ` DECLARE @sql VARCHAR(max)
                    SET @sql= CAST('select result from qeResponse where qerequestID =${id}' as VARCHAR(MAX)   )
                    exec (@sql)`
                }
                runQry(qry,(err,result)=>{
                    res.setHeader("Content-Type","application/json; charset=UTF-8")
                    //res.send(result[0].response)
                    if(result && result[0] && result[0].response ){
                        res.send(result[0].response)
                    } else if(result && result[0]  && result[0].result ){
                        res.send(result[0].result)
                    } else {
                        res.send(result)
                    }
                })
            }else{
                res.send("Permission is required to access this Application")
            }
       // })
    }
      //---------------------------------------------------------------------------
      exports.auditstaterequest= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type
        let id = params.id  
        let index = params.index
        let transactionID = params.transactionID
        let tokenid = params.tokenid
        if(isEmpty(id)){
            id =""
        }
       
        verifyAccess(tokenid,"FCS_ALL,FCS_AUDIT",(valid)=>{
           valid = true
                if(valid){
                    if( ! isEmpty(type)) {
                        let qry = `
                        select sreq.ID,qeRequestID,resource,sres.status,sreq.transactionID,oid,mpi,url,sreq.timestamp,sres.responseTime
                                from stateRequest  sreq
                                full outer join stateresponse sres
                                on sres.stateRequestID = sreq.ID 
                                where  qeRequestID ='${id}'`
                        if(type == 'request') {
                            qry = ` DECLARE @sql VARCHAR(max)
                                    SET @sql= CAST('select request from stateRequest where qeRequestID =${id}' as VARCHAR(MAX)   )
                                    exec (@sql)`
                        }
                        console.log(qry)
                        runQry(qry,(err,result)=>{
                        // res.setHeader("Content-Type","application/json; charset=UTF-8")
                            if(result && result[0] && result[0].request ){
                                res.send(result[0].request)
                            } else if(result && result[0]  && result[0].result ){
                                res.send(result[0].result)
                            } else {
                                res.send(result)
                            }
                        })
                    } else {
                        res.render("audit",{page_title:"FHIR Audit Log "})
                    }
                }else{
                    res.send("Permission is required to access this Application")
                }
        })
    }
    //---------------------------------------------------------------------------
    exports.auditstateresponse= (req,res) =>{
        let params = getauditParams(req)
        let type = params.type
        let id = params.id
        let tokenid = params.tokenid
        //verifyAccess(tokenid,"FCS_ALL,FCS_AUDIT",(valid)=>{
            valid=true
            if(valid){
                let where = ""
                where = isEmpty(where)?"where stateRequestID='"+id+"'":"and stateRequestID='"+id+"'"
            
                let qry = `select stateRequestID,transactionID,status,resourceCount,responseTime,timestamp
                        from stateResponse ${where} `
                if(type == 'response') {
                    qry = ` select response from stateResponse where stateRequestID = '${id}' `
                }
                if(type == 'result') {
                    qry = ` select result from stateResponse where stateRequestID = '${id}' `
                }
                runQry(qry,(err,result)=>{
                    res.setHeader("Content-Type","application/fhir+json; charset=UTF-8")     
                    if(result && result[0] && result[0].response ){
                        res.send(result[0].response)
                    } else if(result && result[0].result ){
                        res.send(result[0].result)
                    } else {
                        res.send(result)
                    }
                }) 
            }else{
                res.send("Permission is required to access this Application")
            }
        //})
    }
      //-------------------------------------------------------------------------------
      exports.insertRequest= (resource,params,data,callback) =>{
        // let data = {params,ourl,args} 
        //let qrydata ={ req,params,profile,response,result:pix}
        //console.log("insertRequest params = \n%s",CJSON.stringify(params))
        let type = 'REQUEST'
        let ourl = data.ourl
        let req = data.req
        let request = data.request
        let profile = data.profile
        let transactionID = params.transactionID?params.transactionID:utils.getuuid()
        let reqid = params && params.reqid?params.reqid:-1
        let qe =params.reqqe
        let jwt = params && params.jwt?params.jwt : params.token
        let oid = params &&  params.oid ?params.oid:'n/a'
        let seq =  params.seq
        let status = params.status
        let response = data.response
        let elapsedtime =  data.elapsedtime ?data.elapsedtime :params && params.elapsedtime?params.elapsedtime:0
        if(isEmpty(transactionID)){
            transactionID = utils.getuuid()
        }
        if(isEmpty(seq)){
            seq = -1
        }
        if(isEmpty(qe)){
            qe = jwt && jwt.decoded ?jwt.decoded.payload.sub :"n/a"
        }
        let targetqe = params.targetqe
        if(isEmpty(targetqe)){
            targetqe = jwt && jwt.decoded ?jwt.decoded.payload.targetSystem:"n/a"
        }
        let mpiid = ourl && ourl.mpiid ?ourl.mpiid: params && params.patient?params.patient:params && params.params ?params.params.mpiid :'n/a'
        if(isEmpty(request)){ 
            request = {params,reqraw:req}
            request =CJSON.stringify(request)
        }
        let homecommunityID = ourl ?qeconfig[ourl.targetqe].OID:"n/a"
    
        let userid = params.userid
        let requestPurpose = params.requestPurpose
        if(isEmpty(requestPurpose)){
            requestPurpose = jwt && jwt.decoded ?jwt.decoded.payload.PurposeOfUse :"n/a"
        }
        let role ="n/a"
        let url = ourl && ourl.url?ourl.url:params && params.url ?params.url:params && params.params && params.params.url?params.params.url:'?'
        let query = params && params.query?params.query:'n/a'
        if(isEmpty(response)) {
            response =  req && req.response ? req.response : {error:"response not Found"}
        }
        let result = params && params.result?params.result:'n/a'
        
        jwt = CJSON.stringify(jwt)
        response = CJSON.stringify(response)
        if(isEmpty(status)){
            if(profile && profile.resource && profile.resource.resourceType == "OperationOutcome"){
                status = profile.resource.issue.code
            } else {
                status = 'No Response'
            }
        }
        let qry = `insert into audit(responseTime,seq,type,reqid,source,target,mpi,oid,transactionID,homecommunityID,userid,requestPurpose,Role,resource,url,query,request,response,result,jwt,status,timestamp)
                OUTPUT inserted.ID
                VALUES('${elapsedtime}','${seq}','${type}','${reqid}','${qe}','${targetqe}','${mpiid}','${oid}','${transactionID}','${homecommunityID}','${userid}','${requestPurpose}',
                '${role}','${resource}','${url}','${query}','${request}','${response}','${result}','${jwt}','${status}',GETDATE() ) `
       //  console.log(qry)
        runQry(qry,(err,result)=>{
            if(callback){
                callback(err,result)
            }
        })
    }
      //-------------------------------------------------------------------------------
    exports.insertResponse = (data,callback) =>{
        //let data = {params,data,response}
        //console.log("auditResponse data = %s",CJSON.stringify(data))
        let type = 'RESPONSE'
        let profile = data.profile
        let result = data.result
        let req = data.req
        let response = data.response
        let request = data.request
        let params = data.params
        let reqid = params.reqid?params.reqid:-1
        let transactionID = params.transactionID
        let elapsedtime =  data.elapsedtime ?data.elapsedtime :0
        let mpiid = params.mpiid
        let oid = params &&  params.oid ?params.oid:'n/a'
        let ourl = params.ourl
        let qe = params.reqqe
        let jwt = params && params.jwt?params.jwt : params.token
        console.log(jwt.decoded.payload)
        if(isEmpty(transactionID)){
            transactionID = utils.getuuid()
        }
        if(isEmpty(qe)){
            qe = jwt && jwt.decoded ?jwt.decoded.payload.sub :"n/a"
        }
        let targetqe = params.targetqe   
        if(isEmpty(targetqe)){
            targetqe = jwt && jwt.decoded ?jwt.decoded.payload.targetSystem:"n/a"
        }
    
        let homecommunityID = ourl ?qeconfig[ourl.targetqe].OID:"n/a"
        let userid = params.userid
        let requestPurpose = params.requestPurpose
        if(isEmpty(requestPurpose)){
            requestPurpose = jwt && jwt.decoded ?jwt.decoded.payload.PurposeOfUse :"n/a"
        }
        let role ="n/a"
        let resource = data.resource
        let url = ourl && ourl.url?ourl.url:params.url
        let query = params.query
    
        let seq =  params.seq

        if(isEmpty(seq)){
            seq = -2
        }
        result = result?result:profile?profile:"not found"
    
        response = response?response : req.response ? req.response : {error:"response not Found"}
        let status = params.status
        if(isEmpty(status)) {
            status = response && response.statusCode?response.statusCode: req && req.statusCode ?req.statusCode: profile && profile.response ?profile.response.status:"n/a"
        }
        if(isEmpty(status) || status == "n/a"){
            if(profile && profile.resource && profile.resource.resourceType == "OperationOutcome"){
                status = profile.resource.issue.code
            }
        }
        response = CJSON.stringify(response).replace(/'/g,"''") 
        result = CJSON.stringify(result).replace(/'/g,"''") 
        jwt = CJSON.stringify(jwt).replace(/'/g,"''") 

        let qry = `insert into audit(responseTime,seq,type,reqid,source,target,mpi,oid,transactionID,homecommunityID,userid,requestPurpose,Role,resource,url,query,request,response,result,jwt,status,timestamp)
        OUTPUT inserted.ID
        VALUES('${elapsedtime}','${seq}','${type}','${reqid}','${qe}','${targetqe}','${mpiid}','${oid}','${transactionID}','${homecommunityID}','${userid}','${requestPurpose}',
        '${role}','${resource}','${url}','${query}','${request}','${response}','${result}','${jwt}','${status}',GETDATE() ) `

        runQry(qry,(err,result)=>{
            if(callback){
                callback(err,result)
            }
        })
    }
    //----------------------------------------------------------
    let getauditParams = (req,callback)=>{
        let headers = req.headers
        let _params = {}
    
        let query = req.query
        let params = req.params
        Object.keys(query).forEach( (element,key)=> {
            _params[element] = query[element] 
        })
        Object.keys(params).forEach( (element,key)=> {
            _params[element] = params[element] 
        }) 
        //------------------
        if(! isEmpty(headers)){
           //console.log("Get Params Headers",headers)
            let auth = headers['authorization']
            let tokenid = headers['X-Token']
           //console.log("\n\nparams.auth = %s\n\ntoken=%s\n\n",auth, auth.split(" ")[1])
            if(auth && isEmpty(_params.token)) {
                _params.token = auth.split(" ")[1]
            }
        }
      //  console.log("getauditParams _params = %j",_params)
        return callback ? callback(_params) : _params    
    }
}()
