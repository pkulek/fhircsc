
let utils = require('../utils') ;
const isEmpty =  utils.isEmpty;



let xcaQueryBundle = exports.xcaQueryBundle = (data)=>{

}
let xcaQueryRetrieveBundle = exports.xcaQueryBundle = (data)=>{

}
let ccdRetrieve = exports.ccdRetrieve = (data)=>{
    // get cross domain document query
    // retrieve documents
    // combine documents unto bundle


}
let ccdRetrieveBundle = exports.ccdRetrieveBundle = (data)=>{

}
//----------------------------------------------------------------------------------
let getPIX_Query = exports.getPIX_Query = function (name,data,callback){ 
    if (isEmpty(name)) {
        name = "PIX_QUERY";
    }
    /*
    data.now = moment().format('YYYYMMDDHHmm');  
    data.expire = moment().add(5, 'minutes').format('YYYYMMDDHHmm');
    data.qeid ="2.16.840.1.113883.4.319";
    data.id = "9559770";
    data.shinny = "2.16.840.1.113883.3.2591.500.1";
    data.qe = "HIXNY";
    //*/
    let soapEnv = `<SOAP-ENV:Envelope xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope">
    <SOAP-ENV:Header>
       <wsa:Action SOAP-ENV:mustUnderstand="true">urn:hl7-org:v3:PRPA_IN201309UV02</wsa:Action>
       <wsa:MessageID>urn:uuid:D07299CE-9699-4427-B149-8816682956DB</wsa:MessageID>
       <wsa:ReplyTo>
          <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
       </wsa:ReplyTo>
       <wsa:To xmlns:regex="http://exslt.org/regular-expressions" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:qeal="http://www.nyehealth.org/SOAP/WSDL/QEAlertBody.xsd">https://sprl.shinnyapi.org:9443/pixmanager</wsa:To>
       <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
          <saml:Assertion ID="_655651ee4e7c4c76a678f5e4d7f279a6" IssueInstant="2016-05-02T15:58:31.015Z" Version="2.0" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema">
             <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=SAML User,OU=SU,O=SAML User,L=Los Angeles,ST=CA,C=US</saml:Issuer>
             <saml:Subject>
                <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org, OU=Domain Control Validated</saml:NameID>
             </saml:Subject>
             <saml:AuthnStatement AuthnInstant="2016-05-02T15:58:30.000Z" SessionIndex="987">
                <saml:SubjectLocality Address="158.147.185.168" DNSName="158.147.185.168"/>
                <saml:AuthnContext>
                   <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</saml:AuthnContextClassRef>
                </saml:AuthnContext>
             </saml:AuthnStatement>
             <saml:AttributeStatement>
                <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue xsi:type="xs:string">SystemMonitor</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue xsi:type="xs:string">sPRLMonitoringToolFHIR</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue>
                      <hl7:Role code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="hl7:CE" xmlns:hl7="urn:hl7-org:v3"/>
                   </saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                   <saml:AttributeValue>
                      <hl7:PurposeOfUse code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="hl7:CE" xmlns:hl7="urn:hl7-org:v3"/>
                   </saml:AttributeValue>
                </saml:Attribute>
             </saml:AttributeStatement>
          </saml:Assertion>
       </Security>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
       <PRPA_IN201309UV02 ITSVersion="XML_1.0" xmlns:hl7="www.hl7.org" xmlns="urn:hl7-org:v3">
          <id root="5DD1B072-34C6-40A3-8FA1-86BA00F27B84"/>
          <creationTime value="20200128112108-0500"/>
          <interactionId root="2.16.840.1.113883.1.6" extension="PRPA_IN201309UV02"/>
          <processingCode code="P"/>
          <processingModeCode code="T"/>
          <acceptAckCode code="AL"/>
          <receiver typeCode="RCV">
             <device classCode="DEV" determinerCode="INSTANCE">
                <id root="2.16.840.1.113883.3.2591.500.1"/>
             </device>
          </receiver>
          <sender typeCode="SND">
             <device classCode="DEV" determinerCode="INSTANCE">
                <id root="${data.qeid}"/>
             </device>
          </sender>
          <controlActProcess classCode="CACT" moodCode="EVN">
             <code code="PRPA_TE201309UV02" codeSystem="2.16.840.1.113883.1.6"/>
             <authorOrPerformer typeCode="AUT">
                <assignedPerson classCode="ASSIGNED"/>
             </authorOrPerformer>
             <queryByParameter>
                <queryId root="AC61DD15-453E-41B8-A694-ABDD4E1B68A8" extension="1"/>
                <statusCode code="new"/>
                <responsePriorityCode code="I"/>
                <parameterList>
                   <patientIdentifier>
                      <value root="${data.qeid}" extension="${data.id}"/>
                      <semanticsText>Patient.id</semanticsText>
                   </patientIdentifier>
                </parameterList>
             </queryByParameter>
          </controlActProcess>
       </PRPA_IN201309UV02>
    </SOAP-ENV:Body>
 </SOAP-ENV:Envelope>`
    
 return callback? callback("",soapEnv):soapEnv

}
//----------------------------------------------------------------------------------
exports.xdsQuery = function (params,callback){ 
    let spurl = params.sprlur
    let paramlist = `
    <v3:parameterList>
        <v3:livingSubjectId>
            <v3:value assigningAuthorityName="${params.qe}" extension="${params.mpiid}" root="${params.qeid}" />
            <v3:semanticsText>LivingSubject.id</v3:semanticsText>
        </v3:livingSubjectId>
    `
    if(!isEmpty(params.gender)){
        paramlist +=     
        `<v3:livingSubjectAdministrativeGender>
            <v3:value code="${params.gender}" />
            <v3:semanticsText>LivingSubject.administrativeGender</v3:semanticsText>
         </v3:livingSubjectAdministrativeGender>`  
    }
    if(!isEmpty(params.birthdate)){
        paramlist +=     
        `<v3:livingSubjectBirthTime>
            <v3:value value="${params.birthdate}" />
            <v3:semanticsText>LivingSubject.birthTime</v3:semanticsText>
        </v3:livingSubjectBirthTime>`
    }
    if(!isEmpty(params.family)){
        let param = `<v3:livingSubjectName>
        <v3:value xsi:type="v3:PN">
            <v3:family>${params.family}</v3:family>
        </v3:value>
        <v3:semanticsText>LivingSubject.name</v3:semanticsText>
        </v3:livingSubjectName>`
        if(!isEmpty(param.given)) {
            param =
            `<v3:livingSubjectName>
            <v3:value xsi:type="v3:PN">
                <v3:family>${params.family}</v3:family>
                <v3:given>${params.given}</v3:given>
            </v3:value>
            <v3:semanticsText>LivingSubject.name</v3:semanticsText>
            </v3:livingSubjectName>`
        }
        paramlist += param
    }
    //---- fill in address stuff
    let aparam = ``;
    aparam += isEmpty(params.address)?``:`<v3:streetAddressLine>${params.address}</v3:streetAddressLine >`
    aparam += isEmpty(params.city)?``:`<v3:city>${params.city}</v3:city >`
    aparam += isEmpty(params.state)?``:`<v3:state>${params.state}</v3:state >`
    aparam += isEmpty(params.country)?``:`<v3:country>${params.country}</v3:country >`
    aparam += isEmpty(params.postalcode)?``:`<v3:country>${params.postalcode}</v3:country >`
    if(!isEmpty(aparam)){
        paramlist += "<v3:patientAddress> <v3:value>"
        + aparam + "</v3:value> <v3:semanticsText>Patient.addr</v3:semanticsText> </v3:patientAddress>"
    }            
    paramlist += "</v3:parameterList>"
    //console.log(paramlist)
    
    let soapEnv = `
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
    <soapenv:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <saml2:Assertion xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xenc="http://www.w3.org/2001/04/xmlenc#" xmlns:exc14n="http://www.w3.org/2001/10/xml-exc-c14n#" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" ID="OHT-SAML-ASSERTION" IssueInstant="2016-12-09T18:18:08.488Z" Version="2.0">
          <saml2:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=SAML User,OU=SU,O=SAML User,L=Los Angeles,ST=CA,C=US</saml2:Issuer>
          <saml2:Subject>
            <saml2:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=SAML User,OU=SU,O=SAML User,L=Los Angeles,ST=CA,C=US</saml2:NameID>
          </saml2:Subject>
          <saml2:Conditions NotBefore="" NotOnOrAfter="" />
          <saml2:AuthnStatement AuthnInstant="2016-12-09T18:18:08.488Z">
            <saml2:SubjectLocality Address="10.1.100.35" DNSName="ShinnyClient" />
            <saml2:AuthnContext>
              <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</saml2:AuthnContextClassRef>
            </saml2:AuthnContext>
          </saml2:AuthnStatement>
          <saml2:AttributeStatement>
            <saml2:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">SystemMonitor</saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">sPRLMonitoringToolFHIR</saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue>
                <hl7:Role xmlns:hl7="urn:hl7-org:v3" xmlns="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="hl7:CE" />
              </saml2:AttributeValue>
            </saml2:Attribute>
            <saml2:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
              <saml2:AttributeValue>
                <hl7:PurposeOfUse xmlns:hl7="urn:hl7-org:v3" xmlns="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="hl7:CE" />
              </saml2:AttributeValue>
            </saml2:Attribute>
          </saml2:AttributeStatement>
        </saml2:Assertion>
       
      </wsse:Security>
      <wsa:To>https://sprl.shinnyapi.org:9448/sXCPDQuery</wsa:To>
      <wsa:ReplyTo>
        <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:MessageID>urn:uuid:30110a3c-f00c-58e8-31f3-a915552432ac</wsa:MessageID>
      <wsa:Action>urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery</wsa:Action>
    </soapenv:Header>
    <soapenv:Body>
      <v3:PRPA_IN201305UV02 xmlns:v3="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ITSVersion="XML_1.0">
        <v3:id extension="455cb222:1556a77dd71:446b"/>
        <v3:creationTime value="20161209181808" />
        <v3:interactionId extension="PRPA_IN201305UV02" root="2.16.840.1.113883.1.6" />
        <v3:processingCode code="P" />
        <v3:processingModeCode code="T" />
        <v3:acceptAckCode code="AL" />
        <v3:receiver typeCode="RCV">
          <v3:device classCode="DEV" determinerCode="INSTANCE">
            <v3:id root="2.16.840.1.113883.3.2591.600.1.99.3.1" />
          </v3:device>
        </v3:receiver>
        <v3:sender typeCode="SND">
          <v3:device classCode="DEV" determinerCode="INSTANCE">
            <v3:id root="${data.qeid}" />
          </v3:device>
        </v3:sender>
        <v3:controlActProcess classCode="CACT" moodCode="EVN">
          <v3:code code="PRPA_TE201305UV02" codeSystem="2.16.840.1.113883.1.18" />
          <v3:queryByParameter>
            <v3:queryId extension="1481307488511486" root="1.2.840.114350.1.13.28.1.18.5.999" />
            <v3:statusCode code="new" />
            <v3:responseModalityCode code="R" />
            <v3:responsePriorityCode code="I" />
            ${paramlist}
          </v3:queryByParameter>
        </v3:controlActProcess>
      </v3:PRPA_IN201305UV02>
    </soapenv:Body>
  </soapenv:Envelope>
  `
  return callback? callback("",soapEnv):soapEnv

}
//----------------------------------------------------------------------------------
exports.xcaQuery = function (name,params,callback){
    if (isEmpty(name)) {
        name = "XCA_QUERY";
    }
    if (isEmpty(params.dev)) {
        params.dev  = "STAGE";
    }
    let element= params.oHIE;
    let sprlurl = element.sprlurl
  
    //console.log("xcaQuery SOAP MPI_OID = %s",element.MPI_OID)
    var soapEnv =  `
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:wsa="http://www.w3.org/2005/08/addressing"  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"> 
        <SOAP-ENV:Header> 
            <wsa:Action SOAP-ENV:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayQuery</wsa:Action> 
            <wsa:MessageID>urn:uuid:2E8C5DF4-1140-11E6-BF83-3336AC0A4F00</wsa:MessageID> 
            <wsa:ReplyTo> 
                <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> 
            </wsa:ReplyTo> 
            <wsa:To>${sprlurl}</wsa:To> 
            <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"> 
                <saml:Assertion ID="XId-2E8C86BC-1140-11E6-BF83-5056AC0A4F00" IssueInstant="2016-05-21T15:03:23.791Z" Version="2.0" xmlns="" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"> 
                    <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:Issuer> 
                    <saml:Subject> 
                        <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:NameID> 
                    </saml:Subject> 
                    <saml:AuthnStatement AuthnInstant="2016-06-21T15:03:23.791Z"> 
                    <saml:SubjectLocality Address="192.168.120.98" DNSName="SPRL.SHINNYAPI.ORG"/> 
                    <saml:AuthnContext> 
                        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef> 
                    </saml:AuthnContext> 
                    </saml:AuthnStatement> 
                    <saml:AttributeStatement> 
                    <saml:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">SystemMonitor</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">sPRLMonitoringTool</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.3.2591.400</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue> 
                            <s01:Role code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/> 
                        </saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue> 
                            <s01:PurposeOfUse code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/> 
                        </saml:AttributeValue> 
                    </saml:Attribute> 
                    </saml:AttributeStatement> 
                </saml:Assertion> 
            </Security> 
        </SOAP-ENV:Header> 
        
        <SOAP-ENV:Body> 
            <query:AdhocQueryRequest xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0"> 
                <query:ResponseOption returnComposedObjects="true" returnType="LeafClass"/> 
                <rim:AdhocQuery id="urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"> 
                    <rim:Slot name="$XDSDocumentEntryPatientId"> 
                      <rim:ValueList> 
                          <rim:Value>'${element.MPI_OID}'</rim:Value> 
                      </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryStatus"> 
                      <rim:ValueList> 
                          <rim:Value>${element.DocumentStatus}</rim:Value> 
                      </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryType"> 
                        <rim:ValueList> 
                            <rim:Value>${element.DocumentType1}</rim:Value> 
                            <rim:Value>${element.DocumentType2}</rim:Value> 
                        </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryFormatCode">
                        <rim:ValueList>
                        <rim:Value>CCDA</rim:Value>
                        </rim:ValueList>
                    </rim:Slot>                
                 </rim:AdhocQuery> 
            </query:AdhocQueryRequest> 
        </SOAP-ENV:Body> 
        </SOAP-ENV:Envelope>
`
  return callback? callback("",soapEnv):soapEnv
}
