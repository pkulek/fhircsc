let config = require('../../config.json');
let utils = require('../utils') ;
const clone =  utils.clone;
const DEBUG = config.DEBUG ;
const STATUS_CODES = require('http').STATUS_CODES
const isEmpty =  utils.isEmpty;

const sql = require("mssql")
let audit = require("../audit")
const CJSON = require("circular-json");
//const dict = require('../dic/dict')("mssql");
//const login = require('./login');
const Client = require('node-rest-client').Client;
const fs = require('fs')

let moment = require('moment'); // for date time
let fhirprofiles = require("./fhir-profiles")
let fhirresources = require("./fhir-resources")
let soapprofiles = require("./soap-profiles")
const authurl = config.authurl

//-----------------------------------------------------------------------------
const livingSubjectQuery = {
        "extension":"1360358721" ,
        "root":"2.16.840.1.113883.3.458.10.3.1",
        "Gender":"F",
        "DOB":"19220406",
        "family":"NOSAL",
        "given":"SHERRY",
        "middle":"A",
        "streetAddressLine":"2173 JARVISVILLE RD RTRT",
        "city":"HUNTINGTON",
        "state":"NY",
        "country":"USA",
        "postalCode":"11743"
}

let PROD_SPRLurls = {
        "base":"http://64.74.242.8:9445",      
        "PIX":"http://64.74.242.8:9445/PIXManager",      
        "PDQ":"http://64.74.242.8:9445/sXCPDQuery",      
        "XCA_QUERY":"http://64.74.242.8:9445/sXCAQuery",      
        "XCA_RETRIEVE":"http://64.74.242.8:9445/sXCARetrieve",      
        "XDS":"http://64.74.242.8:9445/sXCAQuery"      
      }
let STAGE_SPRLurls = {
    "base":"http://192.168.160.23:9448",      
    "PIX":"http://192.168.160.23:9448/PIXManager",      
    "PDQ":"http://192.168.160.23:9448/sXCPDQuery",      
    "urlXCA_QUERY":"http://192.168.160.23:9448/sXCAQuery",      
    "XCA_QUERY":"http://localhost:6002/sXCAQuery",      
    "XCA_RETRIEVE":"http://192.168.160.23:9448/sXCARetrieve",      
    "XDS":"http://192.168.160.23:9448/sXCAQuery"      
    }
//-----------------------------------------------------------------------------
        //var rim1 = "'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1'";   // DocumnetType1
        //var rim2 = "'urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248'";   // DocumentType2
        //var rim3 = "'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved'"; // DocumentStatus
        //,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"

let QEs = ['BRONX','HIXNY','HEALTHECONN','HEALTHIX','HEALTHLINKNY','NYCIG','GRRHIO','HEALTHELINK','SMPI'];
//var QEs = ['HEALTHLINKNY','HEALTHIX','HEALTHELINK','HIXNY','BRONX','NYCIG','HEALTHECONN','GRRHIO'];

let STAGE_HIEProperties = [
        {QE_Name:"SMPI",MPI_ID:"4",QE_ID:"2.16.840.1.113883.3.2591.500.1",MPI_OID:"4^^^&amp;2.16.840.1.113883.3.2591.500.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"BRONX",MPI_ID:"000257887182",QE_ID:"2.16.840.1.113883.3.371.1.2.3",MPI_OID:"000257887182^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HIXNY",MPI_ID:"36192",QE_ID:"2.16.840.1.113883.4.319",MPI_OID:"36192^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHECONN",MPI_ID:"6193709",QE_ID:"2.16.840.1.113883.3.1834.2",MPI_OID:"6193709^^^&amp;2.16.840.1.113883.3.1834.2&amp;ISO",Repository_Id:"2.16.840.1.113883.3.1834.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHIX",MPI_ID:"5470835",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5470835^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHLINKNY",MPI_ID:"1020217012",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1020217012^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"NYCIG",MPI_ID:"1046652",QE_ID:"2.16.840.1.113883.3.2591.100.2",MPI_OID:"1046652^^^&amp;2.16.840.1.113883.3.2591.100.2&amp;ISO",Repository_Id:"2.16.840.1.113883.5.25",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"GRRHIO",MPI_ID:"1360358721",QE_ID:"2.16.840.1.113883.3.458.10.3.1",MPI_OID:"1360358721^^^&amp;2.16.840.1.113883.3.458.10.3.3&amp;ISO",Repository_Id:"",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHELINK",MPI_ID:"000649637162",QE_ID:"2.25.256133121442266547198931747355024016667.1.2.1",MPI_OID:"000649637162^^^&amp;2.25.256133121442266547198931747355024016667.1.2.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
    ];
let PROD_HIEProperties = [
        {QE_Name:"SMPI",MPI_ID:"4",QE_ID:"2.16.840.1.113883.3.2591.500.1",MPI_OID:"4^^^&amp;2.16.840.1.113883.3.2591.500.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"BRONX",MPI_ID:"002412926061",QE_ID:"2.16.840.1.113883.3.371.1.1.3",MPI_OID:"'002412926061^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO'",Repository_Id:"2.16.840.1.113883.3.371.1.1.5.12",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1'",DocumentType2:"'urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248'",DocumentStatus:"'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved'",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HIXNY",MPI_ID:"12953892",QE_ID:"2.16.840.1.113883.4.319",MPI_OID:"12953892^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.2.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHECONN",MPI_ID:"9458451",QE_ID:"2.16.840.1.113883.3.1834.1",MPI_OID:"9458451^^^&amp;2.16.840.1.113883.3.1834.1&amp;ISO",Repository_Id:"1.3.6.1.4.1.21367.2010.1.2.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHIX",MPI_ID:"5160563",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5160563^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHLINKNY",MPI_ID:"1021759186",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1021759186^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.6.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"NYCIG",MPI_ID:"6109116",QE_ID:"2.16.840.1.113883.3.2591.100.3",MPI_OID:"6109116^^^&amp;2.16.840.1.113883.3.2591.100.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.2591.100.3.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"GRRHIO",MPI_ID:"1360910431",QE_ID:"2.16.840.1.113883.3.458.10.1.1",MPI_OID:"1360910431^^^&amp;2.16.840.1.113883.3.458.10.1.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.458.10.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHELINK",MPI_ID:"2000001795808",QE_ID:"2.25.256133121442266547198931747355024016667.1.1.1",MPI_OID:"2000001795808^^^&amp;2.25.256133121442266547198931747355024016667.1.1.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
    ];
    let STAGE_QElist = {
        "SMPI":{QE_Name:"SMPI",MPI_ID:"4",QE_ID:"2.16.840.1.113883.3.2591.500.1",MPI_OID:"4^^^&amp;2.16.840.1.113883.3.2591.500.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "BRONX":{QE_Name:"BRONX",MPI_ID:"000257887182",QE_ID:"2.16.840.1.113883.3.371.1.2.3",MPI_OID:"000257887182^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HIXNY":{QE_Name:"HIXNY",MPI_ID:"36192",QE_ID:"C",MPI_OID:"36192^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHECONN":{QE_Name:"HEALTHECONN",MPI_ID:"6193709",QE_ID:"2.16.840.1.113883.3.1834.2",MPI_OID:"6193709^^^&amp;2.16.840.1.113883.3.1834.2&amp;ISO",Repository_Id:"2.16.840.1.113883.3.1834.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHIX":{QE_Name:"HEALTHIX",MPI_ID:"5470835",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5470835^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHLINKNY":{QE_Name:"HEALTHLINKNY",MPI_ID:"1020217012",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1020217012^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "NYCIG":{QE_Name:"NYCIG",MPI_ID:"1046652",QE_ID:"2.16.840.1.113883.3.2591.100.2",MPI_OID:"1046652^^^&amp;2.16.840.1.113883.3.2591.100.2&amp;ISO",Repository_Id:"2.16.840.1.113883.5.25",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "GRRHIO":{QE_Name:"GRRHIO",MPI_ID:"1360358721",QE_ID:"2.16.840.1.113883.3.458.10.3.3",MPI_OID:"1360358721^^^&amp;2.16.840.1.113883.3.458.10.3.3&amp;ISO",Repository_Id:"",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHELINK":{QE_Name:"HEALTHELINK",MPI_ID:"000649637162",QE_ID:"2.25.256133121442266547198931747355024016667.1.2.1",MPI_OID:"000649637162^^^&amp;2.25.256133121442266547198931747355024016667.1.2.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
        };
    let PROD_QElist = {
        "SMPI":{QE_Name:"SMPI",MPI_ID:"4",QE_ID:"2.16.840.1.113883.3.2591.500.1",MPI_OID:"4^^^&amp;2.16.840.1.113883.3.2591.500.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "BRONX":{QE_Name:"BRONX",MPI_ID:"002412926061",QE_ID:"2.16.840.1.113883.3.371.1.1.3",MPI_OID:"'002412926061^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO'",Repository_Id:"2.16.840.1.113883.3.371.1.1.5.12",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1'",DocumentType2:"'urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248'",DocumentStatus:"'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved'",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HIXNY":{QE_Name:"HIXNY",MPI_ID:"12953892",QE_ID:"2.16.840.1.113883.4.319",MPI_OID:"12953892^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.2.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHECONN":{QE_Name:"HEALTHECONN",MPI_ID:"9458451",QE_ID:"2.16.840.1.113883.3.1834.1",MPI_OID:"9458451^^^&amp;2.16.840.1.113883.3.1834.1&amp;ISO",Repository_Id:"1.3.6.1.4.1.21367.2010.1.2.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHIX":{QE_Name:"HEALTHIX",MPI_ID:"5160563",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5160563^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHLINKNY":{QE_Name:"HEALTHLINKNY",MPI_ID:"1021759186",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1021759186^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.6.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "NYCIG":{QE_Name:"NYCIG",MPI_ID:"6109116",QE_ID:"2.16.840.1.113883.3.2591.100.3",MPI_OID:"6109116^^^&amp;2.16.840.1.113883.3.2591.100.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.2591.100.3.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "GRRHIO":{QE_Name:"GRRHIO",MPI_ID:"1360910431",QE_ID:"2.16.840.1.113883.3.458.10.1.1",MPI_OID:"1360910431^^^&amp;2.16.840.1.113883.3.458.10.1.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.458.10.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        "HEALTHELINK":{QE_Name:"HEALTHELINK",MPI_ID:"2000001795808",QE_ID:"2.25.256133121442266547198931747355024016667.1.1.1",MPI_OID:"2000001795808^^^&amp;2.25.256133121442266547198931747355024016667.1.1.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
    };

//    GET https://evalfhir.directmdemail.com/fhir/baseDstu3/Condition?patient={id}&category=http://hl7.org/fhir/condition-category|health-concern&onset-date=>={dateTime}&onset-date=<={dateTime (later than previous dateTime)}
// GET https://evalfhir.directmdemail.com/fhir/baseDstu3/Condition?patient=56&category=http://hl7.org/fhir/condition-category|health-concern&onset-date=>=2015-06-22&onset-date=<=2015-06-27

    /*
    let qeconfig = {"HEALTHELINK":{
                                   "patient":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Patient/_search?identifier=HEL|${mpiid}"},
                                   "condition":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Condition/_search?patient.identifier=HEL|${mpiid}${code}${date}"},
                                   "observation":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Observation/_search?patient.identifier=HEL|${mpiid}${code}${date}"},
                                 },
                    "HIXNY":{"patient":{url:""},"condition":{url:""},"observation":{url:""}},
                    "HEALTHECONN":{"patient":{url:""},"condition":{url:""},"observation":{url:""}},
                    "GRRHIO":{"patient":{url:""},"condition":{url:""},"observation":{url:""}},
                    "BRONX":{"patient":{url:""},"condition":{url:""},"observation":{url:""}},
                    "HEALTHIX":{"patient":{url:""},"condition":{url:""},"observation":{url:""}}
                }
*/
    //let config ={"QEs":[{"name":"HEL","baseurl":"https://fhirtest.wnyhealthelink.com/fhir/stu3/{resource}/_search/","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]},{"name":"HEALTHIX","baseurl":"https://stage.healthix.org/csp/healthshare/ods/fhir/stu3/","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]},{"name":"HIXNY","baseurl":"https://fhirtest.hixny.com/fhir/dstu2/","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]},{"name":"GRRHIO","baseurl":"https://stage.fhir.rochesterrhio.net/fhir/stu3/{resource}/_search","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]},{"name":"BRONX","baseurl":"https://fhirtest.wnyhealthelink.com/fhir/stu3/{resource}/_search","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]},{"name":"HEC","baseurl":"https://hecstgmc5.myhie.com:9301/FHIR-Test/","resources":[{"name":"Patient","parameters":[{"name":"indetifier","format":"HEL|MPIID"}]},{"name":"Condition","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"code"}]},{"name":"Observation","parameters":[{"name":"patient.identifier","format":"HEL|MPIID"},{"name":"category"},{"name":"code"},{"name":"date","format":"MM-DD-YYYY"}]}]}]}
    //const config= {"QEs":{"HEL":{"name":"HEL","baseurl":"https://fhirtest.wnyhealthelink.com/fhir/stu3/{resource}/_search/","resources":{"Patient":{"name":"Patient","parameters":{"indetifier":{"name":"indetifier","format":"HEL|MPIID"}}},"Condition":{"name":"Condition","parameters":{"patient.identifier":{"name":"patient.identifier","format":"HEL|MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"patient.identifier":{"name":"patient.identifier","format":"HEL|MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}},"HEALTHIX":{"name":"HEALTHIX","baseurl":"https://stage.healthix.org/csp/healthshare/ods/fhir/stu3/","resources":{"Patient":{"name":"Patient","parameters":{"_id":{"name":"_id","format":"MPIID"}}},"Condition":{"name":"Condition","parameters":{"subject:Patient":{"name":"subject:Patient","format":"MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"subject:Patient":{"name":"subject:Patient","format":"MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}},"HIXNY":{"name":"HIXNY","baseurl":"https://fhirtest.hixny.com/fhir/dstu2/","resources":{"Patient":{"name":"Patient","parameters":{"ID":{"name":"","format":"MPIID"}}},"Condition":{"name":"Condition","parameters":{"patient":{"name":"patient","format":"MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"patient":{"name":"patient","format":"MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}},"GRRHIO":{"name":"GRRHIO","baseurl":"https://stage.fhir.rochesterrhio.net/fhir/stu3/{resource}/_search","resources":{"Patient":{"name":"Patient","parameters":{"indetifier":{"name":"indetifier","format":"GRRHIO|MPIID"}}},"Condition":{"name":"Condition","parameters":{"patient.identifier":{"name":"patient.identifier","format":"GRRHIO|MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"patient.identifier":{"name":"patient.identifier","format":"GRRHIO|MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}},"BRONX":{"name":"BRONX","baseurl":"https://fhirtest.wnyhealthelink.com/fhir/stu3/{resource}/_search","resources":{"Patient":{"name":"Patient","parameters":{"indetifier":{"name":"indetifier","format":"GRRHIO|MPIID"}}},"Condition":{"name":"Condition","parameters":{"patient.identifier":{"name":"patient.identifier","format":"GRRHIO|MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"patient.identifier":{"name":"patient.identifier","format":"GRRHIO|MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}},"HEC":{"name":"HEC","baseurl":"https://hecstgmc5.myhie.com:9301/FHIR-Test/","resources":{"Patient":{"name":"Patient","parameters":{"identifier":{"name":"identifier","format":"HEC|MPIID"}}},"Condition":{"name":"Condition","parameters":{"patient_id":{"name":"patient_id","format":"HEC|MPIID"},"code":{"name":"code"}}},"Observation":{"name":"Observation","parameters":{"patient_id":{"name":"patient_id","format":"HEC|MPIID"},"category":{"name":"category"},"code":{"name":"code"},"date":{"name":"date","format":"MM-DD-YYYY"}}}}}}}     
    /*
    Condition
https://fhirtest.wnyhealthelink.com/fhir/stu3/Condition/_search?patient.identifier=HEL|000649637162&code=1231-45&date=2020-01-01

Observation
https://fhirtest.wnyhealthelink.com/fhir/stu3/Observation/_search?patient.identifier=HEL|000649637162&code=5902-2&date=2020-01-01

Patient
https://fhirtest.wnyhealthelink.com/fhir/stu3/Patient/_search?identifier=HEL|000649637162
*/

let qeconfig = config.qeconfig
/*
let qeconfig = {"HEALTHELINK":{"patient":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Patient/_search?identifier=HEL|${mpiid}"},
                                "condition":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Condition/_search?patient.identifier=HEL|${mpiid}${code}${date}"},
                                "observation":{"url":"https://fhirtest.wnyhealthelink.com/fhir/stu3/Observation/_search?patient.identifier=HEL|${mpiid}${code}${date}"},
                         },
                "HIXNY":{"patient":{"url":"https://fhirtest.hixny.com/fhir/dstu2/Patient/${mpiid}"},
                        "condition":{"url":"https://fhirtest.hixny.com/fhir/dstu2/Condition?patient=${mpiid}${code}${date}"},
                        "observation":{"url":"https://fhirtest.hixny.com/fhir/dstu2/Observation?patient=${mpiid}${code}${date}"}},
                "HEALTHECONN":{"patient":{"url":"https://hecstgmc5.myhie.com:9301/FHIR-Test/Patient?identifier=HEC|${mpiid}"},
                            "condition":{"url":"https://hecstgmc5.myhie.com:9301/FHIR-Test/Condition?patient_id=HEC|${mpiid}${code}${date}"},
                            "observation":{"url":"https://hecstgmc5.myhie.com:9301/FHIR-Test/Observation?patient_id=HEC|${mpiid}${code}${date}"}},
                "GRRHIO":{"patient":{"url":"https://stage.fhir.rochesterrhio.net/fhir/stu3/Patient/_search?identifier=GRRHIO|${mpiid}"},
                            "condition":{"url":"https://stage.fhir.rochesterrhio.net/fhir/stu3/Condition/_search?identifier=GRRHIO|${mpiid}${category}${code}${date}"},
                            "observation":{"url":"https://stage.fhir.rochesterrhio.net/fhir/stu3/Observation?patient=${mpiid}${category}${code}${data}"}},
                "BRONX":{"patient":{"url":""},"condition":{"url":""},"observation":{"url":""}},
                "HEALTHIX":{"patient":{"url":"https://stage.healthix.org/csp/healthshare/ods/fhir/stu3/Patient?_id=${mpiid}"},
                            "condition":{"url":"https://stage.healthix.org/csp/healthshare/ods/fhir/stu3/Condition?subject:Patient=${mpiid}${code}${date}"},
                            "observation":{"url":"https://stage.healthix.org/csp/healthshare/ods/fhir/stu3/Observation?subject:Patient=${mpiid}${category}${code}${date}"}}
}
*/
/*  
    dict.open((err)=>{
        console.log(err)
        dict.set("JSON","qeconfig","FHIR","STAGE",qeconfig,"qeconfig update",(err)=>{});    

    })
//*/    
//------------------------------------------------------------------
let writelog = (type,data)=>{
    if(config.writelog){
        let dir = config.logfilepath
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        let time = moment().format('YYYYMMDDHHmmSS')
        let outfile = dir+type+"_"+time+".json"
        fs.writeFileSync(outfile,data ,"utf-8") 
    }
}
//-----------------------------------------------------------------------------------------------
let parseresult = (data,err)=>{
    let result = ""
    if(! isEmpty(data)){
        if(typeof data == 'object') {
            if (Buffer.isBuffer(data)) {
                if(data.toString().includes('{')) {
                    result = JSON.parse(data.toString("utf-8"))
                }else {
                    result = data.toString("utf-8")
                }
            } else {
                result = data
            }
        } else {
            if(typeof data == 'string') {
                if( data.includes("<")  ){ // xml file
                    data = {error:data}
                } else {
                    data = JSON.parse(data)
                }
                
            }
             result = data
        }
    } else {
        err.severity = err.severity?err.severity:"warning"
        let profile = fhirprofiles.operationOutcome(err)
        return profile;
        
    }
    return result
}
//---------------------------------------------------------------------------
let runQry =  (qry,callback) => {    
    new sql.Request().query(qry, (err, result) => {
        if(err) {
           console.error(err)
        }
        if(callback){
            if(err) {
                callback(err,[] )
            } else {
                callback(err,result.recordsets[0])
            }
        }
    })    
}
//---------------------------------------------------------------------------
let buildqry = (params)=>{
    let count = Object.keys(params).length ;
    let qry = ""
    Object.keys(params).forEach(function(elem,key) {
        if(  !isEmpty(params[elem]) &&  elem !=='url' &&  elem !=='type' &&  elem !=='_format') {
            qry += ( qry=="" ? `?${elem}=${params[elem]}` :`&${elem}=${params[elem]}` )
        }
    });
    //console.log("\nbuildqry=%j\n\nresult=%s\n",params,qry)
    return qry;
}
//---------------------------------------------------------------------------
exports.PIXjson2table = (req,res) => {
    let params = getSPRLparams(req)
    let type = isEmpty(params.type) ? "PIX" : params.type;
    let qry = buildqry(params)
    let qryparam = `/sprl/${type}/Query${qry}`
    console.log("PIXjson2table qryparams=%s", qryparam)
    res.render("json2table1",{qryparam});                        
}    
//-----------------------------------------------------------------------------------
exports.PDQjson2table = (req,res) => {
    let params = getSPRLparams(req)
    let type = isEmpty(params.type) ? "PDQ" : params.type;
    let qry = buildqry(params)
//    let qryparam = `/sprl/${type}/Query?tokenid=${params.tokenid}&mpiid=${params.mpiid}|${params.qeid}&qe=${params.qe}&family=${params.family}&given=${params.given}&address=${params.address}&telecom=${params.telecom}&gender=${params.gender}&birthdate=${params.birthdate}  `
    let qryparam = `/sprl/PDQ/Query${qry}`
//    console.log("PIXjson2table qryparams=%s" , qryparam)
    res.render("json2table1",{qryparam});                        
}    
//-----------------------------------------------------------------------------------
exports.XCAjson2table = (req,res) => {
    let params = getSPRLparams(req)
    //let type = isEmpty(params.type) ? "XCA" : params.type;
    let qry = buildqry(params)
    console.log("\n\n\nQRY=%s\n\\n",qry)
    let qryparam = `/sprl/XCA/Query${qry}`
    res.render("json2table1",{qryparam});                        
}    
//-----------------------------------------------------------------------------------
exports.XCARjson2table = (req,res) => {
    let params = getSPRLparams(req)
    //let type = isEmpty(params.type) ? "XCA" : params.type;
    let qry = buildqry(params)
    let qryparam = `Retrieve${qry}&type=JSON`;
    //let qryparam = `/sprl/XCA/Query${qry}`
    console.log("\nXCAR qryparam=\n%s\n",qryparam)
    res.render("json2table1",{qryparam});                        
}    
//---------------------------------------------------------------------------
exports.json2table = (req,res) => {
    let params = getSPRLparams(req)
    console.log("\njson2table\nparams=\n\n%j",params)
    let type = isEmpty(params.type) ? "PIX" : params.type;
    //let qry = buildqry(params)
    //let qryparam = `/sprl/${type}/Query${qry}`
    let qryparam = `sprl/XCA/Retrieve?tokenid=${params.tokenid}&type=JSON&mpiid=${params.mpiid}|${params.qeid}&qe=${params.qe}&dev=${params.dev}&sprlurl=&${params.sprlurl}`;    res.render("json2table1",{qryparam});                        
}    
//-----------------------------------------------------------------------------------
exports.urlSelect = (req,res)=> {
    let params = getSPRLparams(req)
    let type = "PIX";
    if(! isEmpty(params.type)){
        type = params.type
    }
    let opt = `<option value="${eval("STAGE_SPRLurls["+type+"]")}">STAGE</option>` 
    opt += `<option value="${eval("PROD_SPRLurls["+type+"]")}">PROD</option>` 
    res.send(opt)
}  
//-------------------------------------------------------------------------------
exports.getToken = (req,res)=> {
    let params = getSPRLparams(req)
    //console.log("tgetoken params = %j",params)
    //console.log("\n\n\n\n")
    let sub = params.sub;
    let target = params.target;
    token(sub,target,(data)=>{
        res.send(data)
    }) 
   
}  
//-------------------------------------------------------------------------------
let token = exports.token = (qe,targetqe,callback)=> {
    let cl = new Client()    
    let url = `${authurl}/jwt/token/${qe}/${targetqe}`
   // console.log("toke url = %s",url)
    cl.get(url, (data, response) => {       
            callback(data)
    })
}  
//-------------------------------------------------------------------------------
exports.decodeToken = (req,res)=> {
    let params = getSPRLparams(req)
    let cl = new Client()    
    let token = req.query.token
    let url = `${authurl}/jwt/decode/${token}`
  //  console.log("decode token = %s",url)
    let args = {
        headers: { "Authorization":"Bearer "+token} // request headers            
    };
    cl.get(url, args,(data, response) => {          
        res.send(data)
    })
}  
//-------------------------------------------------------------------------------
exports.verifyToken = (req,res)=> {
    let params = getSPRLparams(req)
    let cl = new Client()    
    let url = `${authurl}verify`
    let token =req.query.token
        let args = {
        data: { test: "hello" }, // data passed to REST method (only useful in POST, PUT or PATCH methods)
        path: { "id": 120 }, // path substitution var
        parameters: {"token":token} , // this is serialized as URL parameters
        headers: { "Authorization":"Bearer "+token} // request headers            
    };
    cl.get(url, args,(data, response) => {          
        res.send(data)
    })
}  
//-----------------------------------------------------------------------------------
exports.getQEdata = (req,res)=> {
    let params = getSPRLparams(req)
    if(isEmpty(params.dev)){
        params.dev= "STAGE"
    }
    dict.get("JSON","QElist","SPRL",params.dev,(err,data)=>{

        console.log("DATA = %j",err|data)
        if(! isEmpty(data)) {
            data = JSON.parse(data)
            if(params.type=="select") {
                // let opt = `<option value="2.16.840.1.113883.3.2591.500.1">SMPI</option>`
                let opt = ""
                Object.keys(data).forEach( (element,key)=> {
                    opt += `<option value="${data[element].QE_ID}">${element}</option>\n`
                })                
                res.send(opt)
            } else if(params.type=="odata") {
                res.send(data)          
            } else if(params.type=="qe") {
                res.send(data[params.qe])          
            }else{  
                res.send(data)          
            }
        } else {
            res.send(data)
        }
    })
}  
//-----------------------------------------------------------------------------------
exports.getRESTURLS = (req,res)=> {
    let params = getSPRLparams(req)
    if(isEmpty(params.dev)){
        params.dev= "STAGE"
    }
    dict.get("JSON","URL","REST",params.dev,(err,data)=>{
        if(! isEmpty(data)) {
            data = JSON.parse(data)
            if(params.type=="select") {
                // let opt = `<option value="2.16.840.1.113883.3.2591.500.1">SMPI</option>`
                let opt = ""
                Object.keys(data).forEach(function(element,key) {
                    opt += `<option value="${data[element].QE_ID}">${element}</option>\n`
                })                
                res.send(opt)
            } else if(params.type=="odata") {
                res.send(data)          
            } else if(params.type=="qe") {
                res.send(data[params.qe])          
            }else{  
                res.send(data)          
            }
        } else {
            res.send(data)
        }
    })
}  
//-----------------------------------------------------------------------------------
exports.getSubs = (req,res)=> {
    let params = getSPRLparams(req)
    if(isEmpty(params.dev)){
        params.dev= "STAGE"
    }
    let qry = 'select sub from qeauth '
}  
//----------------------------------------------------------------------------------------------------------------------------
let postXCARetrieve= function(url,args,oHIE,callback){
        let cl = new Client()    
        try{
            if (DEBUG) {                    
                fs.writeFile("/tmp/XCA_Retrieve_SOAPCall_"+oHIE.QE_Name, CJSON.stringify(args,null,2), function(err) {
                    if(err) { console.log(err); }
                });
            }  
            cl.post(url, args, function (data, response) {            
                //oHIE.qrytime = stopwatch.elapsedMilliseconds;
                let ccd = "";
                data = data.toString();
                //if (DEBUG) {                    
                    fs.writeFile("/tmp/XCA_Retrieve_Data_"+oHIE.QE_Name,data, function(err) {
                            if(err) { console.log(err); }
                    });         
                //} 
                if ( ! isEmpty(data) ) {
                    let base64 = require('base-64');
                    let docId = "n/a";
                    let MPI = "n/a" 
                    let status = 'n/a'
                    let failure = 'n/a';       
                    let select = require('xpath.js') ;
                    let dom = require('xmldom').DOMParser ;
                    let xmldoc = new dom().parseFromString(data,"application") ;   
                    let errNodes = select(xmldoc, "//*[local-name()='RegistryError']");
                    let href  = select(xmldoc, "//*[local-name()='Document']/*[local-name()='Include']/@href").toString();
                    if(isEmpty(href)){
                        let doc = select(xmldoc, "//*[local-name()='Document']/text()")
                        ccd =  base64.decode(doc);
                    } else {
                        _ccd = select(xmldoc, "//*[local-name()='ClinicalDocument']")                        
                        //console.log(_ccd.length)
                        if (_ccd.length > 0){
                            ccd = _ccd[0].toString()
                        }
                    }
                    if (DEBUG) {                    
                        fs.writeFile("/tmp/XCA_Retrieve_CCDData_"+oHIE.QE_Name,ccd, function(err) {
                            if(err) { console.log(err); }
                        });         
                    }
                    if (errNodes.length > 0) {
                        errNodes.forEach(function(element, index, theArray) {
                            oHIE.Failure_Description = ""+ errNodes[0].toString() 
                            oHIE.Result = "Failure" ;
                            oHIE.Count += 1;
                            oHIE.ret_count += 1;
                            if (DEBUG) {                    
                                fs.writeFile("XCA_Retrieve_","XCA Retrieve failure =>" +oHIE.QE_Name, function(err) {
                                    if(err) { console.error(err); }
                                });
                            }         
                        });
                    } else  {
                        var nodes = select(xmldoc, "//*[local-name()='RegistryResponse']");
                        if (nodes.length > 0 ){
                            nodes.forEach(function(element, index, theArray) {
                                status = select(element,"@status").toString().split("=")[1].replace(/"/g,'');
                                status = status.split(":").pop(-1);
                                oHIE.Failure_Description = ""  ;
                                oHIE.Count= 0; 
                                oHIE.ret_count = 0;
                                oHIE.Result = status ;
                            });
                        } else {
                            oHIE.Failure_Description = "No Data in Registry Response" 
                            oHIE.Result = "Failure" ; 
                            oHIE.Count += 1;
                            oHIE.ret_count += 1;
                        }
                    }
                    oHIE.Command = "XCA_Retrieve" ;   
                    if (DEBUG) {
                        //console.log("XCARETRIEVE EVAL",oHIE  );
                    }
                    if(callback){
                        callback(oHIE,ccd)
                    }
                     
                } else {
                    oHIE.Count += 1;
                    oHIE.ret_count += 1;
                    oHIE.Result = "Failure" ;
                    oHIE.Command = "XCA_Retrieve" ;
                    oHIE.Failure_Description = "XCA Retirieve: no Data "+response ;                    
                    if (DEBUG){
                        fs.writeFile("/tmp/XCARetrieve_Fail_"+oHIE.QE_Name, "XCA Query response Failure =>", function(err) {
                            if(err) { console.log(err); }
                        });        
                    } 
                    if(callback){
                        callback("error","Error")
                    }
                }   
            }).on("data",function(chunk){

            }); 
        } catch(eX){
            console.error("XCA ERROR Retrieve %s ",eX);
        }      
};
//----------------------------------------------------------------------------------
exports.XCAQuery = (req,res)=>{
    //console.log(req.query.token)
    let params = getSPRLparams(req)
    //console.log("XCAQuery params = %j",params)
    if (! isEmpty(params.qe) ){

        let oHIE = eval(params.dev+"_QElist")[params.qe]
        
        oHIE.MPI_ID = params.qeid
        oHIE.MPI_OID = `${params.qeid.trim()}^^^&amp;${params.mpiid.trim()}&amp;ISO`
        oHIE.Failure_Description =""
        oHIE.DocumentType1 = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"
        oHIE.DocumentType2 = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248"
        oHIE.DocumentStatus ="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"        
        params.oHIE = clone(oHIE)
        params.token = req.query.token
        runxca("XCA_QUERY",params,(data,oH,bundle)=>{
            //res.send(data);
            res.send(bundle);
        });
    } else {
        res.send({error:"No rhio provided"});
    }
}  
//-----------------------------------------------------
let ccd2Bundle = (mpiid,data,callback) =>{
    let _ = require('lodash');
    let bbcms = require('../cda-fhir/lib/parser.js');    
    let makeTransactionalBundle = function (bundle, base, patientId) {
        _.each(bundle.entry, function (value) {
            value.request = {
                'method': (value.resource.resourceType === 'Patient') ? 'PUT' : 'POST',
                'url': (value.resource.resourceType === 'Patient') ? 'Patient/' + patientId : value.resource.resourceType
            };
            value.base = base;
        });
        bundle.type = 'transaction';
        return bundle;
    };
    let stream = require("stream")
    let a = new stream.PassThrough()
    a.write(data)
    a.end()                
    a.pipe(new bbcms.CcdaParserStream(mpiid /* PatientId */))
    .on('data', function (data) {
        var bundle = JSON.stringify(makeTransactionalBundle(data), null, 2);
        if(callback){
            callback(bundle)
        }
        
    })
    .on('error', function (error) {
        console.error(error);
    });        
    
}
//----------------------------------------------------------------------------------
exports.fhir_XCARetrieve = (req,res)=>{
    let params = getSPRLparams(req)
    console.log("\nXCARetrieve\nparams=%j\n\n",params)
    if (! isEmpty(params.qe) ){
        let jobID = date.format('YYYYMMDD-HHmmss-SSS') ;
        let urls = eval(params.dev+"_SPRLurls")
        let oHIE = eval(params.dev+"_QElist")[params.qe]
        oHIE.sprlurl = urls[params.qe]
        oHIE.MPI_ID = params.qeid
        oHIE.MPI_OID = `${params.qeid.trim()}^^^&amp;${params.mpiid.trim()}&amp;ISO`
        oHIE.Failure_Description =""
        oHIE.DocumentType1 = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"
        oHIE.DocumentType2 = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248"
        oHIE.DocumentStatus ="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"   
        params.oHIE = clone(oHIE)
        //console.log("oHIE=%j",params.oHIE)

        
        runxca("XCA_QUERY",params,(data,oH,bundle)=>{        
        //runxca("XCA_QUERY",params,(err,oH)=>{
            XCA_Retrieve(oH,params,(o,ccd)=>{
                if(params.type == 'JSON')  {  
                    // convert to bundle
                    ccd2Bundle(o.MPI_ID+"-"+o.QE_ID,ccd,(bundle)=>{
                        //fs.writeFile("testout5.json",bundle)
                        //console.log(bundle); // Result bundle
                        res.setHeader('Authorization', 'Bearer:'+params.tokenid);    
                        res.setHeader('Content-Type', 'application/json');    
                        res.send(bundle);
                    
                    })
                } else {
                    res.setHeader('Authorization', 'Bearer:'+params.tokenid);    
                    res.setHeader('Content-Type', 'application/xml');    
                    res.send(ccd);
                }
            })
        });
        
    } else {
        res.send({error:"No rhio provided"});
    }
}  
//----------------------------------------------------------------------------------
exports.XCARetrieve = (req,res)=>{
    let params = getSPRLparams(req)
    console.log("\nXCARetrieve\nparams=%j\n\n",params)
    if (! isEmpty(params.qe) ){
        let urls = eval(params.dev+"_SPRLurls")
        let oHIE = eval(params.dev+"_QElist")[params.qe]

        oHIE.sprlurl = urls[params.qe]
        oHIE.MPI_ID = params.qeid
        oHIE.MPI_OID = `${params.qeid.trim()}^^^&amp;${params.mpiid.trim()}&amp;ISO`
        oHIE.Failure_Description =""
        oHIE.DocumentType1 = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"
        oHIE.DocumentType2 = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248"
        oHIE.DocumentStatus ="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"   

        params.oHIE = clone(oHIE)
        console.log("oHIE=%j",oHIE)
        runxca("XCA_QUERY",params,(data,oH,bundle)=>{        
        //runxca("XCA_QUERY",params,(err,oH)=>{
            XCA_Retrieve(oH,params,(o,ccd)=>{
                if(params.type == 'JSON')  {  
                    // convert to bundle
                    ccd2Bundle(o.MPI_ID+"-"+o.QE_ID,ccd,(bundle)=>{
                        //fs.writeFile("testout5.json",bundle)
                        //console.log(bundle); // Result bundle
                        res.setHeader('Authorization', 'Bearer:'+params.tokenid);    
                        res.setHeader('Content-Type', 'application/json');    
                        res.send(bundle);
                    
                    })
                } else {
                    res.setHeader('Authorization', 'Bearer:'+params.tokenid);    
                    res.setHeader('Content-Type', 'application/xml');    
                    res.send(ccd);
                }
            })
        });
    } else {
        res.send({error:"No rhio provided"});
    }
}  
//----------------------------------------------------------------------------------
let runxca = (name,params,callback)=>{
    if (isEmpty(name)) {
        name = "XCA_QUERY" ;
    }    
    if (isEmpty(params.dev)) {
        params.dev =config.devEnv ;
    }        
    let urls = eval(params.dev+"_SPRLurls")
    let url = urls.XCA_QUERY;   
    let redirecturl = urls.urlXCA_QUERY;   
    console.log("runxca SPRL url=%s",url)
    token(params.oHIE.QE_Name,"statewide",(token)=>{
        soapprofiles.xcaQuery('XCA_QUERY',params,(err,xmldata) => {
            fs.writeFile("/tmp/sprl_XCA_Query_XMLDATA_"+params.oHIE.QE_Name, xmldata, function(err) {
                if(err) { console.error(err); }
            });
            if(err){
                console.error(err)
            }
            let args = {
                data: xmldata,
                headers: {  
                    "Content-Type": "text/xml; charset=utf-8" ,
                    "Authorization":"Bearer "+token ,
                    "JWI":params.tokenid,
                    "url":redirecturl
                }  
            };
            console.log("\n\nXCA Query headers %j\n\n\n",args.headers)
            postXCAQuery(url,args,params.oHIE,(data,oH,bundle)=>{
                fs.writeFile("/tmp/sprl_XCA_Retrieve_bundle_"+oH.QE_Name, JSON.stringify(bundle), function(err) {
                    if(err) { console.error(err); }
                    fs.writeFile("/tmp/sprl_XCA_Retrieve_oHIE_"+oH.QE_Name, JSON.stringify(oH), function(err) {
                        if(err) { console.error(err); }
                    });
                });
                if(callback){
                    callback(data,oH,bundle) ;
                }
            });
        });    
    })
   
};
//---------------------------------------------------------------------------------------------------------------------------
let ccdtextdiv =function(data){

let div = `    
<div xmlns="http://www.w3.org/1999/xhtml">
<table style="border: 1pt inset #00008b; line-height: 10pt; width: 100%;" class="header_table">

</div>
`
return {status:"generated",div} ;
}
//----------------------------------------------------------------------------------------------------------------------------
let postXCAQuery= function(url,args,oHIE,callback){
    let cl = new Client();
    let status = "n/a";        
    let qeoid = "n/a"
    let docId = "n/a";    
    //console.log(url)
    try {
        if (DEBUG) {                    
            console.log("\npostXCAQuery oHIE=%j\n\n",oHIE);
            fs.writeFile("/tmp/fhir_XCA_Query_"+oHIE.QE_Name+ '_'+oHIE.MPI_ID , args.data, function(err) {
                if(err) { console.error(err); }
            });         
        }     
        let o = clone(oHIE)
        cl.post(url, args, function (data, response) {            
            data = data.toString();
            data = data.replace(/'/g,'') ;
            o.xmlres = data
            o.document_size ="n/a"
            if (DEBUG) {                    
                fs.writeFile("/tmp/fhir_XCA_Query_Response_"+o.QE_Name+ '_'+o.MPI_ID+".json", CJSON.stringify(response), function(err) {
                    if(err) { console.log(err); }
                });         
            }        
                // create a XCAQueryResponse Bundle
            let xcaresponeBundle = {
                resourceType: "DocumentReference",
                type:"XCA Retrieve Response",
                title:"",
                language:"en-US",
                names:[],
                masterIdentifier:{
                    "system": "SHINY",
                    "value": "urn:uuid:0c3151bd-1cbf-4d64-b04d-cd9187a4c6ff"
                },
                identifier: [],
                status:"",
                docStatus:"",
                text:{status:"generated",div:'"<div xmlns="http://www.w3.org/1999/xhtml"></div>'},
                meta: {
                    lastUpdated: moment(),
                    url:url,
                    //args:args,
                    //data:data
                },
                link:[],
                totalResults:"",
                
                entry:[]                   
            }
            //oHIE.query = respBundle;
            if ( ! isEmpty(data) ) {
                let select = require('xpath.js') ;
                let dom = require('xmldom').DOMParser ;
                var doc = new dom().parseFromString(data) ;   
                var errornodes = select(doc, "//*[local-name()='RegistryError']");                    
                if (errornodes.length > 0 ) {
                    status = errornodes[0].toString();
                    o.Result = "Failure" ;
                    o.Failure_Description = status ;
                } 
                //---------------------------------------------------------------------
                var nodes = select(doc, "//*[local-name()='ExternalIdentifier']"); 
                if (nodes.length > 0 ) {
                    nodes.forEach(function(element, index, theArray) {
                        let attr = select(element,"@identificationScheme").toString().split("=")[1].replace(/"/g,'') ;
                        let value = select(element,"@value").toString().split("=")[1].replace(/"/g,'') ;
                        if (attr =="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" ) {
                            xcaresponeBundle.meta.patientID = value
                            qeoid = value;
                        }
                        if (attr =="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" ) {
                            xcaresponeBundle.masterIdentifier = value
                            docId = value;
                        }
                    });
                } else {

                }               
                var slots = select(doc,"//*[local-name()='Slot']" )
                if(slots.length > 0) {
                    slots.forEach(function (slot,ind){
                        let sl = new dom().parseFromString(slot.toString()) ;                                
                        let name = select(sl,"@name").toString().split("=")[1].replace(/"/g,'') ;
                        let value = select(sl, "//*[local-name()='Value']/text()").toString();                                  
                        if (name =="repositoryUniqueId" ) {
                            o.Repository_Id = value
                        }
                        if (name =="sourcePatientId" ) {
                            o.sourcePatientId = value
                        }
                        if (name =="documentAvailablity" ) {
                            o.documentAvailablity = value
                        }
                        if (name =="size" ) {
                            o.document_size = value
                        }                
                    })  
                } else {

                }
                o.Result = "Success" ;
                o.Failure_Description = "";
                o.Document_Id = docId ;
                o.OID = qeoid
              
                //---------------------------------------------------------------------
                nodes = select(doc, "//*[local-name()='Classification']"); 
                if (nodes.length > 0 ) {
                    let identifier = []
                    nodes.forEach(function(element, index, theArray) {
                        let nodeRepresentation = select(element,"@nodeRepresentation").toString().split("=")[1].replace(/"/g,'') ;
                        let id = select(element,"@id").toString().split("=")[1].replace(/"/g,'') ;
//console.log("identifier=%j",identifier)
                        identifier.push({use:"usual",type:id,system:"",value:nodeRepresentation,assigner:""})
                        
                        let slots = select(element, "//*[local-name()='Slot']")
                        //xcaresponeBundle.slots = {count:slots.length,entry:[]}
                        slots.forEach(function(element, index, theArray) {
                            let name = select(element,"@name").toString().split("=")[1].replace(/"/g,'') ;
                            let value = select(element, "//*[local-name()='Value']/text()")[index].toString();      
                            if (name =="repositoryUniqueId" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="sourcePatientID" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="documentAvailablity" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="languageCode" ) {
                                xcaresponeBundle.language = value
                            }                                
                            let names = select(element, "//*[local-name()='LocalizedString']")
                            let n= []
                            names.forEach(function(element, index, theArray) {
                                let name = select(theArray[index],"@value").toString().split("=")[1].replace(/"/g,'') ;                                    
                                n.push({name})
                            })
                            xcaresponeBundle.entry.push({resource:{resourceType:"Slot",comment:name,value,names:n}})
                        })
                                        
                    });
                    xcaresponeBundle.identifier = identifier ;
                } else {
                    o.Result = "Failure" ;
                    o.Failure_Description = data;
                    xcaresponeBundle = data
                }
                //---------------------------------------------------------
                nodes = select(doc, "//*[local-name()='ExtrinsicObject']"); 
                if (nodes.length > 0 ) {
                    nodes.forEach(function(element, index, theArray) {
                        let id = select(element,"@id").toString().split("=")[1].replace(/"/g,'') ;
                        let home = select(element,"@home").toString().split("=")[1].replace(/"/g,'') ;
                        let mimeType = select(element,"@mimeType").toString().split("=")[1].replace(/"/g,'') ;
                        let objectType = select(element,"@objectType").toString().split("=")[1].replace(/"/g,'') ;
                        let status = select(element,"@status").toString().split("=")[1].replace(/"/g,'') ;
                        /*
                        let names = select(element, "//*[local-name()='LocalizedString']")
                        names.forEach(function(element, index, theArray) {
                            let name = select(element,"@value").toString().split("=")[1].replace(/"/g,'') ;
                            
                            xcaresponeBundle.names.push({name})
                        })
                        */

                        let slots = select(element, "//*[local-name()='Slot']")
                        //xcaresponeBundle.slots = {count:slots.length,entry:[]}
                        slots.forEach(function(element, index, theArray) {
                            let name = select(element,"@name").toString().split("=")[1].replace(/"/g,'') ;
                            let value = select(element, "//*[local-name()='Value']/text()")[index].toString();      
                            if (name =="repositoryUniqueId" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="sourcePatientID" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="documentAvailablity" ) {
                                xcaresponeBundle.meta[name] = value
                            }
                            if (name =="languageCode" ) {
                                xcaresponeBundle.language = value
                            }                                
                            xcaresponeBundle.entry.push({resource:{resourceType:"Slot",comment:name,value}})
                        })                                
                    });
                } else {
                    o.Result = "Failure" ;
                    o.Failure_Description = data;
                    xcaresponeBundle = data
                }
                if (callback){
                    //console.log("bundle=%j\n",xcaresponeBundle)
                    //console.log("\n\nResponsedata = %s\n\n\n",data)
                    callback(data,o,xcaresponeBundle) ;
                }                    
            } else {
                o.Failure_Description =  "Query Error: no Data for :"+JSON.stringify(args,null,2) ;  
                if (callback){
                    callback("Error",o) ;
                }
                if (DEBUG){
                    fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+JSON.stringify(args,null,2), function(err) {
                        if(err) { console.log(err); }
                    });        
                } 
            }
        });
    } catch (Ex) {
            oHIE.Command = "XCA_Query" ;
            oHIE.Document_Id = docId ;
            oHIE.Failure_Description = "XCA Query Error:"+Ex ;
            oHIE.Count += 1;
            oHIE.qry_count += 1;
            if (callback){
                callback("error:"+Ex,oHIE) ;
            }
            fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+Ex, function(err) {
                if(err) { console.log(err); }
            });         
    };
};
//----------------------------------------------------------------------------------
let getXCA_Query = function (name,element,callback){
    if (isEmpty(name)) {
        name = "XCA_QUERY";
    }
    let oid = element.job.includes('SMPI') ? element.SMPI_OID :element.MPI_OID    
    if(DEBUG) {
        console.log("XCA QUERY OID = %s",oid)
    }
    var soapEnv =  `
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:wsa="http://www.w3.org/2005/08/addressing"  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"> 
        <SOAP-ENV:Header> 
            <wsa:Action SOAP-ENV:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayQuery</wsa:Action> 
            <wsa:MessageID>urn:uuid:2E8C5DF4-1140-11E6-BF83-5056AC0A4F00</wsa:MessageID> 
            <wsa:ReplyTo> 
                <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> 
            </wsa:ReplyTo> 
            <wsa:To>https://sprl.shinnyapi.org:9443/sXCPDQuery</wsa:To> 
            <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"> 
                <saml:Assertion ID="XId-2E8C86BC-1140-11E6-BF83-5056AC0A4F00" IssueInstant="2016-05-03T15:03:23.791Z" Version="2.0" xmlns="" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"> 
                    <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:Issuer> 
                    <saml:Subject> 
                        <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:NameID> 
                    </saml:Subject> 
                    <saml:AuthnStatement AuthnInstant="2016-05-03T15:03:23.791Z"> 
                    <saml:SubjectLocality Address="192.168.120.98" DNSName="SPRL.SHINNYAPI.ORG"/> 
                    <saml:AuthnContext> 
                        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef> 
                    </saml:AuthnContext> 
                    </saml:AuthnStatement> 
                    <saml:AttributeStatement> 
                    <saml:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">SystemMonitor</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">sPRLMonitoringTool</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.3.2591.400</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue xsi:type="s:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue> 
                            <s01:Role code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/> 
                        </saml:AttributeValue> 
                    </saml:Attribute> 
                    <saml:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri"> 
                        <saml:AttributeValue> 
                            <s01:PurposeOfUse code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/> 
                        </saml:AttributeValue> 
                    </saml:Attribute> 
                    </saml:AttributeStatement> 
                </saml:Assertion> 
            </Security> 
        </SOAP-ENV:Header> 
        <SOAP-ENV:Body> 
            <query:AdhocQueryRequest xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0"> 
                <query:ResponseOption returnComposedObjects="true" returnType="LeafClass"/> 
                <rim:AdhocQuery id="urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"> 
                    <rim:Slot name="$XDSDocumentEntryPatientId"> 
                    <rim:ValueList> 
                        <rim:Value>'${oid}'</rim:Value> 
                    </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryStatus"> 
                    <rim:ValueList> 
                        <rim:Value>${element.DocumentStatus}</rim:Value> 
                    </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryType"> 
                        <rim:ValueList> 
                            <rim:Value>${element.DocumentType1}</rim:Value> 
                            <rim:Value>${element.DocumentType2}</rim:Value> 
                        </rim:ValueList> 
                    </rim:Slot> 
                    <rim:Slot name="$XDSDocumentEntryFormatCode">
                        <rim:ValueList>
                        <rim:Value>CCDA</rim:Value>
                        </rim:ValueList>
                    </rim:Slot>                
                 </rim:AdhocQuery> 
            </query:AdhocQueryRequest> 
        </SOAP-ENV:Body> 
        </SOAP-ENV:Envelope>
`;

         callback(soapEnv,"",oid);
         if (DEBUG ) {
            fs.writeFile("/tmp/XCA_Query_SOAP_"+element.QE_Name, soapEnv, function(err) {
                if(err) { console.log(err); }
            });
        }

}
//---------------------------------------------------------------------------------
let processXCA = function(job_type,jobID,data,index){
    if(index < data.length ) {
        let oHIE = clone(data[index]);
        getXCA_Query('XCA_QUERY',oHIE,(xmldata,err,oid) => {
            let url = SPRLurls.XCA_QUERY;
            let args = {
                data: xmldata,
                async:true,
                headers: {
                    "Content-Type": "application/soap+xml;charset=UTF-8",
                    "Accept-Encoding": "gzip,deflate",
                    "Proxy-Connection": "Keep-Alive",
                    "Host":"192.168.180.193:9445",
                    "POST":url +' HTTP/1.1'
                }
            };
            oHIE.Repository_Id= "n/a"
            oHIE.Document_Id = "n/a"
            oHIE.document_size = "n/a"           
            postXCAQuery(url,args,oHIE,jobID,(newoHIE,xml,jobID,duration)=>{
                msinsertRes( newoHIE,jobID,duration,err=>{
                    if (oHIE.job == 'SMPI' ){
                        ExtractDocList(xml,oHIE,jobID,(doclist,resp)=>{
                            resInsert(resp,jobID,0,(err )=>{
                                if(doclist.length > 0 ) {                            
                                    processXCARetrieve(doclist,0,jobID,(result)=>{})    
                                }
                            });
                        })                        
                    } else if (oHIE.job == 'QEMPI'){
                        if(newoHIE.Result == "Success" ) {
                            _XCARetrieve(newoHIE.Repository_Id,newoHIE.Document_Id,newoHIE,index,jobID)
                        } 
                    }
                    processXCA(job_type,jobID,data,index+1)    
                })
            });            
        });
    } else {
        setTimeout(function () {
            createNewJobs(job_type,jobID)
        }, 1200);
    }
}

//----------------------------------------------------------------------------------------------------------------------------
let runXCAQuery= function(args,oHIE,callback){
    let cl = new Client();
    let status = "n/a";        
    let docId = "n/a";
    let url = STAGE_SPRLurls.XCA_QUERY 
    try {
        //if (DEBUG) {                    
            //console.log("oHIE=%j",oHIE);
            fs.writeFile("/tmp/sprl_XCA_Query_"+oHIE.QE_Name, args.data, function(err) {
                if(err) { console.error(err); }
            });         
        //}     
        cl.post(url, args, function (data, response) {            
            data = data.toString();
          //  if (DEBUG) {                    
                fs.writeFile("/tmp/sprl_XCA_Query_Response_"+oHIE.QE_Name, data, function(err) {
                    if(err) { console.log(err); }
                });         
          //  }        
                // create a XCAQueryResponse Bundle
            let xcaresponeBundle = {
                resourceType: "DocumentReference",
                type:"XCA Retrieve Response",
                title:"",
                language:"en-US",
                names:[],
                masterIdentifier:{
                    "system": "SHINY",
                    "value": "urn:uuid:0c3151bd-1cbf-4d64-b04d-cd9187a4c6ff"
                },
                identifier: [],
                status:"",
                docStatus:"",
                text:{status:"generated",div:'"<div xmlns="http://www.w3.org/1999/xhtml"></div>'},
                meta: {
                    lastUpdated: moment(),
                    url:url,
                    //args:args,
                    //data:data
                },
                link:[],
                totalResults:"",
                
                entry:[]                   
            }
            //oHIE.query = respBundle;
            if ( ! isEmpty(data) ) {
                let select = require('xpath.js') ;
                let dom = require('xmldom').DOMParser ;
                let doc = new dom().parseFromString(data) ;   
                let errornodes = select(doc, "//*[local-name()='RegistryError']");                    
                if (errornodes.length > 0 ) {
                    let status = errornodes[0].toString();
                    oHIE.Count += 1;
                    oHIE.qry_count += 1;
                    oHIE.Result = "Failure" ;
                    oHIE.Command = "XCA_Query" ; 
                    oHIE.Document_Id = docId ;
                    oHIE.Failure_Description = status ;
                    if (callback){
                        callback("error:"+status,oHIE) ;
                    }
                } else { 
                    let nodes = select(doc, "//*[local-name()='ExternalIdentifier']"); 
                    if (nodes.length > 0 ) {
                        nodes.forEach(function(element, index, theArray) {
                            let attr = select(element,"@identificationScheme").toString().split("=")[1].replace(/"/g,'') ;
                            let value = select(element,"@value").toString().split("=")[1].replace(/"/g,'') ;
                            if (attr =="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" ) {
                                xcaresponeBundle.meta.patientID = value
                                MPI = value;
                            }
                            if (attr =="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" ) {
                                xcaresponeBundle.masterIdentifier = value
                                docId = value;
                            }
                        });
                        oHIE.Result = "Success" ;
                        oHIE.Count = 0;
                        oHIE.qry_count =0 ;
                        oHIE.Failure_Description = "" ;
                        oHIE.Document_Id = docId ;
                        oHIE.Command = "XCA_Query" ;
                    }
                    //---------------------------------------------------------------------
                    if (callback){
                        //console.log("bundle=%j\n",xcaresponeBundle)
                        callback(data) ;
                    }

                }    
            } else {
                oHIE.Result = "Failure" ;
                oHIE.Count += 1;
                oHIE.qry_count += 1;
                oHIE.Document_Id = docId ;
                oHIE.Command = "XCA_Query" ;
                oHIE.Failure_Description =  "Query Error: no Data for :"+JSON.stringify(args,null,2) ;  
                if (callback){
                    callback("Error",oHIE) ;
                }
                if (DEBUG){
                    fs.writeFile("/tmp/sprl_XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+JSON.stringify(args,null,2), function(err) {
                        if(err) { console.log(err); }
                    });        
                } 
            }
        });
    } catch (Ex) {
            oHIE.Command = "XCA_Query" ;
            oHIE.Document_Id = docId ;
            oHIE.Failure_Description = "XCA Query Error:"+Ex ;
            oHIE.Count += 1;
            oHIE.qry_count += 1;
            if (callback){
                callback("Error:"+Ex,oHIE) ;
            }
            fs.writeFile("/tmp/sprl_XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+Ex, function(err) {
                if(err) { console.log(err); }
            });         
    };
};


//----------------------------------------------------------------------------------
let XCA_Retrieve = (oHIE,params,callback)=>{
    if(isEmpty(params.dev)){
        params.dev = "STAGE"
    }
    let url = eval(params.dev+"_SPRLurls").XCA_RETRIEVE
    oHIE.sprlurl = url
    params.oHIE = clone(oHIE);
    getXCA_Retrieve('XCA_RETRIEVE',oHIE,(xmldata) => {          
            fs.writeFile("/tmp/XCA_Retrieve_"+oHIE.QE_Name, xmldata, function(err) {
                if(err) { console.log(err); }
            });
            let args = {
                data: xmldata,
                async:false,
                headers: {  
                    "Content-Type": "text/xml; charset=utf-8" 
                }  
            };
            postXCARetrieve(url,args,oHIE,(oHIE,ccd)=>{
                if(callback){
                    callback(oHIE,ccd)
                }
            });
    });
}     

//-------------------------------------------------------------------------------------
let getXCA_Retrieve = function (name,oHIE,callback){
    if (isEmpty(name)) {
        name = "XCA_RETRIEVE";
    }
    let soapEnv = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">\
          <soap:Header>\
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="true">\
              <saml2:Assertion xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" ID="_655651ee4e7c4c76a678f5e4d7f279a6" IssueInstant="2016-05-02T15:58:31.015Z" Version="2.0">\
                <saml2:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=SAML User,OU=SU,O=SAML User,L=Los Angeles,ST=CA,C=US</saml2:Issuer>\
                <saml2:Subject>\
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org, OU=Domain Control Validated</saml2:NameID>\
                  </saml2:Subject>\
                <saml2:AuthnStatement AuthnInstant="2016-05-02T15:58:30.000Z" SessionIndex="987">\
                  <saml2:SubjectLocality Address="158.147.185.168" DNSName="158.147.185.168"/>\
                  <saml2:AuthnContext>\
                    <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</saml2:AuthnContextClassRef>\
                  </saml2:AuthnContext>\
                </saml2:AuthnStatement>\
                <saml2:AttributeStatement>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">SystemMonitor</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">sPRLMonitoringTool</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue>\
                      <hl7:Role xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="hl7:CE"/>\
                    </saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue>\
                      <hl7:PurposeOfUse xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="hl7:CE"/>\
                    </saml2:AttributeValue>\
                  </saml2:Attribute>\
                </saml2:AttributeStatement>\
              </saml2:Assertion>\
              <wsu:Timestamp wsu:Id="TS-460585">\
                <wsu:Created>2017-05-02T15:58:31.014Z</wsu:Created>\
                <wsu:Expires>2017-05-02T16:58:31.014Z</wsu:Expires>\
              </wsu:Timestamp>\
            </wsse:Security>\
            <Action xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayRetrieve</Action>\
            <MessageID xmlns="http://www.w3.org/2005/08/addressing">urn:uuid:48fccf69-1640-4774-9dd3-de1fc2a7614b</MessageID>\
            <To xmlns="http://www.w3.org/2005/08/addressing">https://sprl.shinnyapi.org:9443/sXCARetrieve</To>\
            <ReplyTo xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">\
              <Address>http://www.w3.org/2005/08/addressing/anonymous</Address>\
            </ReplyTo>\
          </soap:Header>\
          <soap:Body>\
            <RetrieveDocumentSetRequest xmlns="urn:ihe:iti:xds-b:2007" xmlns:ns2="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" xmlns:ns3="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" xmlns:ns4="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" xmlns:ns5="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0">\
              <DocumentRequest>\
                <HomeCommunityId>urn:oid:2.16.840.1.113883.3.2591.400</HomeCommunityId>\
                <RepositoryUniqueId>${oHIE.Repository_Id}</RepositoryUniqueId>\
                <DocumentUniqueId>${oHIE.Document_Id}</DocumentUniqueId>\
              </DocumentRequest>\
            </RetrieveDocumentSetRequest>\
          </soap:Body>\
        </soap:Envelope>`;
   
    if (callback) {
        callback(soapEnv,"");
    }
    return soapEnv
};

//----------------------------------------------------------------------------------
module.exports.htmlsprllist = function(req, res){  // output to jqgrid
   let mpiid = req.params.mpiid ;
   setup_MPIcolumns( (colSetup) => {   
        res.render('mpi',{mpiid:mpiid,page_title:"HeartBeat Monitor ",colsetup:colSetup});                        
   });        
}
//---------------------------------------------------
let setup_MPIcolumns = function(callback) {
            let colsetup = { 
                url:'/SPRL/testmpi', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'QE',name:'qe', editable:false ,width:150},
                    {label:'oid',name:'root', editable:false , width:250},
                    {label:'mpi',name:'ext', editable:false ,width:100 },
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: true,            
                caption: "Config"          
            }
            callback( colsetup);  
};
//-------------------------------------------------------------------------------
exports.querypix = function(req,res){
    let params = getSPRLparams(req)
    let oid = params.oid
    console.log("\n\nPIX params = \n%j\n\n",params)
    if(isEmpty(oid)){
        oid = qeconfig[params.qe].OID;
    }
    params.qeid = oid;
    querypix1(params,req,(err,data,reqid,pixParameters)=>{
       // data = isEmpty(data) ?{"error":err}:data
       console.log(pixParameters)
        res.send(pixParameters) ;
    }); 
};
//----------------------------------------------------------------------------------
let querypix1 = exports.querypix1 = function (params, req,callback) {
    console.log("\n\nPIX1 params = \n%j\n\n",params)
    getPixUsers(params, req,(err, data,reqid,pixParameters) => {
        data = parseresult(data,err)
        callback(err, data,reqid,pixParameters);
   });
/*
    getPixUsers(params,req, (err, data) => {
        data = isEmpty(data) ? { "error": err } : data
        callback(err,data);
    });
*/
};
//------------------------------------------------------------------------------------
let querypixInternal = exports.querypixInternal = function (req,params,oid,mpiid,qe,transactionID,callback) {
    //console.log("\n\nPIX params = \n%j\n\n", params)
    params.qeid=oid
    params.mpiid= mpiid
    params.qe= qe 
    params.transactionID=transactionID 
   //console.log("\n\nquerypixInternal PIX params = \n%j\n\n", params)
    getPixUsers(params, req,(err, data,reqid,pixParameters) => {
         data = parseresult(data,err)
        callback(err, data,reqid,pixParameters);
    });
};
//-------------------------------------------------------------------------------------
let getPixUsers = exports.getPixUsers = (params,req,callback)=>{
    let cl = new Client();
    let now = moment();
    let seq = params.seq
    let transactionID = params.transactionID 
    if(isEmpty(transactionID)){
        transactionID = utils.getuuid()
    }
    if(isEmpty(seq)) {
        seq = 0
    }
    if (1 == 1) {
    //if(isValid(params.tokenid)) {
        try {
            let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
            let sprlurl = config[config.devEnv+"_SPRLurls"].PIX
            let pdata = {}
            let pixParameters = []
     
            pdata.now = moment().format('YYYYMMDDHHmm');  
            pdata.expire = moment().add(5, 'minutes').format('YYYYMMDDHHmm');
            pdata.qeid = params.qeid;
            pdata.id = params.mpiid;
            pdata.shinny = "2.16.840.1.113883.3.2591.500.1";
            pdata.qe = params.qe;
            pdata.sprlurl = params.sprlurl
            soapprofiles.getPIX_Query('PIX_QUERY',pdata,(err,xml)=>{
                let args = {
                    data: xml,
                    headers: {
                        "Content-Type": "application/soap+xml;charset=UTF-8",
                        "Accept-Encoding": "gzip,deflate",
                        "Proxy-Connection": "Keep-Alive",
                        "POST":sprlurl +' HTTP/1.1'
                    }
                };
                params.seq = seq+1
                let parse = require('xml-parser');
                let obj = parse(xml);
                timediff.endtime = moment()
                timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                let data = { params,profile:args,req,result:xml,elapsedtime:timediff.elapsed}
                let reqid = 0
                //audit.insertRequest("sprl/PIX",data,(err,result)=>{
                  //  if(result && result.length > 0 ) {
                   //     reqid = result[0].ID
                  //  }
                  reqid = 0
                    timediff = {starttime:moment(),endtime:moment(),elapsed:0}
                    cl.post(sprlurl, args, function (data, response) {       
                        timediff.endtime = moment()
                        timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                        params.seq += 1
                        params.reqid = reqid
                        params.transactionID = transactionID;
                        let qrydata = {req,params,result:data,response,elapsedtime:timediff.elapsed }
                        //audit.insertResponse(qrydata,(err,result)=>{            
                            data = data.toString();
                            let code = response.statusCode +" - "+response.statusMessage
                            if ( ! isEmpty(data) ) {
                                let select = require('xpath.js') ;
                                let dom = require('xmldom').DOMParser ;
                                let doc = new dom().parseFromString(data) ;   
                                let qryRes = select(doc, "//*[local-name()='queryResponseCode']/@code")
                                if(qryRes){
                                   // console.log(qryRes)
                                    qryRes = qryRes?qryRes.toString().split("=")[1].replace(/"/g,''):"error"
                                }
                                if (qryRes != "OK" ) {
                                    let qryid = select(doc, "//*[local-name()='queryId']/@root").toString().split("=")[1].split(",")[0].replace(/"/g,'');
                                    let alist = [];
                                    let olist = {};
                                    let details = qryRes == 'AA'? `ID:${params.mpiid} Found`:qryRes=='NF'?params.mpiid+' Not Found':`Unknown response ${qryRes}`
                                    let err = {id:params.mpiid,
                                                OID:params.qeid,
                                                now ,
                                                sprlurl,
                                                qryid,
                                                "userurl":params.url,
                                                alist,
                                                olist,
                                                qe:params.qe,
                                                fname:"Warning ",
                                                lname:details
                                            }
                                    let pix = fhirprofiles.pixQueryResponseBundle(err);
                                    let errporf = fhirprofiles.errorresponse(sprlurl,details,response) 
                                    pix.resource.entry = [errporf]
                                    params.reqid = reqid  
                                    let qrydata = {req,params,result:pix,response:data,profile:pix,elapsedtime:timediff.elapsed }
                                    params.seq += 1
                                  //  audit.insertResponse(qrydata,(err,result)=>{
                                     //   callback(err, pix,response,[])   
                                   // })
                                } else { 
                                    //get the number of AA's 
                                    let alist = [];
                                    let olist = {};
                                    let fname = select(doc, "//*[local-name()='given']/text()");
                                    let lname = select(doc, "//*[local-name()='family']/text()");
                                    let patients = select(doc, "//*[local-name()='patient']/*[local-name()='id']");
                                    let aa = select(doc, "//*[local-name()='registrationEvent']/*[local-name()='id']/@assigningAuthorityName").toString().split("=")[1].replace(/"/g,'');
                                    let aaroot = select(doc, "//*[local-name()='registrationEvent']/*[local-name()='id']/@root").toString().split("=")[1].replace(/"/g,'');
                                    let aaext = select(doc, "//*[local-name()='registrationEvent']/*[local-name()='id']/@ext").toString();
                                    let qryid = select(doc, "//*[local-name()='queryId']/@root").toString().split("=")[1].split(",")[0].replace(/"/g,'');
                                    if (patients.length > 0 ){
                                        patients.forEach(function(element, index) {
                                            let qename = select(element,"@assigningAuthorityName").toString().split("=")[1].replace(/"/g,'');
                                            let ext = select(element,"@extension").toString().split("=")[1].replace(/"/g,'');
                                            let root = select(element,"@root").toString().split("=")[1].replace(/"/g,'');
                                            let identifier = {
                                                "use": "usual",
                                                "system": root,
                                                "value": ext,
                                                "assigner": {
                                                    "display":qename                                            }
                                            }
                                            if(params.qeid != root) {
                                                alist.push(identifier)
                                                olist[qename] = identifier
                                                let parameter = {
                                                    "name": qename  ,
                                                    "valueIdentifier":{
                                                        "use":"usual",
                                                        "system" :root ,
                                                        "value":ext 
                                                    }  
                                                }
                                                pixParameters.push(parameter)
                                            }
                                        });
                                    }
                                    let pixParameterProfile = fhirprofiles.pixParameters({parameters:pixParameters})
                                    data = {id:params.mpiid,now,olist,alist,fname,lname,url:params.url,qryid,aa,aaroot,aaext,"userurl":params.url,sprlurl:params.sprlurl,ip:params.ip,"searchtime":0}        
                                    let pix = fhirprofiles.pixQueryResponseBundle(data);
                                    params.reqid = reqid
                                    let qrydata = {req,params,data,response:data,profile:pix,elapsedtime:timediff.elapsed ,result:{pix,pixParameterProfile}}
                                    params.seq += 1
                                   // audit.insertResponse(qrydata,(err,result)=>{
                                        if(callback){
                                            callback(null,pix,response,pixParameterProfile) ; 
                                        }
                                   // })
                                }    
                            } else {
                                    callback({"application":"PIX Query","url":sprlurl,"code":code},data,0)
                            }    
                        //})      
                    })
                    cl.on('socket',  (socket) => {
                        socket.setTimeout(3000);  
                        socket.on('timeout', function() {
                            console.log("timeout for %s",sprlurl)
                            req.abort();
                        });
                    });
                    cl.on("error",(err)=>{
                    //   callback(` {error:${err}}`,xml)
                        timediff.endtime = moment()
                        timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                        console.log(`error:  cl.post(sprl=${sprlurl}, args=${args} ) error:\n%j`,err)
                        let qrydata = {req,params,result:err,elapsedtime:timediff.elapsed }
                        //audit.insertqeResponse(qrydata,(err,result)=>{  
                            params.seq += 1
                     //  audit.insertResponse(qrydata,(err,result)=>{

                     //   })
                    });
               //})
            });
        } catch (Ex) {
            console.log("getPixusers=%s",Ex)
            callback("getPixUsers Error: "+Ex,[],0)        
        };
    } else{
        callback("Invalid token",[],0)        
    }
    return true
}
//-------------------------------------------------------------------------------------
let isValid = (tokenid,callback)=>{
    //console.log("\n\nisValid('%s')",tokenid)
    if(isEmpty(tokenid)){
        if(callback){
          callback( false)
        }                                   
    } else {
        // do lookup
        login.getToken(tokenid,(err,result)=>{
         // console.log("result=%jerr=%s",result,err)
          if(callback){
            callback( ! isEmpty(result))
          }                    
        })        
    }
}
//------------------------------------------------------------------------------------------------------------
let getSPRLparams = (req,callback)=>    {
    let oauth = req.headers['Authorization'] 
    let token = req.query.token
    let input = JSON.parse(JSON.stringify(req.body,null,2)); //POsT DATA should be a Patient Profile
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let userid = req.query.userid;
    let password = req.query.password;
    let secret = req.query.secret;
    let identifier = req.query.identifier;
    let mpiid = req.query.mpiid;    
    let qe = req.query.qe;    
    let oid = req.query.oid;    
    let sub = req.query.sub;     
    let target = req.query.target;    
    let qeid = req.query.qeid;    
    let url = req.getUrl();
    let tokenid ='';    
    let _format = req.query._format
    let _id = req.query._id
    let given = req.query.given
    let family = req.query.family
    let birthdate = req.query.birthdate
    let address = req.query.address
    let city = req.query.city;
    let postalcode = req.query.postalcode;
    let gender = req.query.gender
    let contact = req.query.contact
    let telecom = req.query.telecom
    let jsondata="";
    let dev = req.query.dev;
    let type = req.query.type;
    let _type = req.query._type;
    let sprlurl = req.query.sprlurl;
    if(isEmpty(token)){
        token = req.params.token
    }   
    if(isEmpty(oid)){
        oid = req.params.oid
    } 
    if(isEmpty(userid)){
        userid  = req.params.userid
    } 
    if(isEmpty(password)){
        password  = req.params.password
    } 
    if(isEmpty(secret)){
        secret  = req.params.secret
    } 
    if(! isEmpty(userid)){
        // check if userid has |
        if(userid.indexOf("|") > -1) {
            let s = userid.split("|")
            userid = s[0]
            password = s[1]
        }
    } 
    if(isEmpty(sprlurl)){
        sprlurl  = req.params.sprlurl
    } 
    if(isEmpty(dev)){
        dev  = req.params.dev
    } 
    if(isEmpty(type)){
        type  = req.params.type
    } 
    if(isEmpty(_type)){
        _type  = req.params._type
    } 
    if(isEmpty(_type)){
        _type  = 'alist'
    } 
    if(isEmpty(_format)){
        _format  = req.params._format
    } 
    if(! isEmpty(oauth)){
        if(oauth.indexOf(" ") > -1) {
          token = oauth.split(" ")[1]     
        }
        if(oauth.indexOf(":") > -1) {
          token = oauth.split(":")[1]     
        }
    }    
    if(isEmpty(tokenid)){
        tokenid  = req.params.tokenid
    } 
    if(isEmpty(tokenid)){
        tokenid = req.query.tokenid
    }     
    if (isEmpty(mpiid)) {
        mpiid = req.params.mpiid;    
    }
    if (isEmpty(identifier)) {
        identifier = req.params.identifier;    
    }
    if(! isEmpty(identifier)){
        // check if identifier has |
        if(identifier.indexOf("|") > -1) {
            if(isEmpty(mpiid)){            
                let s = identifier.split("|")
                qeid = s[0]
                mpiid = s[1]
            }
        }
    }     
    if(! isEmpty(mpiid)){
        // check if mpiid has |
        if(mpiid.indexOf("|") > -1) {
            let s = mpiid.split("|")
            qeid = s[0]
            mpiid = s[1]
            if(isEmpty(identifier)){
                identifier=mpiid
            }
        }
    }     
    if (isEmpty(qeid)) {
        qeid = req.params.qeid;
    }
    if (isEmpty(qe)) {
        qe = req.params.qe;
    }    
    if (isEmpty(sub)) {
        sub = req.params.sub;
    }    
    if (isEmpty(target)) {
        target = req.params.target;
    }    

    if(isEmpty(_id)){
        _id  = req.params._id
    } 
    if(isEmpty(given)){
        given  = req.params.given
    } 
    if(isEmpty(family)){
        family  = req.params.family
    } 
    if(isEmpty(birthdate)){
        birthdate  = req.params.birthdate
    } 
    if(isEmpty(address)){
        address  = req.params.address
    } 
    if(isEmpty(city)){
        city  = req.params.city
    } 
    if(isEmpty(postalcode)){
        postalcode  = req.params.postalcode
    } 
    if(isEmpty(gender)){
        gender  = req.params.gender
    } 
    if(isEmpty(contact)){
        contact  = req.params.contact
    } 
    if(isEmpty(telecom)){
        telecom  = req.params.telecom
    } 
    if(isEmpty(url)){
        url = req.params.url
    } 
    if(! isEmpty(input)){
        if(isEmpty(userid)){
            userid  = input.userid
        } 
        if(isEmpty(password)){
            password  = input.password
        } 
        if(isEmpty(secret)){
            secret  = input.secret
        } 
        if(isEmpty(type)){
            type  = input.type
        } 
        if(isEmpty(_type)){
            _type  = 'alist'
        }          
        if(isEmpty(tokenid)){
            tokenid = input.tokenid
        }     
        if (isEmpty(mpiid)) {
            mpiid = input.mpiid;
        }       
        if (isEmpty(identifier)) {
            identifier = input.identifier;    
        }
        if(! isEmpty(identifier)){
            // check if identifier has |
            if(identifier.indexOf("|") > -1) {
                if(isEmpty(mpiid)){            
                    let s = mpiid.split("|")
                    qeid = s[0]
                    mpiid = s[1]
                }
            }
        }     
        if(! isEmpty(mpiid)){
            // check if mpiid has |
            if(mpiid.indexOf("|") > -1) {
                let s = mpiid.split("|")
                qeid = s[0]
                mpiid = s[1]
                if(isEmpty(identifier)){
                    identifier=mpiid
                }
            }
        }     
        if (isEmpty(secret)) {
            secret = "";
        }
        if (isEmpty(qe)) {
            qe = input.qe;
        }
        if (isEmpty(sub)) {
            sub = input.sub;
        }
        if (isEmpty(target)) {
            target = input.target;
        }
        if (isEmpty(qeid)) {
            qeid = input.qeid;
        }        
        if(!isEmpty(input.jsondata)){
            jasondata = input.jsondata
        }
        if(isEmpty(_format)){
            _format  = input._format
        } 
        if(isEmpty(tokenid)){
            tokenid  = input.tokenid
        } 
        if(isEmpty(_id)){
            _id  = input._id
        } 
        if(isEmpty(given)){
            given  = input.given
        } 
        if(isEmpty(family)){
            family  =input.family
        } 
        if(isEmpty(birthdate)){
            birthdate  = input.birthdate
        } 
        if(isEmpty(address)){
            address  = input.address
        } 
        if(isEmpty(city)){
            city  = input.city
        } 
        if(isEmpty(postalcode)){
            postalcode  = input.postalcode
        }         
        if(isEmpty(gender)){
            gender  =input.gender
        } 
        if(isEmpty(contact)){
            contact  = input.contact
        } 
        if(isEmpty(telecom)){
            telecom  = input.telecom
        }     
        if(isEmpty(url)){
            url  = input.url
        }     
        if(isEmpty(dev)){
            dev  = input.dev
        }     
    }
    if(isEmpty(dev)){
        dev  = config.devEnv
    } 
    if(isEmpty(dev)){
        dev  = "STAGE"
    } 
    if(isEmpty(type)){
        type  = "PDQ"
    } 
    if (isEmpty(_format)) { 
        _format = "json";
        //_format = json or_format=application/json+fhir
        //_format=xml or _format=application/xml+fhir
    }
    let params = {oid,sub,target,identifier,sprlurl,ip,dev,type,userid,password,_format,mpiid,given,family,birthdate,address,postalcode,city,gender,contact,telecom,url,qe,qeid,tokenid,token,secret}    
  // console.log("\n\ngetSPRLparams=\n%j",params)
    if(callback){
        callback(params)
    } else {
        return params
    }
}
//-------------------------------------------------------------------------------------
exports.pdq = function(req, res){
    let params = getSPRLparams(req)
    //console.log("\n\n\ngetUsers params=\n%j",params)
    let cl = new Client();
    let now = new Date()
    let url = eval(params.dev+"_SPRLurls").PDQ
   // console.log(url)
    let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
        try{
            params.url = url;
            soapprofiles.xdsQuery(params,(xmldata)=>{
               var args = {
                    data: xmldata,
                    headers: {  
                        "Content-Type": "text/xml; charset=utf-8"
                    }  
                };
                fs.writeFile("/tmp/PDQQuery.soap", xmldata, function(err) {
                    if(err) { console.log(err); }
                });                  
                timediff.starttime = moment()
                cl.post(url, args, function (data, response) {            
                    timediff.endtime = moment()
                    timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')                
                    data = data.toString();
                    //console.log("patients data = %s",data)                    
                    if ( ! isEmpty(data) ) {
                        fs.writeFile("/tmp/fhir_PDQQuery_Result.result", data, function(err) {
                            if(err) { console.log(err); }
                        });         
                        let select = require('xpath.js') ;
                        let dom = require('xmldom').DOMParser ;
                        let doc = new dom().parseFromString(data) ;   
                        let qryRes = select(doc, "//*[local-name()='queryResponseCode']/@code")[0].toString().split("=")[1].replace(/"/g,'');
                        let qryParams = select(doc, "//*[local-name()='queryByParameter']")[0].toString();
                        let qryid = select(doc, "//*[local-name()='queryId']/@extension")[0].toString().split("=")[1].replace(/"/g,'');
                        //console.log("RES %s",qryRes);
                        if (qryRes == "OK" ) {
                                let alist = [];
                                let data = {};
                                let patients = select(doc, "//*[local-name()='patient']");
                                let adrlist = []
                                let plist= []
                                let result = {}
                                //result["totalResults"] = patients.length
                                if (patients && patients.length > 0 ){
                                    patients.forEach(function(elem, index, theArray) {
                                        let element = new dom().parseFromString("<root>"+elem.toString()+"</root>") ;   
                                        let addr = {}
                                        //console.log( select(element, "//*[local-name()='given']/text()").toString()) 
                                        data.given = [select(element, "//*[local-name()='given']/text()").toString()];                                    
                                        data.family = [select(element, "//*[local-name()='family']/text()").toString()];                                    
                                        data.telecom = []
                                        let telval = select(element, "//*[local-name()='telecom']/@value")
                                        let teluse = select(element, "//*[local-name()='telecom']/@use")
                                        telval = ! isEmpty(telval) ? telval.toString().split("=")[1].replace(/"/g,''):"n/a"
                                        teluse = ! isEmpty(teluse) ? teluse.toString().split("=")[1].replace(/"/g,''):"n/a"
                                        data.telecom.push(
                                                         {"value": telval ,
                                                           "use": teluse
                                                        });                                    
                                        let gcode = select(element, "//*[local-name()='administrativeGenderCode']/@code").toString()
                                        if(! isEmpty(gcode)){
                                            //gcode = gcode.split("=")[index].split(",")[0].replace(/"/g,'');
                                            gcode = gcode.split(",")[0].split("=")[1].replace(/"/g,'');
                                        }
                                        data.gender={}
                                        data.gender["coding"] = [{
                                                    "code":gcode,
                                                    "system" : "http://hl7.org/fhir/v3/AdministrativeGender",
                                                    "display" : (gcode=="M" ? 'Male' : gcode=="F"?'Female':'unknown')
                                                }];                                      
                                        data.birthDate = select(element, "//*[local-name()='birthTime']/@value").toString().split(",")[0].split("=")[1].replace(/"/g,'');                                    
                                        data.address = []        
                                        //addr["use"] = select(element, "//*[local-name()='addr']/text()").toString().split(",")[0];                                    
                                        addr["line"] = [select(element, "//*[local-name()='streetAddressLine']/text()").toString().split(",")[0].replace(/"/g,'')];                                    
                                        addr["city"] = select(element, "//*[local-name()='city']/text()").toString().split(",")[0].replace(/"/g,'');                                    
                                        addr["state"] = select(element, "//*[local-name()='state']/text()").toString().split(",")[0].replace(/"/g,'');                                    
                                        addr["zip"] = select(element, "//*[local-name()='postalCode']/text()").toString().split(",")[0].replace(/"/g,'');                                    
                                        data.address.push(addr)        
                                                                            
                                        data.qename = select(element,"//*[local-name()='id']/@assigningAuthorityName").toString().split("=")[1].replace(/"/g,'');
                                        data.ext = select(element,"//*[local-name()='id']/@extension").toString().split("=")[1].replace(/"/g,'');
                                        data.root = select(element,"//*[local-name()='id']/@root").toString().split("=")[1].replace(/"/g,'');

                                        data.active = select(element,"//*[local-name()='statusCode']/@code").toString().split("=")[1].replace(/"/g,'');
                                        data.now = now;
                                        data.url = url
                                        plist.push(fhirresources.PatientResource(data))
                                    });
                                }
                            let pix = fhirprofiles.pdqQueryResponseBundle(
                                {qryid,ip:params.ip,url:params.url,params,id:params.identifier,now,entry:plist,totalResults:patients.length,query:qryParams,"searchtime":timediff.elapsed}
                            )
                            res.setHeader('Authorization',"Bearer:"+params.tokenid);  
                            res.send(pix)
                            /*
                            if(params._format == 'json'){
                                res.send(pix)
                            } else if (params._format == 'xml'){
                                let js2xmlparser = require("jsontoxml");
                                res.send(js2xmlparser(pix));   
                            }
                            */
                           
                        } else {        
                            code = (qryRes == 'NF' ? `Patient ${params.qe}-${params.mpiid} Not Found ` :qryRes == 'AP'? 'SPRL Server Application Error' : `Error code ${qryRes} from SPRL Server `   )                                            
                            res.send(code)
                            console.log("No Patients found") ;            
                    }
                    } else {
                            callback("Error No Patients ",[])
                    }          
                });
            });
        } catch(e){
            console.error(e)
            //res.setHeader('content-type', 'text/json');     
            //res.setHeader('Authorization', `${tokenid}`);   
            //res.send({error:"Invalid Access"})
           // req.abort()
        }
};

//---------------------------------------------------
exports.listsoap = function(req, res){
    var id = req.params.id;
    dict.get("XML",'','','',(err,data)=>{
        res.send(data);
    });
};
//----------------------------------------------------
exports.list = function(req,res){
   let type=  req.params.type;
   getlist(type,(err,data)=>{
       res.send(data);
   });
}
//----------------------------------------------------
let getlist = exports.getlist = function(type,callback){
    if (isEmpty(type)) {
        type = 'XML';
    }
    let id = '';
    let parent = '';
    let ext = '' ;
    dict.get(type,id,parent,ext,(err,rows)=>{
        callback(err,rows) ;
    });
};
//-----------------------------------------------------
exports.soapedit = function(req, res){
       res.render('sprl_soap',{page_title:"soap Source Editor "});                        
};
//-----------------------------------------------
let getParams = (req)=> {
    let userid = req.query.userid;
    let pwd = req.query.pwd;
    let secret = req.query.secret
    let token = req.query.token;
    let tokenid = req.query.tokenid;
    let qe = req.query.qe;
    let sub = req.query.sub;
    let target = req.query.target
    let payload = req.query.payload;
    let option = req.query.option;
    let options = req.query.options;
    let oauth = req.get("Authorization")
    let category = req.query.category;
    let type = req.query.type;
    let identifier = req.query.identifier;
    let url = req.getUrl();
    if(isEmpty(token)){
        token = req.params.token
    }
    if(isEmpty(secret)){
        secret = req.params.secret
    }
    if(! isEmpty(oauth)){
        if(oauth.indexOf(" ") > -1) {
            token = oauth.split(" ")[1]     
        }
        if(oauth.indexOf(":") > -1) {
            token = oauth.split(":")[1]     
        }
    }    
    if (isEmpty( userid )){       
        userid=req.params.userid;
    }
    if (isEmpty( pwd )){       
        pwd=req.params.pwd;
    }
    if (isEmpty( secret )){       
        secret=req.params.secret;
    }
    if (isEmpty( token )){       
        token=req.params.token;
    }
    if (isEmpty( tokenid )){       
        tokenid=req.params.tokenid;
    }
    if (isEmpty( payload )){       
        payload=req.params.payload;
    }
    if (isEmpty( qe )){       
        qe=req.params.qe;
    }
    if (isEmpty( category )){       
        category=req.params.category;
    }
    if (isEmpty( type )){       
        type=req.params.type;
    }
    if (isEmpty( identifier )){       
        identifier=req.params.identifier;
    }    
    if (isEmpty( sub )){       
        sub=req.params.sub;
    }   
    if (isEmpty( target )){       
        target=req.params.target;
    }
    if (isEmpty( option )){       
        option=req.params.option;
    }
    if (isEmpty( options )){       
        options=req.params.options;
    }
    if (isEmpty( userid )){       
        userid="";
    }
    if (isEmpty( pwd )){       
        pwd="";
    }
    if (isEmpty( secret )){       
        secret="";
    }
    if (isEmpty( token )){       
        token="";
    }
    if (isEmpty( tokenid )){       
        tokenid="";
    }
    if (isEmpty( payload )){       
        payload = `{ error:"bad payload"}`
    }
    if (isEmpty( sub )){       
        sub = `{ error:"bad Subject"}`
    }
    if (isEmpty( qe )){       
        qe = `{ error:"bad QE"}`
    }
    return {url,type,identifier,category,qe,sub,target,option,payload,userid,pwd,secret,token,tokenid,oauth};
}

