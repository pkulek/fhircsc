

const devEnv = "STAGE"; //STAGE  or PROD//
let CJSON = require("circular-json");
let fs = require('fs')

let config = require('../../config.json');
let moment = require('moment'); // for date time
let utils = require('../utils') ;
let fhirprofiles = require("./fhir-profiles")

const isEmpty =  utils.isEmpty;



//--------------------------------------------------------------------------------
let PatientResource = exports.PatientResource = (data,callback)=> {
    let res = {
        resource:{
            "resourceType": "Patient",
            "id":data.ext,       
            "name":[{                                            
                "family":data.family,
                "given":data.given
            }],
            "telecom":[data.telecom],
            "gender":data.gender,
            "birthDate":data.birthDate,
            "address" :data.address,
            "managingOrganization" : {
                "display" : data.qename
            },
            "active":`${data.active}`,
            "identifier": [
                {
                    "use": "usual",
                    /*
                    "type": {
                    "coding": [
                        {
                        "system": "http://hl7.org/fhir/v2/0203",
                        "code": "MR"
                        }
                    ]
                    },
                    */
                    "system": data.root,
                    "value": data.ext,
                    assigner:{display:data.qename}
                }
            ],
            "text": {
                "status": "generated",
                "div": `<div xmlns=\"http://www.w3.org/1999/xhtml\">\n      \n      <p>Patient ${data.given} ${data.family} (${data.gender.coding[0].display}) DOB: ${data.birthDate}, ${data.now} ID: ${data.ext} AA: ${data.root} QE:${data.qename} </p>\n    \n    </div>`
            },
            managingOrganization:{display:data.qename},
            active:true
        }
    }
    if(callback){
        callback(res)
    } else {
        return res
    }
}
//--------------------------------------------------------------------------------
let PactitionerResource = exports.PactitionerResource= (data,callback)=>{
let res =  {
        "fullUrl": "http://fhir.healthintersections.com.au/open/Practitioner/example",
        "resource": {
            "resourceType": "Practitioner",
            "id": "example",
            "meta": {
            "lastUpdated": "2013-05-05T16:13:03Z"
            },
            "text": {
            "status": "generated",
            "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n\t\t\t\t\t\t\n            <p>Dr Adam Careful</p>\n\t\t\t\t\t\n          </div>"
            },
            "identifier": [
            {
                "system": "http://www.acme.org/practitioners",
                "value": "23"
            }
            ],
            "name": [
            {
                "family": "Careful",
                "given": [
                "Adam"
                ],
                "prefix": [
                "Dr"
                ]
            }
            ]
        }
    }
    if(callback){
        callback(res)
    } else {
        return res
    }
}
//----------------------------------------------------------------------------------------------------------
    let CompositionResource =exports.CompositionResource = (data,callback) =>{
    let res =             
        {"resource": {
        "resourceType": "Composition",
        "id": "180f219f-97a8-486d-99d9-ed631fe4fc57",
        "meta": {
        "lastUpdated": "2013-05-28T22:12:21Z"
        },
        "text": {
        "status": "generated",
        "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p><p><b>id</b>: 180f219f-97a8-486d-99d9-ed631fe4fc57</p><p><b>meta</b>: </p><p><b>status</b>: final</p><p><b>type</b>: Discharge Summary from Responsible Clinician <span>(Details : {LOINC code '28655-9' = 'Physician attending Discharge summary)</span></p><p><b>encounter</b>: <a>http://fhir.healthintersections.com.au/open/Encounter/doc-example</a></p><p><b>date</b>: 01/02/2013 12:30:02 PM</p><p><b>author</b>: <a>Doctor Dave</a></p><p><b>title</b>: Discharge Summary</p><p><b>confidentiality</b>: N</p></div>"
        },
        "status": "final",
        "type": {
        "coding": [
            {
            "system": "http://loinc.org",
            "code": "28655-9"
            }
        ],
        "text": "Discharge Summary from Responsible Clinician"
        },
        "subject": {
        "reference": "http://fhir.healthintersections.com.au/open/Patient/d1",
        "display": "Eve Everywoman"
        },
        "encounter": {
        "reference": "http://fhir.healthintersections.com.au/open/Encounter/doc-example"
        },
        "date": "2013-02-01T12:30:02Z",
        "author": [
        {
            "reference": "Practitioner/example",
            "display": "Doctor Dave"
        }
        ],
        "title": "Discharge Summary",
        "confidentiality": "N",
        "section": [
        {
            "title": "Reason for admission",
            "code": {
            "coding": [
                {
                "system": "http://loinc.org",
                "code": "29299-5",
                "display": "Reason for visit Narrative"
                }
            ]
            },
            "text": {
            "status": "additional",
            "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n\t\t\t\t\t\t\t\n              <table>\n\t\t\t\t\t\t\t\t\n                <thead>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Details</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td/>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </thead>\n\t\t\t\t\t\t\t\t\n                <tbody>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Acute Asthmatic attack. Was wheezing for days prior to admission.</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td/>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </tbody>\n\t\t\t\t\t\t\t\n              </table>\n\t\t\t\t\t\t\n            </div>"
            },
            "entry": [
            {
                "reference": "urn:uuid:541a72a8-df75-4484-ac89-ac4923f03b81"
            }
            ]
        },
        {
            "title": "Medications on Discharge",
            "code": {
            "coding": [
                {
                "system": "http://loinc.org",
                "code": "10183-2",
                "display": "Hospital discharge medications Narrative"
                }
            ]
            },
            "text": {
            "status": "additional",
            "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n\t\t\t\t\t\t\t\n              <table>\n\t\t\t\t\t\t\t\t\n                <thead>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Medication</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Last Change</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Last ChangeReason</td>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </thead>\n\t\t\t\t\t\t\t\t\n                <tbody>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Theophylline 200mg BD after meals</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>continued</td>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Ventolin Inhaler</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>stopped</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Getting side effect of tremor</td>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </tbody>\n\t\t\t\t\t\t\t\n              </table>\n\t\t\t\t\t\t\n            </div>"
            },
            "mode": "working",
            "entry": [
            {
                "reference": "urn:uuid:124a6916-5d84-4b8c-b250-10cefb8e6e86"
            },
            {
                "reference": "urn:uuid:673f8db5-0ffd-4395-9657-6da00420bbc1"
            }
            ]
        },
        {
            "title": "Known allergies",
            "code": {
            "coding": [
                {
                "system": "http://loinc.org",
                "code": "48765-2",
                "display": "Allergies and adverse reactions Document"
                }
            ]
            },
            "text": {
            "status": "additional",
            "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n\t\t\t\t\t\t\t\n              <table>\n\t\t\t\t\t\t\t\t\n                <thead>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Allergen</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Reaction</td>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </thead>\n\t\t\t\t\t\t\t\t\n                <tbody>\n\t\t\t\t\t\t\t\t\t\n                  <tr>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Doxycycline</td>\n\t\t\t\t\t\t\t\t\t\t\n                    <td>Hives</td>\n\t\t\t\t\t\t\t\t\t\n                  </tr>\n\t\t\t\t\t\t\t\t\n                </tbody>\n\t\t\t\t\t\t\t\n              </table>\n\t\t\t\t\t\t\n            </div>"
            },
            "entry": [
            {
                "reference": "urn:uuid:47600e0f-b6b5-4308-84b5-5dec157f7637"
            }
            ]
        }
        ]
        }
    }
        if(callback){
            callback(res)
        } else {
            return res
        }
}
//----------------------------------------------------------------------------------------------------------------------    
let DocumentReference =exports.DocumentReference = (data,callback) =>{

    let res =  {
        "resourceType" : "DocumentReference",
        // from Resource: id, meta, implicitRules, and language
        // from DomainResource: text, contained, extension, and modifierExtension
        "masterIdentifier" : { Identifier }, // Master Version Specific Identifier
        "identifier" : [{ Identifier }], // Other identifiers for the document
        "status" : "<code>", // R!  current | superseded | entered-in-error
        "docStatus" : "<code>", // preliminary | final | appended | amended | entered-in-error
        "type" : { CodeableConcept }, // R!  Kind of document (LOINC if possible)
        "class" : { CodeableConcept }, // Categorization of document
        "subject" : { Reference:""/* Patient|Practitioner|Group|Device */ }, // Who/what is the subject of the document
        "created" : "<dateTime>", // Document creation time
        "indexed" : "<instant>", // R!  When this document reference was created
        "author" : [{ 
            Reference:""/*Practitioner|Organization|Device|Patient|RelatedPerson*/ }], // Who and/or what authored the document
            "authenticator" : { Reference:"" /*Practitioner|Organization)*/ }, // Who/what authenticated the document
            "custodian" : { Reference:""/*(Organization)*/ }, // Organization which maintains the document
            "relatesTo" : [{ // Relationships to other documents
            "code" : "<code>", // R!  replaces | transforms | signs | appends
            "target" : { Reference:""/*(DocumentReference)*/ } // R!  Target of the relationship
        }],
        "description" : "<string>", // Human-readable description (title)
        "securityLabel" : [{ CodeableConcept }], // Document security-tags
        "content" : [{ // R!  Document referenced
            "attachment" : { Attachment }, // R!  Where to access the document
            "format" : { Coding } // Format/content rules for the document
        }],
        "context" : { // Clinical context of document
            "encounter" : { Reference:""/*(Encounter)*/ }, // Context of the document  content
            "event" : [{ CodeableConcept }], // Main clinical acts documented
            "period" : { Period }, // Time of service that is being documented
            "facilityType" : { CodeableConcept }, // Kind of facility where patient was seen
            "practiceSetting" : { CodeableConcept }, // Additional details about where the content was created (e.g. clinical specialty)
            "sourcePatientInfo" : { Reference:""/*(Patient)*/ }, // Patient demographics from source
            "related" : [
                { // Related identifiers or resources
                "identifier" : { Identifier }, // Identifier of related objects or events
                "ref" : { Reference:""/*(Any)*/ } // Related Resource
                }
            ]
        }
    }

    /*    
let res = {
    "resourceType": "DocumentReference",
    "id": "example",
    "text": {
      "status": "generated",
      "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p><p><b>id</b>: example</p><p><b>contained</b>: </p><p><b>masterIdentifier</b>: urn:oid:1.3.6.1.4.1.21367.2005.3.7</p><p><b>identifier</b>: urn:oid:1.3.6.1.4.1.21367.2005.3.7.1234</p><p><b>status</b>: current</p><p><b>docStatus</b>: preliminary</p><p><b>type</b>: Outpatient Note <span>(Details : {LOINC code '34108-1' = 'Outpatient Note', given as 'Outpatient Note'})</span></p><p><b>class</b>: History and Physical <span>(Details : {http://ihe.net/xds/connectathon/classCodes code 'History and Physical' = 'History and Physical', given as 'History and Physical'})</span></p><p><b>subject</b>: <a>Patient/xcda</a></p><p><b>created</b>: 24/12/2005 9:35:00 AM</p><p><b>indexed</b>: 24/12/2005 9:43:41 AM</p><p><b>author</b>: <a>Practitioner/xcda1</a>, id: a2; Gerald Smitty </p><p><b>authenticator</b>: <a>Organization/f001</a></p><p><b>custodian</b>: <a>Organization/f001</a></p><h3>RelatesTos</h3><table><tr><td>-</td><td><b>Code</b></td><td><b>Target</b></td></tr><tr><td>*</td><td>appends</td><td><a>DocumentReference/example</a></td></tr></table><p><b>description</b>: Physical</p><p><b>securityLabel</b>: very restricted <span>(Details : {http://hl7.org/fhir/v3/Confidentiality code 'V' = 'very restricted', given as 'very restricted'})</span></p><h3>Contents</h3><table><tr><td>-</td><td><b>Attachment</b></td><td><b>Format</b></td></tr><tr><td>*</td><td/><td>History and Physical Specification (Details: urn:oid:1.3.6.1.4.1.19376.1.2.3 code urn:ihe:pcc:handp:2008 = 'urn:ihe:pcc:handp:2008', stated as 'History and Physical Specification')</td></tr></table><blockquote><p><b>context</b></p><p><b>encounter</b>: <a>Encounter/xcda</a></p><p><b>event</b>: Arm <span>(Details : {http://ihe.net/xds/connectathon/eventCodes code 'T-D8200' = 'T-D8200', given as 'Arm'})</span></p><p><b>period</b>: 23/12/2004 8:00:00 AM --&gt; 23/12/2004 8:01:00 AM</p><p><b>facilityType</b>: Outpatient <span>(Details : {http://www.ihe.net/xds/connectathon/healthcareFacilityTypeCodes code 'Outpatient' = 'Outpatient', given as 'Outpatient'})</span></p><p><b>practiceSetting</b>: General Medicine <span>(Details : {http://www.ihe.net/xds/connectathon/practiceSettingCodes code 'General Medicine' = 'General Medicine', given as 'General Medicine'})</span></p><p><b>sourcePatientInfo</b>: <a>Patient/xcda</a></p><h3>Relateds</h3><table><tr><td>-</td><td><b>Identifier</b></td><td><b>Ref</b></td></tr><tr><td>*</td><td>urn:oid:1.3.6.1.4.1.21367.2005.3.7.2345</td><td><a>Patient/xcda</a></td></tr></table></blockquote></div>"
    },
    "contained": [
      {
        "resourceType": "Practitioner",
        "id": "a2",
        "name": [
          {
            "family": "Smitty",
            "given": [
              "Gerald"
            ]
          }
        ]
      }
    ],
    "masterIdentifier": {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.3.6.1.4.1.21367.2005.3.7"
    },
    "identifier": [
      {
        "system": "urn:ietf:rfc:3986",
        "value": "urn:oid:1.3.6.1.4.1.21367.2005.3.7.1234"
      }
    ],
    "status": "current",
    "docStatus": "preliminary",
    "type": {
      "coding": [
        {
          "system": "http://loinc.org",
          "code": "34108-1",
          "display": "Outpatient Note"
        }
      ]
    },
    "class": {
      "coding": [
        {
          "system": "http://ihe.net/xds/connectathon/classCodes",
          "code": "History and Physical",
          "display": "History and Physical"
        }
      ]
    },
    "subject": {
      "reference": "Patient/xcda"
    },
    "created": "2005-12-24T09:35:00+11:00",
    "indexed": "2005-12-24T09:43:41+11:00",
    "author": [
      {
        "reference": "Practitioner/xcda1"
      },
      {
        "reference": "#a2"
      }
    ],
    "authenticator": {
      "reference": "Organization/f001"
    },
    "custodian": {
      "reference": "Organization/f001"
    },
    "relatesTo": [
      {
        "code": "appends",
        "target": {
          "reference": "DocumentReference/example"
        }
      }
    ],
    "description": "Physical",
    "securityLabel": [
      {
        "coding": [
          {
            "system": "http://hl7.org/fhir/v3/Confidentiality",
            "code": "V",
            "display": "very restricted"
          }
        ]
      }
    ],
    "content": [
      {
        "attachment": {
          "contentType": "application/hl7-v3+xml",
          "language": "en-US",
          "url": "http://example.org/xds/mhd/Binary/07a6483f-732b-461e-86b6-edb665c45510",
          "size": 3654,
          "hash": "2jmj7l5rSw0yVb/vlWAYkK/YBwk="
        },
        "format": {
          "system": "urn:oid:1.3.6.1.4.1.19376.1.2.3",
          "code": "urn:ihe:pcc:handp:2008",
          "display": "History and Physical Specification"
        }
      }
    ],
    "context": {
      "encounter": {
        "reference": "Encounter/xcda"
      },
      "event": [
        {
          "coding": [
            {
              "system": "http://ihe.net/xds/connectathon/eventCodes",
              "code": "T-D8200",
              "display": "Arm"
            }
          ]
        }
      ],
      "period": {
        "start": "2004-12-23T08:00:00+11:00",
        "end": "2004-12-23T08:01:00+11:00"
      },
      "facilityType": {
        "coding": [
          {
            "system": "http://www.ihe.net/xds/connectathon/healthcareFacilityTypeCodes",
            "code": "Outpatient",
            "display": "Outpatient"
          }
        ]
      },
      "practiceSetting": {
        "coding": [
          {
            "system": "http://www.ihe.net/xds/connectathon/practiceSettingCodes",
            "code": "General Medicine",
            "display": "General Medicine"
          }
        ]
      },
      "sourcePatientInfo": {
        "reference": "Patient/xcda"
      },
      "related": [
        {
          "identifier": {
            "system": "urn:ietf:rfc:3986",
            "value": "urn:oid:1.3.6.1.4.1.21367.2005.3.7.2345"
          },
          "ref": {
            "reference": "Patient/xcda"
          }
        }
      ]
    }
  }
*/
    if(callback){
        callback(res)
    } else {
        return res
    }
}    
