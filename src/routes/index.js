
/*
 * GET home page.
 */
const config = require('../../config.json');
const utils =require('../utils')
const isEmpty = utils.isEmpty
exports.index = function(req, res){
    let registerCode=req.query.registerCode
    if(isEmpty(registerCode)){
      registerCode= req.params.registerCode
    }
    if(isEmpty(registerCode)){
      registerCode =""
    }
    let fhirhomepage = config.fhirhomepage
    // res.render('fhir_home1', {page_title: 'SPRL',data:"" ,view:true});    
    //let registerCode = req.query.registerCode ? req.query.registerCode :""
  
  //  console.log("registerCode=%s\nhomepage=%s",registerCode,fhirhomepage)
    res.render(fhirhomepage, {page_title: 'SPRL',registerCode:registerCode ,data:"",view:true});    
};
