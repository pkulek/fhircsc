//const dict = require('../dic/dict')
const config = require('../../config.json');
const moment = require('moment'); // for date time
const STATUS_CODES = require('http').STATUS_CODES
const sql = require("mssql")
const https = require('https')
let CJSON = require("circular-json");
let fs = require('fs')
let utils = require('../utils') ;
const clone =  utils.clone;
let audit = require("../audit")
let fhirprofiles = require("./fhir-profiles")
let sprl =  require("./sprl_manager");
const isEmpty =  utils.isEmpty; 
let _qeconfig = config.qeconfig
const authurl = config.authurl
const Client = require('node-rest-client').Client;
let URL = require('url');
//---------------------------------------------
let getjwt = (sub,target,tokenid,callback) =>{
    let cl = new Client()    
    let url = `${authurl}/jwt/token/${sub}/${target}/?tokenid=${tokenid}`
    cl.get(url, (data, response) => {    
        console.log("gtjwt=%s",data)
        callback( data)
    })
} 
//---------------------------------------------
let getuserid = (tokenid,callback) =>{
    let cl = new Client()    
    let url = `${authurl}/userid/${tokenid}`
    cl.get(url, (data, response) => {    
        console.log("get token =%s",data)
        callback( data)
    })
} 
//-----------------------------------------------------------
let configqeparams = (targetqe) =>{
    let qeconfig = clone(_qeconfig)
    return qeconfig
}
//---------------------------------------------------------------------------
let runQry =  (qry,callback) => {    
    new sql.Request().query(qry, (err, result) => {
        if(err) {
           console.log("\n\nFhirManager RunQuery err:%s  \n\nqry=\n%s\n\n",err,qry)
        }
        if(callback){
            if(err) {
                callback(err,[] )
            } else {
                callback(err,result.recordsets[0])
            }
        }
    })    
}
//----------------------------------------------------------------------------------
exports.editConfig = (req,res) =>{
    let params = req.params
    let filename = params.filename
    let mode = params.mode
    if(isEmpty(filename)) {
        filename = __dirname+'\\..\\..\\config.json';
    }
    if(isEmpty(mode)){
        mode="view,tree"
    }
    console.log(__dirname)
    fs.readFile(filename,'utf8',(err,data)=>{
        if(typeof data == 'string'){
            data = JSON.parse(data)
        }
        data = JSON.stringify(data,null,4)
        res.render('json_viewer',{"mode":"edit",data:data,page_title:"Config Editor"} ) 
    })
}
//----------------------------------------------------------------------------------
exports.saveConfig = (req,res) =>{
    let params = req.params
    let filename = params.filename
    let data = params.data
    if(isEmpty(data)){
        data = req.body
    }
    if(isEmpty(filename)) {
        filename = __dirname+'\\..\\..\\config.json';
    }
    if(! isEmpty(data) && ! isEmpty(filename)){
        fs.writeFile( filename,JSON.stringify(data,null,4),(err)=>{
            res.send(data)
        })
    }
}
//----------------------------------------------------------------------------------
exports.getConfig = (req,res) =>{
    let params = req.params
    let key = params.key
    let result = config
    if(! isEmpty(key)){
        result = config[key]
    }
    if(typeof result == 'object' ){
        res.send(result)
    } else {
        res.send(""+result+"")
    }
}
//------------------------------------------------------------------------------
exports.loadHTML = (req,res) =>{
    let params = req.params
    let query = req.query
    let file= query.file
    if(isEmpty(file)) { 
        file = 'FHIRsupportedParameters'
    }
    res.render(file, {page_title: 'FHIR CSC View'});  
}
//------------------------------------------------------------------------
let extracttype = (url,types)=>{
    let q = URL.parse(url,true)
    let type = ""
    types = types || ["Patient","Condition", "Observation", "Procedure", "MedicationRequest","MedicationAdministration","MedicationAdministration","Immunization","Encounter"]
    types.forEach((elem,i)=>{
        if(q.pathname.includes(elem)) {
            type=elem
        }
    })
    return type
}
//------------------------------------------------------------------------
let extractmpi = (url,types)=>{
    //url = "https://192.168.180.151:8443/Procedure?patient.identifier=15233943&date=eq2019-11-05"
    let type = extracttype(url)
    let q = URL.parse(url,true)
    let mpi = ""
    // resource = patient.identifier
    // patient = /xxxx or patient=xxx or _id=xxx identifier=xxx
    // fullurl condition/xxx check for targetqe= 
    types = types || ["patient.identifier","identifier", "patient", "_id"]
    if(type=='Patient'){
        mpi = q.path.split('/')[2]
        if(isEmpty(mpi)){
            Object.keys(q.query).forEach( (element,key)=> {
                if(types.includes(element)){
                    mpi = q.query[element]
                }
            })       
        }
    } else {
        Object.keys(q.query).forEach( (element,key)=> {
            if(types.includes(element)){
                mpi = q.query[element]
            }
        }) 
    }
    return mpi
}
//------------------------------------------------------------------------
let extracttargetqe = (fullurl)=>{
    let qeurls = config.qeurl
    let q = URL.parse(fullurl,true)
    let targetqe = ""
    Object.keys(qeurls).forEach( (element,key)=> {
        let url = qeurls[element].url
        if(url && url.includes(q.hostname)){
            targetqe = qeurls[element].QE
        }
    }) 
    if(isEmpty(targetqe)){
        console.log("qe not found for %s \n in extracttargetqe",fullurl)
    }
    return targetqe
}
//------------------------------------------------------------
let processFHIRResources = (type,req,params,callback) =>{
    let mpiid = params.mpiid
    let token= params.token
    let tokenid = params.tokenid
    let qeconfig = configqeparams()
    let ourls = []
    let addjwt = (ourls,type,data,index,callback) => {
        if(data.length > index) {
            let elem = data[index]
            let mpiid = elem.value
            let targetqe = elem.assigner.display
            if(targetqe != 'SMPI'){
                params.mpiid = mpiid
                params._id = mpiid
                params.targetqe = targetqe
                if( isvalidQE(targetqe) )  { 
                    let ourl = getFHIRurl(params,type)
                    getjwt(ourl.reqqe,ourl.targetqe,tokenid,(jwtData) =>{
                        ourl.jwt = jwtData
                        ourl.url = ourl.url.replace("?patient.identifier="+params.patient,"")
                        ourls.push(ourl)     
                        addjwt(ourls,type,data,index+1,callback)            
                    })
                }  else {
                    addjwt(ourls,type,data,index+1,callback)   
                }
            } else {
                addjwt(ourls,type,data,index+1,callback)        
            }
        } else {
            callback(ourls)
        }
    }
    isValidJWT(token,tokenid,type,(data) =>{
        if(! data.error) {  
            jwt = data.decoded
            let reqqe = jwt.sub
            let oid = qeconfig[reqqe].OID;
            sprl.querypixInternal(req,params,oid, mpiid, reqqe,params.transactionID, (err, pix,response,pixParameters) => {
                pix = parseresult(pix)
                if(! err) {
                    let entries = pix.resource.entry
                    if(entries) {
                        let data =  entries[0].resource?entries[0].resource.identifier:{}
                        let qecount = validqes(data)
                        if(qecount != 0) {
                            let patient = params.patient
                            type += (isEmpty(patient)?'':'.patient')
                            addjwt(ourls,type,data,0,(_ourls)=>{
                                callback(null,_ourls)
                            })
                        } else {
                            callback(null,ourls)
                        }
                    } else {
                        callback(null,ourls)
                    }
                } else {
                    callback(null,ourls)
                }
               
            })
        } else {
            callback("error",ourls)
        }
    })
}
//------------------------------------------------------------------------
exports.getURL = (req,res) =>{
	xgetFHIRparams(req,(params)=>{
		let token= params.token
		let tokenid = params.tokenid
		let url = params.url
		console.log("geturl params = %j",params)
		let type = extracttype(url)
		let mpiid = extractmpi(url)
		let ourl = url
		isValidJWT(token, tokenid,type,(data) => {
			if( isEmpty(data.error) ) {  
				let reqqe = data.decoded.sub
				let targetqe = data.decoded.targetSystem.toUpperCase()
				if( targetqe == 'STATEWIDE' || targetqe == 'SMPI'  ){  
					params.reqqe = reqqe
					params.targetqe = targetqe
					params.mpiid = mpiid
					processFHIRResources(type,req,params,(err,data)=>{
                        ourl = data
						res.send(ourl)
					})
				} else {
                    ourl = getFHIRurl(params,type)   
                    ourl.jwt = token
					res.send(ourl)
				}
			} else {
				res.send({"Error":"Invalid Token"})
			}
		})
	})
}
//------------------------------------------------------------------------
exports.aggregateResult = (req,res) =>{
	xgetFHIRparams(req,(params)=>{
		let body = req.body
        let out = []
        fhirprofiles.resourceResponse('type',{},(profile)=>{
            body.forEach((elem,index)=>{
              //  if(elem.Response.resourceType) {
                    let targetqe = extracttargetqe(elem.Request)
                    let ourl = {"targetqe":targetqe,"url":elem.Request}
                   
                    result = parseresult(elem.Response)
                   //console.log("ELEM=",elem.Request,ourl,result)
                    result= setsingleqeresults(result,ourl)
                   // console.log("aftersetsinglereq %s",JSON.stringify(result,null,4))
                    parseEnteryResponse(params,ourl,result,(out)=>{ 
                       // console.log("out=%s",JSON.stringify(out,null,4))
                        profile = processResult(req,profile,ourl,params,out,elem)	
                       // out.push({"FCSResponse":result,"QEResponse":{"Response":_data,"Request":ourl.url}})
                    })        
             //   } else {
              //      profile = processResult(req,profile,ourl,params,out,elem)	
               // }
            })
            res.send(profile)
        })
    });
}
//--------------------------------------------------------------------
let verifyAccess = (tokenid,role,callback) =>{
    let cl = new Client()    
    let url = `${authurl}/access/verify/${tokenid}/${role}`
    cl.get(url, (data) => {         
        callback( data)
    })
}
//-------------------------------------------------------------------------------
exports.CSC = exports.FCS =(req,res) => {
    let io = req.app.get("socket.io")
	xgetFHIRparams(req,(params)=>{
		let id = params.id
		let resource = params.res
		let targetqe=params.targetqe
		let reqqe = params.reqqe
		let tokenid = params.tokenid
   
		let role = params.role
		let registerCode = params.registerCode
		if(isEmpty(registerCode)){
			registerCode = "n/a"
		}
		let _data = {id,resource,targetqe,reqqe,tokenid,registerCode}
		let fhirhomepage = config.fhirhomepage
		if(isEmpty(fhirhomepage)){
			fhirhomepage = "fhir_home"
		}
		if(io){
			io.emit("LOGIN_MSG","csc")     
		}
		if(isEmpty(role)){
			role = "FCS_VIEW"
		}
		verifyAccess(tokenid,role,(verified)=>{
			console.log("Verified = %s",verified)
			if(verified) {
				res.render('fhir_sprl', {page_title: 'FHIR FSC View',data:JSON.stringify(_data),view:true});    
			} else {
				_data = {id,resource,targetqe,reqqe,tokenid:"",registerCode}
				res.render('fhir_sprl', {page_title: 'FHIR FSC View',data:JSON.stringify(_data),view:true});    
			}  
		})
	});
}

//------------------------------------------------------------------------------------
exports.getQElist = (req,res) =>{
    let params = req.params
    let type = params.type
    let source = ""
    let PIXsource = ""
    let SMPIsource = ""
    let target = ""   
    let ssource = "<select>"   
    let sPIXsource = "<select>"
    let sSMPIsource = "<select>"
    let starget = "<select>"
    let tlist = []
    let slist = []
    let sPIXlist = []
    let sSMPIlist = []
    let _source = config["source"]
    let _target = config["target"]
    let _PIXsource = config["PIXmsource"]
    let _SMPIsource = config["SMPIsource"]
    let result = {}
    let all = {}
    if(isEmpty(type)){
        type = 'all'
    }
    source += `<option value="">Select Source </option>\n`
    Object.keys(_source).forEach( (element,key)=> {
        source += `<option value="${element}">${element}</option>\n`
        ssource += `<option value="${element}">${element}</option>\n`
        slist.push(element)
    })

    target += `<option value="">Select Target </option>\n`
    Object.keys(_target).forEach( (element,key)=> {
        target += `<option value="${element}" >${element}</option>\n`
        starget += `<option value="${element}" >${element}</option>\n`
        tlist.push(element)
    })   
   
    PIXsource += `<option value="">Select Source</option>\n`
    Object.keys(_PIXsource).forEach( (element,key)=> {
        PIXsource += `<option value="${_PIXsource[element].OID}">${element}</option>\n`
        sPIXsource += `<option value="${element}">${element}</option>\n`
        sPIXlist.push(element)
    })
    SMPIsource += `<option value="">Select Source </option>\n`
    Object.keys(_SMPIsource).forEach( (element,key)=> {
        SMPIsource += `<option value="${_SMPIsource[element].OID}">${element}</option>\n`
        sSMPIsource += `<option value="${element}">${element}</option>\n`
        sSMPIlist.push(element)
    })
    sPIXsource += "</select>"    
    sSMPIsource += "</select>"
    ssource += "</select>"
    starget += "</select>"
    all = {target,source,tlist,slist,ssource,starget,PIXsource,sPIXlist,sPIXsource,SMPIsource,sSMPIlist,sSMPIsource}
    if(type == 'all'){
        result = all
    } else {
        result = all[type]
    }
    res.send(result)  
}
//------------------------------------------------------------------------------------
exports.getResource_parameters = (req,res) =>{
    let resource_parameters = config.resource_parameters
    let params = req.params //getFHIRparams(req)
    let _resource = params.resource
    let resource = ""
    let selected = ""
    let result = ""
    let queryid = ""
    let include = ""
    let includeList = []
    let other = ""
    let supportParams = []
    if(isEmpty(_resource)){
        _resource = "condition"
    }
   // console.log(params)
    Object.keys(resource_parameters).forEach( (element,key)=> {
        selected = element.toLowerCase() == _resource.toLowerCase()?"selected":'';
        resource += `<option value="${element}" ${selected}>${element}</option>\n`
        if(element.toLowerCase() == _resource.toLowerCase()){
            let _queryid = resource_parameters[element]['queryid']
            if(_queryid){
                _queryid.forEach(elem => {
                    queryid += `<option value="${elem}">${elem}</option>\n`
                });
            }
            let _include = resource_parameters[element]['include']
            if(_include){
                 // for options
                /*
                _include.forEach(elem => {
                    include += `<option value="${elem}">${elem}</option>\n`
                });
                */
                // for checkboxes
                _include.forEach(elem => {
                    includeList.push(elem)
                   include += ` <input type="checkbox"  class="input1"  id="${elem}" name="includes" value ="${elem}">
                                <label class="label1" for="${elem}">${elem}</label>`
                });
            }
            //
            let _other = resource_parameters[element]['other']
            if(_other){
                _other.forEach(elem => {
                   // console.log("elem="+elem)
                    if(typeof elem =='string'){
                        other += `<div><label  class="paramlabel">${elem}</label>\n<input type="text" id="${elem}" value="" ></div>`
                        supportParams.push(elem)
                    } else { // object
                        Object.keys(elem).forEach( (element,key)=> {
                            other += `<div>
                            <label  class="paramlabel">${element}</label>
                            <select class="select${element}" id="select_${element}" onchange="parameterChange()" >`
                            elem[element].forEach(x =>{
                                other += `<option value="${x}">${x}</option>`
                            })
                           other += `</select></div>`
                           supportParams.push(`select_${element}`)
                        })
                        //other += `<label  class="paramlabel">${JSON.stringify(elem.status)}</label>\n<input type="text" id="${elem}" value="" >`
                    }
                });
            }
       }
    })   
    result = {resource,include,queryid,other,supportParams,includeList}
    res.send(result)  
}
//-------------------------------------------------------------------------------------
let isValidJWT = (token,tokenid,type,callback)=>{ //jwt
    let verified = false
    let role = "SPRL_"+type.toUpperCase()

    console.log("isValidJWT token= %s",token)
    if(isEmpty(token)){
        if(! isEmpty(tokenid)){
            verifyAccess(tokenid,role,(data)=>{
                console.log("verifyaccess\n\n\n%s\n\n",data)
                if (! (data == true)) { 
                    callback({error:"Invalid Token: Access Denied"})
                } else {
                    callback( {valid:"OK"})
                }
            })
        }else {
            if(callback){
                callback( {error:"Invalid JWT and tokenid"})
            }     
        }                              
    } else {
        let cl = new Client()    
        let url = `${authurl}/jwt/verify?token=${token}`
        cl.get(url, (data, response) => {   
            if(! data.error) {  
                jwt = data.decoded
                //test if whitewaall
              
                let jwtSource = config.jwtSource

                console.log("JWT = %s\njwtSource=%j",jwt,jwtSource)
                if(jwt && jwtSource && jwtSource.includes(jwt.sub)){
                    callback( data)
                } else {
                    // text for valid tokenid
                    verifyAccess(tokenid,role,(err,data)=>{
                        console.log("verif access data=%s tokenid=%s ",data,tokenid)
                        callback( {verified:data})
                    }) 
                }
            } else { //jwt not verified
                if(callback){
                    callback( data)
                } 
            }
        })
    }
}
//-----------------------------------------------------------------------------
let getFHIRurl= exports.getFHIRurl = (params,type) => {
    //let URL = require('url')
    let mpiid = params.mpiid      
    let _id = params._id    
    let targetqe = params.targetqe
    let reqqe = params.reqqe
    let patient = params.patient
    let _url = params.url
 
    if(isEmpty(params._id)){
        params._id = mpiid
    }
    let qeconfig = configqeparams()
    if(! isEmpty(targetqe)){
        targetqe = targetqe.toUpperCase()
    }
    if(! isEmpty(reqqe)){
        reqqe = reqqe.toUpperCase()
    }
    
	console.log("targetqe" + targetqe)
    if(qeconfig[targetqe] ) {
        let url = qeconfig[targetqe]?qeconfig[targetqe].url:''
        let resource = qeconfig[targetqe][type]?qeconfig[targetqe][type].resource:''
        if (resource.includes('${_id}') && ! isEmpty(_id)){
            resource = resource.replace('${_id}',_id)
        } else {
            resource = resource.replace('/${_id}','')
        }
        url += resource
        let q1 = URL.parse(_url,true)
        if(q1.search ){
            if(_url.includes("patient.identifier")){
                url+=q1.search.replace("?patient.identifier="+_id,"")
                url=url.replace("&patient.identifier="+_id,"")
                console.log("patient.identifier")
            } else {
                console.log("patient url before replacing:",url)
               
            }
            url=url.replace("&targetqe="+targetqe,"")
            url=url.replace("?targetqe="+targetqe,"")
            //console.log("getFHIRURL : url" ,url)
        
            return {url,reqqe,targetqe,mpiid,_id}
        } else {
            return {}
        }  
    }
}
//---------------------------------------------------
let parseEnteryResponse = (params,ourl,out,callback) =>{
    let fhirhost = config.fhirhost
    let targetqe = ourl.targetqe // params.targetqe
    let reqqe = ourl.reqqe
    let u =[]
    let seturl = (url,_url)=>{
		console.log(url,_url)
        url =   url.replace(_url,fhirhost)
        url += url.includes('?')?"&targetqe="+targetqe:"?targetqe="+targetqe
      //  url += ("" +config.qeconfig[targetqe].ID )
        return url
    }  
    let _seturl_ = (obj,par)=>{
        if(obj) {
            obj.forEach((elem,i)=>{
                let url = elem[par]
                if (par == 'url' ){
                    if(url && elem.relation == 'self') {
                        if(! isEmpty(url) && config.qeconfig[targetqe] ) {
                            elem[par] = seturl(url,config.qeconfig[targetqe].url )  
                        }
                    }
                } else {
                    if(! isEmpty(url) && config.qeconfig[targetqe] ) {
                        elem[par] = seturl(url,config.qeconfig[targetqe].url )  
                    }
                }
            })
        }
    }
    let parse = (ob,callback) =>{
        Object.keys(ob).forEach((elem,key) => {
            let o = ob[elem]
            if(o && o.fullUrl) {
                let url = o.fullUrl
				console.log("targetqe in loop", targetqe)
                o.fullUrl = seturl(url,config.qeconfig[targetqe].url )  
            }			
			else if(o && o.resource  ){
				let resourceUrl = params.url
					let q1 = URL.parse(resourceUrl,true)
					if(q1.search ){
						resourceUrl = resourceUrl.replace(q1.search,"")
						resourceUrl = resourceUrl + "/" + o.resource.id
						if(!resourceUrl.includes("targetqe")){
							resourceUrl +=  resourceUrl.includes("?")?"&targetqe="+targetqe:"?targetqe="+targetqe
						}
					}
					//console.log(resourceUrl)
				if(o.resource.resourceType && o.resource.resourceType != "OperationOutcome"){
					o.resource.fullUrl =resourceUrl
				}
			}
            if(o && o.url && o.relation) {
             //   if(o.relation == 'self') {
                    let url = o.url
                     o.url = seturl(url,config.qeconfig[targetqe].url )  
                    //o.url=params.url
                     //u.push(o)
                     //console.log(u)
             //   }
            }
            if(o && o.reference) {
                let url = o.reference
                o.reference = seturl(url,config.qeconfig[targetqe].url )  
            }
            if(o && o.entry) {
                o.entry.forEach((elem,i)=>{
                    _seturl_(elem.link,"url")
                    _seturl_(elem.entry,"fullUrl")
                })
            }
            if(typeof o == 'object'){
              //  console.log("ob[%s] = %j",elem,typeof o)
                parse(o,callback)
            }
        });
    }
    parse(out)
   // console.log(params.targetqe)
   // if(!params.isStatewide){
   //  out.link = u
   // }
    callback( out)
}
//-----------------------------------------------------------------------------------------------
let parseresult = (data)=>{
    let result = ""
    if(! isEmpty(data)){
        if(typeof data == 'object') {
            if (Buffer.isBuffer(data)) {
                if(data.toString().includes('{')) {
                    result = JSON.parse(data.toString("utf-8"))
                }else {
                    result = data.toString("utf-8")
                }
            } else {
                result = data
            }
        } else {
            if(typeof data == 'string') {
                data = data.trim()
                if( data.startsWith("<")  ){ // xml file
                    data = {error:data}
                } else if( data.startsWith("{")  ){ //jason
                    data = JSON.parse(data)
                } else{

                }
            }
             result = data
        }
    } else {
        let err = {}
        err.severity = err && err.severity?err.severity:"warning"
        let profile = fhirprofiles.operationOutcome(err)
        return profile;
    }
    return result
}
//------------------------------------------------------
let isvalidQE = (qe)=>{      
    return !  config.nonValidQes.includes(qe)
}
//------------------------------------------------------
let validqes = (qelist)=>{
    let qecount = 0
    if(qelist && qelist.length > 0){
        qelist.forEach((elem)=>{
            let targetqe = elem.assigner.display
            qecount +=  isvalidQE(targetqe)?1:0
        })
    }
    return(qecount)
}
//------------------------------------------------------
exports.GetSprlFHIRUrl = function(req,res){
    let params = getFHIRparams(req)
    let ourl = getFHIRurl(params,'patient')    
    res.send(ourl)
}  
//------------------------------------------------------
exports.GetProfile = function(req,res){
    let params = getFHIRparams(req)
    let profile = fhirprofiles.operationOutcome({"id":"test","code":"ERR","details":"error","url":"url","qe":"qe"})
    if(params.profile=='conditionResponse'){
        profile = fhirprofiles.conditionResponse({})
    }
    res.send(profile)
}    
//--------------------------------------------------------
exports.GetPix = (req,res) => {
    let params = getFHIRparams(req)
    let reqqe = params.reqqe
    let tokenid = params.tokenid
    let oid = ""
    let pixResult = params.pixResult?params.pixResult:config.pixResult?config.pixResult:0
    let qeconfig = configqeparams()
    let token= params.token
    isValidJWT(token,tokenid,'PIX',(data) =>{
        if(! data.error) {  
            jwt = data.decoded
           console.log("getPatient jwt = %j",jwt)
           if(! isEmpty(jwt)) {
                reqqe = jwt.sub
                oid = qeconfig[reqqe].OID;
                let mpiid = params.mpiid
                sprl.querypixInternal(req,params,oid, mpiid, reqqe, params.transactionID,(err, pix,response,pixParameters) => {
                    pixParameters&& pixParameters.resource?
                    pixParameters.resource.parameter.push( {
                        name :"targetId",
                        valueReference:params.url
                    }):null
                    if(pixResult == 1){
                        res.send(pix)
                    } else if(pixResult == 2){
                        res.send(pixParameters)
                    } else {
                        res.send({pixParameters,pixBundle:pix})
                    }
                })
            } else {
                res.send(data)
            }
        } else {
            res.send(data)
        }
    })
}
///------------------------------------------------------
exports.GetPatient = (req,res)=>{
   // let params = getFHIRparams(req)
   xgetFHIRparams(req,(params)=>{
        //console.log("GetPatient \n%s",JSON.stringify(params,null,4))
        let token= params.token
        let tokenid = params.tokenid
        isValidJWT(token,tokenid,'PATIENT',(data) =>{
            if(! data.error) {  
                jwt = data.decoded
            //  console.log("getPatient jwt = %j",jwt)
                GetFhirPatient(params,req,res)
            } else {
                res.send({error:data})
            }
        })
    })
}
//------------------------------------------------------
//GetFhirPatient
let GetFhirPatient = (params,req,res) => {
    let type = 'Patient'
    let ourl = getFHIRurl(params,type)    
    let transactionID = utils.getuuid()
    params.seq = 0
    params.transactionID =  transactionID
    if(! isEmpty(ourl)) {
        let reqid = 0
        let qrydata = {req,params,ourl}
        console.log("REQ=\n%s",CJSON.stringify(req.route,null,4))
        getuserid(params.tokenid,(data)=>{
            params.userid = data
       //     console.log("getuserid %s , tokenid=\n%s",data.toString(),params.tokenid)
            audit.insertRequest(type,params,qrydata,(err,result)=>{
                if(result && result.length > 0 ) {
                    reqid = result[0].ID
                }
                params.reqid = reqid
                let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
                fhirPatient(ourl,(data,response)=>{
                    console.log('response from patient')
                    timediff.endtime = moment()
                    timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                    params.seq += 1
                    let qrydata = {req,resource:type,response,result:data,params,elapsedtime:timediff.elapsed }
                    audit.insertResponse(qrydata,(err,result)=>{
                        res.send(data)
                    })               
                })
            })
        })
    } else{
        res.send("No end point to Connect");
    }
}
//--------------------------------------------------------------------
let fhirPatient = (ourl,callback) =>{
    sprl.token(ourl.reqqe,ourl.targetqe,(token)=>{            
        let args = {  
            method:'GET',
            ca:[fs.readFileSync(config.ca,"utf-8")],
            key: fs.readFileSync(config.privateKey,"utf-8"),
            cert: fs.readFileSync(config.publicKey,"utf-8"),
            agent: false,
            headers: {  "Authorization":"Bearer "+token,
                        "Content-Type":"application/fhir+json; charset=UTF-8",
            },
            rejectUnauthorized:false,
            timeout:180000,
            requestConfig: config.clientconfig.requestConfig,
            responseConfig: config.clientconfig.responseConfig
        };
        callurl_http(ourl, args,(data, response) => {      
            let result = data && ! data.error ? parseresult(data) : data
            callback(data,response)
        })
    }) 
};
//---------------------------------------------
let callurl_http = (ourl,args,callback) =>{
    args.timeout = config.httpsTimeout;
    args.keepAlive = false
    let url = ourl.url
    let targetqe = ourl.targetqe
    let qeconfig = configqeparams()
    let qeurl  = qeconfig[targetqe].url
    let qeOID  = "urn:oid:"+qeconfig[targetqe].OID
    let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
    console.log("callurl_http url = %s\n",url)
    let req = https.request(encodeURI(url),args, (response) => {
        timediff.endtime = moment()
        timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
        let result = {}
        let status = response.statusCode
        let data ='';
        console.log("callurl_http code = %s for\n%s\n",status,url)
        response.on('data', function (chunk) {
            data = data + chunk;
        });   
        response.on('end',function(){
            data = parseresult(data)
            response.elapsedtime =  timediff.elapsed
            if(status == 200){
                callback(data,response)
            } else {
                if(data.resourceType=="OperationOutcome")  {
					if(data.meta){
					    if(data.meta.source){

						} else {
							data.meta.source=qeOID
						}
					}
					else{
						data.meta = {"source":qeOID}
					}
                    callback(data,response)
                } else {
                    let err = fhirprofiles.errorresponse(url,data,response) 
					if(err.meta){
					    if(err.meta.source){
							
						} else{
							err.meta.source=qeOID
						}
					}
					else{
						err.meta = {"source":qeOID}
					}
                    callback(err,response)
                }

            }
        });
        response.on('error',(err)=>{
            console.log("httpsError %s for %s",err,url)
        })
    })
    req.on('socket',  (socket) => {
        socket.setTimeout(config.socketTimeout);  
        socket.on('timeout', function() {
            console.log("timeout for %s",url)
            req.abort();
        });
    });
    req.on('error',(error)=>{
        console.log("call_HTTPS_error %s for %s",error.code,url);      
        let response  = fhirprofiles.errorresponse(url,error,{statusCode:error.code,headers:args.headers}) 
        callback(response,error)
    })
    req.on('requestTimeout', function (req) {
        console.log('request has expired');
        req.abort();
    })
    req.on('responseTimeout', function (res) {
        console.log('response has expired');     
    });
    req.end()
}
//----------------------------------------------------------
let decodejwtToken = (token,callback)=>{
    let cl = new Client()  
    let url = `${authurl}/jwt/decode?token=${token}`
    let args = {
        parameters: {"token":token} , // this is serialized as URL parameters
        headers: { "Authorization":"Bearer "+token,"X-Token":"test"} // request headers             
    };
    cl.get(url, args,(data, response) => {          
        callback(data)
    })
}
//----------------------------------------------------------
let _processResource = (type,req,res) => {
	xgetFHIRparams(req,(params)=>{
		let token= params.token
		let reqqe = params.reqqe
		let targetqe = params.targetqe
        let tokenid = params.tokenid
        let transactionID = utils.getuuid()
        params.seq = 0
		console.log("_processResource \n%s \n%nparams=%j",token,tokenid,params)
		isValidJWT(token, tokenid,type,(data) => {
            console.log("PROCESS RESOURCE seq = %s\n%j",params.seq,params)
            params.transactionID = transactionID
            let qrydata = {req,params,jwtdata:data,token,tokenid}
            audit.insertRequest(type,params,qrydata,(err,result)=>{
                // console.log(err||result)
                if(result && result.length > 0 ) {
                    reqid = result[0].ID
                }
                if( isEmpty(data.error) ) {  
                    if( targetqe== 'STATEWIDE'  ){  // 
                        console.log(`\n\nCALLING STATEWIDE ${type}s \n\n`)
                        _FHIRResources(req,res,type,params)
                    } else {
                        console.log(`\n\nCALLING ${type}\n\n`)
                        _FHIRResource(req,res,type,params)
                    }
                } else {
                // res.render('error403')
                res.send({error:data})
                }
            })
		})
	});
}
//----------------------------------------------------------
exports.GetObservation = (req,res) => {
    _processResource('Observation',req,res)
}
//---------------------------------------------
exports.GetImmmunization = (req,res) => {
    _processResource('Immunization',req,res)
}
//---------------------------------------------
exports.GetEncounter = (req,res) => {
   // console.log("calling encouter",req.params,req.query)
    _processResource('Encounter',req,res)
}
//---------------------------------------------
exports.GetProcedure = (req,res) => {
    _processResource('Procedure',req,res)
}
//---------------------------------------------
exports.GetMedication = (req,res) => {
    _processResource('MedicationRequest',req,res)
}
//---------------------------------------------
exports.GetmedicationAdministration = (req,res) => {
    _processResource('MedicationAdministration',req,res)
}
//----------------------------------------------------------
exports.GetCondition = (req,res) => {
	_processResource('Condition',req,res)
}
//---------------------------------------------------------------------
let _FHIRResource = (req,res,type,params)=>{
    let patient = params.patient
    type += (isEmpty(patient)?'':'.patient')
    let method = params.method;
    let token = params.token
    let ourl = getFHIRurl(params,type)
 
    if(isEmpty(method)){
        method = "GET"
    }
    ourl["sprlurl"] = req.headers.host+req.url
    let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
    _fhirresource(type,req,ourl,params,(data,response)=>{
        let _data = clone(data)
        result = parseresult(data)
        result= setsingleqeresults(result,ourl)
        result.request ={"Method":"Get", "url":params.url}
        if(result.fullUrl){
            
        }else{
            if(params.url.includes("targetqe=")) {
                result.fullUrl = params.url
            } else{
                result.fullUrl = params.url + (params.url.includes("?") ? 
                "&targetqe="+params.targetqe : 
                "?targetqe="+params.targetqe)
            }
        }
        // send only result from qe not bundle
        parseEnteryResponse(params,ourl,result,(out)=>{ 
            timediff.endtime = moment()
            timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
            params.seq += 1
            let qrydata = {req,params,resource:type,response,result:{"FCSResponse":result,"QEResponse":{"Response":_data,"Request":ourl.url}},elapsedtime:timediff.elapsed }
            //audit.insertqeResponse(qrydata,(err,result)=>{
            audit.insertResponse(qrydata,(err,result)=>{
                res.send({"FCSResponse":result,"QEResponse":{"Response":_data,"Request":ourl.url}})
            })
        })
    })
}
//-----------------------------------------------------------------------------------------------
let _fhirresource =(type,req,ourl,params,callback) =>{
    
    let method = params.method;
    //params.seq += 1
    if(isEmpty(method)){
        method = "GET"
    }
    //console.log("_fhirresource params = %j\n\n req.params=%j\n\nreq.query = %j\n\n",params,req.params,req.query)
    sprl.token(ourl.reqqe,ourl.targetqe,(token)=>{
        let args = {  
            method,
            ca:[fs.readFileSync(config.ca,"utf-8")],
            key: fs.readFileSync(config.privateKey,"utf-8"),
            cert: fs.readFileSync(config.publicKey,"utf-8"),
            agent: false,
            headers: {  "Authorization":"Bearer "+token,
                        "Content-Type":"application/fhir+json; charset=UTF-8",
            },
            rejectUnauthorized:false,
            timeout:config.httpsTimeout,
            requestConfig: config.clientconfig.requestConfig,
            responseConfig: config.clientconfig.responseConfig
        };          
        decodejwtToken(token,(jwtdata)=>{
            let reqid = 0
            let qrydata = {req,params,ourl,args,jwtdata}
           
            //audit.insertstateRequest(type,qrydata,(err,result)=>{
            params.seq += 1
            audit.insertRequest(type,params,qrydata,(err,result)=>{
            // console.log(err||result)
                if(result && result.length > 0 ) {
                    reqid = result[0].ID
                }
                let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
                callurl_http(ourl,args,(data,response)=>{
                    timediff.endtime = moment()
                    timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                    params.seq += 1
                    let qrydata = {id:reqid,resource:type,params,result:data,response,elapsedtime:timediff.elapsed}
                      audit.insertResponse(qrydata,(err,result)=>{
                       // console.log(err||result)
                    })
                    callback(data,response)
                })
            })       
        })
    }) 
};
//-----------------------------------------
let _FHIRResources =  (req,res,type,params) => {
    params.host = req.headers.host
    let showlog = params.showlog?params.showlog:config.showlog
    params.seq += 1
    processResources(type,req,res,params,(error,out,pix)=>{
        if(out.resource && out.resource == 0 ) {
            fhir_profile.operationOutcome({severity:"warning"},(out)=>{
                if(showlog==1) {
                    res.send({result:out,pix,error})
                } else {
                    res.send(out)
                }
            })
        } else {
            if(showlog==1) {
                res.send({result:out,pix,error})
            } else {
                res.send(out)
            }
        }
    })
};

//-------------------------------------------------------------------------------------
let setsingleqeresults = (result,ourl)=> {
    let qeconfig = configqeparams()
    let targetqe = ourl.targetqe
    let qeurl = qeconfig[targetqe].url
    let qeOID = "urn:oid:"+qeconfig[targetqe].OID
    if(result.entry && result.entry.length > 0 ) {
        result.entry.forEach((elem)=>{
            if(elem.resourceType && elem.resourceType == 'Bundle') {

            } else {
				if(! elem.resource.meta) {
                    elem.resource.meta = {"source":qeOID}
                } else {   
                    if(!elem.resource.meta.source)  {
                        elem.resource.meta.source = qeOID
                    }
                }
            }
        })
    } else {
       // console.log("Result without entry2" , result)
        if(result.resourceType && result.resourceType == 'Bundle') {
            if(result.total==0) {
                //console.log("Result bundle without entry2")
                //console.log("qeurl",qeOID)
                fhirprofiles.operationOutcome({
                    id:"NoDataFound",
                    url:"",
                    code:"200",
                    severity:"warning",
                    error:"Empty Successfull Response",
                    targetqe:qeOID
                },(out)=>{
                    result = out
                })
            }
        }else if(result.resourceType && result.resourceType == 'OperationOutcome') {
         //   console.log("Result with operation outcome2")
            if(! result.meta) {
                result.meta = {"source":qeOID}
            } else {
				if(!result.meta.source)	{
					result.meta.source = qeOID
				}
			}
        } else{
			if(! result.meta) {
                result.meta = {"source":qeOID}
            } else {
				if(!result.meta.source)	{
					result.meta.source = qeOID
				}
			}
		}
    }
    return result
}
//----------------------------------------------------------------------------------------------------------
let getResourcesAsynch = (type,req,profile,params,entries,callback) =>{
    let id = params.id
    let count = 0
    let seq = params.seq
    //console.log("getResourceAsynch params = %j\n\n req.params=%j\n\nreq.query = %j\n\n",params,req.params,req.query)
    if(entries) {
        let data =  entries[0].resource?entries[0].resource.identifier:{}

        let qecount = validqes(data)
        console.log("get resourcesAsync smpi data",data)
        if(qecount == 0) {
           profile.response =  fhirprofiles.operationOutcome({
               "id":0,
                code:"200",
                error:"Patient exists only in requesting qe",
                //url:"smpi url",
                qe:"Requesting QE : "+params.reqqe,
                severity:"Warning"
            })
            callback (profile)    
       }else {
            let patient = params.patient
            type += (isEmpty(patient)?'':'.patient')
           // console.log("data",data)
            data.forEach((elem,i)=>{
                let mpiid = elem.value
                let targetqe = elem.assigner.display
                if(targetqe == 'SMPI'){

                } else {
                    let reqqe = params.reqqe
                    params.mpiid = mpiid
                    params._id = mpiid
                    params.targetqe = targetqe
                   // console.log("valid qe = %s\nmpi=%s",targetqe,mpiid)
                    if( isvalidQE(targetqe) )  { 
                        let ourl = getFHIRurl(params,type)
                        params.seq = seq+1
						ourl.url=ourl.url.replace("?patient.identifier="+params.patient,"")
							_fhirresource(type,req,ourl,params,(result,response)=>{
  							parseEnteryResponse(params,ourl,result,(out)=>{ 
								count++
								profile = processResult(req,profile,ourl,params,out,response)	
								if(count == qecount){
									//console.log("count %s of %s",count,qecount)
									callback (profile)    
								} 
							})                           
						     
                        })
                    }  
                }
            })
       }
    } else{
       callback (profile)    
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------
let getresources = (type,req,profile,params,data,index,callback)=>{    
    let id = params.id
    //console.log("getresources params = %j\n\n req.params=%j\n\nreq.query = %j\n\n",params,req.params,req.query)
    if(data && index < data.length){   
        let mpiid = data[index].value
        let targetqe =  data[index].assigner.display
        let reqqe = params.reqqe
        // console.log("valid qe = %s",targetqe)
        if( isvalidQE(targetqe) )  { 
            let ourl = getFHIRurl(params,type)
            _fhirresource(type,req,ourl,params,(result,response)=>{
                profile = processResult(req,profile,ourl,params,result,response)
                getresources(type,req,profile,params,data,index+1,callback)
            })
        } else{
            getresources(type,req,profile,params,data,index+1,callback)
        }
    } else {
       // console.log("getresources for %s callback index=%s",type,index)
        callback(profile)
    }
}
//------------------------------------------------------------
let processResources = (type,req,res,params,callback) =>{
    let mpi = params.mpi
    let mpiid = params.mpiid
    let sprlurl = params.url
    let reqqe = params.reqqe
    let targetqe = params.targetqe
    let oid = ""
    let pixResult = params.pixResult?params.pixResult:config.pixResult?config.pixResult:0
    let qeconfig = configqeparams()
    let seq = params.seq
    if(isEmpty(seq)) {
        seq = 1
    }
    //console.log("reqqe=%s targetqe=%s qeconfig=%j",reqqe,targetqe,qeconfig)
    if(reqqe){ 
        reqqe = reqqe.toUpperCase()
        if (isvalidQE(reqqe) ){
            oid = qeconfig[reqqe].OID
        }
    }
    if(isEmpty(oid)){
        oid = qeconfig[targetqe].OID
    }
    //console.log("\n\nreqqe = %s \noid =%s\n \nmpi =%s\n \nmpiid =%s\nseq=%s",reqqe,oid,mpi,mpiid,seq)
    let method = params.method;
    if(isEmpty(method)){
        method = "GET"
    }
    if(! isEmpty(oid)){
        let qrydata ={ req,profile:{},response:{},result:params}
     
            sprl.querypixInternal(req,params,oid, mpiid, reqqe,params.transactionID, (err, pix,response,pixParameters) => {
                pix = parseresult(pix)
                fhirprofiles.resourceResponse('type',{},(profile)=>{
                    profile.resource.link = {
                        "relation":"self", 
                        "url":params.url
                    }
                    if( config.includepix ) {
                        if(pixResult == 1){
                            profile.resource.entry.push(pix)
                            profile.resource.total ++
                        } else if(pixResult == 2){
                            profile.resource.entry.push(pixParameters)
                            console,log("parmeters",pixParameters)
                            profile.resource.total ++
                        } else {
                            profile.resource.entry.push({pixParameters,pixBundle:pix})
                            profile.resource.total ++
                        }
                    }
                    if(! err) {
                        let entries = pix.resource.entry
                        profile.resource.request = {
                            method,
                            url:sprlurl
                        }   
                        if(config.useAsynch){
                        
                            let reqid = 0
                            let qrydata ={ req,profile,response,result:pix}
                            params.seq = seq+1
                            //audit.insertqeRequest(type+'s',qrydata,(err,result)=>{
                            audit.insertRequest(type,params,qrydata,(err,result)=>{
                                if(result && result.length > 0 ) {
                                    reqid = result[0].ID
                                }
                                params.reqid = reqid
                                let timediff = {starttime:moment(),endtime:moment(),elapsed:0}
                                getResourcesAsynch(type,req,profile,params,entries,(profile) =>{
                                    timediff.endtime = moment()
                                    timediff.elapsed = timediff.endtime.diff(timediff.starttime,'millseconds')
                                    params.seq += 1
                                    let qrydata = {req,resource:type,params,profile,response,result:profile,elapsedtime:timediff.elapsed }
                                    //audit.insertqeResponse(qrydata,(err,result)=>{
                                    audit.insertResponse(qrydata,(err,result)=>{
                                        callback(err,profile,pix)
                                    })
                                })
                            })
                        }else {    
                            getresources(type,req,profile,params,entries[0].resource.identifier,0,(profile)=>{
                                callback(err,profile,pix)
                            })
                        }
                    } else {
                        callback(err,profile,pix)
                    }
                })
            })
      
    } else {
        let errporf = fhirprofiles.errorresponse(sprlurl,"qe OID not found " + reqqe,{statsCode:"401"}) 
        callback({error:"Invalid QE "+reqqe},errporf,{})
    }
}
//---------------------------------------------------------------------------
let processResult= (req,profile,ourl,params,result,response) =>{
    let qeconfig = configqeparams()
    let nestedBundle = params.nested ?params.nested:config.nestedBundle
    let targetqe = ourl.targetqe
    let qeurl =  qeconfig[targetqe].url
    let qeOID = "urn:oid:"+qeconfig[targetqe].OID
    if(typeof result == 'string' ){
        if( result.includes("<")  ){
            result = {error:result}
        } else {
            result = JSON.parse(result)
        }
    }
    if( result && ! result.error) {
        if( ! nestedBundle    ) {
            if(result.entry && result.entry.length > 0 ) {
                result.entry.forEach((elem)=>{
                    if(elem.resourceType && elem.resourceType == 'Bundle') {
                    }else {
						if (elem.resource.meta){
							if(elem.resource.meta.source){
								
							}else{
								elem.resource.meta.source =qeOID
							}
						}else{
                            elem.resource.meta = {"source":qeOID}
                        }
                        profile.resource.entry.push(elem)
                        profile.resource.total ++
                    }
                })
            } else {
              //  console.log("Result without entry" , result)
                if(result.resourceType && result.resourceType == 'Bundle') {
                    if(result.total==0) {
                        console.log("Result bundle without entry")
                        fhirprofiles.operationOutcome({
                            id:0,
                            url:"",
                            code:"200",
                            severity:"warning",
                            error:"Empty Successfull Response",
                            targetqe:qeOID,
                        },(out)=>{
                            profile.resource.entry.push(out)
                            profile.resource.total ++
                        })
                       //operation outcome for Empty successfull reponse
                    }
                }else if(result.resourceType && result.resourceType == 'OperationOutcome') {
                    console.log("Result with operation outcome")
					if (result.meta){
							if(result.meta.source){
								
							}else{
								result.meta.source =qeOID
							}
						}
					else{
						result.meta = {"source":qeOID}
					}
                    profile.resource.entry.push(result)
                    profile.resource.total ++

                }else {
				    profile.resource.entry.push(result)
                    profile.resource.total ++
                }

			}
            
        } else {
            if(! result.request) {
                result.request = {"method":"GET", "url":ourl.url}   
            }                        
            if(! result.response) {
                if(result.error) {

                } else {
                    result.response =  {"status": response.statusCode,
                        "details":response.headers ,
                        "location":response.responseUrl,
                        "outcome":STATUS_CODES[response.statusCode],
                        "lastModified":response && response.headers && response.headers.date ?response.headers.date:'n/a'                        
                    }
                }                        
            }
            profile.resource.entry.push(result)
            profile.resource.total ++                                       
        }      
    } else {
        let err = fhirprofiles.errorresponse(ourl,result,response)
        profile.resource.entry.push(err)
        profile.resource.total ++     
    }
    return profile
}
//
//----------------------------------------------------------------------
let processentry = (params,batch,entry,index,resplist,callback)=>{
    //console.log("process entry = %j",entry)
    let id = batch.id+"~"+index
    let reqqe = batch.id.split("~")[0].toUpperCase()
    let qeconfig = configqeparams()
    if(index < entry.length){
        let oid = qeconfig[reqqe].OID;
        let mpiid = params.mpiid
        let mpi = entry[index].request.url.split("/")[2]
        let type = entry[index].request.url.split("/")[1]
        let method = entry[index].request.method
        let reqqe = params.reqqe.toUpperCase()
        sprl.querypixInternal(null,params,oid, mpiid, reqqe, params.transactionID,(err, data,response,pixParameters) => {
            data = parseresult(data)
            if(! data.error) {
                params.id =id        
                params.reqqe = reqqe
                params.method = method
                params.type = type
                params.reqid = reqid
                let funccall = eval(method.toLowerCase()+type.toLowerCase())
                eval("fhirprofiles."+type.toLowerCase()+"Response")({},(profile)=>{
                    funccall(profile,params,data,0,(result)=>{
                        resplist.push(result)
                        processentry(params,batch,entry,index+1,resplist,callback)
                    })
                })                
            } else {
                processentry(params,batch,entry,index+1,resplist,callback)
            }
        })
    } else {
        fhirprofiles.sprlBatchResponseBundle({"entry":resplist,id,reqqe},(out)=>{
            callback(out)
        })
    }
}
//--------------------------------------------------------------
let processbatchfile = (params,filenames,index,list,callback)=>{
    if(index < filenames.length){
        let fname = filenames[index].fullname
        let filename = filenames[index].filename
        let batch = JSON.parse(fs.readFileSync(fname,"utf-8"))
        let entry = batch.entry
        console.log("filenmae = %s\nbatch = %j \nentry = %j\nindex=%s",fname,batch,entry,index)
        processentry(params,batch,entry,0,[],(ent)=>{
            let dir = config.batchrespath
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            let time = moment().format('YYYYMMDDHHmmSS')
            let outfile = dir+"/"+time+"_"+filename  

            let backupdir = config.batchbackuppath         
            let newname = backupdir+"/" +filename                               
            if (!fs.existsSync(backupdir)){
                fs.mkdirSync(backupdir);
            }
            fs.writeFileSync(outfile,JSON.stringify(ent,null,3),"utf-8")
            // move current file
            /*
            fs.rename(fname,newname,(err)=>{
                if(err) console.log(err)
            })
            //*/    
            list.push(ent)
            console.log("processbatchfile index = %s",index)
            processbatchfile(params,filenames,index+1,list,callback)
        })
    } else {
        if(callback){
            callback(list)
        }    
    }
}
//-----------------------------------------------------------------
let processBatch = (params,callback)=> {
    getBatchFiles(params,(files)=>{
       if(files.error) {
            callback({"error":files.error})
       } else {
           let index = 0
           let list = []
           processbatchfile(params,files,index,list,(result)=>{
                //console.log("OUTPUT result = %j",result)
                callback(result)
           })
       }
   })
}
//----------------------------------------------------------------
let  inserttest = (type,params,ourl,out) =>{
    let qry = 
    `
    insert into tests (sprlurl,qe,qereq,mpi,link,request,response,status,error,timestamp) 
                values('${ourl.sprlurl}','${params.targetqe}','${params.reqqe}','${params.mpi}','${ourl.url}','${type}','${JSON.stringify(out.result,null,2)}','${out.status}','${out.error}',GETDATE())
    `
    runQry(qry,(err,result)=>{
        if(DEBUG) {
            console.log("qry=%s params=%j res=%s",params,err,result)
        }
    })
}
//-----------------------------------------------------------------
exports.fhirquery = (req,res)=>{
    let params = getFHIRparams(req)
    getfhirQuery((err,result)=>{
        res.send(result)
    })
}
//----------------------------------------------------------------
let  getfhirQuery = (callback) =>{
    let qry = 
    `
    select * from fhirquery
    order by timestamp desc
    `
    runQry(qry,(err,result)=>{
        if(callback){
            callback(err,result)
        }
    })
}
//-----------------------------------------------------------------
exports.fhirqueryedit = (req,res)=>{
    let params =req.params
    let query = params.query
    let data = req.body
    //console.log("fhirqueryedit data = %j",data)
    let newdate = data.timestamp.replace("T"," ").replace("Z","")

    if(data){
        let qry = 
            `update fhirquery SET 
            type = '${data.type}'
            ,resource = '${data.resource}'  
            ,reqqe = '${data.reqqe}'
            ,target = '${data.target}'
            ,mpi = '${data.mpi}'
            ,baseurl = '${data.baseurl}'
            ,code = '${data.code}'
            ,date = '${data.date}'
            ,category = '${data.category}'
            ,url = '${data.url}'
            WHERE timestamp = '${newdate}' `
        runQry(qry,(err,result)=>{
            res.send(result)
            if(err) console.log(err)
        })
    }
   
}
//-----------------------------------------------------------------
exports.fhirquerydelete = (req,res)=>{
    let params =req.params
    let query = params.query
    let data = req.body
    //console.log(req.body,params,query)
    if(data){
        let  dates = ""
        data.rows.forEach(element => {
            let newdate = element.timestamp.replace("T"," ").replace("Z","")
            dates += isEmpty(dates)?"'"+newdate+"'":",'"+newdate+"'"
        });
        let qry = 
            `delete from fhirquery  WHERE timestamp in (${dates}) `
        runQry(qry,(err,result)=>{
             //  console.log(result)
        })
    }
    res.send({"data":req.body})
}
//----------------------------------------------------------------
let  geturlhitory = (callback) =>{
    let qry = 
    `
    select * from urlhistory
    order by timestamp desc
    `
    runQry(qry,(err,result)=>{
        if(callback){
            callback(err,result)
        }
    })
}
//-----------------------------------------------------------------
exports.urlhistory = (req,res)=>{
    let params = getFHIRparams(req)
    geturlhitory((err,result)=>{
        res.send(result)
    })
}
//-----------------------------------------------------------------
exports.urlhistoryadd = (req,res)=>{
    let params =req.params
    let query = params.query
    let data = req.body
    //console.log(req.body,params,query)
    let newdate = data.timestamp.replace("T"," ").replace("Z","")
    if(data){
        let qry = 
            `insert into urlhistory(type,url,timestamp) VALUES('${data.type}','${data.url}', GETDATE() )`
        runQry(qry,(err,result)=>{
               // console.log(qry,err)
        })
    }
    res.send({"data":req.body}) 
}
//-----------------------------------------------------------------
exports.urlhistoryedit = (req,res)=>{
    let params =req.params
    let query = params.query
    let data = req.body
   // console.log(req.body,params,query)
    let newdate = data.timestamp.replace("T"," ").replace("Z","")
    if(data){
        let qry = 
            `update urlhistory SET type = '${data.type}' ,url = '${data.url}' WHERE timestamp = '${newdate}' `
        runQry(qry,(err,result)=>{
             //   console.log(qry,err)
        })
    }
    res.send({"data":req.body})
}
//-----------------------------------------------------------------
exports.urlhistorydelete = (req,res)=>{
    let params =req.params
    let query = params.query
    let data = req.body
  //  console.log(req.body,params,query)
    //let newdate = data.timestamp.replace("T"," ").replace("Z","")
    if(data){
        let  dates = ""
        data.rows.forEach(element => {
            let newdate = element.timestamp.replace("T"," ").replace("Z","")
            dates += isEmpty(dates)?"'"+newdate+"'":",'"+newdate+"'"
        });
        let qry = 
            `delete from urlhistory  WHERE timestamp in (${dates}) `
        runQry(qry,(err,result)=>{
              //  console.log(result)
        })
    }
    res.send({"data":req.body})
}
//----------------------------------------------------------------
let  urlInsert = (type,hurl,callback) =>{
    let qry = 
    `insert into urlhistory(type,url,timestamp) VALUES('${type}','${hurl}', GETDATE() )`
   // console.log("INSERT qry = %s",qry)
    runQry(qry,(err,result)=>{
        if(callback){
            callback(err,result)
        }
    })
}
//-----------------------------------------------------------------
exports.urlinsert = (req,res) =>{
    let params = getFHIRparams(req)
    let hurl = params.hurl
    let type = params.type
   // console.log("URLINSERT params = %j",params)
    urlInsert(type,hurl,(err,result)=>{
        res.send(result)
    })
}
//----------------------------------------------------------------
exports.fhirqueryInsert =(req,res) =>{
    let params = getFHIRparams(req)
    let data = req.body
    if(data){
        let qry = 
        `INSERT into fhirquery (type,reqqe,target,mpi,resource,baseurl,code,date,category,url,timestamp) 
         VALUES('${data.type}','${data.reqqe}', '${data.target}', '${data.mpi}', '${data.resource}', '${data.baseurl}', '${data.code}', '${data.date}', '${data.category}', '${data.url}', GETDATE() )`
       // console.log("INSERT qry = %s",qry)
        runQry(qry,(err,result)=>{
            res.send(result)
        })
    }
}
//-----------------------------------------------------------------
exports.jsonviewer = (req,res) =>{
    let data = req.body
    if(isEmpty(data)){
        data=req.params.data
    }   
    if(isEmpty(data)){
        data=req.query.data
    }
   // console.log("DATA %j",data)
    res.render('json_viewer',{mode:"view",data,page_title:""})
    
    //res.send(data)
}
//----------------------------------------------------------------------
let OIDtoQE = (oid)=>{
    let result = "STATEWIDE"
    let qeconfig = configqeparams()
    Object.keys(qeconfig).forEach( (element,key)=> {
        if(qeconfig[element].OID == oid){
            result = element
        }
    })         
    return(result)
}
//-------------------------------------------------------------------------------------------------
let xgetFHIRparams =  (req,callback)=>    {
    let headers = req.headers
    let _params = {}
    let query = req.query
    let params = req.params
    let body = req.body
	_params.url = req.getUrl();
    Object.keys(query).forEach( (element,key)=> {
        _params[element] = query[element] 
    })
    Object.keys(params).forEach( (element,key)=> {
        _params[element] = params[element] 
    }) 
    Object.keys(body).forEach( (element,key)=> {
        _params[element] = body[element] 
    }) 

	
    if(isEmpty(_params.patient))
	{
		_params.patient = _params["patient.identifier"]
	}

	 
    if(! isEmpty(_params.patient)){
        _params._id = _params.patient  
		_params.mpiid = _params.patient
    }
	
	
	if(! isEmpty(_params["patient.identifier"])){
        _params._id = _params["patient.identifier"]  
		_params.mpiid = _params["patient.identifier"]  
		_params.patient = _params["patient.identifier"] 
    }
	if(! isEmpty(_params.identifier)){
        _params._id = _params.identifier 
		_params.mpiid = _params.identifier
    }
    if(! isEmpty(_params.sourceIdentifier)){
        if(_params.sourceIdentifier.indexOf("|") > -1) {
            _params.mpiid = _params.sourceIdentifier.split("|")[1]     
            let oid = _params.sourceIdentifier.split("|")[0]
            _params.oid = oid
            _params.reqqe = OIDtoQE(  oid.replace("urn:oid:","") )   
        } else {
            _params.mpiid = _params.sourceIdentifier
        }
    }
    if(_params.mpiid && _params.mpiid.indexOf("|") > -1) {
       // let oid = mpiid.split("|")[0]
        _params.reqqe = OIDtoQE(  _params.mpiid.split("|")[0].replace("urn:oid:","") )   
        _params.mpiid = _params.mpiid.split("|")[1]    
    }
    _params.isStatewide = _params.targetqe=="STATEWIDE"
    //------------------
    if(! isEmpty(headers)){
        let auth = headers['authorization']
        let tokenid = headers['X-Token']
        if(auth && isEmpty(_params.token)) {
            _params.jwtToken = auth.split(" ")[1]
			_params.token = _params.jwtToken
			//console.log("_params.jwtToken" + _params.jwtToken)
        }
    }
    if(isEmpty(_params.sub) && ! isEmpty(_params.reqqe)) {
        _params.sub =  _params.reqqe
    }
    if(isEmpty(_params.target && !isEmpty(_params.targetqe)) ) {
        _params.target =  _params.targetqe
    }  
    if(! isEmpty(_params.jwtToken) ){
		decodejwtToken(_params.jwtToken,(data)=>{
            _params.jwt = {"decoded":data,"jwt":_params.jwtToken}
			_params.targetqe = data && data.payload && data.payload.targetSystem ? data.payload.targetSystem:"STATEWIDE"
			_params.target = data && data.payload && data.payload.targetSystem ? data.payload.targetSystem:"STATEWIDE"
            _params.reqqe = data && data.payload  && data.payload.sub ? data.payload.sub:"STATEWIDE"
			callback(_params)
		})
    }else{
		callback(_params)
	}  
}
//===============================
let getFHIRparams = exports.getFHIRParams = (req,callback)=>    {
    let headers = req.headers
    let mpi = req.query.mpi;   
    let mpiid = req.query.mpiid;      
    let _id = req.query._id;   
    let targetqe = req.query.targetqe;     
    let tarqeid = req.query.tarqeid;    
    let reqqe = req.query.reqqe;    
    let url = req.getUrl();
    let host = req.protocol + "://" + req.get('host') 
    let smpiid = req.query.smpiid
    let category = req.query.category
    let code = req.query.code
    let date = req.query.date
    let resource= req.query.resource
    let resourceQuery = req.query.resourceQuery
    let qeid = req.query.qeid    
    let hurl = req.query.hurl
    let type = req.query.type    
    let profile = req.query.profile
    let file = req.query.file
    let showlog = req.query.showlog
    let nested = req.query.nested
    let method = req.query.method
    let transactionID = req.query["transactionID"]
    let token = req.query.token
    let jwt = req.query.jwt    
    let sub = req.query.sub    
    let target = req.query.target
    let sourceIdentifier = req.query.sourceIdentifier
    let targetSystem = req.query.targetSystem
    let patient_subject =req.query["patient.subject"]
    let pixResult = req.query.pixResult
    let _format = req.query._format
    let _include = req.query._include
    let tokenid = 'n/a'
    ///  Added 20200727
    if(! isEmpty(headers)){
        // console.log("Get Params Headers",headers)
         tokenid =  headers['X-Token']
         let auth = headers['authorization']
        //console.log("\n\nparams.auth = %s\n\ntoken=%s\n\n",auth, auth.split(" ")[1])
         if(auth && isEmpty(token)) {
             token = auth.split(" ")[1]
         }
     }	   
    let clinicalStatus  = req.query.clinicalStatus
    let isStatewide=false
    tokenid = req.query.tokenid
    if(isEmpty( tokenid) ){
        tokenid= req.params.tokenid
    }
	let patient = req.query["patient"]
    
	if(isEmpty(patient))
	{
		patient = req.query["patient.identifier"]
	}
    if(isEmpty(_id)) {
        _id = req.params._id
    } 
    if(isEmpty(clinicalStatus)) {
        clinicalStatus = req.params.clinicalStatus
    }  
	
    if(isEmpty(patient)) {
		patient = req.params["patient"]
    }
	 if(isEmpty(patient)) {
		
		patient = req.params["patient.identifier"]
    }
    let participant = req.query.participant
    if(isEmpty(participant)) {
        participant = req.params.participant
    }
    let performer = req.query.performer
    if(isEmpty(performer)) {
        performer = req.params.performer
    }
    let diagnosis = req.query.diagnosis
    if(isEmpty(diagnosis)) {
        diagnosis = req.params.diagnosis
    }
    let service_provider = req.query["service-provider"]
    if(isEmpty(service_provider)) {
        service_provider = req.params["service_provider"]
    }
    let part_of = req.query["part-of"]
    if(isEmpty(part_of)) {
        part_of = req.params["part-of"]
    }
    let location = req.query.location
    if(isEmpty(location)) {
        location = req.params.location
    }
    let requestor = req.query.requestor
    if(isEmpty(requestor)) {
        requestor = req.params.requestor
    }
    let asserter = req.query.asserter
    if(isEmpty(asserter)) {
        asserter = req.params.asserter
    }
    let medication = req.query.medication
    if(isEmpty(medication)) {
        medication = req.params.medication
    }
    let reaction = req.query.reaction
    if(isEmpty(reaction)) {
        reaction = req.params.reaction
    }
    let status = req.query.status
    if(isEmpty(status)) {
        status = req.params.status
    }
    let intent = req.query.intent
    if(isEmpty(intent)) {
        intent = req.params.intent
    }
    let encounter = req.query.encounter
    if(isEmpty(encounter)) {
        encounter = req.params.encounter
    }
    if(isEmpty(_include)) {
        _include = req.params._include
    }
    //
    if(isEmpty(patient_subject)){
        patient_subject = req.params["patient.subject"]
    }
    if(isEmpty(sourceIdentifier)){
        sourceIdentifier = req.params.sourceIdentifier
    }
    if(isEmpty( targetSystem)) {
        targetSystem = req.params.targetSystem
    }
    if(isEmpty(hurl)){
        hurl = req.params.hurl
    }   
     if(isEmpty(type)){
        type = req.params.type
    }  
    if(isEmpty(mpi)){
        mpi = req.params.mpi
    } 
    if(isEmpty(mpiid)){
        mpiid = req.params.mpiid
    } 
    if(isEmpty(targetqe)){
        targetqe  = req.params.targetqe
    } 
    if(isEmpty(tarqeid)){
        tarqeid  = req.params.tarqeid
    } 
    if(isEmpty(reqqe)){
        reqqe  = req.params.reqqe
    } 
    if(isEmpty(qeid)){
        reqqeid  = req.params.reqqeid
    } 
    if(isEmpty(smpiid)){
        smpiid  = req.params.smpiid
    } 
    if(isEmpty(category)){
        category  = req.params.category
    } 
    if(isEmpty(code)){
        code  = req.params.code
    } 
    if(isEmpty(date)){
        date  = req.params.date
    } 
    if(isEmpty(resource)){
        resource  = req.params.resource
    } 
    if(isEmpty(resourceQuery)){
        resourceQuery = req.params.resourceQuery
    }
    if(isEmpty(profile)){
        profile  = req.params.profile
    }    
    if(isEmpty(file)){
        file  = req.params.file
    } 
    if(isEmpty(showlog)){
        showlog  = req.params.showlog
    }  
    if(isEmpty(nested)){
        nested  = req.params.nested
    }     
    if(isEmpty(method)){
        method  = req.params.method
    } 
    if(isEmpty(method)){
        method  = "GET"
    } 
    if(isEmpty(transactionID)) {
        transactionID = req.params.transactionID
    }
    if(isEmpty(transactionID)) {
        transactionID =  utils.getuuid()
    }   
    if(isEmpty(token)) {
        token =  req.params.token
    }   
    if(isEmpty(jwt)) {
        jwt =  req.params.jwt
    }
    if(isEmpty(sub)) {
        sub =  req.params.sub
    }
    if(isEmpty(sub) && ! isEmpty(reqqe)) {
        sub =  reqqe
    }
    if(isEmpty(target)) {
        target =  req.params.target
    }
    if(! isEmpty(target) ) {
        targetqe =  target
    }   
    if(! isEmpty(sub) ) {
        reqqe =  sub
    }
    if(isEmpty(_format)){
        _format = req.params._format
    }  
    if(isEmpty(_format)){
       // _format = "json"
    }
    if(isEmpty(pixResult)){
        pixResult = req.params.pixResult
    }
   
    if(! isEmpty(patient_subject)){
        mpiid = patient_subject   
    }
    if(! isEmpty(patient)){
        _id = patient  
		mpiid = patient
    }
    if(! isEmpty(sourceIdentifier)){
        if(sourceIdentifier.indexOf("|") > -1) {
            mpiid = sourceIdentifier.split("|")[1]     
            let oid = sourceIdentifier.split("|")[0]
            reqqe = OIDtoQE(  oid.replace("urn:oid:","") )   
        } else {
            mpiid = sourceIdentifier
        }
    }
    if(mpiid && mpiid.indexOf("|") > -1) {
       // let oid = mpiid.split("|")[0]
        reqqe = OIDtoQE(  mpiid.split("|")[0].replace("urn:oid:","") )   
        mpiid = mpiid.split("|")[1]    
    }
    isStatewide = targetqe=="STATEWIDE"
   //console.log("MPIID from getFHIRURL", mpiid)
    let params = {tokenid,clinicalStatus,_include,intent,status,encounter,patient,_format,pixResult,targetSystem,sourceIdentifier,_id,patient_subject,sub,target,jwt,token,transactionID,mpi,mpiid,reqqe,targetqe,qeid,code,smpiid,category,date,resource,resourceQuery,profile,url,hurl,type,file,showlog,nested,method,isStatewide}    
    //console.log("\n\n\nGETparams: params = \n%j \n\nurls= \n%s ",params,req.url)
 //    console.log("GET PARAMS",params)
    return callback ? callback(params) : params
    
}
