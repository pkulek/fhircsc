
const config = require('../../config.json')
const Client = require('node-rest-client').Client;

exports.redirect = (req,res)=>{
    let io = req.app.get("socket.io")
    let body = req.body
    let requestMethod = req.method;
    let authurl = config.authurl
    let url = req.url
    let cl = new Client()    
    let newurl = `${authurl}${url}`
    if(requestMethod == 'GET') {
        cl.get(newurl, (data, response) => {    
            if(io){   
              io.emit('LOGIN_CLIENT',data);  
            }
            res.send(data)
        })
    }
    if(requestMethod == 'POST') {
        let args = {
            data: body,
            headers: { "Content-Type": "application/json" }
        };
        if(io){   
            io.emit('LOGIN_CLIENT',body);  
        }
        cl.post(newurl,args, (data, response) => {       
            res.send(data)
        })
    }
}
