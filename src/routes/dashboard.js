

const fs = require('fs')

const config = require('../../config.json');
let utils = require('../utils') ;

const isEmpty =  utils.isEmpty;
const DEBUG = config.DEBUG;
const sql = require('mssql');
const moment = require('moment'); // for date time
//--------------------------------------------------------------------------
const SPRLurls = config[config.devEnv+"_SPRLurls"]
//--------------------------------------------------------------------------
let runQry =  exports.runQry = function(qry,callback){    
    //let req = new sql.Request();
    //req.query(qry, (err, result) => {
    new sql.Request().query(qry, (err, result) => {
        if(err) {
            console.log(err)
        }
        if(callback){
            if(err) {
                callback(err,[] )
            } else {
                callback(err,result.recordsets[0])
            }
        }
    })    
}
//-----------------------------
exports.dashboard = (reqe,res)=>{
    res.render('dashboard')
}
//---------------------------------------------
exports.sendmessage = function(req, res){
    let io = req.app.get("socket.io")
    let data = req.body;
    let users = data.users ; // should be an array of user ids
    let parent = data.parent ;
    let msg = data.msg ;
    if(isEmpty(parent) ){
        parent = "PM_MESSAGE";
    }
    if(isEmpty(msg)){
        msg = "no message";
    }
    if(! isEmpty(users)){
        if(typeof users == 'string'){
            users = users.split(",")
        }
        users.forEach((userid) => {
            let pipemsg = `${parent}${userid}`
           // console.log("send msg to %s, %s via pipe %s",userid,msg,pipemsg)
            io.emit(pipemsg,msg)          
        });           
        dict.savemessage(users,parent,msg,(err,data)=>{           
        })             
        res.send("OK")

    } else{
        res.send("No Users")
    }
 };
//-----------------------------
exports.shell = (req,res)=>{
    let io = req.app.get("socket.io")
    let commands = req.params.commands
    res.setHeader('Content-Type','text/html');
    Client = require('ssh2').Client;
    const conn = new Client();
    const encode = 'utf8';

    if(isEmpty(commands)){
        commands = req.query.commands
    }
    console.log("cmd = %s ",eval(commands))
    if(isEmpty(commands)){
        commands =  [
            `sudo su`,
            `pm2 show 4`,
            'exit',
            'exit'
        ];        
    } else {
        commands = eval(commands)
    }
    const connection = {
        host: '192.168.180.131',
        username: 'pkulek',
        password: 'Remember1'
    }
    conn.on('ready', () =>{
      // avoid the use of console.log due to it adds a new line at the end of
      // the message
        process.stdout.write('Connection :: ready');
    
        let command = '';
        let pwSent = false;
     //   let su = false;
     /*
        let commands = [
            `sudo su`,
            'ls -lt',
            `pm2 list`,
            'exit',
            'exit'
        ];        
        */
        conn.shell((err, stream) => {
        if (err) {
          console.log(err);
        }
        stream.on('exit', function (code) {
            process.stdout.write('Connection :: exit\n');
            conn.end();
            res.end(code)
        }).on('data', function(data) {
            process.stdout.write(data.toString(encode));
            res.write(data.toString(encode))
            io.emit('TERM',data.toString(encode))
          //console.log(data.toString(encode))
            if (command.indexOf('sudo') !== -1 && !pwSent) {
                if (data.indexOf(':') >= data.length - 2) {
                    pwSent = true;
                    stream.write(connection.password + '\n');
                }
            } else {
                // detect the right condition to send the next command
              
                let dataLength = data.length > 2;
                let commonCommand = data.indexOf('$') >= data.length - 2;
                let suCommand = data.indexOf('#') >= data.length - 2;
                if (dataLength && (commonCommand || suCommand )) {
                    if (commands.length > 0) {
                        command = commands.shift();
                        console.log("command=%s",command)
                        stream.write(command + '\n');
                    }
                }
            }
        });
        // first command
        command = commands.shift();
        console.log(command)
        stream.write(command + '\n');
      });
    }).connect(connection);
}
//-------------------------------------------------------------------
exports.ssh = (req,res) =>{
    res.setHeader('Content-Type','text/html');
    let cmd = req.params.cmd
    if(isEmpty(cmd)){
        cmd = 'pm2 log 4'
    }
    let Client = require('ssh2').Client;
    let conn = new Client();
    conn.on('ready', function() {
        console.log('Client :: ready');
        conn.exec(cmd,{pty:true}, (err, stream) => {
            if (err) console.log(err);
            stream.on('close', (code, signal) => {
                console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                //res.send(signal)
                conn.end();
            }).on('data', (data) => {
                res.send(data)
            // stream.write('exit\n')
            //  res.setHeader('Content-Type','text');
            //  res.send(data)
                console.log('STDOUT: ' + data);
                
            }).stderr.on('data', function(data) {
            // res.send({data})
                console.log('STDERR: ' + data);
            });
        });
    }).connect({
        host: '192.168.160.131',
        port: 22,
        username: 'pkulek',
        password:"EndorKroner#1"
    });
}
//--------------------------
exports.sftp = (req,res) =>{
    let cmd = req.params.cmd
    if(isEmpty(cmd)){
        cmd = 'uptime'
    }
    let Client = require('ssh2').Client;
    let conn = new Client();
    conn.on('ready', function() {
        console.log('Client :: ready');
        conn.sftp(function(err, sftp) {
          if (err) throw err;
          sftp.readdir('/opt/hbmapp/views', function(err, list) {
            console.log(err|| list);
        //    res.setHeader('Content-Type','text');
            res.send(err||list);
            conn.end();
        });
    });
}).connect({
        host: '192.168.160.131',
        port: 22,
        username: 'pkulek',
        password:"EndorKroner#1"
    });
}
