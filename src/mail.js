

const isEmpty =  require('./utils').isEmpty;

const DEBUG = true;

let config =  require('../config.json');
let SENDMAIL = true ;
let email   = require("emailjs");
//let dict = require('./dic/dict')();
let emailserver = config.email_server
if (isEmpty(emailserver)){
    emailserver =  {
        user:    "", 
        password:"", 
        host:    "192.168.170.230", 
        port:25, 
        ssl:     false
     }
}
let server  = email.server.connect(emailserver)
   
/*
// send the message and get a callback with an error or details of the message that was sent
server.send({
   text:    "i hope this works", 
   from:    "you <username@your-email.com>", 
   to:      "someone <someone@your-email.com>, another <another@your-email.com>",
   cc:      "else <else@your-email.com>",
   subject: "testing emailjs"
}, function(err, message) { console.log(err || message); });
*/

module.exports = server;

//---------------------------------------------------------------------------------------
module.exports.sendEmail = (obj,mailtype,oMail) => {
    let cMail = '';
    if (isEmpty(mailtype)) {
       mailtype = "NYeCPM" ;
    };
    if (typeof oMail == 'object') {
        cMail = JSON.stringify(oMail);        
    }
    if (typeof oMail == 'string') {
        cMail = oMail;        
    }
    //let cMail = JSON.stringify(oMail);
    if( cMail != '') {
        if (DEBUG) { 
            //console.log("email.send %s\n Template = %s",mailtype,cMail) ;
            //console.log("email.send object = %j",obj) ;
        }
       // dict.get("JSON",mailtype,"MAIL",'en-US',(err,maildata)=> {
            /*
            if (! isEmpty(maildata)) {
                if (typeof maildata == 'object') {
                    cMail = JSON.stringify(maildata);
                } else {
                    cMail = maildata ;
                }
            } else {                    
                dict.set("JSON",mailtype,"MAIL",'en-US',cMail,(err,data)=>{});
            }
            */
            let sprintf = require("sprintf-js").sprintf;
            if(DEBUG){
                //console.log("email.send = mailtype= %s\n  cMail =%s",mailtype,cMail);
            }
            try {
                cMail = sprintf(cMail,obj);
            } catch(ex) {
                console.error("sprintf error=%s",ex);
            }
            //oMail = JSON.parse(cMail);
            if (DEBUG) {        
                  console.log("mail template \nmsg type =%s\n mail=%j\n obj=%j\n\n",mailtype,oMail,obj);
            }
            if (SENDMAIL) {
                server.send(oMail, (err, message) => {
                    if (DEBUG) { 
                        console.log("\n%s email response result =%j\n message=%s\n Error=%s\n\n",mailtype,message,err);
                    }
                });
            }
       // });           
    }
    return 0;
}
