const DEBUG = false

let utils = require('../utils') ;
const isEmpty =  utils.isEmpty;
const buildWhere = utils.buildWhere
module.exports = (config,dict) => {
    if(! config) {
        config = require("./config.json");
    }   
    if(! dict) {
        dict = require('./dict')()
    }
    //-------------------------------------------------------------------------
    let getParams = (req)=> {
        let query = req.query ;
        let params = req.params;
        if (DEBUG) {
            console.log("GetParams incomming query=%j params=%j",query,params)
        }
        let type = params.type;
        let id = params.id;
        let parent = params.parent;
        let ext = params.ext;  
        let notes = params.notes;  
        let cargo = params.cargo;  
        let lang = params.lang;
        let dir = params.dir;
        let file = params.file;
        let theme = params.theme;
        let data = req.body;    
        if(isEmpty(theme)){
            theme = query.theme;
        }
        if (isEmpty(file)) {  
            file=""; 
            if (! isEmpty(query.file)) {
                file = query.file;
            }
        }
        if (isEmpty(dir)) {  
            dir=""; 
            if (! isEmpty(query.dir)) {
                dir = query.dir;
            }
        }
        if (isEmpty(lang)) {  
            lang = req.headers["accept-language"] ;
            if (! isEmpty(query.lang)) {
                lang = query.lang;
            }        
        }
        if (isEmpty( type )){ 
            type=""; 
            if (! isEmpty(query.type)) {
                type = query.type;
            }
        }
        if ( isEmpty( id )){
            id="" ; 
            if (! isEmpty(query.id)) {
                id = query.id;
            }
        }    
        if ( isEmpty( parent )){ 
            parent="" ; 
            if (! isEmpty(query.parent)) {
                parent = query.parent;
            }
        }    
        if ( isEmpty( ext )){ 
            ext="" ; 
            if (! isEmpty(query.ext)) {
                ext = query.ext;
            }
        }     
        if ( isEmpty( cargo )){ 
            cargo="" ; 
            if (! isEmpty(query.cargo)) {
                cargo = query.cargo;
            }
        }       
        if ( isEmpty( cargo ) && ! isEmpty(data)){ 
            cargo=data ; 
        }       
        if ( isEmpty( notes )){ 
            notes="" ; 
            if (! isEmpty(query.notes)) {
                notes = query.notes;
            }
        }    
        let pars = {data,type,id,parent,ext,cargo,notes,lang,file,dir,theme}; // new es6 way
        if (DEBUG) {
            console.log("getParams.params = %j",pars);
        }
        return(pars); 
    }
   //-----------------------------------------------------------------------
    module.set = module.save = (req,res) => {
        let pars = getParams(req);   
        dict.set(pars,(err,data)=>{
            res.send(err||data);
        });
    };   
    //----------------------------------------------------------------------------------
    module.get = (req,res) => {
        let pars = getParams(req);   
        dict.get(pars,(err,rows)=>{
            res.send(err||rows);
        });
    };
    //----------------------------------------------------------------------------------
    module.delete = (req,res) => {
        let pars = getParams(req);   
        dict.remove(pars,(err,rows)=>{
            res.send(err||rows);
        });
    };
    //--------------------------------------------------------------------------------
    module.dicexport = (req,res)=> {
        let fs = require('fs');
        let params = getParams(req);    
        let dir = params.dir;
        let qry = `select * from dict where type not like '~%' `;
        let _writefile = (rows,index,dir,callback) => {
            if(index < rows.length) {
                let element = rows[index];
                let file = dir+`/${element.id}~${element.parent}~${element.ext}.${element.type}` ;
                fs.writeFile(file,element.cargo,(err)=>{
                    _writefile(rows,index+1,dir,callback)
                });
            } else {              
                if (callback) {
                    callback(index)
                }       
            }
        }       
        dict.query(qry,(err, rows, fields) => {
            if (isEmpty(dir) ){
                dir = config.exportPath;
            };
            let shell = require('shelljs');
            shell.mkdir('-p', dir);                
            _writefile(rows,0,dir,(err)=>{
                res.send("OK - Export Finished records = "+rows.length)
            })
        });
    };
    //-----------------------------------------------------------------------------------
    module.dicimport = (req,res)=> {
        let params = getParams(req);    
        let dir = params.dir;
        let pType = params.type;
        if (isEmpty(pType)) {
            pType = '*';
        }
        let _savefile = (files,index,callback)=>{
            if(files && index < files.length) {
                let part = files[index].split('.')[0];            
                let type = files[index].split('.')[1];
                let s = part.split('~');            
                if((pType == '*')  || (type ==  pType)){
                    let notes = dir+files[index]
                    console.log(notes)
                    //*
                    let cargo = require('fs').readFileSync(dir+files[index], 'utf8');      
                    dict.save(type,s[0],s[1],s[2],cargo,notes,(err,result)=>{
                        if(index < files.length) {
                            _savefile(files,index+1,callback)
                        }
                    })
                } else {
                    _savefile(files,index+1,callback)
                }  
            } else { 
                if(callback){
                    callback(index)
                }           
            }
        }       
        if (isEmpty(dir) ){
            dir = config.importPath;
        };
        require('fs').readdir(dir,'binary',(err,files)=>{
            if( files) {
                _savefile(files,0,(index)=>{
                    res.send("OK "+pType+" count = "+index);
                })            
            } else {
                res.send("No files found ")
            }
        })
    
    };    
    //-----------------------------------------------------------------------------------
    module.savefile = (req,res)=> {
        let pars = getParams(req);    
        if(isEmpty(pars.file)) {
            res.send("No file name")
        } else {
            if(require('fs').existsSync(pars.file)) {
                let cargo = require('fs').readFileSync(pars.file, 'utf8');      
                let f = require('path').basename(pars.file)
                let notes = `{"file":"${pars.file}"}`
                if(isEmpty(pars.type)) {
                    pars.type = f.split('.')[1]            
                }
                if(isEmpty(pars.id)) {
                    pars.id = f.split('.')[0]            
                }
                if(isEmpty(pars.parent)) {
                    pars.parent = "FILE"            
                }
                dict.save(pars.type,pars.id,pars.parent,'import',cargo,notes,(err,result)=>{
                    if(err){
                        res.send(err)
                    }else{
                        res.send( pars.file+"<br> Saved OK")
                    }
                })
            } else {
                res.send("file not found"+pars.file)
            }
        }
    };    
    //----------------------------------------------------------------------------------------
    module.backup = (req,res)=> {
        let params = getParams(req);      
        let d = new Date(); 
        let date = moment(d).format('YYYYMMDD');   
        let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
        let where = buildWhere(params) ;
        console.time("Backup Test")
        if ( isEmpty(where) ) { // for backup all
            where = " where type not like '~%'"; 
        }
        let _insert = (rows,index,callback) => {
            if(index < rows.length) {
                let element = rows[index];
                dict.insert('~'+element.type,element.id ,element.parent, element.ext, element.cargo,element.notes,(err)=>{
                    _insert(rows,index+1)
                })    
            } else {
                res.send("OK - Backup Finished records = "+rows.length)
                console.timeEnd("Backup Test")
                if (callback) {
                    callback(null)
                }       
            }
        }
        let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
        dba.query(qry, (err, rows) => {
            if (rows && rows.length > 0 ) {
                _insert(rows,0)
            }     
        });
    }
    //-------------------------------------------------------------  
    module.index = (req, res) =>{
        let id = req.query.id;
        let userid = req.query.userid;
        let qe = req.query.qe;
        let theme = req.query.theme;
        if (isEmpty(theme)){
            theme = 'redmond'
        }    
        res.render('urlhistory',{page_title:'Dictionary Management',theme:theme,qe:qe,userid:userid});
    }     
    //-------------------------------------------------------------  
    module.manager = (req, res) =>{  //
        let id = req.query.id;
        let token = req.query.tokenid;    
        res.render('.\views\main_dictadmin',{page_title:'Dictionary Mgmt',theme:'overcast'});
    };
    //-------------------------------------------------------------
    module.getMsg = (req,res) => {
        var pars = getParams(req);
        pars.type = 'MSG'; 
        dict.get(pars.type,pars.id,pars.parent,pars.ext,(err,result)=>{
            res.send(err||result)
        });
    }
    //--------------------------------------------------------------
    module.getJson = (req,res)=>{
        var pars = getParams(req);
        pars.type = 'JSON'; 
        dict.get(pars.type,pars.id,pars.parent,pars.ext,(err,result)=>{
            res.send(err||result)
        });
    }

    //----------------------------------------------------------------------------------
    module.dicDelete = (req,res)=>{
        var pars = getParams(req);   
        dict.del(pars.type,pars.id,pars.parent,pars.ext,(err,result)=>{
            res.send(err||result)
        });
    }
    //----------------------------------------------------------------------------------
    module.getLANG = (req,res)=>{
        let id = req.query.id;
        let parent = req.query.parent;
        let ext = req.query.ext;
        let lang1 = req.query.lang1;
        let lang2 = req.query.lang2;
        let translate = req.query.translate;
        let type = "MESG";
        if (isEmpty(translate)) {
        //      translate =false ;
        } 
        console.log("\n%s \n%s \n%s \n%s\n%s",id,lang1,lang2,parent,translate)
        getLANG(id,parent,lang1,lang2,translate,(err,data)=>{
            res.send(JSON.stringify(data,null,2))
        })
    } 
    //----------------------------------------------------------------------------------
    module.saveLANG = (req,res)=>{
        let input = JSON.parse(JSON.stringify(req.body));    
        console.log("INPUT:\n%j",input);  
        let id = req.query.id;
        let parent = req.query.parent;
        let ext = req.query.ext;
        let lang1 = req.query.lang1;
        let lang2 = req.query.lang2;
        let type = "MESG";
        if(isEmpty(input.type)) {
            input.type = "MESG"
        }
        if(isEmpty(input.ext)) {
            input.ext = "en-US"
        }
        console.log("\n%s \n%s \n%s \n%s \n%s \n%s \n%s",input.type,input.id,input.lang1,input.lang2,input.parent,input.ext,input.ext2)
        set(input.type,input.id,input.parent,input.ext,input.lang1,()=>{
            set(input.type,input.id,input.parent,input.ext2,input.lang2)
        })
        res.send("OK")
    }
    //--------------------------------------------------------------------------------------
    module.getLangList = (req,res)=>{
        let id = req.query.id;
        let parent = req.query.parent;
        let option = req.query.option;
        lookup_lang(option,id,parent,(sel)=>{
            res.send(sel);
        })
    }     
    
//----------------------------------------------------------------------------------
exports.getUSERS = function(callback){  
    let type = "USER";
    let parent = "CREDENTIALS";
    let qry = "select  id as iduser, parent, notes, CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"'" ;
    dict.get(type,'',parent,'',(err,data)=>{
        if (err)  console.log("Error getting result 05 : %s ",err );
        callback(err,data);               
    });    
}
//----------------------------------------------------------------------------------
exports.ISUSER = (userid,callback) => {  
    let type = "USER";
    let parent = "CREDENTIALS";
    //var qry = "select count(*) as qty from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"' and id = '"+userid+"'" ;
    let result = false;
    dict.get(type,id,parent,'',(err,rows)=>{
        if (err)  console.log("Error getting result 06 : %s ",err );
        result = ( rows && rows.length > 0 );
        callback( result   ,err);               
    });    
}
   //----------------------------------------------------------------------------------
   module.isDict = (req,res) => {
        let schema = req.query.schema;
        let table = req.query.table;
        let result = 0;
        if (isEmpty( table )){       
            table='dict';
        }
        if ( isEmpty( schema )){
            schema='dict' ;
        }       
        let qry = "SELECT COUNT(*) AS isTable FROM information_schema.tables WHERE TABLE_SCHEMA = '"+schema+"' AND TABLE_NAME = '"+table+"'" ;
        dict.query(qry,(err, rows, fields)=>{           
            if(err) {           
                result=0;    
                res.status(400).send('Error')                       
            } else {
                result = rows[0].isTable ;
                res.status(200).send(result > 0 ? "Pong " : "No Dictionary found " )  
            }
        });        
        return result;
    }    
    
    //----------------------------------------------------------------------------------
    // upload file by POST
    /* sample form for upload
        <div id="uploaddialog">
            <form ref='uploadForm' 
            id='uploadForm' 
            action='/upload' 
            method='post' 
            encType="multipart/form-data">
                <input type="file" name="sampleFile" />
                <input type='submit' value='Upload!' />
            </form>		
        </div>
    */
    module.upload = (req, res) => {
        if (! req.files) {
            res.send('No files to upload.');
            return;
        }
        var sampleFile = req.files.sampleFile;
        var ext =  sampleFile.name.split('.').pop();
        if (DEBUG) {
            console.log(req.files);
            console.log(sampleFile.name);
        }
        var qry = ` INSERT INTO ${DICT_TABLE} SET ? `;
        let values = {
                    type:ext.toUpperCase(),
                    id: sampleFile.name,
                    parent:sampleFile.mimetype,
                    ext:ext.toUpperCase(),
                    cargo: sampleFile.data
                };
        dict.query(qry, values, (err, data) => {
            if(err) {           
                console.log('upload error');
                res.status(400).send('Error')                       
            } else {
                console.log('Uploaded OK ') 
                res.status(200).send('Uploaded')  
            }
        });
        return  ;
    };


    return module
}

