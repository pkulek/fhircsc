
let utils = require('./utils') ;
let moment = require('moment'); // for date time
let dba = null;
const isEmpty =  utils.isEmpty;
let _config = {};
//-----------------------------------------------------------------------------------
module.exports = function (dba) {
    if(isEmpty(dba)) {
        
    }
    // export data from mysql into sqlite
    module.mysql2sqlite = (req,res) => {
        let params = getParams(req);    
        let sqlitedic = params.sqlitedic ;
        let mysqldb =  require('./dic_mysql');
        if (isEmpty(sqlitedic) ){
            sqlitedic = '/tmp/dict.export';
        }
        let qry = `select * from dict where type not like '~%'`;
        if (! isEmpty(params.type)) {        
            qry = `select * from dict where type ='${params.type}'`;
        }
        mysqldb.query(qry,function(err, rows, fields){
            let sqlite = require('./dic_sqlite') ;
            sqlite.create(sqlitedic);
            rows.forEach((part, index) => { 
                if (DEBUG) {
                    console.dir(part.cargo);
                }
                res.write("ID = "+part.type + "~"+part.id+"")
                sqlite.save(part.type,part.id,part.parent,part.ext,part.cargo,part.notes) ;
                if (index == (rows.length-1) ){
                    res.end("OK") ;     
                    setTimeout(function() { // give time for cache to clear
                        sqlite.close();  
                    }, 1350 );            
                } 
            }); 
        });
    } 
    //-----------------------------------------------------------------------------------
    // export data from  sqlite into mysql
    module.sqlite2mysql = (req,res) => {
        let params = getParams(req);    
        let qry = `select * from dict where type ='${params.type}'`;
        let mysqldb =  require('./dic_mysql');
        let sqlite = require('./dic_sqlite') ;
        sqlite.query(qry,function(err, rows){
            rows.forEach((part, index) => { 
                if (DEBUG) {
                    console.dir(part.cargo);
                }
                mysqldb.save(part.type,part.id,part.parent,part.ext,part.cargo,part.notes) ;
                if (index == (rows.length-1) ){
                    setTimeout(function() { // give time for cache to clear
                        sqlite.close();  
                    }, 1350 );            
                } 
            }); 
            res.send("OK") ;     
        });
    } 
    //----------------------------------------------------------------------------------
    module.getSQN = function(id,prefix,callback) {
        let type = "SQN";
        let parent = "";
        let result = 1;
        let ext = "";
        let _found = false;
        if ( isEmpty(id) ) {
            id = "PROJECTS"
        }
        if ( isEmpty(prefix) ) {
            prefix = ""
        }
        let incr = 1;     
        let qry=  `select parent,ext from ${DICT_TABLE} where type = '${type}' and id = '${id}' `
        dba.query(qry,function(err, rows, fields){        
            if (err)  console.log("Error getting Sequence Number : %s ",err );
            if ( rows && rows.length > 0 ){
                result = parseInt(rows[0].parent);               
                ext = rows[0].ext;               
                _found = true
            }
            if(! isEmpty(ext)){
                prefix = ext;
            }
            result += incr;
            result = ("000000000" + result).slice(-6)
            
            if ( _found ){
                qry = `update ${DICT_TABLE} set parent = '${result}',ext = '${prefix}' where type = '${type}' and id = '${id}' `
            } else {
                qry = `insert into ${DICT_TABLE} set parent = '${result}', type = '${type}' ,id = '${id}' ,ext = '${prefix}' `
            }
            dba.execute(qry,(err)=>{
            //    callback( err,result);               
            })
            if(callback){
            callback( err,prefix+result);               
            }
        });    
        return true;
    }
    //----------------------------------------------------------------------------------
    module.saveROUTE = function(id,cargo,parent,callback){
        let type = 'ROUTE';
        let ext = '';
        if(isEmpty(parent)) { 
            parent = 'EXPRESS'; 
        }
        save(type,id,parent,ext,cargo,(err,data)=>{
            if (callback) {
                callback(err,data);
            }
            
        });
    }
    //----------------------------------------------------------------------------------
    module.getROUTE = function(id,parent,callback){
        let type = 'ROUTE';
        let ext = '';
        if(isEmpty(id)) {
            id = '';
        }
        if(isEmpty(parent)) {
            parent = 'EXPRESS';
        }
        get(type,id,(err,rows)=>{ 
            if ( rows && rows.length > 0  ) {
                callback(err,rows[0].cargo);               
            } else {
                callback(err,rows[0].cargo);               
            }
        });    
    }
 //----------------------------------------------------------------------------------
 module.saveGRP = (id,cargo,parent,callback) => {
    var type = 'GRP';
    var ext = '';
    save(type,id,parent,ext,cargo,(err,data)=>{
        if (callback) {
            callback(err,data);
        }
        
    });
}
//----------------------------------------------------------------------------------
module.getGRP = (id,callback) => {  
    var type = "GRP";
    get(type,id,(err,rows)=>{ 
        if ( err) {
            callback(rows,require('../routes/login').getRoles() ,err);
            console.log("Error dict.getGRP : %s ",err );
        } else {
            if (rows && rows.length > 0 ){
                callback(rows[0].cargo.split(','),require('../routes/login').getRoles(),err);       
            } else {
                callback('','',id+" Not Found");
            }      
        }
    });    
}
//----------------------------------------------------------------------------------
module.getUSERS = (callback) => {  
    let type = "USER";
    let parent = "CREDENTIALS";
    //let qry = "select  id as iduser, parent, notes, CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"' LIMIT 1;" ;
    get(type,'',parent,'',(err,data) => {
        if (err)  console.log("Error getting result 05 : %s ",err );
        //console.log(data)
        callback(err,data);               
    });    
}
//----------------------------------------------------------------------------------
module.ISUSER = (userid,callback) => {  
    var type = "USER";
    var parent = "CREDENTIALS";
    //var qry = "select count(*) as qty from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"' and id = '"+userid+"'" ;
    var result = false;
    get(type,userid,parent,'',(err,rows)=>{
        if (err)  console.log("Error getting result 06 : %s ",err );
        result = ( rows && rows.length > 0 );
        callback( result   ,err);               
    });    
}
//----------------------------------------------------------------------------------
module.getHTM = (req,res) => {  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "HTM";
    var qry = `select  cargo from ${DICT_TABLE} where type= '${type}' and id ='${id}' and parent = '${parent}' and ext = '${ext}'` ;
    get(type,id,parent,ext,(err,rows)=>{    
        if (err)  console.log(`Error getting result 05 : ${err}` );
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'text/html');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" Help not found");               
        }
    });    
    return;
}
//----------------------------------------------------------------------------------
module.getPDF = (req,res) => {  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "PDF";
    get(type,id,parent,ext,(err,rows)=>{
        if (err)  console.log("Error getting result 05 : %s ",err );
        // set header
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'application/pdf');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" PDF not found");               
        }
    });    
}
//----------------------------------------------------------------------------------
module.getJPG = (req,res) => {  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "JPEG";
    get(type,id,parent,ext,(err,rows)=>{
        if (err)  console.log("Error getting result 05 : %s ",err );
        // set header
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'application/jpeg');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" JPEG not found");               
        }
    });    
}
//----------------------------------------------------------------------------------
module.savePWDHistory = (token,callback) => { // cargo = token
    console.log('Save PWD HISTORY= %j',token);
    let type = 'PWD';
    let ext = '';
    let parent = "PWDHISTORY";
    let id = token.userid;
    save(type,id,parent,ext,token,(err,rows)=>{
        if (callback){
            callback(err,rows);
        }
    });                    
    return 'OK';
};
//----------------------------------------------------------------------------------
module.getPWDHistory = (userid,callback) => { // cargo = token
    var type = 'PWD';
    var ext = '';
    var parent = "PWDHISTORY";
    var datetime = moment().format('YYYYMMDD');
    var id = userid;
    //console.log('get PWD HISTORY='+userid);
    get(type,id,parent,ext,(err,rows)=>{
        if (err)  console.log("Error getting pwd history : %s ",err );
        callback(err,rows);               
    }); 
    return 'OK';
};
//----------------------------------------------------------------------------------
let getLANG = (id,parent,lang1,lang2,transl,callback) => {
    if (isEmpty(lang1)) {
        lang1 ='en-US' ;
    } 
    if (isEmpty(lang2)) {
        lang2 ='es-ES' ;
    }  
    let type = "MESG";
    let ext = lang1;           
let qry = `select 
type,
parent,
id as msgid,
cargo as lang1 ,
(select cargo from dict where type = '${type}' and ext = '${lang2}' and d.id = id and d.parent = parent ) as lang2,
(select ext from dict where type = '${type}' and ext = '${lang2}' and d.id = id and d.parent = parent ) as ext2,
ext,
cargo
from dict d 
where type = '${type}' and ext = '${lang1}'
order by id `
    dba.query(qry,(err, rows, fields)=> {
        if (err) {
            console.log("Error getting Messages : %s ",err );
        } 
        let cnt = rows.length ;
        let count = 0;
        if (cnt > 0 ) {
            rows.forEach( (element, index) => { // convert blob to text from buffer
                    rows[index].cargo = element.cargo.toString()  
                    if (element.lang1) {  
                        rows[index].lang1 = element.lang1.toString()
                    }
                    if (element.lang2) {  
                        rows[index].lang2 = element.lang2.toString()
                    }  
                    if (isEmpty(element.ext2)){
                        rows[index].ext2 = lang2
                    } 
                    if (transl > 0 ) {
                        if (isEmpty(element.lang2)){
                            // ms lang translate
                            const BingAppId = '599FA87C780A9743C56B85FA00AF65BA6ED641D3';//'73C8F474CA4D1202AD60747126813B731199ECEA';                                
                            let http = require('http');
                            let url = `http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=${BingAppId}&text=${rows[index].lang1}&from=${lang1}&to=${lang2}` ;
                            http.get(url, (resp) => {
                                let data = '';
                                // A chunk of data has been recieved.
                                resp.on('data', (chunk) => {
                                    data += chunk;
                                });
                                // The whole response has been received. Print out the result.
                                resp.on('end', () => {
                                    count++
                                    //console.log("Result data = %s",data);
                                    if ( data.substring(0,7) == '<string' ) {
                                        let tag = data.match(/<string [^>]+>([^<]+)<\/string>/)[1];
                                        //let tag = utils.Tags(data,'<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','</string>')[0].name ;
                                        rows[index].lang2 = tag;
                                    } else {
                                        rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1                                    
                                    }
                                    if( transl == 2 ) {
                                        try{ 
                                            let type = rows[index].type ;
                                            let id = rows[index].msgid ;
                                            let parent = rows[index].parent ;
                                            let ext1 = lang1 ;
                                            let ext2 = lang2 ;
                                            let cargo1 = rows[index].lang1;
                                            let cargo2 = rows[index].lang2;
    console.log("SAVE TRANSLATE LANGUAGE \ntype=%s \nid=%s \nparent=%s \next1=%s \next2=%s \ncargo1=%s \ncargo2=%s",type,id,parent,ext1,ext2,cargo1,cargo2)
                                            
                                            set(type,id,parent,ext1,cargo1,()=>{
                                                set(type,id,parent,ext2,cargo2);
                                            });
                                        } catch(e) {
                                            console.error(e)
                                        }
                                    }
                                    if (cnt == count) {
                                        callback(err,rows);
                                    }
                                });        
                            }).on("error", (err) => {
                                count++
                                console.log("Error: " + err.message);
                                rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1
                                if (cnt == count) {
                                    callback(err,rows);
                                }
                            });
                            /* // google translate
                            let translate = require('google-translate-api')
                            translate(rows[index].lang1,{from:`${lang1.substr(0,2)}`,to:`${lang2.substr(0,2)}`}).then(res => {
                                count ++    
                                rows[index].lang2 = res.text ;
                                // save 
                                if( transl == 2 ) {
                                    try{ 
                                        set(type,id,parent,lang1,rows[index].lang1,(err,data)=>{
                                            set(type,id,parent,lang2,rows[index].lang2)
                                        } );
                                    } catch(e) {
                                        console.error(e)
                                    }
                                }
                                if (cnt == count) {
                                    callback(err,rows);
                                }
                            }).catch(err=> {
                                count++;
                                rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1
                                if (cnt == count) {
                                    callback(err,rows);
                                }
                            });
                            */
                        } else {
                            count ++
                        }
                    } else {
                        count ++
                    }
                    if (cnt == count) {
                        console.log("Exit reached")
                        callback(err,rows);
                    }
            });
        }  else {
            callback("Error",[]);                    
        }      
    });       
};
 //----------------------------------------------------------------------------------
 let lookup_lang = module.lookup_lang = (option,id,parent,callback)=>{
    let langs = require('../languages1.json').lang;
    if(isEmpty(id)) {
        id = "languagelist"
    }
    if(isEmpty(parent)) {
        parent = "languagelookup"
    }
    if(isEmpty(option)) {
        option = 0;
    }
    let olist = "";
    let sellist = "";
    let count = Object.keys(langs).length ;
    let counter = 0 ;
    Object.keys(langs).forEach((element,key) => {
        counter ++;
        olist += `<option value="${element}">${langs[element]}</option>\n`
        if (counter == count) {
            if (option) {
                sellist = olist
            } else {
                sellist = `<select name ="${id}" id="${id}" class="${parent}">  ${olist}  </select>` 
            }
            callback(sellist);            
        }
    });
}

    //----------------------------------------------------------------------------------
    module.getURL = (id,parent,defurl,callback) => {  
        var type = "URL";
        var ext = '';
        if ( isEmpty(parent) ) {
            parent = "RESTURL";
        }
        set(type,id,parent,ext,defurl,(err,data)=>{
            callback(err,data);
        });
    };
    //----------------------------------------------------------------------------------
    module.getSQL = (id,parent,defsql,callback) => {  
        var type = "SQL";
        var ext = '';
        if ( isEmpty(parent) ) {
            parent = "PP_SQL";
        }
        if ( isEmpty( defsql )){ defsql="select * from portal.users" } ;
        set(type,id,parent,ext,defsql,(err,data)=>{
            callback(err,data);
        });    
    };
    //----------------------------------------------------------------------------------
    module.getSOAP = (id,parent,defenv,callback) => {  
        var type = "XML";
        var ext = '';
        if ( isEmpty(parent) ) {
            parent = "SOAPENV";
        }
        if ( isEmpty( defenv )){ defenv="" } ;
        var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
        dba.query(qry,(err, rows, fields) => {
            if (err) {
                callback(defenv,"{Error:'soap envelope not found'}")         
                console.log("Error getting soap envelope : %s ",err );
            }
            if (rows) {
                if (rows && rows.length > 0 ){
                    var qty= rows[0].qty
                    if ( qty > 0 ) {
                        qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                        var query = dba.query(qry,  (err, rows) => {
                                if (err) {
                                    callback(defenv,JSON.stringify({error:err}));
                                } else {
                                    callback( rows[0].cargo ,"");
                                }
                        });
                    } else {
                        if (! isEmpty(defenv) ) {
                            save( type,id,parent,ext, defenv, (err,result)=>{
                                callback(result,err);
                            });
                        } else {
                                callback("","No Envelope to save");
                        }
                    };
                }
            }
        });                    
    };
  
    //---------------------------------------------------
    module.GetUserMsg = (req, res) => {
        let type = req.params.type;
        let id = req.params.id;
        let parent = req.params.parent;
        let lang = req.params.lang;
        if( isEmpty(type)) {
            type = req.query.type ;
        }
        if( isEmpty(type)) {
            type = 'MESSAGE' ;
        }
        if( isEmpty(id)) {
            id = req.query.id ;
        }
        if( isEmpty(parent)) {
            parent = req.query.parent ;
        }
        if( isEmpty(parent)) {
            parent = 'PM_MESSAGE' ; // or PM_NOTE
        }        
        if( isEmpty(lang)) {
            lang = req.query.lang ;
        }
        if (isEmpty(lang)) {
            lang = req.headers["accept-language"] ;
        }
        let ext = "";        
        let qry=  `select CONVERT(cargo USING utf8) as message ,date,id,parent as msgtype from ${DICT_TABLE} where type = '${type}'   `
        if(! isEmpty(id)){
            qry += ` and id='${id}' `            
        }
        if(! isEmpty(parent)){
            qry += ` and parent='${parent}' `
        }
        dba.query(qry,(err, rows, fields) =>{        
            if (err) {
                res.send(err);
                if(DEBUG) {
                    console.error(err)
                }
            } else {
                res.send(rows );
            }            
        });    
    }; 
    //---------------------------------------------------
    module.GetWebMsg = (req, res) => {
        let id = req.params.id;
        let parent = req.params.parent;
        let lang = req.params.lang;
        let msgdef = req.params.msgdef;
        console.log("Params = %j",req.params)
        console.log("Query = %j",req.query)
        if( isEmpty(parent)) {
            parent = req.query.parent ;
        }
        if (isEmpty(lang)) {
            lang = req.headers["accept-language"] ;
        }
        getMesg(id,parent,lang,msgdef,(data,err) => {
            res.send( data);
        });      
    };

    //----------------------------------------------------
    module.getMSG = getMesg = (id,parent,lang,def,callback) => {
        set("MESG",id,parent,lang,def,(err,data)=> {
                callback(data);
        });
    };
    //----------------------------------------------------------------------------------
    let getEmail = module.getEmail = (id,parent,ext,cargo,callback)=>{  
        let type = "JSON";
        if ( isEmpty( parent )){ parent="MAIL" } ;
        if ( isEmpty( id )){ id="PP_MAIL" } ;
        if ( isEmpty( ext )){ ext="en-US" } ;    
        get(type,id,parent,ext,(err,data)=>{
            if (isEmpty(data)  ){
                if ( ! isEmpty(cargo) ) {            
                    save(type,id,parent,ext,cargo,(err,data)=>{
                        if (callback) { callback(err,data ); }
                    });
                } else {
                    if (callback) { callback(err,data ); }
                }
            } else {
                if (callback) {callback(err, data );}
            }
        });
    };
    //----------------------------------------------------------------------------------
    let getJS = module.getJS = (id,parent,ext,cargo,callback)=>{  
        let type = "JSCRIPT";
        if ( isEmpty( id )){ id="PP_JAVASCRIPT" } ;
        if ( isEmpty( parent )){ parent="PP_PORTAL" } ;
        if ( isEmpty( ext )){ ext="MAIN" } ;    
        get(type,id,parent,ext,(err,data)=>{
            if (isEmpty(data)  ) {
                if ( ! isEmpty(cargo) ) {            
                    save(type,id,parent,ext,cargo,(err,data)=>{
                        if (callback) { callback(err,data ); }
                    });
                } else {
                    if (callback) { callback(err,data ); }
                }
            } else {
                if (callback) {callback(err, data );}
            }
        });
    };
    //----------------------------------------------------------------------------------
    module.saveTOKN = (id,parent,cargo,callback) => {
        var type = 'TOKN';
        var ext = '';
        var datetime = moment().format('YYYYMMDD');
        if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
        removeExpiredTOKN('PPTOKEN',(err)=>{
            set(type,id,parent,ext,cargo,(err,data)=>{
                if(callback){
                    callback(err,data)
                }
                //console.log(err||data);
            });
        });
        return("OK");
    };
    //----------------------------------------------------------------------------------
    module.getTOKN = (id,callback) => {  
        var parent='PPTOKEN' ; 
        get("TOKN",id,parent,'',(err,data)=>{
            if (DEBUG) {        
                console.log("dict.getTOKN = %j\n            err=%s",data,err);
            }
            if (! isEmpty(data)) {
                if(callback) {
                    callback(err,data);
                }
            } else {
                if(callback) {
                    callback(err,"{}")    
                }
            }
        });
        return 0;
    }
    //--------------------------------------------------------------------------------
    module.getToken = (id,callback) => {  
        if (isEmpty(id) ) {
            callback("no ID",'')
        } else{
            let parent='PPTOKEN' ; 
            let type ='TOKN'
            let qry = `select  CONVERT(cargo USING utf8) as cargo from ${DICT_TABLE}  where id = '${id}' and type= '${type}' and parent = '${parent}' LIMIT 1` ;        
            dba.query(qry,(err, rows, fields) => {
                if (err)  console.log("Error getting result 05 : %s ",err );
                //console.log(data)
                if (rows.length > 0 ){
                    callback(err,rows[0].cargo);
                }   else {
                    callback("no user",'Error')
                }            
            });    
        }
    }
    //----------------------------------------------------------------------------------
    let saveUser = module.saveUSER = (token,callback) =>{ // cargo = token
         if(typeof token == 'string'){
            token = JSON.parse(token)
        }
        let type = 'USER';
        let ext = '';
        let parent = "CREDENTIALS";
        let id = token.userid;
        if (! isEmpty(id)) {
            console.log("token type = ",typeof token)
            save(type,id,parent,ext,token,(err,data) => {
                //console.log("dict.saveUser callback saving token=%j\n\n\nreturndata = %s",token,data)                
                if (callback){
                    callback(err,data);
                }
            });
        } else {
            //console.log("dict.saveUser Error saving token %j",token)
            callback("Error: empty userid",token)
        }
  
        return 'OK';
    };
    //----------------------------------------------------------------------------------
    let getUSER = module.getUSER = (id,callback) => {  //returns token
        let type = "USER";
        let parent = "CREDENTIALS";
        let qry = `select  CONVERT(cargo USING utf8) as cargo from ${DICT_TABLE}  where id = '${id}' and type= '${type}' and parent = '${parent}' LIMIT 1` ;
        dba.query(qry,(err, rows, fields) => {
            if (err)  console.log("Error getting result 05 : %s ",err );
            //console.log(data)
            if (rows && rows.length > 0 ){
                callback(err,rows[0].cargo);
            }   else {
                callback("no user",'{}')
            }            
        });    
    }
   
    //----------------------------------------------------------------------------------
    module.getFLOW = (id,parent,callback) => {  
        let type = 'JSCRIPT' ;
        let ext = ''; 
        if (isEmpty(id)) {
            id = 'AUTHENTICATE'
        }
        if (isEmpty(parent)) {
            parent='FLOW' ;
        }
        get(type,id,parent,ext,(err,rows)=>{
            if (DEBUG) {        
                console.log("dict.getFLOW = %j err=%s",rows,err);
            }
            callback( err,rows[0].cargo); // should be a function
        });
        return 0;
    }
    //----------------------------------------------------------------------------------
    let removeTOKN = module.removeTOKN = (tokenid,parent,callback) => {
        if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
        let pType = 'TOKN';
        let qry = `delete from ${DICT_TABLE} where type = '${pType}' and parent ='${parent}' and id = '${tokenid}' ` ;
        dba.query(qry,  (err, rows) => {
            if(err) {
                console.error(err )
            };
            if (callback) {
                callback(err)
            }
        });
        return 0;
    }
    //----------------------------------------------------------------------------------
    let removeExpiredTOKN = module.removeExpiredTOKN = (parent,callback) =>{
        if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
        let pType = 'TOKN';
        let qry = `select CONVERT(cargo USING utf8) as cargo from ${DICT_TABLE} where type = '${pType}' and parent ='${parent}'` ;
        dba.query(qry,  (err, rows) => {
            if(err) {
                console.error(err )
            };
            let tokenExpireFormat = 'YYYYMMDDHHmm';
            let now = moment().format(tokenExpireFormat);
            if ((!isEmpty(rows)) && (rows.length > 0)) {
                rows.forEach((_row)=>{
                    try {
                        cargo = JSON.parse(_row.cargo)
                        if( cargo) {
                            let expire = cargo.expire;
                            let id  = cargo.tokenid;
                            let userid = cargo.userid;
                            //console.log("now=%s exp=%s",now,expire)
                            if (isEmpty(expire) || now > expire )   {         
                                remove(pType,id,parent,'',(err,rows)=>{
                                    if(DEBUG) {
                                        console.log("Deleted %j",rows)
                                    }
                                });                        
                            }    
                        }
                    } catch (err)  {
                        console.error(err)
                    }                            
                });                
            }
            if (callback) {
                callback(err)
            }
        });
        return 0;
    }
       //----------------------------------------------------------------------------------
        let remove = module.remove = (type,id,parent,ext,callback)=>{
            dba.remove(type,id,parent,ext,(err,rows)=>{
                if(callback){
                    callback(err,rows) ;
                }
            });
        }
       //-----------------------------------------------------------------------------------
       let setuser = function(userid){
        if( isEmpty(userid)){
            userid = 'saadmin' ; 
        }
        try {
            get("USER",userid,"CREDENTIALS",'',(err,data)=>{
                if (DEBUG) {
                    console.log("setuser default err= %s\n data= %j",err,data);
                }
                if (err || data.length == 0 ) {
                    let login = require('../routes/login');
                    let token = login.newToken(userid,'Remember!1');
                    if (DEBUG){
                        console.log("setuser new token = %j",token);
                    }
                    token.valid = 1;
                    token.active = "Yes" ;
                    token.pwdReset = "No";
                    token.group ="SUPERADMIN";
                    token.roles = "PP_ALL",
                    login.saveUser(token,(err,data)=>{
                        //console.log(err||data)
                    }) ;            
                    save("USER",userid,"CREDENTIALS",'',token,(err,data)=>{
                        //console.log("setuser token =  %j result=%s",token,err||data);
                    });                
                }
            });
        } catch(ex){
            console.trace("Error creating user :"+ex)
        }
    }
    //--------------------------------------------------------------------------
    module.savemessage = (users,parent,message,callback)=>{
        if(typeof users == 'string'){
            users = users.split(",")
        }        
        if(isEmpty(parent) ){
            parent = "PM_MESSAGE"
        }
        if(isEmpty(message) ){
            message = "no message"
        }

        let index = 0
        sendmsg(users,index,parent,message)
    }
    //-------------------------------------------------------------------------
    module.getmessage = (userid,parent,callback)=>{
        if ( isEmpty( parent )){ parent='PM_MESSAGE' ; } ;
        if ( isEmpty( userid )){ userid='' ; } ;
        let type = 'MESSAGE';
        let wand = isEmpty(userid) ? "": " and id = '"+userid+"'"
        qry = `select id as userid,CONVERT(cargo USING utf8) as msg, DATE_FORMAT(date,'%W %D %M %Y - %k %i') as date from ${DICT_TABLE} where type= '${type}' and parent='${parent}' ${wand} order by date desc` ;        
        dba.query(qry, function (err, rows) {
            //console.log("\nget message \nqry=%s\nrows=%j,\nerr=%s\n",qry,rows,err)
            if (err) {
                callback(err,"");
            } else {
                callback("", rows );
            }
        });
    }
    //-------------------------------------------------------------------------
    module.mysqlgridqry = (qry,callback) =>{
        dba.query(qry, function (err, rows,fields) {
            if(rows && rows.length > 0) {
                fields.forEach((eleme,i)=> {
                    
                })
            }
        });
    }
    //--------------------------------------------------------------------------
    let sendmsg = (ausers,index,parent,message)=>{
        userid = ausers[index];
        if (! isEmpty(userid)) {
            add("MESSAGE",userid,parent,'',message,(err,data)=>{
                if(ausers.length > index) { 
                    index = index + 1
                    sendmsg(ausers,index,parent,message)
                }
            })
        }
    }
    //--------------------------------------------------------------------------
    module.get_ejsTemplate = (name,path,callback)=>{
        if (typeof path == 'function' && isEmpty(callback)){
            callback = path;
            path = config.templatePath;
        }    
        if (isEmpty(path)) {
            path = config.templatePath;
        } 
        get("EJS",name,"TEMPLATE","",(err,template)=>{ //get ejs template from dictionary
            if ( isEmpty(template)){  // fallback to file based if empty
                try {
                    let fs = require('fs');
                    let filePath = path + `/${name}.ejs`;
                    if ( fs.existsSync(filePath) ) {
                        template = fs.readFileSync(filePath, 'utf8');
                        save("EJS",name,"TEMPLATE","",template);
                        callback("",template);
                    } else {
                        callback(err,template);
                        console.error("DICT:Template File not found:"+filePath );                    
                    }               
                } catch(e){
                    console.error(e);
                    callback(e,"");
                }
            } else {
                callback(err,template);
            }
        });
    }

return module;
};
