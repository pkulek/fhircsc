
// main functions
// dict = require("dict.js")(dba,config) use dictionary passing in optional config config file and database type
// save(type,id,parent,ext,cargo) saves or updates the cargo 
// get(type,id,parent,ext,callback(err,cargo)) get the cargo for type


const utils = require('../utils') ;
const moment = require('moment'); // for date time
const DICT_TABLE = 'dict';
const isEmpty =  utils.isEmpty;
const clone = utils.clone;
let dba = null;
module.exports = (db, config) => {
    if(isEmpty(config) ) {
        config = require("./config.json");
    }
    //console.log(config.dict)
    let DEBUG =  config['DEBUG'];
    if( isEmpty(db)) {
        if(!isEmpty(config) ){
            db = config.dict.db
        } else {
            db = 'sqlite';
        }
    }
    if( isEmpty(dba)) {
        dba = require('./dic_'+db)(config.dict[db]);
    }
    //-------------------------------------
    module.open = (callback) =>{
        dba.open((error)=>{
            if(callback){
                callback(error)
            }
        })
    }
    //--------------------------------------
    module.config = (_config) => {
        if(_config){
            config = clone(_config)
        }
        return config ;
    }
    if (DEBUG) {
        console.log("DICT CONFIG= %j",config.dict[db])
    }
    //-----------------------------------------------------------------------------------
    let load = module.load = (file,callback)=> {
        let fs = require('fs');
        if ( fs.existsSync(file) ) {
            //mysql_import(file,(err,rows)=>{
            dba.import(file,(err,rows)=>{
                if (callback) {
                    callback(err,rows);
                } 
            });
        } else {
            console.log("Dictionary export file not found: " + file);        
        }
    };
    //---------------------------------------------------------------------------------
    let query = module.query = (qry,callback)=> {
        if ( dba.connected() ) {
            dba.query(qry,(err,rows)=>{
                if(callback){
                    callback(err,rows)
                }
            })
        } else {
            if(callback){
                callback("Database not connected",[]);        
            } 
        }
        return 0
    };
    //---------------------------------------------------------------------------------
    let get = module.get = (type,id,parent,ext,callback)=> {
        if (typeof type =='object') { // pass in param = json
            o = type;
            if (typeof id == 'function'){
                callback = id;
            }
            id = o.id;       
            parent = o.parent;       
            ext = o.ext;       
            type = o.type;
        }   
        if ( dba.connected() ) {
            if(DEBUG){
                console.log("type=%s,id=%s,parent=%s,ext=%s",type,id,parent,ext)
            }
            dba.get(type,id,parent,ext,(err,data)=>{
                if(data && data.length > 0 ) {
                    if(DEBUG) {
                        console.log("dict.get len =%s data= %j",data.length,data)
                    } 
                    if (callback) {
                        if(data.length == 1) {
                            callback(err,data[0].cargo);
                        } else {
                            callback(err,data);
                        }
                    } 
                } else {
                    if (callback) {
                        callback(err,"");
                    } 
                }
            });
        } else {
            if(callback){
                callback("Database not connected","");        
            } 
        }
        return 0
    };
  
    //----------------------------------------------------------------------------------
    let set = save = module.set = module.save =  (type,id,parent,ext,cargo,notes,callback) => { 
        if (typeof type =='object') { // pass in param = json
            o = type;
            if (typeof id == 'function'){
                callback = id;
            }
            id = o.id;       
            parent = o.parent;       
            ext = o.ext;       
            type = o.type;
            notes= o.notes
            cargo = o.cargo
        }   
        if (typeof cargo == 'object' ){
            let CJSON = require("circular-json"); 
            cargo = CJSON.stringify(cargo,null,3);
        }
        //console.log("set = %s",cargo)
        dba.save(type,id,parent,ext,cargo,notes,(err,data)=>{
            if(callback){
                get(type,id,parent,ext,(err,data)=>{
                    callback(err,data);
                })
            }
        });
        return 0
    };
     //----------------------------------------------------------------------------------
     let del = module.remove =(type,id,parent,ext,callback) => {   
        if (typeof type =='object') { // pass in param = json
            o = type;
            if (typeof id == 'function'){
                callback = id;
            }
            id = o.id;       
            parent = o.parent;       
            ext = o.ext;       
            type = o.type;
        }   
        if ( dba.connected() ) {
            if(DEBUG){
                console.log("type=%s,id=%s,parent=%s,ext=%s",type,id,parent,ext)
            }
            dba.remove(type,id,parent,ext,(err,rows)=>{
                if(callback){
                    callback(err,rows);        
                } 
            });
        } else {
            if(callback){
                callback({"error":"Database not connected"},[]);        
            } 
        }
    }
    //----------------------------------------------------------------------------------
    let getfile = module.getfile = (type,id,parent,ext,path, callback) => {
        console.log("get file")
        get(type,id,parent,ext,(err,row)=>{
            if(err) { console.log(err); }
            let fs = require('fs');
            fs.writeFile(path+"\\"+id+"."+type, row, (err) => {
                if(err) { console.log(err); }
                if(callback){
                    callback(err)
                }
            });
        })       
    };   
    
    //----------------------------------------------------------------------------------
    module.savePWDHistory = function(token,callback){ // cargo = token
        console.log('Save PWD HISTORY= %j',token);
        let type = 'PWD';
        let ext = '';
        let parent = "PWDHISTORY";
        let id = token.userid;
        save(type,id,parent,ext,token,(err,rows)=>{
            if (callback){
                callback(err,rows);
            }
        });                    
       
    };


     //----------------------------------------------------------------------------------
     module.saveEMAIL = function(token,callback){ // cargo = token
        console.log('Save EMAIL= %j',msg)
        let type = 'EMAIL';
        let ext = '';
        let parent = "FCSREGISTER";
        let id = token.userid;
        save(type,id,parent,ext,token,(err,rows)=>{
            if (callback){
                callback(err,rows);
            }
        });                    
       
    };


    return module;

};





