
/*

*/

let utils = require('../utils') ;
let moment = require('moment'); // for date time
let connected = false;
let conn = null ;
const DICT_TABLE = 'dict';
let _config = "";
const USEBACKUP = false
let pool = null;
const USEPOOL = false

const isEmpty =  utils.isEmpty; 
const buildWhere = utils.buildWhere

module.exports =  (config) => {
        if(isEmpty(config) ) {
            config = { 
                    "dba":"mysql",
                    "mysql":{
                        "host":"localhost",
                        "user":"root",
                        "password":"EndorKroner#1",
                        "port":"3306",
                        "typeCast":true,
                        "database":"dict"
                    }                 
                }            
        }
         config.DEBUG = false;         
        //console.log("mysql config = %j",config)
        //----------------------------------------------------------------------------------
        let open = module.open = (callback) => {
            if(! _connected()) {
                try {
                    let mysql = require('mysql');
                    console.log("Starting mysql %j",config)
                    //*
                    if(USEPOOL) {
                        pool = mysql.createPool(config)
                        pool.getConnection((err, connection) => {
                            if (err) {
                                connected = false;
                                console.error(err)
                            } else {
                                conn = connection
                                connected = true;
                            }
                        });
                        //*/
                    } else {
                    ///*                
                        conn = mysql.createConnection(config,'single');
                        conn.connect(function(err) {
                            if (err) {
                                connected = false;
                                console.error(err)
                            } else {
                                connected = true;
                            }
                            console.log("MYSQL connected%s err %s",connected,err)
                            if(callback){
                                callback(connected)
                            }
                        })
                    }
                    //*/
                } catch(ex){
                    connected = false;
                    if(callback){
                        callback(ex)
                    }
                    console.trace(ex)
                }    
            }
        }
        //----------------------------------------------------------------------------------
        let _connected = module.connected = ()=> {
            return connected;
        }
        //----------------------------------------------------------------------------------
        module.close = (callback)=> {
            if(conn){
                console.log("closing mysql %s",connected)
                conn.end();
                connected = false;
                if(callbck){
                    callback(conn)
                }
            }
        }
        //----------------------------------------------------------------------------------
        let savefile = module.savefile = function(type,id,parent,ext,cargofile, callback){
            if (! isEmpty(cargofile)) {
                let fs = require('fs');
                fs.readFile( cargofile, function (err, data) {           
                    if (err) {
                        console.trace(err); 
                    } else {                
                        save(type,id,parent,ext,'','',(err,result)=>{ // save details  
                            let where = buildWhere(type,id,parent,ext); // update and add cargo - if not done this way saves stream as data 
                            conn.query(`UPDATE ${DICT_TABLE} SET cargo = ? ${where}  ` , [ data ], function (err) { }); 
                            if (! isEmpty(callback)) {
                                callback(err,data);
                            };
                        });                    
                    };
                });              
            };
        };
        //---------------------------------------------------------------------------------
        let query = module.query = module.execute = (qry,callback)=> {
            conn.query(qry,(err, rows, fields)=>{
                if(callback){
                    callback(err,rows,fields);
                }
            });
        }
        //----------------------------------------------------------------------------------
        let insert = module.insert = (type,id,parent,ext,cargo,notes,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDDHHmmss');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let post = {id, type,parent, cargo,notes, date_time, date, ext}; // es6 format 
            let qry = ` INSERT INTO ${DICT_TABLE} SET ? `
            if(USEPOOL){
                pool.getConnection((err, conn) => {
                    conn.query(qry,post,(err, rows, fields) => {                    
                        if (err) { console.log("dict.mysql_get Error getting result : %s ",err );}
                        if (config.DEBUG) {
                            console.log("dict.mysql_get ROWS=%j\n\nqry = %s",rows,qry);
                        }        
                        conn.release();
                        if (callback) {
                            callback(err,rows);
                        }
                    });                
                });
             } else {
            //*/
            //*
                conn.query(qry,post, (err, result,fields)=> { 
                    if (callback) {
                        callback(err,result); 
                    }  
                });
            //*/
            }
        }
        //----------------------------------------------------------------------------------
        let update = module.update = (type,id,parent,ext,cargo,notes,callback) =>{
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDDHHmmss');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();

            let post = {cargo,notes,date_time}; // es6 format 
            if(isEmpty(notes)){
                post = {cargo,date_time}; // es6 format 
            }
            let qry = ` UPDATE ${DICT_TABLE} SET ? where type ='${type}' and id = '${id}' and parent = '${parent}' and ext = '${ext}' `
            if(USEPOOL) {
            //*
                pool.getConnection((err, conn) => {
                    conn.query(qry,post,(err, rows, fields) => {                    
                        if (err) { console.log("dict.mysql_get Error getting result : %s ",err );}
                        if (config.DEBUG) {
                            console.log("dict.mysql_get ROWS=%j\n\nqry = %s",rows,qry);
                        }   
                        conn.release();     
                        if (callback) {
                            callback(err,rows);
                        }
                    });                
                });
            //*/
            } else {
            //*
                conn.query(qry,post,  (err, rows,fields) => { 
                    if(err) console.log("mysql update err %s/n%s",err,qry)
                    if (callback) {
                        callback(err,rows); 
                    }
                });
            //*/
            }
        }
        //----------------------------------------------------------------------------------
        let set = save = module.save = module.set = function(type,id,parent,ext,cargo,notes,callback){
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDDHHmmss');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext);
            let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
            if(USEPOOL){
                //*
                pool.getConnection((err, conn) => {
                    conn.query(qry,(err, rows, fields) => {                    
                        if (err)  console.log("\nError getting result mysql_dicSave : %s\n for qry %s\n\nrows=%s ",err,qry ,rows);
                        // backup record - should  work only on update not insert as it should not be there
                        if(USEBACKUP) {                    
        //                    backup(type,id,parent,ext);          
                        }
                        let qty= 0;
                        if (rows && rows.length > 0 ) {  
                            qty = rows[0].qty;
                        }            
                        if ( qty > 0 ) {
                            update(type,id,parent,ext,cargo,notes,(err,rows)=>{
                                if(callback){
                                    conn.release();
                                    callback(err,rows)
                                }
                            })
                        } else {
                            insert(type,id,parent,ext,cargo,notes,(err,rows)=>{
                                if(callback){
                                    conn.release();
                                    callback(err,rows)
                                }
                            })
                        }
                        conn.release();
                    });                
                });
            } else {
                //*/

                //*
                conn.query(qry,(err, rows,fields)=>{
                        if (err)  console.log("\nError getting result mysql_dicSave : %s\n for qry %s\n\nrows=%s ",err,qry ,rows);
                    // backup record - should  work only on update not insert as it should not be there
                    if(USEBACKUP) {                    
    //                    backup(type,id,parent,ext);          
                    }
                    let qty= 0;
                    if (rows && rows.length > 0 ) {  
                        qty = rows[0].qty;
                    }            
                    if ( qty > 0 ) {
                        update(type,id,parent,ext,cargo,notes,(err,rows)=>{
                            if(callback){
                                callback(err,rows)
                            }
                        })
                    } else {
                        insert(type,id,parent,ext,cargo,notes,(err,rows)=>{
                            if(callback){
                                callback(err,rows)
                            }
                        })
                    }
                });                
                //*/
            }
        };
        //----------------------------------------------------------------------------------
        let get = module.get = (type,id,parent,ext,callback)=> {
            let result= 0;
            if (isEmpty(callback)){
                if (isEmpty(ext) && typeof parent === 'function'){
                    callback = parent;
                    parent = "";
                }
                if (typeof  ext === 'function' ){        
                    callback = ext;
                    ext = "";
                }
            }
            let where = buildWhere(type,id,parent,ext) ;
            let qry = `select  type,id,parent,ext,CONVERT(cargo USING utf8) as cargo from ${DICT_TABLE} ${where}`  ;
            if (config.DEBUG) { 
                console.log("Qry=%s",qry);
            }
            if(USEPOOL){
                //*
                pool.getConnection((err, conn) => {
                    conn.query(qry,(err, rows, fields) => {                    
                        if (err) { console.log("dict.mysql_get Error getting result : %s ",err );}
                        if (config.DEBUG) {
                            console.log("dict.mysql_get ROWS=%j\n\nqry = %s",rows,qry);
                        }
                        conn.release();        
                        callback(err,rows);

                    });                
                });
                //*/
            } else {
                    conn.query(qry,(err, rows, fields) => {                    
                        if (err) { console.log("dict.mysql_get Error getting result : %s \n%s",err,qry );}
                        if (config.DEBUG) {
                            console.log("dict.mysql_get ROWS=%j\n\nqry = %s",rows,qry);
                        }        
                        callback(err,rows);
                    });                
                //*/
            }
        };
        //----------------------------------------------------------------------------------
        let dicexport = module.dicexport = (type,id,parent,ext,callback) => {
            let date = new moment().format('YYYYMMDDHHmmss') ;
            let where = buildWhere(type,id,parent,ext) ;
            if ( isEmpty(file) ) {
                file = `/tmp/dict_${date}.csv` ;
            }
            let qry = `SELECT * FROM ${DICT_TABLE} ${where} INTO OUTFILE '${file}' `;
            if(config.DEBUG){
                console.log(qry);
            }
            conn.query(qry, function (err, rows) {
                if (err) { console.log("dictexport mysql  : %s\n%s ",err ,qry);}
                rows.forEach(function(element, index) {
                    fs = require('fs');
                    file = `/tmp/${element.id}~${element.parent}~${element.ext}.${element.type}` ;
                    fs.writeFile(file,element.cargo, function (err) {
                        if (err) {
                            console.log(err);
                        }
                    });
                });
                if (callback) {
                    callback(err,rows);
                } 
            });
        };
        //----------------------------------------------------------------------------------
        let dicimport = module.dicimport = (file,callback) => {
            if ( isEmpty(file) ) {
                file = `/tmp/dict.bak` ;
            }
            if ( fs.existsSync(file) ) {
                let qry = `LOAD DATA INFILE '${file}' INTO TABLE ${DICT_TABLE} `;
                conn.query(qry, function (err, rows) {
                    if (callback) {
                        callback(err,rows);
                    };
                    if (config.DEBUG ){
                        console.log(err||rows)
                    }
                });
            } else {
                console.log("Import File %s not Found",file);
            }
        };
        //----------------------------------------------------------------------------------
        // backup rows in database for editing tools
        let backup = module.backup = (type,id,parent,ext,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDDHHmmss');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext) ;
            if ( isEmpty(where) ) { // for backup all
                where = " where type not like '~%'"; 
            }
            let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
            //--
            let _insert = (rows,index,callback) => {
                let element = rows[index];
                let post = {id:element.id, type:'~'+element.type,parent:element.parent, ext:element.ext, cargo:element.cargo,notes:element.notes, date_time:date_time, date:date}; 
                qry = "INSERT INTO "+DICT_TABLE+" SET ? "
                conn.query(qry,post, function (err, rows) {                
                //dba.insert('~'+element.type,element.id ,element.parent, element.ext, element.cargo,element.notes,(err)=>{
                    if(index < rows.length) {
                        _insert(rows,index+1)
                    } else {
                        if (callback) {
                            callback(err)
                        }       
                    }
                })    
            }
            //--
            conn.query(qry, function (err, rows) {
               if (rows && rows.length > 0 ) {
                    _insert(rows,0)
                } else {
                    if (callback) {
                        callback(err)
                    }       
                }      
                    /*
                    rows.forEach(function(element, index) {
                        let post = {id:element.id, type:'~'+element.type,parent:element.parent, ext:element.ext, cargo:element.cargo,notes:element.notes, date_time:date_time, date:date}; 
                        qry = "INSERT INTO "+DICT_TABLE+" SET ? "
                        let query = conn.query(qry,post, function (err, rows) {
                            //console.log(rows);
                        }, this);
                    });
                    if (callback) {
                        callback(err,rows)
                    } 
                    */                    
                     
            });
        };
        //----------------------------------------------------------------------------------
        var remove = module.remove = (type,id,parent,ext,callback)=> {   
            let where = buildWhere(type,id,parent,ext) ;
            if (! isEmpty(where)) {
                var qry = `delete from ${DICT_TABLE} ${where} ` ;
                conn.query(qry,function(err, rows, fields){
                    if (callback) {
                        callback(err,rows) ;
                    }
                });
            }
        }
        //----------------------------------------------------------------------------------
        module.create = function(config,schema,table,force,callback){  
            createdict(config,schema,table,force,(err,result)=>{
                if( callback) {
                    callback(err,result);
                }
            });        
            return 0;
        }     
        //----------------------------------------------------------------------------------
        let isDict = (conn,schema,table,callback) => {
            let result = 0;
            if (isEmpty( table )){       
                table='dict';
            }
            if ( isEmpty( schema )){
                schema='dict' ;
            }       
            let qry = "SELECT COUNT(*) AS isTable FROM information_schema.tables WHERE TABLE_SCHEMA = '"+schema+"' AND TABLE_NAME = '"+table+"'" ;
            query(qry,(err, rows, fields)=>{
                if(rows && rows.length > 0 ){           
                    result =  rows[0].isTable ;
                    if(callback){
                        callback(err,result)
                    }
                }
            });        
            return result;
        }    
        //----------------------------------------------------------------------------------  
        let createdict = function (config,schema,table,force,callback){
            let schematable = '';
            // get connection
            if ( isEmpty( schema )){
                schema = 'dict'
            }
            if ( isEmpty( table )){
                table = "dict";
            }
            if (! isEmpty(force)) {
                if (typeof force == 'function' && isEmpty(callback)) {
                    callback = force ;
                    force = false;
                }
            } else {
                force = false;
            }
            if (isEmpty(config)) {
                config = {
                    host     : 'localhost',
                    user     : 'root',
                    password : 'EndorKroner#1',
                    port : 3306,
                    typeCast :true,
                    database : schema
                }
            }
            try{
                let connection = mysql.createConnection(config,'single');
                connection.connect((err)=>{
                    if (! err) {
                        isDict(connection,schema,table,(err,result)=>{
                        if (result = 0 || force) { 
                                let qry = `CREATE TABLE ${schematable} ( \
                                            type varchar(10) ,\
                                            id varchar(20) ,\
                                            parent varchar(20) ,\
                                            ext varchar(10) ,\
                                            cargo blob,\
                                            date datetime ,\
                                            notes text,\
                                            date_time varchar(22) ,\
                                            flags int(10) unsigned ,\
                                            KEY data (date_time), \
                                            KEY type (type,id,parent),\
                                            KEY parent (type,parent,id)) \
                                            ENGINE=MyISAM  DEFAULT CHARSET=utf8`;       
                                connection.query(qry,function(err, rows, fields){
                                    connection.close()       
                                    callback(err||rows,1);    
                                });
                            }
                        });
                    } else {
                        connected = false;
                        callback("Error Connweting:"+schematable,0)
                    }
                });
            } catch(ex) {
                connected = false;
                callback(ex,0);    
            }
            return 0;
        }
        open()
    return module;
};





