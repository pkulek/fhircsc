
let dict = require('../dict');
let utils = require('../../utils');
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddhhmmss.l');
const DEBUG = false;

//----------------------------------------------------
exports.upload = (req, res) => {
     if (! req.files)
        return res.status(400).send('No files were uploaded.');
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file 
    let tempFile = req.files.tempFile;
    if(DEBUG) {
        console.log("",tempFile);
    }
  // Use the mv() method to place the file somewhere on your server 
  tempFile.mv('/tmp/tempfile.tmp', function(err) {
    if (err)
      return res.status(500).send(err);
 
    res.send('File uploaded!');
  });
};
//----------------------------------------------------
exports.list = function(req,res){
   var type = req.params.type;
   getlist(type,(err,data)=>{
       res.send(data);
   });
}
//----------------------------------------------------
let getlist = exports.getlist = function(type,callback){
    if (utils.isEmpty(type)) {
        type = 'JSON';
    }
    var id = '';
    var parent = '';
    var ext = '';
    dict.get(type,id,parent,ext,(err,rows)=>{
        callback(err,rows) ;
    });
};
//----------------------------------------------------
exports.editor = function(req, res){
   var tokenid = req.params.tokenid ;
   var type = req.params.type;
   if (isEmpty( type )){       
        type="XML";
   } 
   res.send("ok")
};
//----------------------------------------------------
exports.post = function(req, res) {
    let id = req.query.id ;
    let type = req.query.type;   
    let parent = req.query.parent; 
    let ext = req.query.ext;
    let data = req.body;
    if (DEBUG) {
        console.log("%s",data.cargo);
    }
    dict.set(data.type,data.id,data.parent,data.ext,data.cargo,(err,result)=>{
        if (DEBUG){
            console.log("Result:%s %s",result,err)
        }
    });
    res.send("200 OK");
};
//---------------------------------------------------
exports.test = function(req, res){
    // get ejs from dict and render
    dict.get("EJS","dicedit_test","TEMPLATE","en-US",(err,data)=>{
        if( isEmpty(data)){ // file 
            res.render('dicedit_test',{page_title:"JSON Source2 Editor "});           
        } else {  //database
            let ejs = require("ejs"); 
            let html = ejs.render(data,{page_title:"JSON Source4 Editor "}) ;   
            res.send(html) ;   
        }
    });                       
};
//---------------------------------------------------
exports.json = function(req, res){
       res.render('dicedit_json',{page_title:"JSON Source Editor "});                        
};
//---------------------------------------------------
exports.html = function(req, res){
       res.render('dicedit_html',{page_title:"HTML Source Editor "});                        
};
//---------------------------------------------------
exports.xml = function(req, res){
       res.render('dicedit_xml',{page_title:"XML Source Editor "});                        
};
//---------------------------------------------------
exports.javascript = function(req, res){
       res.render('dicedit_js',{page_title:"Javascript Source Editor "});                        
};
//---------------------------------------------------
exports.email = function(req, res){
       res.render('dicedit_email',{page_title:"email Editor "});                        
};
//---------------------------------------------------
exports.ejs = function(req, res){
       res.render('dicedit_ejs',{page_title:"EJS Source Editor "});                        
};
//----------------------------------------------------
exports.save = function(req, res){
   let tokenid = req.params.tokenid ;
   let type = req.params.type;
   let input = JSON.parse(JSON.stringify(req.body));
   if (DEBUG) {
        console.log(JSON.stringify(input));
   }
   if (isEmpty( type )){       
        type="JSON";
   }
};


