
const moment = require('moment'); // for date time
const utils = require('../utils') ;
const DICT_TABLE = 'dict';
const DICT_DB = 'dict.dic'
const DEBUG = false ;
let connected = false;
const fs = require('fs') ;

const isEmpty =  utils.isEmpty; 
const buildWhere = utils.buildWhere
let db = null
//----------------------------------------------------------------------------------
module.exports = function (config) {
        db = require('mssql');
        if(isEmpty(config) ) {
           config = {
                "user":"fhiruser"
                ,"password": "fhiruser"
                ,"server": "192.168.130.109" 
                ,"database": "FHIR" 
                ,"connectTimeout":180000
                ,"requestTimeout":180000
                ,"pool": {
                    "max": 10,
                    "min": 0,
                    "idleTimeoutMillis": 180000
                }        
              }              
        }        
        //console.log("mssql config = %j",config)
        //----------------------------------------------------------------------------------
        let buildWhere = (type,id,parent,ext)=>{
            if (typeof type == 'object'){
                let o = type;
                id = o.id;
                parent = o.parent;
                ext = o.ext ;
                type = o.type;
            }
            let where = "";
            if( typeof type == 'string' ){
                where += (where == "") ? ` where type = '${type}' `: ` and type = '${type}' `;
            }
            if( typeof id == 'string' ){
                where += (where == "") ? ` where id = '${id}' ` : ` and id = '${id}' `;
            }
            if( typeof parent == 'string' ){
                where += (where == "") ? ` where parent = '${parent}' ` : ` and  parent = '${parent}' `
            }
            if( typeof ext == 'string' ){
                where += (where == "") ? ` where ext = '${ext}' ` : ` and  ext = '${ext}' `;
            }
            return where;   
        }
        //----------------------------------------------------------------------------------
        module.connected = ()=> {
            return connected;
        }
        //----------------------------------------------------------------------------------
        module.close = ()=> {
            db.close();
        }

        //---------------------------------------------------------------------------------
        let query = module.query = module.execute = function(qry,callback){
            try{
                let request = new db.Request();
                request.query(qry,(err,result) => {
                    if(err) {
                        console.error(err)
                    }
                    if(result) {
                        callback(err,result.recordsets[0]);
                    } else {
                        callback(err,[{LErr:JSON.stringify(err)}]);                    
                    }
                });               
            } catch(Ex) {
                console.trace(Ex);
                callback(err,[{LErr:Ex}]);                    
            }    
        }
        //----------------------------------------------------------------------------------
        let insert = module.insert = function(type,id,parent,ext,cargo,notes,callback){
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let request = new db.Request();
            let qry = ` INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES('${type}','${id}','${parent}','${ext}','${cargo}','${notes}',GETDATE(),'${date_time}') `
            request.query(qry,(err,result)=>{
                if(err) {
                    console.error(err,qry)
                }
                if(callback) {
                  callback(err,result);
                }
            })
        }
        //----------------------------------------------------------------------------------
        let update = module.update = function(type,id,parent,ext,cargo,notes,callback){
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let request = new db.Request();
            let where = buildWhere(type,id,parent,ext);
            cargo = cargo.replace(/"/g,'');
            let qry = `UPDATE ${DICT_TABLE} SET cargo = '${cargo}' ,date_time = '${date_time}', notes = '${notes}' ${where}` ;
            request.query(qry,(err,result)=>{
                if(err) {
                    console.error(err)
                }
                if(callback) {
                  callback(err,result);
                }
            })
        }
        //----------------------------------------------------------------------------------
        let save = module.save = module.set = (type,id,parent,ext,cargo,notes,callback)=> {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext);
            let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
            let request = new db.Request();
            request.query(qry,(err,result) => {
                if(err) {
                    console.error(err)
                }
                let qty = result.recordset[0].qty;
                if ( qty > 0 ) {
                    qry = `UPDATE ${DICT_TABLE} SET cargo = '${cargo}', date_time = '${date_time}'  ${where}` ;
                } else {
                    qry = ` INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES('${type}','${id}','${parent}','${ext}','${cargo}','${notes}',GETDATE(),'${date_time}') `
                };
                //console.log(qry)
                request.query(qry,(err,result)=>{
                    if(err) console.error(err)
                    if(callback) {
                        callback(err,"OK");
                    }
                })
            });             
        };
        //----------------------------------------------------------------------------------
        let get = module.get = (type,id,parent,ext,callback)=> {
            let result= 0;
            let where = buildWhere(type,id,parent,ext) ;
            if (isEmpty(callback)){
                if (isEmpty(ext) && typeof parent === 'function'){
                    callback = parent;
                    parent = "";
                }
                if (typeof  ext === 'function' ){        
                    callback = ext;
                    ext = "";
                }
            }
            try{
                let qry = `select  type,id as dicid ,parent,ext, cargo from ${DICT_TABLE} ${where}` ;
                let request = new db.Request();
                request.query(qry,(err,result) => {
                    if(err) {
                        console.error(err)
                    }
                    if(result) {
                        callback(err,result.recordsets[0]);
                    } else {
                        callback(err,[{LErr:JSON.stringify(err)}]);                    
                    }
                });               
            } catch(Ex) {
                console.trace(Ex);
                callback(err,[{LErr:Ex}]);                    
            }    



        /*

            let result= 0;
            //console.log("sqlite get %s%s%s",type,id,parent)
        c
            try {
                let where = buildWhere(type,id,parent,ext) ;
                //console.log("where=%s",where);
                let qry = `select  type, id ,parent,ext, cargo from ${DICT_TABLE} ${where} ` ;
                if (connected) {
                    let request = new db.Request();
                    let qry = "insert into Heartbeat_Results (QE_Name,QE_ID,Repository_Id,Document_Id,MPI_ID,Command,TimeStamp,Result,Failure_Description,Count,SetID,ResponseTime)  VALUES('" +data.QE_Name+"','"+
                            data.QE_ID+"', '"+data.Repository_Id+"','"+data.Document_Id+"','"+data.MPI_ID+"','"+data.Command+"',GETDATE(),'"+
                            data.Result+"','"+data.Failure_Description+"',"+count+","+jobID+","+ResponseTime+ ")";
                    request.query(qry);
                } else {
                    console.error("MSSQL DICT not Connected")
                }
            } catch(ex){
                console.trace(ex);
            }
            return result;
        */
        };
        //----------------------------------------------------------------------------------
        let savefile = module.savefile = function(type,id,parent,ext,cargofile, callback){
            if (! isEmpty(cargofile)) {
                let fs = require('fs');
                fs.readFile( cargofile, function (err, data) {           
                    if (err) {
                        console.trace(err); 
                    } else {                
                        save(type,id,parent,ext,data,'',(err,result)=>{ // save details                       
                            if (! isEmpty(callback)) {
                                callback(err,data);
                            };
                        });
                    };
                });              
            };
        };
        //----------------------------------------------------------------------------------
        let getfile = module.getfile = function(type,id,parent,ext,filename, callback){
            if (! isEmpty(filename)) {
                get(type,id,parent,ext,(err,rows)=>{
                    if(err) { console.log(err); }
                    let fs = require('fs');
                    fs.writeFile(filename, rows[0].cargo, function(err) {
                        if(err) { console.log(err); }
                    });
                    if(callback){
                        callback(err,rows[0].cargo)
                    }
                })       
            };
        };
        //----------------------------------------------------------------------------------
        // backup rows in database for editing tools
        let backup = module.backup = (type,id,parent,ext,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext) ;
            if ( isEmpty(where) ) { // for backup all
                where = " where type not like '~%'"; 
            }
            let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
            query(qry,(err,rows)=>{
                if (rows && rows.length > 0 ) { 
                    rows.forEach(function(element, index) {
                        try {
                            let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES (?,?,?,?,?,?,?,?) `;
                            let stmt = db.prepare(qry,element.id, '~'+element.type,element.parent, element.ext, element.cargo, element.notes, date_time, date);
                            stmt.run();
                            stmt.finalize();             
                        } catch(ex){
                            console.trace("dic_sqlite.mysql_backup Error: %s",ex);
                        }                               
                    });             
                }
                if( callback){
                    callback(err,rows)
                }
            })
        };
        //----------------------------------------------------------------------------------
        let remove = module.remove = (type,id,parent,ext,callback)=> {   
            let where = buildWhere(type,id,parent,ext) ;
            var qry = `delete from ${DICT_TABLE} ${where} ` ;
            query(qry,function(err, rows, fields){
                if (callback) {
                    callback(err,rows) ;
                }
            });
        }
        //----------------------------------------------------------------------------------
        let close = module.close = ()=>{
            db.close();
        }
        //----------------------------------------------------------------------------------
        let open = module.create = module.open = (callback)=>{
            //console.log("DICT mssql config = \n%j\n",config)
            db.connect(config,(err)=> {
                connected = isEmpty(err)
                connected = err? false : true
                console.log("Starting DICT MS SqlServer %s for %j ",err||"OK",config);
                if(callback){
                    callback(err)
                }
            });
        
        }      
        /*
        open(config,(err)=>{
            if(callback){
                callback(err)
            }
        });
        */
        return module;
}  





