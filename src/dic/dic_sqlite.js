
let utils = require('../utils') ;
let moment = require('moment'); // for date time
const DICT_TABLE = 'dict';
const DICT_DB = 'dict.dic'
const DEBUG = false  ;
let _connected = false;
let fs = require('fs') ;
let db = '';
const isEmpty =  utils.isEmpty; 
const buildWhere = utils.buildWhere
module.exports = (config)=> {
        //----------------------------------------------------------------------------------
        module.connected = ()=> {
            return _connected;
        }
        //----------------------------------------------------------------------------------
        module.close = (callback)=> {
            db.close((err)=>{
                if(callback){
                    callback(err)
                }
            });
        }
        //---------------------------------------------------------------------------------
        let query = module.query = (qry,callback)=>{
            db.serialize(()=>{
                db.all(qry, (err, rows) =>{
                    if (callback) {
                        callback(err,rows) ;
                    }
                });         
            });        
        }
        //--------------------------------------------------------------------------------------
        let execute = module.execute = (qry,data,callback) => {
            db.serialize(() => {
                db.run(qry,data, (err) =>{
                    if (callback) {
                        callback(err,[]);
                    }
                    if( DEBUG) {
                        console.log("dic_sqlite.execute qry=%s  error=%s",qry,err)
                    }                             
                });
            })
        }
        //----------------------------------------------------------------------------------
        let update = module.update = (type,id,parent,ext,cargo,notes,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext);
            let qry = `UPDATE ${DICT_TABLE} set parent = ?,cargo = ?,notes=?,date = ?,date_time = ? ${where} ` ;
            if (DEBUG) {
                console.log("sqlite.update qry=%s",qry) ;
            }
            db.serialize(() => {
                db.run(qry,[parent,cargo,notes,date,date_time], (err)=> {
                    if(err){
                        console.log("Update error:\n%s\n%j ",err,{type,id,parent,ext})
                    }
                    if (callback) {
                        callback(err,[]);
                    }
                })
            })
        }
        //----------------------------------------------------------------------------------
        let insert = module.insert = (type,id,parent,ext,cargo,notes,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD HH:mm:ss');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES (?,?,?,?,?,?,?,?) `;
            db.serialize( () => {
                db.run(qry,[type,id,parent,ext,cargo,notes,date,date_time], (err)=> {
                    if(err){
                        console.log("Inseert error:\n%s\n%j ",err,{type,id,parent,ext})
                    }
                    if (callback) {
                        callback(err,[]);
                    }

                })
            })
        }
        //----------------------------------------------------------------------------------
        let set = save = module.save = module.set = (type,id,parent,ext,cargo,notes,callback)=>{
            let d = new Date(); 
            //let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext);
            let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
            if (DEBUG) {
                console.log("sqlite.save qry=%s",qry) ;
            }
            db.serialize(()=>{
                db.all(qry, (err, rows)=> {
                    if (rows && rows.length > 0 && rows[0].qty > 0) { // update    
                        update(type,id,parent,ext,cargo,notes,(err,result)=>{
                            if(callback){
                                callback(err,result)
                            }

                        });
                    } else {
                        insert(type,id,parent,ext,cargo,notes,(err,result)=>{
                            if(callback){
                                callback(err,result)
                            }
                        });
                    }
                });         
            });        
        };
        //----------------------------------------------------------------------------------
        let get = module.get = (type,id,parent,ext,callback)=> {
            let result= 0;
            let Buffer = require('buffer').Buffer;
            if (isEmpty(callback)){
                if (isEmpty(ext) && typeof parent === 'function'){
                    callback = parent;
                    parent = "";
                }
                if (typeof  ext === 'function' ){        
                    callback = ext;
                    ext = "";
                }
            }
            let where = buildWhere(type,id,parent,ext) ;
            let qry = `select  type, id ,parent,ext, cargo from ${DICT_TABLE} ${where} ` ;
            if (_connected) {
                db.serialize( (err) =>{
                    db.all(qry,(err, rows) => {
                        if(rows && rows.length > 0 ) {
                            rows.forEach((r,i)=>{
                                if(r.cargo) {
                                    rows[i].cargo = r.cargo.toString('binary')
                                }
                            })
                        }
                        if(callback){
                            callback(err,rows);
                        }
                    });
                });
            } else {
                if(callback){
                    callback("SQLITE not Connected",[]);
                }
                console.error("SQLITE not Connected")
            }
        };
        //----------------------------------------------------------------------------------
        // backup rows in database for editing tools
        let backup = module.backup = (type,id,parent,ext,callback) => {
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(type,id,parent,ext) ;
            if ( isEmpty(where) ) { // for backup all
                where = " where type not like '~%'"; 
            }
            let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
            query(qry,(err,rows)=>{
                if (rows && rows.length > 0 ) { 
                    rows.forEach((element, index) => {
                        try {
                            let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES (?,?,?,?,?,?,?,?) `;
                            let stmt = db.prepare(qry,element.id, '~'+element.type,element.parent, element.ext, element.cargo, element.notes, date_time, date);
                            stmt.run();
                            stmt.finalize();             
                        } catch(ex){
                            console.trace("dic_sqlite.mysql_backup Error: %s",ex);
                        }                               
                    });             
                }
                if( callback){
                    callback(err,rows)
                }
            })
        };
        //----------------------------------------------------------------------------------
        let remove = module.remove = (type,id,parent,ext,callback)=> {   
            let where = buildWhere(type,id,parent,ext) ;
            var qry = `delete from ${DICT_TABLE} ${where} ` ;
            query(qry,(err, rows, fields) => {
                if (callback) {
                    callback(err,rows) ;
                }
            });
        }
        //----------------------------------------------------------------------------------
        let close = module.close = ()=>{
            db.close();
        }
        //----------------------------------------------------------------------------------
        let open = module.create = module.open = (callback)=>{
            let sqlite3 = require('sqlite3').verbose();
            let fs = require('fs');
            if(DEBUG){
                console.log("sqlite config = %j",config);
            }
            if ( fs.existsSync(config.schema)) {
                try {
                    db = new sqlite3.Database(config.schema);
                    _connected = true ;
                    if(callback){
                        callback("connected %s",_connected)
                    }
                } catch(ex){
                    _connected = false ;
                }
            } else {
                db = new sqlite3.Database(config.schema);
                // check if table exists
                let qry = "CREATE TABLE dict ( \
                        type CHAR(10) ,\
                        id CHAR(20) ,\
                        parent CHAR(20) ,\
                        ext CHAR(10) ,\
                        cargo BLOB,\
                        date datetime ,\
                        notes TEXT,\
                        date_time CHAR(22) ,\
                        flags INT ); \
                        CREATE INDEX type on dict (type,id,parent,ext);"
                try{                
                    _connected = true ;
                    db.serialize(() => {                    
                        db.run(qry);    
                    });
                    //db.close();
                } catch(ex){
                    _connected = false ;
                    console.trace(ex);
                }
            }
            //console.log("starting sqllite %j",db);
            db.on('error',(err)=>{
                console.error(err);
                _connected = false ;
            });
        }     

        if(isEmpty(db)){
            open()
        } else {
            console.log("Already started sqllite %j",db);
        }
        return module;
};




