
/*
 * GET users listing.
 */
/*
 The structure for dict table
CREATE DATABASE log;	
use log;
CREATE TABLE log (                     
  type varchar(10) DEFAULT NULL,                    
  id varchar(20) DEFAULT NULL,
  parent varchar(20) DEFAULT NULL,
  mpi varchar(50) DEFAULT NULL,                                           
  cargo blob,
  date datetime varchar(22),
  notes text,
  userid varchar(30) default 'system',                                                       
  flags int(10) unsigned DEFAULT NULL,
  KEY type (type,id,parent),
  KEY parent (type,parent,id)                   
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */
var CJSON = require('circular-json');
var crypt = 'PPmUQ39Sq1Ha4Y';
var LOG_TABLE = 'log.log';
var dict = require('../dic/dict');
var login = require('./login');
var moment = require('moment'); // for date time
var mysql = require('mysql');

var conn = mysql.createConnection({
  host     : '192.168.160.151',
  user     : 'root',
  password : '',
  port : 3306,
  typeCast :true,
  database : 'dict'
},'single');
//conn.connect();
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//----------------------------------------------------------------------------------
function getParams(req){
    var type = req.query.type;
    var userid = req.query.userid;
    var id = req.query.id;
    var parent = req.query.parent;
    var ext = req.query.ext;  
    var notes = req.query.notes;  
    var cargo = req.query.cargo;  
    var lang = req.headers["accept-language"] ;
    var tokenid = req.query.tokenid;
    //console.log("admin HEADERS:="+JSON.strinigy(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( userid )){
        userid="system" ;
    }    
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){
        parent=lang ;
    }    
    if ( isEmpty( ext )){
        ext="" ;
    }     
    if ( isEmpty( cargo )){
        cargo="" ;
    }       
    if ( isEmpty( notes )){
        notes="" ;
    }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang};// emulate multiple return values
}
//---------------------------------------------------------------------------------
adminlist = function(type,callback){
    //var qry = "select type,datetime,userid,id as logid,parent,ext,notes,CONVERT(cargo USING utf8) as cargo from "+LOG_TABLE+"  " ;
    var qry = "select type,datetime,userid,id as logid,parent,ext,notes,CONVERT(cargo USING utf8) as cargo from "+LOG_TABLE+" WHERE type = 'AUDIT' order by datetime desc" ;
    conn.query(qry,function(err, rows, fields){
        callback(rows,err);
    });     
}

//----------------------------------------------------------------------------------
function _log(userid,type,id,parent,ext,notes,cargo){ // parent= type LOG INFO DEBUG ERROR etc
    var d = new Date(); 
    var hrTime = process.hrtime();
    //var datetime = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    var datetime = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + hrTime[1];
    //console.log("datetime="+ datetime);
    if ( isEmpty( ext )){         
        ext='' ; 
    } ;
    if ( isEmpty( userid )){         
        userid='n/a' ; 
    } ;
    if ( isEmpty( type )){ 
        type=logtype[0] ; 
    } ;
    if ( isEmpty( parent )){ 
        parent='PATIENTPORTAL' ; 
    } ;
    if ( isEmpty( id )){ 
        id ='LOGGING' ; 
    } ;
    if ( isEmpty( notes)){ 
        notes ='' ; 
    } ;
    if ( isEmpty( cargo)){ 
        cargo ='' ; 
    } ;
    //CAST(AES_DECRYPT(email,'"+crypt+"') as CHAR(60))  decemail
    var qry = "INSERT INTO "+LOG_TABLE+" SET ?" ;
    var values = {
        userid:userid,
        type:type,
        id: id,
        parent:parent,
        ext:ext,
        cargo: cargo,
        notes: notes,
        datetime:datetime
    };
    /*
    var qry = "INSERT INTO "+LOG_TABLE+" set userid ='"+userid+"'" +
                    ", type = '"+type+"'" +
                    ", id = '"+id+"'" +
                    ", parent = '"+parent+"'" +
                    ", ext = '"+ext+"'" +
                    ", datetime = '"+datetime+"'" +
                    ", notes = AES_ENCRYPT('"+ notes + "','"+crypt+"')" +
                    ", cargo = AES_ENCRYPT('"+ cargo + "','"+crypt+"')"
    console.log("LOG qry= "  +qry)
    conn.query(qry, function(err, rows)  {
     */
    var query = conn.query(qry,values,function(err, rows){    
        
        if (err)  console.log("Error getting result logging._log : %s ",err );
    });                    
}
//---------------------------------------------------
exports.list = function(req, res){
     var input = JSON.parse(JSON.stringify(req.params));
     var id = req.params.id;
     var type = req.params.type;
     logreport("",function(data,err){
         if (type =='JSON') {
            res.send(data);
         } else if (type='XML') {
            res.send(data);
         } else {
            res.send(data);
         }  
     });
};
//---------------------------------------------------
exports.htmllist = function(req, res){
    logging_columns(function(data){
        res.render('logging',{page_title:"PP logging Report ",colsetup:data});                           
    });     
};
//---------------------------------------------------
admin_columns = function(callback) {
let colsetup = { 
        url:'/logging', 
        altRows: true, 
        deepempty: true,
        autowidth: true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",   
        autorowheight: false,
        //autoheight: false,    
        //height:'auto',
        //width:'auto',
        colModel:[
            //{label:'type',name:'type',index:'type', sortable:true , editable:false, width:150, search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']},editoptions:{readonly:true}},
            {label:'datetime',name:'datetime',index:'datetime', sortable:true , editable:false, width:150, search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']},editoptions:{readonly:true}},
            //{label:'type',name:'type',index:'type', sortable:true ,width:110, editable:false ,editoptions:{readonly:true} ,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
            {label:'userid',name:'userid',index:'userid', sortable:true ,width:110, editable:false ,editoptions:{readonly:true} ,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
            {label:'request',name:'parent',index:'parent', editable:false ,width:125,editoptions:{size:26},search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
            {label:'mpi',name:'ext',index:'ext', editable:false ,width:85,editoptions:{size:26},search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
            {label:'session id',name:'logid',index:'logid', editable:false , width:145, editoptions:{size:26},search:true, searchoptions:{sopt:['bw','cn','bn','eq','nc','ew','en']}},
            {label:'request',name:'notes',index:'notes', editable:true ,width:650, edittype:"textarea", editoptions:{size:400,rows:"2",cols:"100"},sortable:true,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
            // {label:'response',name:'cargo',index:'cargo', editable:true,hidden:false ,width:450,edittype:"textarea",  editoptions:{size:400,rows:"2",cols:"100"},sortable:true,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
            {label:'', width:1,search:false}    
        ],
        loadonce:true,
        toppager:true,
        rowNum: 25,
        rowTotal:500000,
        sortname: "datetime",
        sortorder: "desc",
        viewrecords: true,
        gridview: true,
        autoencode: true,            
        caption: "Logs",
        grouping: false,
        groupingView : {
            groupField : ['userid'],
            groupColumnShow : [true],
            groupText : ['<b>group </b>'],
            groupCollapse : true,
            groupOrder: ['desc'],
            groupSummary : [false],
            groupDataSorted : false
        }
        /*
        ,formatDisplayField:[
            function(dislayValue){
                return String(displayValue).substring(0, 7);
            }
        ],
        isInTheSameGroup:function(x,y){
            return String(x).substring(0, 7) == String(y).substring(0, 7); 
        } 
        */      
    };    
    dict.set("JSON","ADMINCOLUMNS","JQGRID","",colsetup,function(err,data) {
        if ( isEmpty(data)) {
            callback( colsetup);
        } else {
            callback(JSON.parse(data)); 
        }       
    });      
};


