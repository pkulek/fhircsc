
require('console-stamp')(console, 'yyyymmddhhmmss.l');

let CJSON = require('circular-json');
let crypt = 'PPmUQ39Sq1Ha4Y';
let LOG_TABLE = 'log.log';
let dict = require('../dic/dict');
let login = require('./login');
let moment = require('moment'); // for date time
let mysql = require('mysql');
let tokenExpire = 5;//in minutes
let tokenExpireFormat = 'YYYYMMDDhhmm';
let logtype = ["AUDIT","TRACE","DEBUG","INFO","WARNING","ERROR"];

let conn = mysql.createConnection({
  host     : '192.168.160.151',
  user     : 'root',
  password : '',
  port : 3306,
  typeCast :true,
  database : 'log'
},'single');
//conn.connect();
//----------------------------------------------------------------------------------
let isEmpty =(str) => { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//----------------------------------------------------------------------------------
let getParams = (req)=>{
    let type = req.query.type;
    let userid = req.query.userid;
    let id = req.query.id;
    let parent = req.query.parent;
    let ext = req.query.ext;  
    let notes = req.query.notes;  
    let cargo = req.query.cargo;  
    let lang = req.headers["accept-language"] ;
    let tokenid = req.query.tokenid;
    console.log("logging HEADERS:="+JSON.strinigy(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( userid )){
        userid="system" ;
    }    
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){
        parent=lang ;
    }    
    if ( isEmpty( ext )){
        ext="" ;
    }     
    if ( isEmpty( cargo )){
        cargo="" ;
    }       
    if ( isEmpty( notes )){
        notes="" ;
    }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang};// emulate multiple return values
}
//---------------------------------------------------------------------------------
let logreport = (type,callback) => {
    let qry = "select type,datetime,userid,id as logid,parent,ext,notes,CONVERT(cargo USING utf8) as cargo from "+LOG_TABLE+"  order by datetime desc LIMIT 1000"  ;
    //var qry = "select type,datetime,userid,id as logid,parent,ext,notes,CONVERT(cargo USING utf8) as cargo from "+LOG_TABLE+" WHERE type = 'AUDIT' order by datetime desc" ;
    conn.query(qry,function(err, rows, fields){
        console.log(err);
        callback(rows,err);
    });     
}
//----------------------------------------------------------------------------------
exports.log = function(req,res){ //call as POST  parent= type LOG INFO DEBUG ERROR etc
    let pars = getParams(req);
    let tokenid = pars.tokenid ;
    if (isEmpty(pars.notes)) {
        pars.notes = JSON.stringify(req.body);
    }
    if (isEmpty(pars.cargo)) { 
        pars.cargo = JSON.stringify(res);
    }
    _log(pars.userid, pars.type, pars.id, pars.parent, pars.notes ,pars.cargo);
    res.send("OK");
};
//----------------------------------------------------------------------------------
exports.LOG = function(userid,type,id,parent,ext,notes,cargo) { 
    if ( typeof notes == 'object' ) {
        notes = CJSON.stringify(notes);
    } 
    if ( typeof cargo == 'object' ) {
        cargo = CJSON.stringify(cargo);
    } 
    _log(userid,type, id, parent,ext, notes,cargo);
    //res.send("OK");
    return 1;
};
//----------------------------------------------------------------------------------
let _log = (userid,type,id,parent,ext,notes,cargo) => { // parent= type LOG INFO DEBUG ERROR etc
    let d = new Date(); 
    let hrTime = process.hrtime();
    //var datetime = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    let datetime = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + hrTime[1];
    //console.log("datetime="+ datetime);
    if ( isEmpty( ext )){         
        ext='' ; 
    } ;
    if ( isEmpty( userid )){         
        userid='n/a' ; 
    } ;
    if ( isEmpty( type )){ 
        type=logtype[0] ; 
    } ;
    if ( isEmpty( parent )){ 
        parent='PATIENTPORTAL' ; 
    } ;
    if ( isEmpty( id )){ 
        id ='LOGGING' ; 
    } ;
    if ( isEmpty( notes)){ 
        notes ='' ; 
    } ;
    if ( isEmpty( cargo)){ 
        cargo ='' ; 
    } ;
    //CAST(AES_DECRYPT(email,'"+crypt+"') as CHAR(60))  decemail
    let qry = `INSERT INTO ${LOG_TABLE} SET ? ` ;
    let values = {
        userid:userid,
        type:type,
        id: id,
        parent:parent,
        ext:ext,
        cargo: cargo,
        notes: notes,
        datetime:datetime
    };
    /*
    var qry = "INSERT INTO "+LOG_TABLE+" set userid ='"+userid+"'" +
                    ", type = '"+type+"'" +
                    ", id = '"+id+"'" +
                    ", parent = '"+parent+"'" +
                    ", ext = '"+ext+"'" +
                    ", datetime = '"+datetime+"'" +
                    ", notes = AES_ENCRYPT('"+ notes + "','"+crypt+"')" +
                    ", cargo = AES_ENCRYPT('"+ cargo + "','"+crypt+"')"
    console.log("LOG qry= "  +qry)
    conn.query(qry, function(err, rows)  {
     */
    let query = conn.query(qry,values,function(err, rows){    
        
        if (err)  console.log("Error getting result logging._log : %s ",err );
    });                    
}
//---------------------------------------------------
exports.list = function(req, res){
     let input = JSON.parse(JSON.stringify(req.params));
     let id = req.params.id;
     let type = req.params.type;
     logreport("",function(data,err){
         if (type =='JSON') {
            res.send(data);
         } else if (type='XML') {
            res.send(data);
         } else {
            res.send(data);
         }  
     });
};
//---------------------------------------------------
exports.htmllist = (req, res) => { // display audit list in jqgrid
    // get token ID for req
    let tokenid = req.params.tokenid;
    let id = req.params.id;    
    logging_columns(function(data){
        //_log(token.userid, "AUDIT",tokenid,'AUDIT LIST RES','','Retrieved OK',data);
        dict.get_ejsTemplate('logging1',req.app.get('views'),(err,template)=>{
            if(! isEmpty(template)){
                let ejs = require('ejs');
                res.send(ejs.render(template,{"options":filename,page_title:"PP logging Report ",colsetup:data}));
            } else {
                res.render('logging1',{page_title:"PP logging Report ",colsetup:data});                           
            }     
        })   

        //res.render('logging',{page_title:"PP logging Report ",colsetup:data});                           
    });     
};
//---------------------------------------------------
exports.html_list = (req, res) => { // display audit list in jqgrid
    // get token ID for req
    let tokenid = req.params.tokenid;
    let id = req.params.id;    
    login.Token(tokenid,function(data,err) {
        if (isEmpty(err) ){
            let token = JSON.parse(data);
            _log(token.userid,"AUDIT",tokenid,'AUDIT LIST REQ','',token,res);                
            login.authorised(tokenid,['PP_AUDITVIEW'],function(result,roles){
                if (result) {      
                    logging_columns(function(data){
                        _log(token.userid, "AUDIT",tokenid,'AUDIT LIST RES','','Retrieved OK',data);  
                        res.render('logging',{page_title:"PP logging Report ",colsetup:data});                           
                     });     
                } else {
                    res.send(false);    
                }
            });
        } else {
            res.send(false);
        }
    });         
};
//---------------------------------------------------
let logging_columns = (callback) => {
        dict.get("JSON","LOGGERCOLUMNS","JQGRID","",(err,data)=> {
            if ( isEmpty(data)) {
                let colsetup = { 
                    url:'/logging', 
                    altRows: true, 
                    deepempty: true,
                    autowidth: true,
                    ignoreCase: true,
                    datatype: "json",
                    mtype: "GET",   
                    autorowheight: false,
                    //autoheight: false,    
                    //height:'auto',
                    //width:'auto',
                    colModel:[
                        //{label:'type',name:'type',index:'type', sortable:true , editable:false, width:150, search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']},editoptions:{readonly:true}},
                        {label:'datetime',name:'datetime',index:'datetime', sortable:true , editable:false, width:150, search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']},editoptions:{readonly:true}},
                        {label:'type',name:'type',index:'type', sortable:true ,width:110, editable:false ,editoptions:{readonly:true} ,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                        {label:'userid',name:'userid',index:'userid', sortable:true ,width:110, editable:false ,editoptions:{readonly:true} ,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                        {label:'request',name:'parent',index:'parent', editable:false ,width:125,editoptions:{size:26},search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                        {label:'mpi',name:'ext',index:'ext', editable:false ,width:85,editoptions:{size:26},search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                        {label:'session id',name:'logid',index:'logid', editable:false , width:145, editoptions:{size:26},search:true, searchoptions:{sopt:['bw','cn','bn','eq','nc','ew','en']}},
                        {label:'request',name:'notes',index:'notes', editable:true ,width:650, edittype:"textarea", editoptions:{size:400,rows:"2",cols:"100"},sortable:true,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                       // {label:'response',name:'cargo',index:'cargo', editable:true,hidden:false ,width:450,edittype:"textarea",  editoptions:{size:400,rows:"2",cols:"100"},sortable:true,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                        {label:'', width:1,search:false}    
                    ],
                    loadonce:true,
                    toppager:true,
                    rowNum: 25,
                    rowTotal:500000,
                    sortname: "datetime",
                    sortorder: "desc",
                    viewrecords: true,
                    gridview: true,
                    autoencode: true,            
                    caption: "Logs",
                    grouping: false,
                    groupingView : {
                        groupField : ['userid'],
                        groupColumnShow : [true],
                        groupText : ['<b>group </b>'],
                        groupCollapse : true,
                        groupOrder: ['desc'],
                        groupSummary : [false],
                        groupDataSorted : false
                    }
                };
                // save to dictionary
                dict.set("JSON","LOGGERCOLUMNS","JQGRID","",colsetup);
                callback( colsetup);
            } else {
                callback(JSON.parse(data)); 
            }       
        });      
};


