
const isEmpty = require("../utils").isEmpty

module.exports = function (config) {
    if(isEmpty(config)){
        config = require('../../config.json')
    }
    const sql = require('mssql');
    const jwt = require('jsonwebtoken');
    const fs = require('fs')
    const login = require('./login')(config)
    //-----------------------------------------------------------------------------------------
    let payload = module.payload = (qe,callback) =>{
        // get from auth table
        login.jwtPayload(qe,(err,result)=>{
            if(callback){
                callback(err,result)
            }
        })
    }
   //-----------------------------------------------------------------------------------------
   let gentoken = module.gentoken = (params,callback) =>{   
        let reqqe = params.sub 
        let target = params.target
        payload(reqqe,(err,result)=>{
            if(result){
                result.targetSystem = target
            }
            let privateKey = fs.readFileSync(config.privateKey);
            jwt.sign(result, privateKey,config.jwtOption, function(err, token) {
                if(callback){
                    callback(err,token)
                }
            });
        })
    }
    //-----------------------------------------------------------------------------------------
    module.gettoken = (req,res) =>{
        let params = getParams(req)
        //console.log(params)
        gentoken(params,(error,token)=>{
            res.send(token)
        })
    }
    //-----------------------------------------------------------------------------------------
    module.saveUser = (req,res) =>{
        let params = getParams(req)
        login.saveUser({"userid":params.userid,"pwd":params.pwd},(err,result)=>{        
            //console.log("JWT result = %j",result)
            if( ! isEmpty(result.tokenid)){
                gentoken(result,null,(error,token)=>{
                    res.send(token)
                })
            } else {
                res.send('Invalid ID')
            }
        })
    }
    //---------------------------------------------------------------------------------------
    module.saveToken = (userid,token,callback) => {    
        login.saveToken(userid,token,(err,result) =>{  // {tokenid:token}
            //console.log(err||result)
        })
    }
    //-----------------------------------------------------------------------------------------
    module.login = (req,res) =>{
        let params = getParams(req)
        login.login(params.userid,params.pwd,(err,result)=>{        
            //console.log("JWT result = %j",result)
            if( isEmpty(result.tokenid)){
                gentoken(result,null,(error,token)=>{
                    res.send(token)
                })
            } else {
                res.send('Invalid ID')
            }
        })
    }
    //-----------------------------------------------------------------------------------------
    let decode = module.decode = (token,option, callback) =>{
        //let decoded = jwt.decode(token,{complete: true}) 
        if( isEmpty(option)) {
            option = {complete: true}
        }       
        let decoded = jwt.decode(token,option) 
        if(callback) {
            callback("",decoded)
        } else {
            return decoded
        }
    }
    //-----------------------------------------------------------------------------------------
    let verify = module.verify = (token, secret,options, callback) =>{
        if(isEmpty(secret)){
            secret = fs.readFileSync(config.publicKey); 
        }
        options = options || config.jwtVerifyOption
        options = {}
        //options = options || {issuer: 'SHINNYapi.org'}
        jwt.verify(token,secret,options,(err,decoded) => {
            if (err) {
                console.log(err)
            }
            if(callback) {
                callback(err,decoded)
            } else{
                return decoded
            }
        })
    }
    //-----------------------------------------------------------------------------------------
    module.xverify = (req,res) =>{
        let params = getParams(req)
        let token = params.token
        let secret = params.secret
        let options = params.options
        console.log("\n_verify params = %j\n",params)
        verify(token,secret,options,(err,decoded)=>{
            res.send(err||decoded)
        })
    }
    //-----------------------------------------------------------------------------------------
    module.getdecoded = (req,res) =>{
        let params = getParams(req)
        let token = params.token
        let option = params.option
        //console.log(token)
        decode(token,option,(err,decoded)=>{
            res.send(decoded)
        })
    }
    //-----------------------------------------------------------------------------------------
    let getParams = (req)=> {
        let userid = req.query.userid;
        let pwd = req.query.pwd;
        let secret = req.query.secret
        let token = req.query.token;
        let tokenid = req.query.tokenid;
        let qe = req.query.qe;
        let sub = req.query.sub;    
        let target = req.query.target;
        let payload = req.query.payload;
        let option = req.query.option;
        let options = req.query.options;
        let oauth = req.get("Authorization")
        let MAC = req.get("Authorization: MAC")
        if(isEmpty(token)){
            token = req.params.token
        }
        if(isEmpty(secret)){
            secret = req.params.secret
        }
        if(! isEmpty(oauth)){
            if(oauth.indexOf(" ") > -1) {
                token = oauth.split(" ")[1]     
            }
            if(oauth.indexOf(":") > -1) {
                token = oauth.split(":")[1]     
            }
        }    
        if (isEmpty( userid )){       
            userid=req.params.userid;
        }
        if (isEmpty( pwd )){       
            pwd=req.params.pwd;
        }
        if (isEmpty( secret )){       
            secret=req.params.secret;
        }
        if (isEmpty( token )){       
            token=req.params.token;
        }
        if (isEmpty( tokenid )){       
            tokenid=req.params.tokenid;
        }
        if (isEmpty( payload )){       
            payload=req.params.payload;
        }
        if (isEmpty( qe )){       
            qe=req.params.qe;
        }
        if (isEmpty( sub )){       
            sub=req.params.sub;
        }
        if (isEmpty( target )){       
            target=req.params.target;
        }
        if (isEmpty( option )){       
            option=req.params.option;
        }
        if (isEmpty( options )){       
            options=req.params.options;
        }
        if (isEmpty( userid )){       
            userid="";
        }
        if (isEmpty( pwd )){       
            pwd="";
        }
        if (isEmpty( secret )){       
            secret="";
        }
        if (isEmpty( token )){       
            token="";
        }
        if (isEmpty( tokenid )){       
            tokenid="";
        }
        if (isEmpty( payload )){       
            payload = `{ error:"bad payload"}`
        }
        if (isEmpty( sub )){       
            sub = `{ error:"bad Subject"}`
        }
        if (isEmpty( qe )){       
            qe = `{ error:"bad QE"}`
        }
        return {qe,sub,target,option,payload,userid,pwd,secret,token,tokenid,oauth};
    }
    //-----------------------------------------------------------------------
    return module
};