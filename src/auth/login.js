const DEBUG = false;
const LOGGER = false;

const config = require('../../config.json')
const utils = require('../utils') ;
const isEmpty =  utils.isEmpty;
const sql = require(config.db);
const saltChars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const crypto = require('crypto');
const moment = require('moment'); // for date time
const passwordHash = require('./password-hash');
const email = require('../mail');
//
let LOGINRESET = config.loginconfig.loginreset;
let tokenExpire = config.loginconfig.tokenExpire;
let pwdExpire = config.loginconfig.pwdExpire?config.loginconfig.pwdExpire:6000
let tokenExpireFormat = config.loginconfig.tokenExpireFormat;
//
const DICT_TABLE = "dict" 
const FCS_SADMIN = ['FCS_ALL'] ;
const FCS_ADMIN = ['FCS_ALL'] ;
const FCS_OPERATIONS = ['FCS_USERSVIEW','FCS_USEREDIT','FCS_USERDELETE','FCS_USERREGISTER'] ;
const FCS_USER = ['FCS_USER'] ;
const _roles = ['FCS_ALL','FCS_ADD','FCS_EDIT','FCS_VIEW','FCS_AUDIT','FCS_REPORTSVIEW','FCS_REPORTS', 
              'FCS_USEREDIT','FCS_USERDELETE','FCS_USERREGISTER','FCS_USERADMINVIEW','FCS_ADMINUSERCREATE' ];         
const _groups = [ {'SUPERADMIN':FCS_SADMIN} ,
                {'FCS_ADMIN':FCS_ADMIN} ,
                {'FCS_USER':FCS_USER},
                {'FCS_OPERATIONS':FCS_OPERATIONS}] ;     

module.exports = function () {  
   //-------------------------------------------------------------------------
    let generatePassword = (len)=> {
        len = len || 12
        let p ='!@#$+-*&_'
        let randomstring=''
        let s=Math.floor(Math.random() * (p.length-1));
        for(let i=0; i<len; ++i){
            randomstring += s==i?
                            saltChars.charAt(Math.floor(Math.random() * saltChars.length)):
                            saltChars.charAt(Math.floor(Math.random() * saltChars.length));
        }
        return randomstring;
    }
    //-----------------------------------------------------------------------------------
    let genTokenID = (len) => {
        len = len || 20 ;
        if (crypto && crypto.randomBytes) {
            return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
        } else {
            for (var i = 0, salt = ''; i < len; i++) {
                salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
            }
            return salt;
        }
    }
    //---------------------------------------------------------------------------------
     module.htmllogin = (req,res)=>{
        res.render('login', {page_title: 'Login'});    
    }
    //---------------------------------------------------------------------------------
    module.htmladmin = (req,res)=>{
        res.render('adminusers', {page_title: 'Login'});    
    }
    //---------------------------------------------------------------------------------
    module.update = (req,res)=>{
        let params = getLoginParams(req)
        console.log("UPDATE params = %j",params) 
        saveUser(params,(err,result)=>{
            res.send(result)
        } )         
    }
    //--------------------------------------------------------------------------------
    module.userlist = (req,res)=>{
        getuserlist((err,data)=>{
            res.send(data)
        })
    }
    //---------------------------------------------------------------------------------
    let getuserlist = (callback) =>{
        let list = {cargo:[]}
        let qry = ` select id,cargo from dict where type = 'USER' and parent ='CREDENTIALS' `
        runQry(qry,(err,result)=>{
            result.forEach((elem,i)=>{
                list.cargo.push(JSON.parse(elem.cargo))
            })
            if (callback) {
                callback(err,list);
            } 
        })
    }
    //--------------------------------------------------------------------------
    let runQry =  module.runQry = (qry,callback) => {    
        try {
            //console.log("\nrunQry=%\n",qry)
            new sql.Request().query(qry, (err, result) => {
                if(err) {
                     console.log("lgoin  Connect error%s\n\n Run query=%s",err,qry)
                }
                if(callback){
                    if(err) {
                        callback(err,{} )
                    } else {
                        callback(err,result.recordsets[0])
                    }
                }
            })    
        } catch(err){
            console.log("run ry error=%s",err)
            callback(err,{})
        }
    }
    //------------------------------------------------------------------------------		                
    let saveUser= module.saveUser = (data,callback) => {
        let qry = ` -- insert or update 
         `
        let userid = data.userid
        var now = moment().format(tokenExpireFormat);  
        tokenid = genTokenID()
        getUser(userid,(err,result)=> {
            if( ! isEmpty(result)) {
                    // go through data and update  result
                    Object.keys(data).forEach((key) =>{
                        result[key] = data[key];
                    });
                    console.log("saveUser=%j",result)
                    qry += ` UPDATE dict SET date = GETDATE() ,cargo = '${JSON.stringify(result)}' where type = 'USER' and parent = 'CREDENTIALS' and id='${userid}' `
            } else {
                qry += ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'USER', '${userid}', 'CREDENTIALS','', GETDATE(),'${JSON.stringify(data)}' ) `
            }
            runQry(qry,(err,result)=>{
                if (callback) {
                    callback(err,result);
                } 
            })
        })
    }
    //------------------------------------------------------------------------------		                
    let getUser = module.getUser = (userid,callback) => {
        let qry = 
    `
        select cargo from dict where id = '${userid}' and type = 'USER' and parent = 'CREDENTIALS'
    `
        runQry(qry,(err,result)=>{
            let cargo = {}
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            } else {
                err = "USER NOT FOUND"
            }
            if (callback) {
                callback(err,cargo);
            } 
        })
    }
    //-----------------------------------------------------------------------------------------
    module.passwordChange = (req,res) => { //GET returns tokenid
        let params = getLoginParams(req)
      //  let params = req.params
        let userid = params.userid
        let password = params.password
        let source = params.source
        _login(userid,password,source,(err,token)=>{
            res.send(token)
        })
    }
    //-----------------------------------------------------------------------------------
    let sendMail = (email_config,callback)=>{
        email.send(email_config, 
            (err, message) => { 
                callback(err,message)
            }
        )
    }
    //-----------------------------------------------------------------------------------------
    module.login = (req,res) => { //GET returns tokenid
        let params = getLoginParams(req)
        //let params = req.params
        let userid = params.userid
        let password = params.password
        let source = params.source
      //  console.log("Login params=%j",params)
        _login(userid,password,source,(err,token)=>{
            res.send(token)
        })
    }
    //------------------------------------------------------------------------------------------
    let _login = module._login = (userid,password,source,callback) => { //GET returns tokenid
        let now = moment().format(tokenExpireFormat);  
        getUser(userid,(err,user)=>{
            //console.log(user)
            if(err)   console.log("Error User Login : %s ",err );
            if ( ! isEmpty(user) ) {
                passwordHash(password).verifyAgainst(user.hash, (error, verified)=> {   
                        if(error) {
                            user.msg = 'PASSWORD_ERROR';
                            user.msgDetail = 'Error in verification, Something went wrong!';
                        }
                        user.count +=1
                        //console.log(verified)
                        if(verified){
                            let validsource = user.source.split(',').includes(source)
                            let valid = [1,'1','Yes'].includes(user.valid )
                            let active = [1,'1','Yes'].includes(user.active)
                            let pwdReset = [1,'1','Yes'].includes(user.pwdReset)
                            user.lastAccess = now
                            // check if password expired
                            user.status = "USER OK";           
                            user.msg = "USER OK";                  
                            //console.log(valid,active,pwdReset)  
                            if ( ! active ){
                                user.status = "ACCOUNT INVALID";
                                user.msg = "ACCOUNT INVALID";
                                user.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
                                //disableUser(user)
                            } else if (! valid){
                                user.status = "INVALID TOKEN";
                                user.msg = "INVALID TOKEN";
                            } else if ( pwdReset) {
                                user.status = "RESET PASSWORD";
                                user.msg = "RESET PASSWORD";
                            } else if (now > user.pwdExpire ){
                                console.log("expired now=%s  ex=%s" ,now,user.pwdExpire)
                                user.status = "EXPIRED PASSWORD";
                                user.msg = "EXPIRED PASSWORD";
                            } else if (now > user.expire ){
                              //  user.msg = "EXPIRED TOKEN";
                               //  user.status = "EXPIRED TOKEN";
                            } else {
                                user.status = "USER OK"                
                            }                         
                            console.log(user.status)
                            if(user.status == "USER OK"){
                                user.tokenid = genTokenID() ;
                                console.log("user tokenid = %s",user.tokenid)
                                user.lastAccess=now;           
                                user.expire = moment().add(- tokenExpire, 'minutes').format(tokenExpireFormat);
                                user.count=0
                                let use2Factor = [1,'1','Yes'].includes(user.use2Factor )
                                if (use2Factor && ! user.asasent )  {
                                    let asacode = genPasscode(6)
                                    user.passcode = asacode
                                    let msg = ` Use this code for FCS ASA verification`
                                    let email_config = {
                                        text:    msg ,
                                        from:    "FCS@nyehealth.org", 
                                        to:      `${user.email}`,
                                        cc:      "dpal@nyehealth.org",
                                        subject: asacode
                                    }
                                    sendMail(email_config,(err,msg)=>{
                                        user.msg = 'ASA code sent'  
                                        user.status = "ASA CODE SENT"
                                        user.asasent = 1 
                                        saveToken(user.userid,user,(err,result)=>{
                                            console.log(err,result)
                                            savePWDHistory(user,(err,result)=>{
                                                callback(err,user)
                                            })
                                        })       
                                    })
                                } else { //asa code already 
                                    user.msg = 'Verified' 
                                    user.status = 'VERIFIED'

                                    user.asasent = 0
                                    console.log("SAVING TOKEN %j",user)
                                    saveToken(user.userid,user,(err,result)=>{
                                        console.log(err,result)
                                        savePWDHistory(user,(err,result)=>{
                                            callback(err,user)
                                        })
                                    })    
                                    callback(err,user)
                                    console.log("Login verified=%s \nerror=%s\n\ntoken=%j\nsource=%s",verified,error,user,source)
                                }
                            }  else { 
                                console.log("NOT OKK",user.status,user.count)
                                if(user.count > 4 ) {
                                    user.expire = moment().add(- tokenExpire, 'minutes').format(tokenExpireFormat);
                                    user.valid = 0;
                                    user.msg = "USER LOCKED OUT"
                                    user.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
                                    //disableUser(user)
                                } else {
                                    let validsource = user.source?user.source.split(',').includes(source):""
                                    let valid = [1,'1','Yes'].includes(user.valid )
                                    let active = [1,'1','Yes'].includes(user.active)
                                    let pwdReset = [1,'1','Yes'].includes(user.pwdReset)
                                    user.msg = ! validsource ? 'Source Not verified ' :""
                                    user.msg += ! verified ? ' userid or password not verified':""
                                    user.msg += valid?"":" not a validated user"
                                }
                                saveToken(user.userid,user,(err,result)=>{
                                    console.log(err,result)
                                    savePWDHistory(user,(err,result)=>{
                                        callback(err,user)
                                    })
                                })   
                            }
                        } else {
                            user.status = "PASSWORD NOT VERIFIED";
                            user.msg = "Password not verified ";
                            callback(err,user)
                        }
                })
            } else {
                user.status = "USER NOT FOUND";
                user.msg = "User not found ";
                callback(err,user)
            }
        });               
    };
      //------------------------------------------------------------------------------		                
      let saveToken = (userid,token,callback) => {
        let type = 'TOKN';
        let date_time = moment().format('YYYYMMDD');
      //  removeExpiredTOKN((err)=>{ 
            let  qry = ` insert into ${DICT_TABLE}(type,id,parent,date,date_time,cargo) VALUES( '${type}', '${token.tokenid}', '${userid}', GETDATE(),'${date_time}','${JSON.stringify(token)}' ) `
            runQry(qry,(err,result)=>{
                if(callback) {
                    callback(err,result)
                }
            });
      // })
    }
    /*
    //----------------------------------------------------------------------------
    module.saveToken = (userid,token,callback) => { //GET returns tokenid
        //params = {tokenid:'asfdasdasdffdsadfsa',userid:'admin',password:'admin',langid:'en-US'};
        let now = moment().format(tokenExpireFormat);  
        let user = {"userid":userid,"token":token}
        let tokenid = genTokenID()
        getUser(userid,(err,user)=>{
            if(err)   console.log("Error save token : %s ",err );
            // userid and password exists
            if ( ! isEmpty(user) ) {
                user = JSON.stringify(user).replace(/'/g,"''") 
            }
            let qry = `
                insert into dict (type,userid,token,tokenid,cargo,date) 
                        VALUES('TOKEN' , '${userid}','${token}' , '${tokenid}', '${user}'  , GETDATE() )
            `
            runQry(qry,(err,result)=>{
                //console.log(err||result)
                if(DEBUG) {
                    console.log("saveToken qry=%j \n err=%s",qry,err)
                }
                if (callback) {
                    callback(err,result);
                } 
            });
        });               
    };
    */
   
    //------------------------------------------------------------------------------		                
    module.genPwd = (length) => {
        return generatePassword(length) ;
    }
    //------------------------------------------------------------------------------		                
    let getRoles = (userid,callback) => { 
        getUser(userid,(err,token)=>{
            if  ( ! isEmpty( err ) ) {
                console.error(err)
                callback(err,_roles)
            } else {
                if(typeof token == 'string') {
                    token= JSON.parse(token);
                }
                callback(err,token.roles)
            }
        })
    };
    //------------------------------------------------------------------------------		                
    let setRoles =  (userid,roles,callback) => { 
        //add roles to token
        try {
            getUser(userid,(err,token)=>{
                if  ( ! isEmpty( err ) ) {
                    console.error(err)
                    callback(err,{})
                } else {
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    //set new roles and save
                    token.roles = roles;
                    saveUser(token,function(err,data){           
                        if(callback){
                            callback(err,data)
                        }
                    })            
                }
            })
        } catch(e) {
            callback(e,"")
        }
    };
    //-----------------------------------------------------------------------------
    module.setRoles =  (req,res) => { 
        let tokenid = req.body.tokenid;
        let userid = req.body.userid;
        let roles = req.body.roles;
        if(isEmpty(tokenid)) {
            tokenid = req.query.tokenid;
        }
        if(isEmpty(userid)) {
        userid = req.query.userid;
        }
        if(isEmpty(roles)) {
            roles= req.query.roles;
        }    
        // lookup user
        try {
            if(isEmpty(userid) ){
                if( !isEmpty(tokenid) ){
                    getToken(tokenid,function(err,token){
                        token = JSON.parse(token);
                        setRoles(token.userid,roles)    
                    });
                }
            } else{
                setRoles(userid,roles)
            }
            res.send("Done")
        } catch(e) {
            res.send(e)
        }
    }
    //-----------------------------------------------------------------------------
    module.getRoles =  (req,res) => { 
        let tokenid = req.query.tokenid;
        let userid = req.query.userid;
        // lookup user
        try {
            if(isEmpty(userid) ){
                if( !isEmpty(tokenid) ){
                    getToken(tokenid,function(err,token){
                        token = JSON.parse(token);
                        getRoles(token.userid,(err,data)=>{
                            res.send(data)
                        })    
                    });
                }
            } else{
                getRoles(userid,(err,data)=>{
                    res.send(data)
                })
            }
        } catch(e) {
            res.send(e)
        }
    }
    //------------------------------------------------------------------------------		                
    module.getGroups = () => { 
        return(_groups);
    };
    //----------------------------------------------------------------------------
    let qry = `delete from dict where type = 'TOKN and list.includes()` 
    //-----------------------------------------------------------------------------
    let removeExpiredTOKN = (callback) =>{
        let pType = 'TOKN';
        let qry = `select * from ${DICT_TABLE} where type = '${pType}' ` ;
        runQry(qry,(err,result)=>{
            let now = moment().format(tokenExpireFormat);
            if ((!isEmpty(result)) && (result.length > 0)) {
                result.forEach((_row)=>{
                    try {
                        let _userid = _row.parent
                        let _tokenid = _row.id
                        token = JSON.parse(_row.cargo)
                        if( token) {
                            let expire = token.expire;
                            let tokenid  = token.tokenid;
                            let userid = token.userid;
                            //console.log("now=%s exp=%s",now,expire)
                            if (isEmpty(expire) || now > expire )   {         
                               // remove(pType,id,parent,'',(err,rows)=>{
                                   
                                        console.log("Deleted %j",_row)
                                  
                               // });                        
                            }    
                        }
                    } catch (err)  {
                        console.error(err)
                    }                            
                });   
            }
            if (callback) {
                callback(err)
            }
        })
    
    }
  
    //------------------------------------------------------------------------------
    let Token = module.Token = function(req,res) {
        let params = getLoginParams(req)
        let tokenid = params.tokenid
        let userid = params.userid
        let qry = `select top 1 cargo from ${DICT_TABLE}  where id = '${tokenid}' and type= 'TOKN' order by date desc ` ;    
        let cargo = {}    
        runQry(qry,(err,result)=>{
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            }
            res.send(cargo)
        })  
    }
    //------------------------------------------------------------------------------
    let token = module.token = (tokenid,callback)=>{
        let qry = `select top 1 cargo from ${DICT_TABLE}  where id = '${tokenid}' and type= 'TOKN' order by date desc ` ;    
        runQry(qry,(err,result)=>{
            let cargo = {}    
            if( ! isEmpty(result)) {
                cargo = JSON.parse(result[0].cargo);
            }
            callback(err,cargo)
        })  
    }
    //--------------------------------------------------------------------------------
    let getToken = module.getToken = (userid,callback) => {  
        if (isEmpty(userid) ) {
            callback("no ID",'')
        } else{
            let parent='FCS_TOKEN' ; 
            let type ='TOKN'
            let qry = `select cargo from ${DICT_TABLE}  where id = '${userid}' and type= '${type}' and parent = '${parent}' ` ;        
            let cargo = {}
            runQry(qry,(err,result)=>{
                if( ! isEmpty(result)) {
                    cargo = JSON.parse(result[0].cargo);
                }
                if (callback) {
                    let tokekenid = cargo.tokenid 
                    callback(err,tokenid);
                } 
            })
        }
    }
    //------------------------------------------------------------------------------		                
    let expireToken = module.expireToken = function(tokenid,callback)  {
        removeExpired(function(err){
            if ( ! isEmpty(tokenid)){ 
                getTOKN(tokenid,function(err,result){
                    if(DEBUG){
                        console.log("get token=%s",result);
                    }
                    callback( err,result) ;
                });
            } else {
                let token = newToken();
                token.msg = 'Invalid token';
                callback("Error",token);
            };
        });
    };
    //----------------------------------------------------------------------------
    let loginFlow = function(status,tokenid)  {
        getTOKN(tokenid, function(err,token){
            if (DEBUG) {
            // console.log("jtoken = %j err= %s",token,err)
            }
            if (isEmpty(token)) {
                token = newToken();
            } else {
                if (typeof token == 'string' ) {
                    token = JSON.parse(token)                                    
                }
            }
            let flowMsg = token['msg'] ;
            let userid = token.userid;
            let roles = token["roles"]
            getFLOW(status, "LOGIN_FLOW",function(err,data){ // returns a JS function
                if(LOGGER){
                    logger.LOG(userid,"AUDIT",tokenid,status,"",roles,JSON.stringify(token));
                }
                try {
                    let proc = eval(flowStatus) ;  //eg ATTEMPT2 = text
                    if (config.DEBUG) {
                        console.log("Login Flow Called, flow status = %s , token = %j ",flowStatus ,token);
                    }
                    if ( isEmpty(data)) {
                        eval(proc);
                    }else{
                        eval(data);
                    }
                } catch(ex) {
                    console.error(ex);
                }
            }); 
        });
        return false;
    };
    //-----------------------------------------------------------------------------
    module.Roles = function(req,res){ 
        var tokenid = req.query.tokenid;
        // lookup user
        getToken(tokenid,function(err,token){
            if (! isEmpty(err)) {
                res.send( ["NON"] );
            } else {
                if(typeof token == 'string'){
                    token = JSON.parse(token);
                }
                res.send( token.roles ); 
            }    
        });
    }
    //-----------------------------------------------------------------------------

//    {"tokenid":"6c760d3da77f510529fe","use2Factor":1,"passcode":0,"userid":"pkulek1","pwdExpire":"202105251026","pwdReset":0,"attempt":5,"fname":"Peter","lname":"Peter","email":"pkulek@nyehealth.org","phone":"9907753211","sms":"","hash":"pbkdf2$1$4899bf0b550aee5912af459d8d9ea6c93073ec8d$75f56530fac098afcab04922a7c404ab78c7f70f","salt":"75f56530fac098afcab04922a7c404ab78c7f70f","link":"/FCS","callurl":"/","count":1,"lastAccess":"","msg":" userid or password not verified","theme":"redmond","langid":"en-US","valid":1,"active":1,"baseurl":"","msgDetail":"","roles":["FCS_USERSVIEW","FCS_USEREDIT","FCS_USERDELETE","FCS_USERREGISTER"],"avatar":"","expire":"202102221814","source":"HIXNY, BRONX"}
    module.verifyToken = function (req,res){ 
        let params = getLoginParams(req)

        var tokenid = params.tokenid;
        // lookup user
        if(tokenid){
            getToken(tokenid,(err,token)=>{
                if (! isEmpty(err)) {
                    res.send( false);
                } else {
                    token = JSON.parse(token);
                    res.send( token.tokenid == tokenid ); 
                }    
            });
        } else {
            res.send("Invalid token")
        }
    }
    //-------------------------------------------------------------------------------
    module.htmlregister   = (req,res)=>{

        res.render('registeruser', { page_title: 'register'});
    
    }
    //-----------------------------------------------------------------------------
    let isUser=(userid,callback)=>{
        let type = "USER";
        let parent = "CREDENTIALS";
        let qry = `select count(*) as qty from dict where type= '${type}' and parent = '${parent}' and id = '${userid}' `;
        let result = false;
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result[0]);               
        });    
    }
    //------------------------------------------------------------------------------
    module.approve = (req,res) => {
        let params = getLoginParams(req)
        let userid = params.userid
        getUser(userid,(err,user)=>{
            console.log("user=%j",user)
            if(err)   console.log("Error Approve : %s ",err );
            password='Remember1'
            passwordHash(password).hash((error, hash,salt,key) =>{
                user.salt = salt;
                user.hash = hash;
                user.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                user.valid =1
                user.active=1
                user.pwdReset=1
                saveUser(user,(err,data) => {    
                    let msg = 
                    `Congratulations your account has been approved
                    Temporary password is ${password} for userid ${userid}. 
                    Please use the link ${config.fhirhost} to login in.
                    You will need to change your password at the first login 
                    `
                    let email_config = {
                        text:    msg ,
                        from:    "FCS@nyehealth.org", 
                        to:      `${token.email}`,
                        cc:      "dpal@nyehealth.org",
                        subject: "FHIR Central Services"
                    }
                    user.msg = msg
                    sendMail(email_config,(err,result)=>{
                        saveEMAIL(user,(err,result)=>{
                            email.send(email_config, (err, message) => { 
                                console.log("email err="+err);
                                console.log("email message="+JSON.stringify(message));
                            } );
                            user.status = msg
                            res.send(user);
                        });      
                    }) 
                })
            })
        })
    }
      //------------------------------------------------------------------------------
      module.disable = (req,res) => {
        let userid = req.params.userid
        getUser(userid,(err,user)=>{
            console.log("user=%j",user)
            if(err)   console.log("Error Approve : %s ",err );
            user.valid =0
            user.active=0
            saveUser(user,(err,data) => {    
                res.send(user);
            });      
        })
    }
    //--------------------------------------------------
    let savePWDHistory = (token,callback) =>{ // cargo = token
       // console.log('Save PWD HISTORY= %j',token);
        let userid = token.userid
        let msg = token.msg
        let  qry = ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'PWD', '${userid}', 'PWDHISTORY','', GETDATE(),'${JSON.stringify(token)}' ) `
        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result);               
        });              
    };
    //--------------------------------------------------
    let saveEMAIL = (token,callback) =>{ // cargo = token
        console.log('SaveEMAIL= %j',token);
        let userid = token.userid
        let qry = ` insert into dict(type,id,parent,ext,date,cargo) VALUES( 'EMAIL', '${userid}', 'FCSREGISTER','', GETDATE(),'${JSON.stringify(token)}' ) `

        runQry(qry,(err,result)=>{
            if (err)  console.log("Error getting result 06 : %s ",err );
            callback( err,result);               
        });              
    };
    //------------------------------------------------------------------------------
    module.register = (req,res) => {
        let params = getLoginParams(req)
        console.log("register params = %j\n\data=%j",params)
        let token = newToken();
        let password = generatePassword(16);
        token.userid = params.userid;
        token.fname = params.fname;
        token.lname = params.fname;
        token.phone = params.phone;
        token.email = params.email;
        token.group = params.group;
        token.source=params.source
        isUser(params.userid,(err,result)=>{
            if(result && result.qty > 0) { // user exist
                token.msg = 'user id found'
                token.status = `"User already exists please select another eg:${params.userid+'1'}`
                res.send(token)
            } else {
                password='Remember1' // remove after testing
                passwordHash(password).hash(function(error, hash,salt,key) {
                    token.salt = salt;
                    token.hash = hash;
                    token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                    token.valid = 0; // activate
                    token.active = 0; // activate
                    token.pwdReset =1 ; // need to reset password
                    token.expire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                    token.roles = FCS_OPERATIONS ;           
                    saveUser(token,function(err,data){    
                        savePWDHistory(token,(err,result)=>{
                            let msg = 
` You have applied to be registered as a new user on the FHIR Central Services.  
 FCS team is reviewing your request and will update you on approval.`
                            let email_config = {
                                text:    msg ,
                                from:    "FCS@nyehealth.org", 
                                to:      `${token.email}`,
                                cc:      "dpal@nyehealth.org",
                                subject: "FHIR Central Services"
                            }
                            token.msg = msg
                            sendMail(email_config,(err,message)=>{
                                console.log("email err="+err);
                                console.log("email message="+JSON.stringify(message));
                                saveEMAIL(token,(err,result)=>{
                                    token.status = msg
                                    res.send(token);
                               });      
                            })
                           
                        });    
                    });
                }); 
            }
        })
    }
    //------------------------------------------------------------------------------		
    module.resetpwd = function(req,res) {  // change password in the Admin tool for admin users
        let params = getLoginParams(req)
        let userid = params.userid
        let tokenid = params.tokenid 
        if ( ! isEmpty(userid)) {
            getUser(userid, function(err,data)  {
                if  ( ! isEmpty( err ) ) {
                    console.log("get user error",err)
                    res.send(err);
                } else {
                    let token = data
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    let newpassword = generatePassword()
                  
                    passwordHash(newpassword).hash( function(error, hash,salt,key) {
                        if (error) {
                            console.error("resetpwd err= %j",error)
                            res.send(error);
                        } else {
                            // to do check to see if password already exists in history
                            token.salt = salt;
                            token.hash = hash;
                            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            token.pwdReset = 1;                            
                            saveUser(token,function(err,data) {
                                //console.log("\n\nreset pwd save user token = %j\n\n",token)
                                savePWDHistory(token, (err,result)=> {
                                    let msg = 
                                    ` Your new password has been reset, use temporary ${newpassword} it will expire in ${token.pwdExpire}.  
                                     click on this link ${config.fhirhost} to login and it will prompt you to change your password`
                                    let email_config = {
                                        text:    msg ,
                                        from:    "FCS@nyehealth.org", 
                                        to:      `${email}`,
                                        cc:      "dpal@nyehealth.org",
                                        subject: "FHIR Central Services"
                                    }
                                    token.msg = msg
                                    sendMail(email_config,(err,message)=>{
                                        console.log("email err="+err);
                                        console.log("email message="+JSON.stringify(message));
                                        saveEMAIL(token,(err,result)=>{
                                          
                                            res.send(token);
                                        });      
                                    })  
                                });                         
                            });
                        }    
                    }); 
                }       
            });
        }
    }
    //-----------------------------------------------------------------------------------
    let genPasscode = function(len) {
        len = len || 8 ;
        let sChars = '0123456789';
        let sCharsCount = sChars.length;      
        let out = ''
        for (var i = 0, salt = ''; i < len; i++) {
            out += sChars.charAt(Math.floor(Math.random() * sCharsCount));
        }
        return out;
    }
    //-----------------------------------------------------------------------------------
    module.forgotPassword = (req,res) =>{
        let params = getLoginParams(req)
        let email = params.email;
        let userid = params.userid;
        let tokenid = params.tokenid;
        if ( ! isEmpty(userid)) {
            getUser(userid, (err,data) => {
                if  ( ! isEmpty( err ) ) {
                    console.error(err)
                    res.send(err);
                } else {
                    let token = data
                    if(typeof token == 'string') {
                        token= JSON.parse(token);
                    }
                    let newpassword = generatePassword()
       
                    console.log("forgot password token = %j",token)
                    passwordHash(newpassword).hash( (error, hash,salt,key) =>{
                        if (error) {
                            console.error(error)
                            res.send(error);
                        } else {
                            // to do check to see if password already exists in history
                            token.salt = salt;
                            token.hash = hash;
                            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            token.pwdReset = "0";      
                            token.status = "PASSWORD CHANGE"
                            token.msg = `Password Changed for User ${userid}`
                            let msg = 
                            ` Your new password has been generated ${newpassword} it will expire in ${token.pwdExpire}.  
                             click on this link ${config.fhirhost} to login and it will prompt you to change your password`
                            let email_config = {
                                text:    msg ,
                                from:    "FCS@nyehealth.org", 
                                to:      `${email}`,
                                cc:      "dpal@nyehealth.org",
                                subject: "FHIR Central Services"
                            }
                            sendMail(email_config,(err,message)=>{
                                console.log("email err="+err);
                                console.log("email message="+JSON.stringify(message));
                                saveEMAIL(token,(err,result)=>{
                                    saveUser(token,(err,data) =>{
                                        savePWDHistory(token, (err,result)=> {
                                            console.log("callback err=%,result=%s",err,result)
                                        });                         
                                    });       
                                });   
                                res.send(token);   
                            })
                        }
                    })
                }
            })
        }
    }
    //------------------------------------------------------------------------------		
    module.changepwd = function(req,res){  // change password on the 
        let params = getLoginParams(req)
        let userid = params.userid
        let password = params.password
        let newpassword = params.newpassword
        console.log("change pwd params = %j",params)
        getUser(userid,(err,user)=>{
           // console.log(user,err)
            if(err)  {
                console.log("Error Change password : %s ",err );
                user.status = err
                user.msg = err
                res.send(user);
            } else {
                // check if old password = saved password            
                passwordHash(password).verifyAgainst(user.hash, (error, verified)=> {   
                    if(error) {
                        user.msg = 'PASSWORD_ERROR';
                        user.msgDetail = 'Error in verification, Something went wrong!';
                        res.send(user);
                    } 
                    if(verified) {
                        passwordHash(newpassword).hash((error, hash,salt,key) =>{
                            user.salt = salt;
                            user.hash = hash;
                            user.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                            user.pwdReset = "Yes"
                            saveUser(user,(err,data) => {    
                                user.status = "PASSWORD CHANGE"
                                user.msg = `Password Changed for User ${userid}`
                                savePWDHistory(user,(err,result)=>{
                                    let link = config.fhirhost
                                    console.log("\n\nconfig=%j\n\n",config)
                                    let msg = 
                                    ` Your new password has been generated "${newpassword}" it will expire in ${user.pwdExpire}.  
                                     click on this link ${link} to login and it will prompt you to change your password`
                                    let email_config = {
                                        text:    msg ,
                                        from:    "FCS@nyehealth.org", 
                                        to:      `${email}`,
                                        cc:      "dpal@nyehealth.org",
                                        subject: "FHIR Central Services"
                                    }
                                    sendMail(email_config,(err,message)=>{
                                        console.log("email err="+err);
                                        console.log("email message="+JSON.stringify(message));
                                        saveEMAIL(user,(err,result)=>{
                                                 
                                        });   
                                        res.send(user);   
                                    })
                                })
                            });
                        })
                    } else {
                        user.status = "INVALID PASSWORD"
                        user.msg = "Invalid password please contact your administrator"
                        res.send(user);
                    }
                })
            }
        })       
    }
    //------------------------------------------------------------------		                
    let captchaImg = function() {                   
        let num = parseInt(Math.random()*9000+1000);                                   
        let p = new captchapng(70,25,num); // width,height,numeric captcha
        p.color(115, 95, 197, 100);  // First color: background (red, green, blue, alpha)
        p.color(30, 104, 21, 255); // Second color: paint (red, green, blue, alpha)
        var img = p.getBase64();
        return {img:img,num:num};
    };
    //------------------------------------------------------------------------------		     
    module.captchapng = function(req, res){         
        var capt = captchaImg();                         
        var validcode = new Buffer(capt.img).toString('base64');
        var imgbase64 = new Buffer(capt.img,'base64');
        res.send(imgbase64);
    };
    //------------------------------------------------------------------------------		     
    module.captcha = function(req, res){         
        var capt = captchaImg();                         
        var validcode = new Buffer(capt.img).toString('base64');       
        //res.render('login', { page_title: 'Captcha',image:capt.img, num:capt.num});
        res.send({ num:capt.num, image:capt.img});
    };
    //-----------------------------------------------------------------------------
    let removeToken = function(tokenid,callback){
        //redisRemoveExpired();
        removeTOKN(tokenid,"",function(err){
            if (callback){
                callback(err)
            }
        })
    };
    //-----------------------------------------------------------------------------
    var isRole = module.isRole = function(tokenid,role,callback ) {
        getTOKN(tokenid,function(err,res){
            if( isEmpty(err) && !isEmpty(res) ){
                token = JSON.parse(res) ;
                let result = false;
                let uroles= token.roles;
                let alist = role;
                if (typeof uroles == 'string'){
                    alist = uroles.split(',');
                } else {
                    alist = uroles;
                }
                if (typeof role == 'string'){
                    arole = role.split(',');
                }
                alist.forEach((part)=>{
                    if( part == 'FCS_ALL' ){
                        result = true 
                    } else {
                        arole.forEach((_role)=>{
                            if(  part == _role ) {
                                result = true;
                            }
                        })
                    }    
                })
                if(callback) {
                    callback(result) 
                } else{
                    return result
                }
            } else{
                if(callback) {
                    callback(false) 
                } else{
                    return false
                }            
            }
        });
    };        
    //----------------------------------------------------------------------------
    let authorised = module.authorised = (tokenid,roles,callback)=>{
        getTOKN(tokenid,function(err,token){
            if ( isEmpty(err)){
                //console.log("login.authorised jtoken = %j err= %s",token,err)
                if (typeof token == 'string' && ! isEmpty(token)) {
                    token = JSON.parse(token)
                }
                let userid = token.userid;
                if ( (! isEmpty(callback))  && (! isEmpty(token))  ) {
                    token.password='***********';
                    if (typeof roles == 'string' ) {
                        roles = roles.split(',');
                    }
                    //console.log('login._authorized roles='+roles);
                    if (typeof roles == 'array' ) {
                        var testroles = token.roles ;
                        if (typeof testroles == 'string' ) {
                            testroles = testroles.split(',');
                        }                    
                        //console.log('_authorized testroles='+testroles);
                        let uroles = new sets.Set(roles) ;
                        let test = ( uroles.contains(["PP_ALL","PP_ADMIN"]) || uroles.contains(roles) );
                        callback(test,testroles,token) ;
                    } else {
                        callback(false);
                    }
                } else {
                    if(LOGGER){
                        logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,'security token not found');
                    }            
                    callback(false);
                }
            } else {
                if(LOGGER){
                    logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,err);
                }            
                callback(false);
            }
        });
        return false;
    } 
    //------------------------------------------------------------------------------		
    var resetDisabledUser = function(token,callback){
        console.log("\n\n\nresetting user token = \n\n%j\n\n",token)
        saveUser(token,function(err,data){           
            if(callback){
                callback(err,data)
            }
        })
    }
    //-----------------------------------------------------------------------------
    let _disableUser = function(token,reset=true) { 
        let now = moment().format(tokenExpireFormat);
        let tokenid = token.tokenid;
        token.lastAccess=now;
        token.tokenid = '' ;
        //token.msg = "USER DISABLED"                
        token.valid = 0;        
        removeToken(tokenid,function(err){
            saveUser(token,function(err,data){                                
                // reset user after 10 mins
                //console.log("disabling user token=%j",token)
                if(reset) {
                    token.tokenid = '' ;
                    token.msg = "USER ENABLED"                
                    token.valid = 0;    
                    //console.log("set timwout for user token=%j",token,LOGINRESET/1000)
                    setTimeout( resetDisabledUser ,LOGINRESET,token);                    
                }
                
            });   
        })
    }
    //-----------------------------------------------------------------------------
    module.disableUser = function(req,res) { 
        let now = moment().format(tokenExpireFormat);
        let tokenid = req.body.tokenid;
        let userid = req.body.userid;
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if( ! isEmpty(tokenid) ) {
            getToken(tokenid,function(err,data) {
                if ( ! isEmpty(err)){
                    res.send({"msg":err});
                } else {
                    let token = data;                    
                    _disableUser(token)
                    res.send("User Disabled")
                }
            })
        }    
    }
    //-----------------------------------------------------------------------------
    module.verifyUser = (req,res) => { 
        let now = moment().format(tokenExpireFormat);
        let state = req.body.state;
        let tokenid = req.body.tokenid;
        let userid = req.body.userid;
        let password = req.body.password;
        let newpassword = req.body.newpassword;
        let captcha = req.body.captcha;
        let logincount = parseInt(req.body.logincount);
        let token  = {}
        console.log("req.body=%j",req.body)
        if ( isEmpty( state )){
            state="LOGIN" ;
        }    
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if ( isEmpty( userid )){
            userid='' ;
        }    
        if ( isEmpty( password )){
            password='' ;
        }       
        if ( isEmpty( newpassword )){
            newpassword='' ; 
        }       
        getToken(tokenid,function(err,data) {
            if( isEmpty(err) && ! isEmpty(data)){
                if(typeof data == 'string'){
                    token = JSON.parse(data);
                } else {
                    token = data;
                }
            }                
            getUser(userid,function(err,data){ // check if user still exists
                logincount += 1
                if  ( ! isEmpty( err ) ) {
                    let msg = "User not found "
                    if (logincount > 3){
                        msg += 'Too many attempts'                    
                    }
                    res.send({"msg":msg,"tokenid":"",token,logincount,passcode:"undefined"});                        
                } else {
                    if( isEmpty(tokenid) ){ // should be on first try of token expire
                        token = JSON.parse(data)
                        token.msg="USER_OK"
                        // Creating new tokent
                        token.expire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                        token.tokenid = genTokenID() ;
                        token.lastAccess=now;                    
                        token.valid = 1;
                        if(DEBUG){
                            console.log("Creating New Token %j",token)
                        }
                    }
                    passwordHash(password).verifyAgainst(token.hash, function(error, verified) {                   
                        if(error) {
                            token.msg = 'PASSWORD_ERROR';
                            token.msgDetail = 'Error in verification, Something went wrong!';
                        }
                        if( verified ) {  
                            logincount = 0;
                            token.msg = "USER OK";                     
                            if ( token.active != "1" ){
                                token.msg = "ACCOUNT INVALID";
                                token.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
                                disableUser(token)
                            } else if (token.valid != "1"){
                                token.msg = "INVALID TOKEN";
                            } else if ( token.pwdReset == "1" ) {
                                token.msg = "RESET PASSWORD";
                            } else if (now > token.pwdExpire ){
                                console.log("expired now=%s  ex=%s" ,now,token.pwdExpire)
                                token.msg = "EXPIRED PASSWORD";
                            } else if (now > token.expire ){
                                token.msg = "EXPIRED TOKEN";
                            } else {
                                token.msg = "USER OK"                
                            }
                            if(isEmpty(token.use2Factor)){
                                token['use2Factor'] = 1;
                            }
                            // send email or sms                    
                            if( token.use2Factor == '1' && token.msg =='USER OK' ) {
                                token.passcode = genPasscode(6)
                                console.log('Sending Mail')                        
                                if(! isEmpty(token.sms) && ! isEmpty(token.phone) ){
                                    try {
                                        let msg = {
                                            text:    `Pass code : ${token.passcode}` ,
                                            to:      token.phone+'@'+token.sms,
                                            cc:      "pkulek@nyehealth.org",
                                            subject: `passcode ${token.passcode}`
                                        }
                                        email.sendEmail(token,"FCS_PASSCODE", msg)
                                        token.msg = "Passcode Sent";
                                    } catch(ex){
                                        console.error(ex) 
                                    }                        
                                } else {
                                    try {
                                        if( ! isEmpty(token.email) ){
                                            let msg = {
                                                text:    `Your pass code is ${token.passcode}` ,
                                                from:    "FCS_admin@nyecemail.com", 
                                                to:      token.email,
                                                cc:      "pkulek@nyehealth.org",
                                                subject: `passcode ${token.passcode}`
                                            }
                                            email.sendEmail(token,"FCS_PASSCODE", msg)
                                            token.msg = "Passcode Sent";
                                        } else {
                                            token.msg = "No Email";                                
                                        }
                                    } catch(ex) {
                                        console.error(ex) 
                                    }                        
                                }
                            } else {

                            }                    
                        } else {
                            if(logincount > 4 ) {
                                token.expire = moment().add(- tokenExpire, 'minutes').format(tokenExpireFormat);
                                token.valid = 0;
                                token.Active = 0;
                                token.msg = "USER LOCKED OUT"
                                token.msgDetail = 'User Locked Out \nPlease retry after 10 minutes or contact your system Administrator';
                                disableUser(token)
                            } else {
                                token.msg = 'Not Verified'
                            }
                        }
                        saveToken(userid,token);
                        res.send({token:token,"msg":token.msg,tokenid:token.tokenid,logincount,passcode:token.passcode});            
                    });     
                }       
            });
        })   
    };
    //-----------------------------------------------------------------------------
    module.logout = (req,res) => { // logou
        var now = moment().format(tokenExpireFormat);  
        var tokenid = req.query.tokenid;
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if(LOGGER){
            logger.LOG(userid,"AUDIT",tokenid,'LOGOUT REQ','','','');
        }
        getToken(tokenid,(err,token)=>{
            getUser(token.userid,function(err,data){
                    if  ( ! isEmpty( err ) ) {
                        if(LOGGER){
                            logger.LOG(userid,"AUDIT",tokenid,'LOGOUT ERROR','',userid+': not found','');
                        }
                        res.send("Error");
                    } else {
                        let token = JSON.parse(data);            
                        // Creating hash and salt
                        let tokenxpire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                        token.valid = 0;
                        token.expire = tokenxpire ;
                        token.lastAccess=now;
                        token.tokenid='' ;
                        res.send('OK');
                    }       
            });
        })
    };
    //-----------------------------------------------------------------------------
    module.userprofile =  (req,res) => { // logou
        var tokenid = req.query.tokenid;
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if(isEmpty(tokenid)){
            res.send("Error: no id");
            return;
        }
        getToken(tokenid,(err,token)=>{
            getUser(token.userid,function(err,data){
                    if  ( ! isEmpty( err ) ) {
                        res.send("Error");
                    } else {
                        let token = JSON.parse(data);            
                        res.send(token);
                    }       
            });
        })
    };

    //------------------------------------------------------------------------------		                
    let newToken = module.newToken = (userid,password) => {
        let _token = { 
                tokenid: genTokenID(),
                use2Factor:1,
                passcode:0,
                userid: userid,
                password: password,
                pwdExpire:moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ,
                pwdReset:1,
                attempt:5,
                fname:'',
                lname:'',
                email:'',
                phone:'',
                sms:"",
                hash:'',
                salt:'',
                link: '/FCS',
                callurl: '/',
                count: 0,
                lastAccess: '',
                msg: '',
                theme: 'redmond',
                langid:'en-US',
                valid: 0,
                active: 0,
                baseurl:'',
                msg:"",
                msgDetail:"",
                group:"FCS_USER",
                roles:['FCS_USERSVIEW'],
                avatar:"",
                expire:moment().add(tokenExpire, 'minutes').format(tokenExpireFormat)
        };
        return _token;
    };
    //-------------------------------------------------------------------------------------------------
    let getLoginParams = (req,callback)=>    {
        let headers = req.headers
        let _params = {}
        let query = req.query
        let params = req.params
        let data = req.body
        if(! isEmpty(headers)){
            let auth = headers['authorization']
           //console.log("\n\nparams.auth = %s\n\ntoken=%s\n\n",auth, auth.split(" ")[1])
            if(auth && isEmpty(_params.token)) {
                _params.token = auth.split(" ")[1]
            }
        }

        if(query){
            Object.keys(query).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = query[element] 
                }
            })
        }
        if(params){
            Object.keys(params).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = params[element] 
                }
            })
        }
        if(data) {
            Object.keys(data).forEach( (element,key)=> {
                if(isEmpty(_params[element])) {
                    _params[element] = data[element] 
                }
            }) 
        }
       console.log("_params = %j",_params)
        return callback ? callback(_params) : _params
    }
    return module
//------------------------------------------------------------------------------
}		
