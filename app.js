
    require('console-stamp')(console, 'yyyymmddhhmmss.l');
    const config = require('./config.json')
    const utils = require('./src/utils')
    const CJSON = require("circular-json"); 
    const cors = require('cors');

    const CLUSTER = config.useCluster
    const SOCKETIO = config.useSocketIO
    const HTTPS = config.useHTTPS;

    //------------------------------------------------------------------------------------
    let startApp= () => {
        let express = require('express');

        let bodyParser = require('body-parser');
        let path = require('path');  
        let ejs = require('ejs'); 
        app = express();

        app.use(cors());
        app.use( bodyParser.json() );       // to support JSON-encoded bodies
        app.use(bodyParser.urlencoded({ extended: true   })); 
        app.use( express.json() );       // to support JSON-encoded bodies
        app.use(express.urlencoded({     // to support URL-encoded bodies
            extended: true  
        })); 
        app.set('port', process.env.PORT || config.port);
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'ejs');
      
        app.use(express.static(path.join(__dirname, 'public')));
        app.use((req, res, next) => { // add getUrl to req
            req.getUrl = ()=> {
                return req.protocol + "://" + req.get('host') + req.originalUrl;
            }
            return next();
        });             
        app.all('/*', (req, res, next)=> {
            //console.log(req.headers.host + req.url)
        
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            next();
        });
        let connectsql = (callback)=>{
            const sql = require("mssql");
            let db = config[config.db+"config"+"_"+config.devEnv]
            sql.connect(db, err=>{
                callback(db,err)
            })   
        }
        let startHTTP=(port) =>{
            const http = require('http');
            port = port || config.httpPort
            server = http.createServer(app,(req,res)=>{})
            const sql = require("mssql");
            server.listen(port,()=>{
                console.log("\n\nHTTP - "+ config.app+" Version "+config.version+" listening on port: "+port);
                let db = config[config.db+"config"+"_"+config.devEnv]
                connectsql( (db,err)=>{
                    console.log("sql server %s connected %s",JSON.stringify(db),err?err:"OK")
                })    
            });
        }
        let startHTTPS = (port) =>{
            const fs = require('fs')
            const https = require('https');
            port = port || config.httpsPort
            var https_options = {
                key: fs.readFileSync(config.privateKey),
                cert: fs.readFileSync(config.publicKey), 
                ca:fs.readFileSync(config.qeca),
            };
            server=  https.createServer(https_options,app).listen(port,()=>{
                console.log(`\n\nHTTPS  - ${config.app} Version ${config.version}\nlistening on port: ${port} ` );
                require("public-ip").v4().then(ip => {
                    console.log("\nPublic ip address", ip);
                });
                connectsql( (db,err)=>{
                    console.log("\nsql server %s connected %s",JSON.stringify(db),err?err:"OK")
                })    
                utils.testConnection(config.authurl,(err,result) =>{
                    console.log(`\nAuthorization  Server: %s ${result?'is':'not'} available:`,config.authurl )
                })
                //*
                utils.testConnection(config.PROD_authurl,(err,result) =>{
                    console.log(`\nPROD Authorization  Server: %s ${result?'is':'not'} available:`,config.PROD_authurl )
                })
                utils.testConnection(config.STAGE_authurl,(err,result) =>{
                    console.log(`\nSTAGE Authorization  Server: %s ${result?'is':'not'} available:`,config.STAGE_authurl )
                })
                //*/                
                utils.testConnection(config[config.devEnv+"_SPRLurls"].base,(err,result) =>{
                    console.log(`\nSPRL Server: %s ${result?'is':'not'} available:`,config[config.devEnv+"_SPRLurls"].base )
                })
            });
        }
        if(HTTPS) {
            startHTTPS()
        } else {
            startHTTP()
        }    
        if(SOCKETIO){
            const http = require('http');
            let socketio = require('socket.io');
            const io = socketio(server)
            app.set('socket.io', io);      
            io.on('connection',(socket) => {  
                app.set('socket', socket);     
                console.log('IO Client connected...',CJSON.stringify(socket.id));   
                // socket.emit('msgWelcome',{"message":"Welcome to the FHIR Central services Connector"})                
                socket.on('join', function(data) {
                    console.log(data);
                    //socket.emit('msgWelcome',{"message":"Hello User wel-come to FHIE Cenral services window !"}) 
                }); 
                socket.on("LOGIN_CLIENT",(data) => {  
                    //console.log("LOGIN_CLIENT",data);  
                }); 
            })
        }
        function exitHandler(options, err) {
            if (options.cleanup) { 
                console.log('Cleaning Up');
            }
            if (err) console.log(err.stack);
            if (options.exit) {
            }
        }
        // EXIT cleanup if needed
        //do something when app is closing
        process.on('beforeExit', exitHandler.bind(null, {exit:true}));
        process.on('exit', exitHandler.bind(null,{cleanup:true}));
        //catches ctrl+c event
        process.on('SIGINT', exitHandler.bind(null, {exit:true}));
        process.on('SIGQUIT', exitHandler.bind(null, {exit:true}));
        process.on('SIGTERM', exitHandler.bind(null, {exit:true}));
        //catches uncaught exceptions
        process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
        
        app.get('/process/exit',(req, res, next) => {
            process.exit(0)
        });   
        app.get('/process/config',(req, res, next) => {
            res.send(process.config)
            next()
        });
        app.get('/process/memory',(req, res, next) => {
            res.send(process.memoryUsage())
            next()
        });
        app.get('/process/release',(req, res, next) => {
            res.send(process.release)
            next()
        });
        app.get('/process/arch',(req, res, next) => {
            res.send(process.arch)
            next()
        });        
        app.get('/process/connected',(req, res, next) => {
            res.send(process.connected)
            next()
        });
        app.get('/process/cpu',(req, res, next) => {
            res.send(process.cpuUsage())
            next()
        });
        app.get('/process/env',(req, res, next) => {
            res.send(JSON.stringify(process.env,null,4))
            next()
        });

        let routes = require('./src/routes');

       
        app.get('/', routes.index); 
        app.get('/accesscode/:registerCode', routes.index); 

        let redirect = require('./src/routes/redirecturl').redirect
        app.get('/jwt/token',redirect)
        app.get('/jwt/token/:sub',redirect) 
        app.get('/jwt/token/:sub/:target',redirect)
        app.post('/jwt/token',redirect)

        app.get('/jwt/decode',redirect)
        app.get('/jwt/decode/:token',redirect)

        app.get('/jwt/verify',redirect)


        app.get('/authorize',redirect);
        app.post('/authorize',redirect);

        app.post('/slogin/:registerCode',redirect);  
        app.get('/slogin/:registerCode',redirect);
     
        app.post('/login',redirect);
        app.get('/login',redirect);
        app.get('/login/:userid',redirect);
        app.get('/login/:userid/:password',redirect);
        app.get('/login/:userid/:password/:source',redirect);

        app.get('/login/html',  redirect)
    
        app.get('/login/verifyASA/"tokenid/:userid/:code',  redirect)
        app.post('/login/verifyASA', redirect)
        
        app.get('/logout',redirect);
        app.get('/logout/:tokenid',redirect);
        app.get('/access/verify',redirect)
        app.get('/access/verify/:tokenid',redirect)
        app.get('/access/verify/:tokenid/:role',redirect)
                
        app.post('/userpassword/reset/:userid',redirect);
        app.get('/userpassword/reset/:userid',redirect);
        app.post('/userpassword/forgot',redirect);
        app.post('/userpassword/change',redirect);

        app.get('/register/code/:userid',redirect)
        app.get('/register/token/:registerCode',redirect)
        
        app.get('/token/verify/:tokenid',redirect);
        app.get('/token/token/:tokenid',redirect);
        
        app.post('/admin/register/user',redirect); 


        app.get('/userid/:tokenid',redirect);
        //------------------------------------------------
        let sprl = require('./src/routes/sprl_manager');
        app.get('/sprl/PIX/Query',sprl.querypix)    
        app.get('/sprl/PIX/:qe/:mpiid',sprl.querypix)
        app.get('/sprl/PIX/:qe/:mpiid/:oid',sprl.querypix)
        app.get('/sprl/PIX/:qe/:mpiid/:oid/:tokenid',sprl.querypix)
        //-----------------------------
        let fhir = require('./src/routes/fhir_manager');
       
        app.get('/sprl/url',fhir.GetSprlFHIRUrl)
        app.get('/loadhtml',fhir.loadHTML)

        app.get('/FCS',fhir.CSC)
        app.get('/FCS/:tokenid',fhir.CSC)
        app.post('/FCS',fhir.CSC)

        app.get('/PIXm',fhir.GetPix)
        app.get('/PIXm/:mpiid',fhir.GetPix)    
        app.get('/PIXm/:mpiid/:tokenid',fhir.GetPix)    
        //get config stuff
        app.get('/fhir/config/',fhir.getConfig)
        app.get('/fhir/config/:key',fhir.getConfig)
          


        app.post('/geturl',fhir.getURL)
        app.post('/aggregateResult',fhir.aggregateResult)

        app.get('/fhir/profile',fhir.GetProfile)
        app.get('/fhir/profile/:profile',fhir.GetProfile)
        app.get('/fhir/resource/parameters',fhir.getResource_parameters)       
        app.get('/fhir/resource/parameters/:resource',fhir.getResource_parameters)
        app.get('/fhir/qelist',fhir.getQElist)
       
        app.get('/fhir/qelist/:type',fhir.getQElist)
 
        //Patient ----------------------------------
        app.get('/Patient',fhir.GetPatient)             
        app.get('/Patient/:_id',fhir.GetPatient)        
        app.get('/Patient/PIXm',fhir.GetPix)          
        app.get('/Patient/PIXm/:mpiid',fhir.GetPix)          
        //Condition ---------------------------------
        app.get('/Condition',fhir.GetCondition) 
        app.get('/Condition/:_id',fhir.GetCondition)
        //Observation
        app.get('/Observation',fhir.GetObservation)
        app.get('/Observation/:_id',fhir.GetObservation)
    
        // Encounter
        app.get('/Encounter',fhir.GetEncounter) 
        app.get('/Encounter/:_id',fhir.GetEncounter) 
        // Procedure
        app.get('/Procedure',fhir.GetProcedure) 
        app.get('/Procedure/:_id',fhir.GetProcedure) 
        // MedicationRequest
        app.get('/MedicationRequest',fhir.GetMedication)
        app.get('/MedicationRequest/:_id',fhir.GetMedication) 
        // Immunization
        app.get('/Immunization',fhir.GetImmmunization) 
        app.get('/Immunization/:_id',fhir.GetImmmunization) 
        //medicationAdministration
        app.get('/MedicationAdministration',fhir.GetmedicationAdministration) 
       
        //FHIRrest  Queries -----------------------------------
      
        app.get('/urlinsert',fhir.urlinsert)      
        app.get('/urlinsert/:hurl',fhir.urlinsert)
        app.get('/urlhistory',fhir.urlhistory)                    
        app.post('/urlhistory/add',fhir.urlhistoryadd)     
        app.post('/urlhistory/edit',fhir.urlhistoryedit)          
        app.post('/urlhistory/delete',fhir.urlhistorydelete)   
        app.delete('/urlhistory/delete',fhir.urlhistorydelete)   
        app.post('/fhirquery/insert',fhir.fhirqueryInsert)   
        // fhir query
        app.get('/fhir/query',fhir.fhirquery)
        app.post('/fhir/query/edit',fhir.fhirqueryedit)           
        app.post('/fhir/query/delete',fhir.fhirquerydelete)       
        
        // json viewer------------------------
        
        app.get('/config/edit',fhir.editConfig)
        app.post('/config/save',fhir.saveConfig)

        app.get('/json/viewer',fhir.jsonviewer)
        app.post('/json/viewer',fhir.jsonviewer)
        //--audit -----------------------------------------------
        let audit = require('./src/audit');
        app.get('/audit/qerequest',audit.auditqerequest)
        app.get('/audit/qeresponse',audit.auditqeresponse)
        app.get('/audit/staterequest',audit.auditstaterequest)
        app.get('/audit/stateresponse',audit.auditstateresponse)
        app.get('/audit/view',audit.auditView)
        app.get('/audit/html',audit.auditHtml)

        app.get('/audit/auditViewRequest',audit.auditViewRequest)
        app.get('/audit/auditViewResponse',audit.auditViewResponse)
        app.get('/audit/auditViewResult',audit.auditViewResult)
        
         //dashboard
         /*
        let dashboard = require('./src/routes/dashboard');
        app.get('/fhir/dashboard',dashboard.dashboard)
        // ssh test
        app.get('/ssh/exec',dashboard.ssh)
        app.get('/ssh/exec/:cmd',dashboard.ssh)

        app.get('/sftp/dir',dashboard.sftp)

        app.get('/shell',dashboard.shell)
        app.get('/shell/:commands',dashboard.shell)
        //*/
    }    
    //-------------------------------------------------------------------------------
    if(CLUSTER) {
        let cluster = require('cluster');
        if(cluster.isMaster) {
            let numWorkers = require('os').cpus().length-1;
            console.log('Master cluster setting up ' + numWorkers + ' workers...');
            for(var i = 0; i < numWorkers; i++) {
                cluster.fork();
            }
            cluster.on('online', function(worker) {
                console.log('Worker ' + worker.process.pid + ' is online');
            });
            cluster.on('exit', function(worker, code, signal) {
                console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
                console.log('Starting a new worker');
                cluster.fork();
            });
        } else {
            startApp()
        }
    } else {
        startApp()
    }
   


